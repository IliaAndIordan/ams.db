CREATE DATABASE  IF NOT EXISTS `ams_al` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_al`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_al
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ams_airline`
--

DROP TABLE IF EXISTS `ams_airline`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ams_airline` (
  `al_id` int NOT NULL AUTO_INCREMENT,
  `al_name` varchar(512) DEFAULT NULL,
  `iata` varchar(3) DEFAULT NULL,
  `logo` varchar(2000) DEFAULT NULL,
  `livery` varchar(2000) DEFAULT NULL,
  `slogan` varchar(512) DEFAULT NULL,
  `user_id` int DEFAULT NULL,
  `ap_id` int DEFAULT NULL,
  `adate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `udate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`al_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ams_airline`
--

LOCK TABLES `ams_airline` WRITE;
/*!40000 ALTER TABLE `ams_airline` DISABLE KEYS */;
INSERT INTO `ams_airline` VALUES (1,'D M Amerik','DMA','',NULL,'To fly, To serve',2,108,'2021-10-02 20:51:02','2021-10-02 20:51:02'),(7,'Ams System','AMS','',NULL,'There\'s no better way to fly',1,108,'2021-10-15 19:50:54','2021-10-15 19:50:54'),(8,'North Macedonia','MNA','',NULL,'Fly North Macedonia',3,199,'2024-06-25 09:05:36','2024-06-25 09:05:36'),(10,'Albania Airways','ALA','',NULL,'Fly Abania',8,128,'2024-06-26 22:22:13','2024-06-26 22:22:13'),(11,'Philippine Airlines','PAL',NULL,NULL,'The Heart of the Filipinos.. Worldwide',9,272,'2024-08-25 10:11:13','2024-09-18 16:37:35'),(12,'ASL','BNJ','',NULL,'We Fly, you are the captain',10,NULL,'2024-09-25 09:53:57','2024-09-25 09:53:57'),(13,'ASL','BNJ','',NULL,'We fly, you are the captain',10,NULL,'2024-09-25 09:55:08','2024-09-25 09:55:08'),(14,'ASL','BNJ','',NULL,'We fly, you are the captain',10,NULL,'2024-09-25 09:57:06','2024-09-25 09:57:06');
/*!40000 ALTER TABLE `ams_airline` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 14:16:31
