CREATE DATABASE  IF NOT EXISTS `ams_al` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_al`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_al
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `v_al_callc_param_h`
--

DROP TABLE IF EXISTS `v_al_callc_param_h`;
/*!50001 DROP VIEW IF EXISTS `v_al_callc_param_h`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_al_callc_param_h` AS SELECT 
 1 AS `al_id`,
 1 AS `al_name`,
 1 AS `user_id`,
 1 AS `display_distance_ml`,
 1 AS `hub_tx_offset_min`,
 1 AS `ht_to_st_offset_min`,
 1 AS `pax_weight_kg`,
 1 AS `pax_weight_kg_measure`,
 1 AS `max_ticket_price_ppd_kg_per_km`,
 1 AS `max_ticket_price_cpd_kg_per_km`,
 1 AS `m_to_ft`,
 1 AS `m_to_ft_measure`,
 1 AS `landing_fee_p_kg_mtow`,
 1 AS `l_to_us_gal`,
 1 AS `l_to_us_gal_measure`,
 1 AS `km_to_nm`,
 1 AS `km_to_nm_measure`,
 1 AS `km_to_mile`,
 1 AS `km_to_mile_measure`,
 1 AS `kg_to_lbs`,
 1 AS `kg_to_lbs_measure`,
 1 AS `ground_crew_fee_p_kg_mtow`,
 1 AS `flight_charter_pct_from_max_price`,
 1 AS `fbo_fee_discount_base`,
 1 AS `boarding_fee_p_pax`,
 1 AS `afr_min_aircraft_speed_kmph`,
 1 AS `distance_unit`,
 1 AS `km_per_h_to_knots`,
 1 AS `km_per_h_to_knots_measure`,
 1 AS `ticket_price_factor_b`,
 1 AS `ticket_price_factor_f`,
 1 AS `cost_per_fh_factor`,
 1 AS `hub_ap_id`,
 1 AS `ht_to_utc_h`,
 1 AS `price_per_l`,
 1 AS `st_to_utc_min`,
 1 AS `ht_to_utc_min`,
 1 AS `st_to_ht_min`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_oil_price`
--

DROP TABLE IF EXISTS `v_oil_price`;
/*!50001 DROP VIEW IF EXISTS `v_oil_price`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_oil_price` AS SELECT 
 1 AS `oil_price_id`,
 1 AS `price_per_l`,
 1 AS `adate`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ap_flp_shedules_count`
--

DROP TABLE IF EXISTS `v_ap_flp_shedules_count`;
/*!50001 DROP VIEW IF EXISTS `v_ap_flp_shedules_count`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ap_flp_shedules_count` AS SELECT 
 1 AS `ap_id`,
 1 AS `ap_type_id`,
 1 AS `region_id`,
 1 AS `subregion_id`,
 1 AS `country_id`,
 1 AS `state_id`,
 1 AS `flight_shedules_count`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_al_callc_param`
--

DROP TABLE IF EXISTS `v_al_callc_param`;
/*!50001 DROP VIEW IF EXISTS `v_al_callc_param`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_al_callc_param` AS SELECT 
 1 AS `al_id`,
 1 AS `al_name`,
 1 AS `user_id`,
 1 AS `display_distance_ml`,
 1 AS `hub_ap_id`,
 1 AS `ht_to_utc_h`,
 1 AS `dhub_tz_offset_min`,
 1 AS `ht_to_st_offset_min`,
 1 AS `param_value`,
 1 AS `measure`,
 1 AS `param_name`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `v_al_callc_param_h`
--

/*!50001 DROP VIEW IF EXISTS `v_al_callc_param_h`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb3 */;
/*!50001 SET character_set_results     = utf8mb3 */;
/*!50001 SET collation_connection      = utf8mb3_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_al_callc_param_h` AS select `p`.`al_id` AS `al_id`,`p`.`al_name` AS `al_name`,`p`.`user_id` AS `user_id`,`p`.`display_distance_ml` AS `display_distance_ml`,`p`.`dhub_tz_offset_min` AS `hub_tx_offset_min`,`p`.`ht_to_st_offset_min` AS `ht_to_st_offset_min`,max((case when (`p`.`param_name` = 'pax_weight_kg') then `p`.`param_value` end)) AS `pax_weight_kg`,max((case when (`p`.`param_name` = 'pax_weight_kg') then `p`.`measure` end)) AS `pax_weight_kg_measure`,max((case when (`p`.`param_name` = 'max_ticket_price_ppd_kg_per_km') then `p`.`param_value` end)) AS `max_ticket_price_ppd_kg_per_km`,max((case when (`p`.`param_name` = 'max_ticket_price_cpd_kg_per_km') then `p`.`param_value` end)) AS `max_ticket_price_cpd_kg_per_km`,max((case when (`p`.`param_name` = 'm_to_ft') then `p`.`param_value` end)) AS `m_to_ft`,max((case when (`p`.`param_name` = 'm_to_ft') then `p`.`measure` end)) AS `m_to_ft_measure`,max((case when (`p`.`param_name` = 'landing_fee_p_kg_mtow') then `p`.`param_value` end)) AS `landing_fee_p_kg_mtow`,max((case when (`p`.`param_name` = 'l_to_us_gal') then `p`.`param_value` end)) AS `l_to_us_gal`,max((case when (`p`.`param_name` = 'l_to_us_gal') then `p`.`measure` end)) AS `l_to_us_gal_measure`,max((case when (`p`.`param_name` = 'km_to_nm') then `p`.`param_value` end)) AS `km_to_nm`,max((case when (`p`.`param_name` = 'km_to_nm') then `p`.`measure` end)) AS `km_to_nm_measure`,max((case when (`p`.`param_name` = 'km_to_mile') then `p`.`param_value` end)) AS `km_to_mile`,max((case when (`p`.`param_name` = 'km_to_mile') then `p`.`measure` end)) AS `km_to_mile_measure`,max((case when (`p`.`param_name` = 'kg_to_lbs') then `p`.`param_value` end)) AS `kg_to_lbs`,max((case when (`p`.`param_name` = 'kg_to_lbs') then `p`.`measure` end)) AS `kg_to_lbs_measure`,max((case when (`p`.`param_name` = 'ground_crew_fee_p_kg_mtow') then `p`.`param_value` end)) AS `ground_crew_fee_p_kg_mtow`,max((case when (`p`.`param_name` = 'flight_charter_pct_from_max_price') then `p`.`param_value` end)) AS `flight_charter_pct_from_max_price`,max((case when (`p`.`param_name` = 'fbo_fee_discount_base') then `p`.`param_value` end)) AS `fbo_fee_discount_base`,max((case when (`p`.`param_name` = 'boarding_fee_p_pax') then `p`.`param_value` end)) AS `boarding_fee_p_pax`,max((case when (`p`.`param_name` = 'afr_min_aircraft_speed_kmph') then `p`.`param_value` end)) AS `afr_min_aircraft_speed_kmph`,max((case when (`p`.`param_name` = 'distance_unit') then `p`.`param_value` end)) AS `distance_unit`,max((case when (`p`.`param_name` = 'km_per_h_to_knots') then `p`.`param_value` end)) AS `km_per_h_to_knots`,max((case when (`p`.`param_name` = 'km_per_h_to_knots') then `p`.`measure` end)) AS `km_per_h_to_knots_measure`,max((case when (`p`.`param_name` = 'ticket_price_factor_b') then `p`.`param_value` end)) AS `ticket_price_factor_b`,max((case when (`p`.`param_name` = 'ticket_price_factor_f') then `p`.`param_value` end)) AS `ticket_price_factor_f`,max((case when (`p`.`param_name` = 'cost_per_fh_factor') then `p`.`param_value` end)) AS `cost_per_fh_factor`,`p`.`hub_ap_id` AS `hub_ap_id`,`p`.`ht_to_utc_h` AS `ht_to_utc_h`,`op`.`price_per_l` AS `price_per_l`,timestampdiff(MINUTE,utc_timestamp(),now()) AS `st_to_utc_min`,((`p`.`ht_to_utc_h` * 60) + (timestampdiff(MINUTE,utc_timestamp(),now()) - 120)) AS `ht_to_utc_min`,((-(1) * timestampdiff(MINUTE,utc_timestamp(),now())) + ((`p`.`ht_to_utc_h` * 60) + (timestampdiff(MINUTE,utc_timestamp(),now()) - 120))) AS `st_to_ht_min` from (`v_al_callc_param` `p` join `v_oil_price` `op` on((1 = 1))) group by `p`.`al_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_oil_price`
--

/*!50001 DROP VIEW IF EXISTS `v_oil_price`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb3 */;
/*!50001 SET character_set_results     = utf8mb3 */;
/*!50001 SET collation_connection      = utf8mb3_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_oil_price` AS select `cfg_oil_price`.`oil_price_id` AS `oil_price_id`,`cfg_oil_price`.`price_per_l` AS `price_per_l`,`cfg_oil_price`.`adate` AS `adate` from `cfg_oil_price` where (`cfg_oil_price`.`oil_price_id` = 1) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ap_flp_shedules_count`
--

/*!50001 DROP VIEW IF EXISTS `v_ap_flp_shedules_count`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb3 */;
/*!50001 SET character_set_results     = utf8mb3 */;
/*!50001 SET collation_connection      = utf8mb3_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ap_flp_shedules_count` AS select `a`.`ap_id` AS `ap_id`,`a`.`ap_type_id` AS `ap_type_id`,`a`.`region_id` AS `region_id`,`a`.`subregion_id` AS `subregion_id`,`a`.`country_id` AS `country_id`,`a`.`state_id` AS `state_id`,ifnull(`flpns`.`flight_shedules_count`,0) AS `flight_shedules_count` from (`ams_wad`.`cfg_airport` `a` left join (select `flpn`.`dep_ap_id` AS `ap_id`,count(distinct `flpns`.`flpns_id`) AS `flight_shedules_count` from ((`ams_al_flp_number` `flpn` left join `ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpn_id` = `flpn`.`flpn_id`))) left join `ams_al_flp_group` `g` on((`g`.`grp_id` = `flpns`.`grp_id`))) where (`g`.`is_active` = 1) group by `flpn`.`dep_ap_id`) `flpns` on((`flpns`.`ap_id` = `a`.`ap_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_al_callc_param`
--

/*!50001 DROP VIEW IF EXISTS `v_al_callc_param`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb3 */;
/*!50001 SET character_set_results     = utf8mb3 */;
/*!50001 SET collation_connection      = utf8mb3_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_al_callc_param` AS select `a`.`al_id` AS `al_id`,`a`.`al_name` AS `al_name`,`a`.`user_id` AS `user_id`,`s`.`display_distance_ml` AS `display_distance_ml`,`ba`.`ap_id` AS `hub_ap_id`,`ba`.`ap_utc` AS `ht_to_utc_h`,((ifnull(`ba`.`ap_utc`,0) * 60) + (timestampdiff(MINUTE,utc_timestamp(),now()) - 120)) AS `dhub_tz_offset_min`,(((ifnull(`ba`.`ap_utc`,0) * 60) + (timestampdiff(MINUTE,utc_timestamp(),now()) - 120)) - timestampdiff(MINUTE,utc_timestamp(),now())) AS `ht_to_st_offset_min`,if(((`s`.`display_distance_ml` = 1) and (`p`.`measure` is not null)),`p`.`param_value`,if((`p`.`measure` is null),`p`.`param_value`,1)) AS `param_value`,if(((`s`.`display_distance_ml` = 1) and (`p`.`measure` is not null)),`p`.`measure`,`p`.`measure_base`) AS `measure`,`p`.`param_name` AS `param_name` from (((`ams_airline` `a` left join `cfg_callc_param` `p` on((1 = 1))) join `ams_user_settings` `s` on((`s`.`user_id` = `a`.`user_id`))) left join `ams_wad`.`cfg_airport` `ba` on((`ba`.`ap_id` = `a`.`ap_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Dumping events for database 'ams_al'
--

--
-- Dumping routines for database 'ams_al'
--
/*!50003 DROP PROCEDURE IF EXISTS `ams_al_flp_number_schedule_copy_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `ams_al_flp_number_schedule_copy_day`(IN in_grp_id INT, IN in_from_wd_id INT,IN in_to_wd_id INT)
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.ams_al_flp_number_schedule_copy_day', stTime, logNote);
END;


DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.ams_al_flp_number_schedule_copy_day', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.ams_al_flp_number_schedule_copy_day';
set countRows = 0;
SET logNote=CONCAT('Start copy ( in_grp_id:',in_grp_id, ', in_from_wd_id:', in_from_wd_id,') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

SELECT count(*) as rowsCount INTO countRows
FROM ams_al.ams_al_flp_number_schedule sh
where sh.grp_id = in_grp_id and sh.wd_id = in_from_wd_id;

IF countRows>0 THEN

	SET logNote=CONCAT('Prepare ',countRows, ' rows to copy to ', in_to_wd_id,') ->' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    DELETE sh.* FROM ams_al.ams_al_flp_number_schedule sh
    where sh.grp_id = in_grp_id and sh.wd_id = in_to_wd_id;
    
    SET countRows = ROW_COUNT();
    COMMIT;
    
    SET logNote=CONCAT('Deleted ',countRows, ' rows for wd: ', in_to_wd_id,') ->' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_al.ams_al_flp_number_schedule
    (flpn_id, grp_id, al_id, wd_id, dtime_h, dtime_min, atime_h, atime_min, 
    flight_h, price_e, price_b, price_f, price_c_p_kg)
    select flpn_id, grp_id, al_id, in_to_wd_id as wd_id, dtime_h, dtime_min, atime_h, atime_min, 
    flight_h, price_e, price_b, price_f, price_c_p_kg
    from ams_al.ams_al_flp_number_schedule
    where grp_id = in_grp_id and wd_id = in_from_wd_id;
    
	SET countRows = ROW_COUNT();
    COMMIT;
    SET logNote=CONCAT('Inserted ',countRows, ' rows for wd: ', in_to_wd_id,') ->' );
	call ams_wad.log_sp(spname, stTime, logNote);
	
END IF;

SET logNote=CONCAT('Finish ', '<- ' );
call ams_wad.log_sp(spname, stTime, logNote);

SELECT s.flpns_id as flpnsId,
	s.flpn_id as flpnId,
	fn.flpn_number as flpnNumber,
	concat(LPAD(fn.flpn_number, 4, '0'), ' ', w.wd_abbr, ' ',
	if(dep.ap_iata, dep.ap_iata, dep.ap_icao) ,' ',
	if(arr.ap_iata, arr.ap_iata, arr.ap_icao) ) as flpnsName,
	s.grp_id as grpId,
	s.al_id as alId,
	s.wd_id as wdId,
	s.dtime_h as dtimeH,
	s.dtime_min as dtimeMin,
	s.atime_h as atimeH,
	s.atime_min as atimeMin,
	s.flight_h as flightH,
	s.price_e as priceE,
	s.price_b as priceB,
	s.price_f as priceF,
	s.price_c_p_kg as priceCpKg,
	s.adate, s.udate,
	fn.dep_ap_id as depApId,
	fn.arr_ap_id as arrApId,
	d.distance_km as distanceKm,
	d.min_rw_lenght_m as minRunwayM,
	d.min_mtow_kg as minMtowKg
FROM ams_al.ams_al_flp_number_schedule s
join ams_al.ams_al_flp_number fn on fn.flpn_id = s.flpn_id
join ams_wad.cfg_airport_destination d on d.dest_id = fn.dest_id
join ams_wad.cfg_weekday w on w.wd_id = s.wd_id
join ams_wad.cfg_airport arr on arr.ap_id = fn.arr_ap_id
join ams_wad.cfg_airport dep on dep.ap_id = fn.dep_ap_id
where s.grp_id = in_grp_id and s.wd_id = in_to_wd_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ams_al_flp_number_schedule_reset_prise` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `ams_al_flp_number_schedule_reset_prise`(IN in_al_id INT)
BEGIN
DECLARE i_route_id, i_route_idx, affectedRows INT;
DECLARE countRows  INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_flpns CURSOR FOR SELECT route_id
					FROM ams_al.ams_al_flp_number flpn
					where flpn.al_id=in_al_id group by route_id;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.ams_al_flp_number_schedule_reset_prise', stTime, logNote);
END;

DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.ams_al_flp_number_schedule_reset_prise', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.ams_al_flp_number_schedule_reset_prise';
set countRows = 0;
set affectedRows = 0;
set i_route_idx = 0;
SET logNote=CONCAT('Start-> in_al_id:', in_al_id, '.' );
call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_flpns;

read_loop: LOOP
	
    FETCH cursor_flpns INTO i_route_id;
	
	IF done THEN
		SET logNote=CONCAT('No i_route_id in queue found ', 'done' );
		 call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
	set i_route_idx = i_route_idx+1;
	SET logNote=CONCAT(format(i_route_idx,0),' route_id: ',i_route_id, ' ...');
	call ams_wad.log_sp(spname, stTime, logNote);
    
    update ams_al.ams_al_flp_number flpn
	join ams_wad.cfg_airport_destination d on d.dest_id = flpn.dest_id
	join ams_al.ams_airline a on a.al_id = flpn.al_id
	join ams_al.ams_user_settings us on us.user_id = a.user_id
	join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'max_ticket_price_cpd_kg_per_km' ) cpdmax on 1=1
	set flpn.price_e = CEIL(d.max_ticket_price_e * us.ticket_price_factor) ,
	flpn.price_b = CEIL(d.max_ticket_price_b * us.ticket_price_factor) ,
	flpn.price_f = CEIL(d.max_ticket_price_f * us.ticket_price_factor) ,
	flpn.price_c_p_kg = ROUND( ((d.distance_km * cpdmax.param_value)*us.ticket_price_factor), 4) 
	where flpn.al_id = in_al_id and flpn.route_id=i_route_id;
	
    SET countRows = ROW_COUNT();
    COMMIT;
    
    SET logNote=CONCAT('Updated ',format(countRows,0), ' flpn rows for route_id: ', i_route_id,' ' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    update ams_al.ams_al_flp_number_schedule flpns
	join ams_al.ams_al_flp_number flpn on flpn.flpn_id = flpns.flpn_id
	set flpns.price_e = flpn.price_e, 
	flpns.price_b = flpn.price_b, 
	flpns.price_f = flpn.price_f, 
	flpns.price_c_p_kg = flpn.price_c_p_kg
	where flpns.al_id = in_al_id and flpn.route_id = i_route_id;
    
    SET countRows = ROW_COUNT();
    COMMIT;
    set affectedRows = affectedRows + countRows;
    SET logNote=CONCAT('Updated ',format(countRows,0), ' flpns rows for route_id: ', i_route_id,' ' );
	call ams_wad.log_sp(spname, stTime, logNote);
                    
	END LOOP;
 
CLOSE cursor_flpns;

SET logNote=CONCAT('Finish ', '<- ' );
call ams_wad.log_sp(spname, stTime, logNote);

select affectedRows as affected_rows from dual;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_create_after` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_create_after`(in in_al_id INT)
BEGIN

DECLARE countRows, vbank_id, vtr_id, vuser_id INT;
DECLARE val_name, viata VARCHAR(1000);
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('sp_al_create_after', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'sp_al_create_after';
set countRows = 0;

SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);

	SELECT al_name, iata, user_id INTO val_name, viata, vuser_id FROM ams_al.ams_airline 
	where al_id=in_al_id;
	SELECT count(*) INTO countRows FROM ams_al.ams_bank where al_id=in_al_id;
    
	SET logNote=CONCAT('Airline ', val_name, ', iata ', viata,', countRows ', countRows,' after create.');
	call ams_wad.log_sp(spname, stTime, logNote);
    
     IF countRows > 0 THEN
		SELECT bank_id INTO vbank_id FROM ams_al.ams_bank  where al_id=in_al_id;
		SET logNote=CONCAT('found vbank_id ', vbank_id, ' for in_al_id ',in_al_id , '' );
		call ams_wad.log_sp(spname, stTime, logNote);
    ELSE
		INSERT INTO ams_al.ams_bank
		(al_id, amount)
		VALUES (in_al_id,  0);
		
		SET vbank_id = LAST_INSERT_ID();
        SET logNote=CONCAT('inserted vbank_id ', vbank_id, ' for in_al_id ',in_al_id , '' );
		call ams_wad.log_sp(spname, stTime, logNote);
	END IF;
    
	IF vbank_id>0 THEN
		call ams_al.sp_tr_al_create(vbank_id);
    END IF;

	SELECT count(*) INTO countRows FROM ams_al.ams_airline_counters where al_id=in_al_id;
    
	SET logNote=CONCAT('Airline ', val_name, ', iata ', viata,', airline_counters countRows ', countRows,' after create.');
	call ams_wad.log_sp(spname, stTime, logNote);
	IF countRows = 0 THEN
		INSERT INTO ams_al.ams_airline_counters
		(al_id, fl_transfer, fl_charter, lf_shedule)
		VALUES (in_al_id,  0, 0, 0);
        
        SET logNote=CONCAT('inserted ams_airline_counters row', ' for airline ',in_al_id , '' );
		call ams_wad.log_sp(spname, stTime, logNote);
	END IF;
    COMMIT;
    
    -- INsert record in HUB table
    INSERT INTO ams_al.ams_al_hub (ap_id, al_id, hub_name) 
    SELECT ap.ap_id, al.al_id,  
	concat(IF(al.ap_id = ap.ap_id, 'Base ', 'Hub '), ' ', ifnull(ap.ap_icao, ap.ap_iata),' ', ap.ap_name) as hubName 
	FROM (select in_al_id as al_id) h 
	join ams_al.ams_airline al on al.al_id = h.al_id 
	join ams_wad.cfg_airport ap on ap.ap_id = al.ap_id;
    COMMIT;

SET logNote=CONCAT('Updated airport max values for ', countRows, ' Airports.');
call ams_wad.log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flpn_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flpn_create`(IN in_al_id INT, IN in_dep_ap_id INT, IN in_arr_ap_id INT, IN in_flpn_number INT, OUT out_flpn_id INT)
BEGIN

DECLARE countRows, i_dest_id, i_route_id INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_al_flpn_create', stTime, logNote);
END;


DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_al_flpn_create', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_al_flpn_create';
set countRows = 0;
SET logNote=CONCAT('Start (in_al_id:', in_al_id , ', in_dep_ap_id:',in_dep_ap_id, ', in_arr_ap_id:', in_arr_ap_id,') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

SELECT r.flpn_id INTO out_flpn_id
FROM ams_al.ams_al_flp_number r 
where r.al_id = in_al_id and r.dep_ap_id=in_dep_ap_id and r.arr_ap_id = in_arr_ap_id;


	IF out_flpn_id is null THEN
    
		call ams_wad.get_cfg_airport_destination(in_dep_ap_id, in_arr_ap_id, i_dest_id);
        
        call ams_al.sp_flp_route_get(in_dep_ap_id, in_arr_ap_id, i_route_id);
        
         SET logNote=CONCAT('i_dest_id:', i_dest_id , ', i_route_id:', i_route_id );
		call ams_wad.log_sp(spname, stTime, logNote);
        
        IF in_flpn_number is null THEN
			select (max(ifnull(flpn_number,0))+1) INTO in_flpn_number   
			from ams_al.ams_al_flp_number where al_id=in_al_id;
		END IF;
        
        INSERT INTO ams_al.ams_al_flp_number
        (flpn_number, al_id, dep_ap_id, arr_ap_id, dest_id, route_id, 
		flight_h, price_e, price_b, price_f, price_c_p_kg)
		select p.flpn_number, p.al_id, p.dep_ap_id, p.arr_ap_id, p.dest_id, p.route_id,
		-- d.max_flight_h as flight_h,
		ROUND((d.distance_km/s.min_speed_kmph), 2) as flight_h,
		ROUND(flp.param_value* d.max_ticket_price_e, 2) as price_e, 
		ROUND(flp.param_value* d.max_ticket_price_b, 2) as price_b,
		ROUND(flp.param_value* d.max_ticket_price_f, 2) as price_f,
		ROUND(flp.param_value * (d.distance_km * pwkg.param_value), 4) as price_c_p_kg
		from 
		(select in_al_id as al_id, in_dep_ap_id as dep_ap_id, in_arr_ap_id as arr_ap_id, 
		i_dest_id as dest_id, i_route_id as route_id, in_flpn_number as flpn_number from dual) p
		join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id 
		join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'max_ticket_price_cpd_kg_per_km' ) pwkg on 1=1
		join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'flight_charter_pct_from_max_price' ) flp on 1=1
        join(SELECT min(cruise_speed_kmph) as min_speed_kmph FROM ams_ac.cfg_mac where cruise_speed_kmph is not null) s on 1=1;
        
        SET out_flpn_id = LAST_INSERT_ID();
		COMMIT;
        
		SET logNote=CONCAT(' out_flpn_id ', out_flpn_id);
		call ams_wad.log_sp(spname, stTime, logNote);
    
		
        
	END IF;
    
    SET logNote=CONCAT('Finish ', '<- ' );
	call ams_wad.log_sp(spname, stTime, logNote); 
    
    SELECT out_flpn_id from dual;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ams_al_flp_number_schedule_delete` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ams_al_flp_number_schedule_delete`(IN in_flpns_id INT)
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_ams_al_flp_number_schedule_delete', stTime, logNote);
END;


DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_ams_al_flp_number_schedule_delete', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_ams_al_flp_number_schedule_delete';
set countRows = 0;
SET logNote=CONCAT('Start (in_flpns_id:', in_flpns_id,') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

SELECT count(*) INTO countRows
FROM ams_ac.ams_al_flight_plan
where flpns_id = in_flpns_id;

SET logNote=CONCAT('Found ', countRows,'planned flights' );
call ams_wad.log_sp(spname, stTime, logNote);

IF countRows = 0 THEN
	DELETE FROM ams_al.ams_al_flp_number_schedule
	WHERE flpns_id = in_flpns_id;
	SET countRows =  ROW_COUNT();
	SET logNote=CONCAT('Deleted: ', countRows, ' rows in ams_al.ams_al_flp_number_schedule.');
	call ams_wad.log_sp(spname, stTime, logNote);
ELSE
   Update ams_al.ams_al_flp_number_schedule
   set grp_id = null
   WHERE flpns_id = in_flpns_id;
   SET countRows =  ROW_COUNT();
   SET logNote=CONCAT('Updated grp_id for : ', countRows, ' rows in ams_al.ams_al_flp_number_schedule.');
	call ams_wad.log_sp(spname, stTime, logNote);
END IF;
    
SET logNote=CONCAT('Finish ', '<- ' );
call ams_wad.log_sp(spname, stTime, logNote); 
    
SELECT countRows from dual;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ams_al_flp_number_schedule_st` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ams_al_flp_number_schedule_st`()
BEGIN
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_ams_al_flp_number_schedule_st', stTime, logNote);
END;

	set stTime = current_timestamp();
	set spname = 'ams_al.sp_ams_al_flp_number_schedule_st';
	set countRows = 0;
	SET logNote=CONCAT('Start', '->' );
	call ams_wad.log_sp(spname, stTime, logNote);

	TRUNCATE ams_al.ams_al_flp_number_schedule_st;
    SET countRows =  ROW_COUNT();
    commit;
    SET logNote=CONCAT('Truncated  ', format(countRows,0), ' records in ams_al.ams_al_flp_number_schedule_st.');
	call ams_wad.log_sp(spname, stTime, logNote);
    
    set countRows = 0;
    
    INSERT INTO ams_al.ams_al_flp_number_schedule_st
    (flpns_id, fl_count, revenue, avg_delay_min, avg_pax_pct_full, avg_payload_kg_pct_full)
	SELECT flpns.flpns_id, count(flh.fl_id) as fl_count,  
	round(sum(ifnull(flh.revenue,0)),2) as revenue,
	round(avg(ifnull(flh.delay_min,0)),0) as avg_delay_min,
	round(avg(ifnull(flph.pax_pct_full,0)),0) as avg_pax_pct_full,
	round(avg(ifnull(flph.payload_kg_pct_full,0)),0) as avg_payload_kg_pct_full
	FROM ams_al.ams_al_flp_number_schedule flpns
	left join ams_ac.ams_al_flight_plan_h flph on flph.flpns_id = flpns.flpns_id
	left join ams_ac.ams_al_flight_h flh on flh.flp_id = flph.flp_id
	group by flph.flpns_id;
	SET countRows = ROW_COUNT();
	COMMIT;
	SET logNote=CONCAT('Inserted  ', format(countRows,0), ' records in ams_al.ams_al_flp_number_schedule_st.');
	call ams_wad.log_sp(spname, stTime, logNote);
	
	SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ams_bank_h_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ams_bank_h_day`()
BEGIN
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('sp_ams_bank_h_day', stTime, logNote);
END;

	set stTime = current_timestamp();
	set spname = 'sp_ams_bank_h_day';
	set countRows = 0;
	SET logNote=CONCAT('Start', '->' );
	call ams_wad.log_sp(spname, stTime, logNote);

	call ams_al.sp_tr_post();

	INSERT INTO ams_al.ams_bank_h
	( bank_id, al_id, amount)
	SELECT b.bank_id, b.al_id, b.amount 
	FROM ams_al.ams_bank b;
	
	SET countRows = countRows + ROW_COUNT();
	COMMIT;

	SET logNote=CONCAT('History records created for ', countRows, ' bank accounts. ');
	call ams_wad.log_sp(spname, stTime, logNote);

	SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_flp_route_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_flp_route_create`(IN in_dep_ap_id INT, IN in_arr_ap_id INT, OUT out_route_id INT)
BEGIN

DECLARE countRows, i_dest_id, i_rev_dest_id INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_flp_route_create', stTime, logNote);
END;


DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_flp_route_create', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_flp_route_create';
set countRows = 0;
SET logNote=CONCAT('Start (',in_dep_ap_id, ', ', in_arr_ap_id,') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

SELECT r.route_id INTO out_route_id
FROM ams_al.ams_flp_route r 
where (r.dep_ap_id=in_dep_ap_id and r.arr_ap_id = in_arr_ap_id or 
r.dep_ap_id=in_arr_ap_id and r.arr_ap_id = in_dep_ap_id );

IF out_route_id is null then

	call ams_wad.get_cfg_airport_destination(in_dep_ap_id, in_arr_ap_id, i_dest_id);
    
    call ams_wad.get_cfg_airport_destination( in_arr_ap_id, in_dep_ap_id, i_rev_dest_id);

	SET logNote=CONCAT('i_dest_id:', i_dest_id , ', i_rev_dest_id:', i_rev_dest_id );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	-- route_id , route_nr as routeNr,
	INSERT INTO ams_al.ams_flp_route
		( route_name, dest_id, rev_dest_id, dep_ap_id,  arr_ap_id, flight_h, price_e, price_b, price_f, price_c_p_kg)
	select concat(ifnull(dep.ap_iata, dep.ap_icao), ' - ', ifnull(arr.ap_iata, arr.ap_icao), ' - ', ifnull(dep.ap_iata, dep.ap_icao)) as route_name,
		d.dest_id, rd.dest_id, d.dep_ap_id, d.arr_ap_id,
		-- d.max_flight_h as flight_h,
        ROUND((d.distance_km/s.min_speed_kmph), 2) as flight_h,
		ROUND(flp.param_value* d.max_ticket_price_e, 2) as price_e, 
		ROUND(flp.param_value* d.max_ticket_price_b, 2) as price_b,
		ROUND(flp.param_value* d.max_ticket_price_f, 2) as price_f,
		ROUND(flp.param_value * (d.distance_km * pwkg.param_value), 4) as price_c_p_kg
	From (select i_dest_id as dest_id, i_rev_dest_id as rev_dest_id from dual) cfg
	join ams_wad.cfg_airport_destination d on d.dest_id = cfg.dest_id
    join ams_wad.cfg_airport_destination rd on rd.dest_id = cfg.rev_dest_id
	join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id
	join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
	join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'max_ticket_price_cpd_kg_per_km' ) pwkg on 1=1
	join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'flight_charter_pct_from_max_price' ) flp on 1=1
    join(SELECT min(cruise_speed_kmph) as min_speed_kmph FROM ams_ac.cfg_mac where cruise_speed_kmph is not null) s on 1=1;

	SET out_route_id = LAST_INSERT_ID();
			
	SET logNote=CONCAT('inserted out_route_id ', out_route_id);
	call ams_wad.log_sp(spname, stTime, logNote);
	commit;

	UPDATE ams_al.ams_flp_route set route_nr = out_route_id where route_id = out_route_id;

	commit;
END IF;

SET logNote=CONCAT('Finish ', '<- ' );
call ams_wad.log_sp(spname, stTime, logNote);

SELECT out_route_id from dual;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_flp_route_get` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_flp_route_get`(IN in_dep_ap_id INT, IN in_arr_ap_id INT, OUT out_route_id INT)
BEGIN

DECLARE countRows, i_dest_id, i_rev_dest_id INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_flp_route_create', stTime, logNote);
END;


DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_flp_route_create', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_flp_route_create';
set countRows = 0;
SET logNote=CONCAT('Start (',in_dep_ap_id, ', ', in_arr_ap_id,') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

SELECT r.route_id INTO out_route_id
FROM ams_al.ams_flp_route r 
where (r.dep_ap_id=in_dep_ap_id and r.arr_ap_id = in_arr_ap_id or 
r.dep_ap_id=in_arr_ap_id and r.arr_ap_id = in_dep_ap_id );

IF out_route_id is null then

	call ams_wad.get_cfg_airport_destination(in_dep_ap_id, in_arr_ap_id, i_dest_id);
    
    call ams_wad.get_cfg_airport_destination( in_arr_ap_id, in_dep_ap_id, i_rev_dest_id);

	SET logNote=CONCAT('i_dest_id:', i_dest_id , ', i_rev_dest_id:', i_rev_dest_id );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	-- route_id , route_nr as routeNr,
	INSERT INTO ams_al.ams_flp_route
		( route_name, dest_id, rev_dest_id, dep_ap_id,  arr_ap_id, flight_h, price_e, price_b, price_f, price_c_p_kg)
	select concat(ifnull(dep.ap_iata, dep.ap_icao), ' - ', ifnull(arr.ap_iata, arr.ap_icao), ' - ', ifnull(dep.ap_iata, dep.ap_icao)) as route_name,
		d.dest_id, rd.dest_id, d.dep_ap_id, d.arr_ap_id,
		d.max_flight_h as flight_h,
		ROUND(flp.param_value* d.max_ticket_price_e, 2) as price_e, 
		ROUND(flp.param_value* d.max_ticket_price_b, 2) as price_b,
		ROUND(flp.param_value* d.max_ticket_price_f, 2) as price_f,
		ROUND(flp.param_value * (d.distance_km * pwkg.param_value), 4) as price_c_p_kg
	From (select i_dest_id as dest_id, i_rev_dest_id as rev_dest_id from dual) cfg
	join ams_wad.cfg_airport_destination d on d.dest_id = cfg.dest_id
    join ams_wad.cfg_airport_destination rd on rd.dest_id = cfg.rev_dest_id
	join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id
	join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
	join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'max_ticket_price_cpd_kg_per_km' ) pwkg on 1=1
	join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'flight_charter_pct_from_max_price' ) flp on 1=1;

	SET out_route_id = LAST_INSERT_ID();
			
	SET logNote=CONCAT('inserted out_route_id ', out_route_id);
	call ams_wad.log_sp(spname, stTime, logNote);
	commit;

	UPDATE ams_al.ams_flp_route set route_nr = out_route_id where route_id = out_route_id;

	commit;
END IF;

SET logNote=CONCAT('Finish out_route_id:', out_route_id, ' <- ' );
call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_st_ac_revenue_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_st_ac_revenue_day`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_fl_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_st_ac_revenue_day', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_st_ac_revenue_day';
set countRows = 0;

SET logNote=CONCAT('Start ->' );
call ams_wad.log_sp(spname, stTime, logNote);

	-- Check for not compressed records in ams_al.ams_bank_transaction
    select count(YEARWEEK(payment_time)) as days_to_compress
	INTO countRows
	from  ams_al.ams_bank_transaction
	where YEARWEEK(payment_time)<YEARWEEK(now()) and 
    YEARWEEK(payment_time) > (select ifnull(max(tr_week), DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) from ams_ac.st_ac_revenue_day_tr );
	
    SET logNote=CONCAT('Transactions to process: ', countRows );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    IF countRows > 0 THEN
    
		set countRows = 0;

		INSERT INTO ams_ac.st_ac_revenue_day_tr 
		(tr_year, tr_month, tr_week, tr_date, ac_id, al_id, tr_type_id, expences, income, revenue)
		SELECT year(tre.payment_time) as tr_year, month(tre.payment_time) as tr_month,
		YEARWEEK(tre.payment_time) as tr_week, DATE(tre.payment_time) as tr_date, 
		tre.aircraft_id as ac_id,  b.al_id, tre.tr_type_id,
		Round( sum( if(b.bank_id = tre.bank_id_from, ifnull(tre.amount, 0), 0)) ,2) as expences,
		Round( sum( if(b.bank_id = tre.bank_id_to, ifnull(tre.amount, 0), 0)) ,2) as income,
		Round( 
			sum( if(b.bank_id = tre.bank_id_to, ifnull(tre.amount, 0), 0)) -
			sum( if(b.bank_id = tre.bank_id_from, ifnull(tre.amount, 0), 0)),2) as revenue
		FROM ams_ac.ams_aircraft ac
		join ams_al.ams_bank b on b.al_id = ac.owner_al_id
		join ams_al.ams_bank_transaction tre on tre.aircraft_id = ac.ac_id
		join ams_al.cfg_transaction_type tt on tt.tr_type_id = tre.tr_type_id
		where YEARWEEK(tre.payment_time)<YEARWEEK(now()) and 
			YEARWEEK(tre.payment_time) > (
            select ifnull(max(tr_week), DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) from ams_ac.st_ac_revenue_day_tr)
		group by year(tre.payment_time) , month(tre.payment_time) , DATE(tre.payment_time), 
			tre.aircraft_id, b.bank_id, b.al_id, tre.tr_type_id, tt.tr_name
		order by tr_year, tr_month, tr_date;

        SET countRows =  ROW_COUNT();
        SET logNote=CONCAT('Inserted: ', countRows, ' rows in ams_ac.st_ac_revenue_day_tr.');
        call ams_wad.log_sp(spname, stTime, logNote);
        
        INSERT INTO ams_ac.st_ac_revenue_day 
		(tr_year, tr_month, tr_week, tr_date, ac_id, al_id, expences, income, revenue)
        SELECT year(tre.payment_time) as tr_year,
			month(tre.payment_time) as tr_month,
			YEARWEEK(tre.payment_time) as tr_week,
			DATE(tre.payment_time) as tr_date, 
			tre.aircraft_id as ac_id, b.al_id,
			Round(sum(if(tre.tr_id is null,0,ifnull(tre.amount, 0))),2)  as expences,
			Round(sum(if(tri.tr_id is null,0,ifnull(tri.amount, 0))),2)  as income,
			Round((sum(if(tri.tr_id is null,0,ifnull(tri.amount, 0))) - 
			sum(if(tre.tr_id is null,0,ifnull(tre.amount, 0)))),2) as revenue
		FROM ams_al.ams_bank b
		 join ams_al.ams_bank_transaction tre on b.bank_id = tre.bank_id_from
		 join ams_al.ams_bank_transaction tri on b.bank_id = tri.bank_id_to
		join ams_al.cfg_transaction_type tt on tt.tr_type_id = tre.tr_type_id
		where tre.aircraft_id is not null and YEARWEEK(tre.payment_time)<YEARWEEK(now()) and 
		 YEARWEEK(tre.payment_time) > (
         select ifnull(max(tr_week), DATE_SUB(CURDATE(), INTERVAL 1 MONTH)) from ams_ac.st_ac_revenue_day)
		group by year(tre.payment_time) , month(tre.payment_time) , DATE(tre.payment_time), 
		tre.aircraft_id, b.al_id
		order by tr_year, tr_month, tr_date;
        
        SET countRows =  ROW_COUNT();
        SET logNote=CONCAT('Inserted: ', countRows, ' rows in ams_ac.st_ac_revenue_day.');
        call ams_wad.log_sp(spname, stTime, logNote);
        
        COMMIT;
    
    END IF;
 
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_st_al_ap_pc_tranfered_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_st_al_ap_pc_tranfered_day`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_st_al_ap_pc_tranfered_day', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_st_al_ap_pc_tranfered_day';
set countRows = 0;

SET logNote=CONCAT('Start ->' );
call ams_wad.log_sp(spname, stTime, logNote);
INSERT INTO ams_al.st_al_ap_pc_tranfered_day
	(al_id, ap_id, fl_count, pax_f, pax_b, pax_e, cargo_kg)
SELECT p.al_id, p.ap_id, count(distinct(p.fl_id)) as fl_count,
	sum(ifnull(p.pax_f,0 )) as pax_f,
	sum(ifnull(p.pax_b,0 )) as pax_b,
	sum(ifnull(p.pax_e,0)) as pax_e,
	round(sum(ifnull(p.cargo_kg, 0)), 2) as cargo_kg			
from ams_al.ams_al_ap_pc_tranfered p
group by p.al_id, p.ap_id;
SET countRows =  ROW_COUNT();
SET logNote=CONCAT('Inserted: ', countRows, ' rows in ams_al.st_al_ap_pc_tranfered_day.');
call ams_wad.log_sp(spname, stTime, logNote);
COMMIT;

delete from ams_al.ams_al_ap_pc_tranfered;
SET countRows =  ROW_COUNT();
SET logNote=CONCAT('Deleted ', countRows, ' rows from ams_al.ams_al_ap_pc_tranfered.');
call ams_wad.log_sp(spname, stTime, logNote);
COMMIT;

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);    

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_st_bank_transaction_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_st_bank_transaction_day`()
BEGIN

DECLARE countRows, i_yearweek INT;
DECLARE max_st_date date;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_fl_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_bank_transaction_day', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_bank_transaction_day';
set countRows = 0;

SET logNote=CONCAT('Start ->' );
call ams_wad.log_sp(spname, stTime, logNote);

	select date(ifnull( max( tr_date),DATE_SUB(CURDATE(), interval 1 year))) 
    INTO max_st_date from ams_al.st_bank_transaction_day;
	
    
	SET logNote=CONCAT('Transactions after max_st_date: ', max_st_date );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	-- Check for not compressed records in ams_al.ams_bank_transaction
    select count(tre.tr_id) as days_to_compress
	INTO countRows
	from  ams_al.ams_bank_transaction tre
	where DATE(tre.payment_time)< DATE_SUB(now(), interval 1 day)  
	and DATE(tre.payment_time)  > max_st_date;
	
    SET logNote=CONCAT('Transactions to process: ', countRows );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    IF countRows > 0 THEN
    
		set countRows = 0;
        
        INSERT INTO ams_al.st_bank_transaction_day
			(tr_year, tr_month, tr_week, tr_date,
			bank_id, al_id, tr_type_id, tr_name,
			expences, income)
        SELECT year(tre.payment_time) as tr_year, month(tre.payment_time) as tr_month,
			YEARWEEK(tre.payment_time) as tr_week, DATE(tre.payment_time) as tr_date, 
			b.bank_id, b.al_id as al_id,  tre.tr_type_id,tte.tr_name,
			Round( sum( ifnull(tre.amount, 0)) ,2) as expences,
			null as income
		FROM  ams_al.ams_bank_transaction tre
        join ams_al.cfg_transaction_type tte on tte.tr_type_id = tre.tr_type_id
        join ams_al.ams_bank b on b.bank_id = tre.bank_id_from
		where DATE(tre.payment_time)< DATE_SUB(now(), interval 1 day)  
			and DATE(tre.payment_time) > max_st_date
		group by year(tre.payment_time)  , month(tre.payment_time) , 
        DATE(tre.payment_time),  b.bank_id, b.al_id, tre.tr_type_id, tte.tr_name
		order by tr_year, tr_month, tr_date;

        SET countRows =  ROW_COUNT();
        SET logNote=CONCAT('Inserted: ', countRows, ' rows expences.');
        call ams_wad.log_sp(spname, stTime, logNote);
        
        INSERT INTO ams_al.st_bank_transaction_day
			(tr_year, tr_month, tr_week, tr_date,
			bank_id, al_id, tr_type_id, tr_name,
			expences, income)
		SELECT year(tre.payment_time) as tr_year, month(tre.payment_time) as tr_month,
			YEARWEEK(tre.payment_time) as tr_week, DATE(tre.payment_time) as tr_date, 
			b.bank_id, b.al_id as al_id,  tre.tr_type_id,tte.tr_name,
			null as expences,
			Round( sum( ifnull(tre.amount, 0)) ,2) as income
		FROM  ams_al.ams_bank_transaction tre
        join ams_al.cfg_transaction_type tte on tte.tr_type_id = tre.tr_type_id
        join ams_al.ams_bank b on b.bank_id = tre.bank_id_to
		where DATE(tre.payment_time)< DATE_SUB(now(), interval 1 day)  
			and DATE(tre.payment_time) > max_st_date
		group by year(tre.payment_time)  , month(tre.payment_time) , 
        DATE(tre.payment_time),  b.bank_id, b.al_id, tre.tr_type_id, tte.tr_name
		order by tr_year, tr_month, tr_date;
       
		SET countRows =  ROW_COUNT();
        SET logNote=CONCAT('Inserted: ', countRows, ' rows income.');
        call ams_wad.log_sp(spname, stTime, logNote);
		COMMIT;
        
        INSERT INTO ams_al.st_bank_revenue_day 
			(tr_year, tr_month, tr_week, tr_date,
			bank_id, al_id, expences, income, revenue) 
		SELECT tr_year, tr_month, tr_week, tr_date, bank_id, al_id,
			sum(ifnull(expences, 0)) as expences, sum(ifnull(income, 0)) as income,
			sum(ifnull(income, 0)) - sum(ifnull(expences, 0)) as revenue
		FROM ams_al.st_bank_transaction_day
			where DATE(tr_date)< DATE_SUB(now(), interval 1 day)  
					and DATE(tr_date) > max_st_date
			group by tr_year, tr_month, tr_week, tr_date,
				bank_id, al_id
			order by tr_date, bank_id;
        
        SET countRows =  ROW_COUNT();
        SET logNote=CONCAT('Inserted: ', countRows, ' rows in ams_al.st_bank_revenue_day.');
        call ams_wad.log_sp(spname, stTime, logNote);
        
        COMMIT;
    
    END IF;

call ams_al.sp_ams_al_flp_number_schedule_st();
 
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_ac_boarding` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_ac_boarding`(IN in_fl_log_id INT, out o_tr_id int)
BEGIN

DECLARE i_tr_type_id, i_bank_id_from, i_ams_bank_id, i_al_id, i_ac_id, i_d_ap_id, i_fl_id  INT;
DECLARE d_amount DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_description VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_tr_ac_boarding', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_tr_ac_boarding';
set countRows = 0;
set i_tr_type_id = 3; -- Boarding
set i_ams_bank_id = 1;

SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

SELECT  b.bank_id as bank_id_from, 
	ROUND( 
	(p.boarding_fee_p_pax* ifnull(dep.ap_type_id, 1)/4)*
	(ac1.pilots_count + ifnull(ac1.crew_count,0) + ifnull(fl.pax, 0))
		,2) as amount,
    concat(IFNULL(dep.ap_iata, dep.ap_icao),' ', tt.tr_name, ' for flight ', fl.fl_name) as description,
    fl.al_id, fl.ac_id, fl.d_ap_id, fl.fl_id
    
INTO i_bank_id_from, d_amount, s_description, i_al_id, i_ac_id, i_d_ap_id, i_fl_id
FROM ams_ac.ams_al_flight fl
join ams_al.ams_bank b on b.al_id = fl.al_id
join ams_ac.ams_al_flight_log l on fl.fl_id = l.fl_id
join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id
join ams_ac.ams_aircraft ac1 on ac1.ac_id = ac.ac_id
join ams_ac.cfg_mac macr on macr.mac_id = ac1.mac_id
join ams_al.cfg_transaction_type tt on tt.tr_type_id = i_tr_type_id
join ams_ac.cfg_aircraft_flight_status acfs on acfs.acfs_id = l.acfs_id
join ams_al.v_al_callc_param_h p on p.al_id = ac1.owner_al_id
where l.fl_log_id = in_fl_log_id;

	INSERT INTO ams_al.ams_bank_transaction
	(bank_id_from, bank_id_to, amount, description, tr_type_id, ap_id, aircraft_id, fl_id, posted)
	VALUES(i_bank_id_from, i_ams_bank_id, ifnull(d_amount,0), s_description, i_tr_type_id, i_d_ap_id, i_ac_id, i_fl_id, 0);
	
	SET o_tr_id = LAST_INSERT_ID();
	
	SET logNote=CONCAT('Transaction ', o_tr_id, ' from ',i_bank_id_from,' to ',i_ams_bank_id , ' for ',d_amount, ' $.' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
	
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_ac_cargo_load` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_ac_cargo_load`(IN in_fl_log_id INT, out o_tr_id int)
BEGIN

DECLARE i_tr_type_id, i_bank_id_from, i_ams_bank_id, i_al_id, i_ac_id, i_d_ap_id, i_fl_id  INT;
DECLARE d_amount DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_description VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_tr_ac_cargo_load', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_tr_ac_cargo_load';
set countRows = 0;
set i_tr_type_id = 4; -- Cargo Load
set i_ams_bank_id = 1;

SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

SELECT  b.bank_id as bank_id_from, 
	ROUND( 
	p.landing_fee_p_kg_mtow*(ifnull(dep.ap_type_id, 1)/4)*
	round(((ifnull(ac1.pilots_count,1) + ifnull(ac1.crew_count,0) + ifnull(fl.pax, 0) )*p.pax_weight_kg)  + ifnull(macr.empty_w_kg,1),0)
		,2) as amount,
    concat(tt.tr_name, ' for flight ', fl.fl_name) as description,
    fl.al_id, fl.ac_id, fl.d_ap_id, fl.fl_id
    
INTO i_bank_id_from, d_amount, s_description, i_al_id, i_ac_id, i_d_ap_id, i_fl_id
FROM ams_ac.ams_al_flight fl
join ams_al.ams_bank b on b.al_id = fl.al_id
join ams_ac.ams_al_flight_log l on fl.fl_id = l.fl_id
join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id
join ams_ac.ams_aircraft ac1 on ac1.ac_id = ac.ac_id
join ams_ac.cfg_mac macr on macr.mac_id = ac1.mac_id
join ams_al.cfg_transaction_type tt on tt.tr_type_id = i_tr_type_id
join ams_ac.cfg_aircraft_flight_status acfs on acfs.acfs_id = l.acfs_id
join ams_al.v_al_callc_param_h p on p.al_id = ac1.owner_al_id
where l.fl_log_id = in_fl_log_id;

	INSERT INTO ams_al.ams_bank_transaction
	(bank_id_from, bank_id_to, amount, description, tr_type_id, ap_id, aircraft_id, fl_id, posted)
	VALUES(i_bank_id_from, i_ams_bank_id, d_amount, s_description, i_tr_type_id, i_d_ap_id, i_ac_id, i_fl_id, 0);
	
	SET o_tr_id = LAST_INSERT_ID();
	
	SET logNote=CONCAT('Transaction ', o_tr_id, ' from ',i_bank_id_from,' to ',i_ams_bank_id , ' for ',d_amount, ' $.' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
	
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_ac_cargo_unload` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_ac_cargo_unload`(IN in_fl_log_id INT, out o_tr_id int)
BEGIN

DECLARE i_tr_type_id, i_bank_id_from, i_ams_bank_id, i_al_id, i_ac_id, i_d_ap_id, i_fl_id  INT;
DECLARE d_amount DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_description VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_tr_ac_cargo_unload', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_tr_ac_cargo_unload';
set countRows = 0;
set i_tr_type_id = 6; -- Cargo Unload
set i_ams_bank_id = 1;

SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

set i_bank_id_from = 0;
/*
SELECT  b.bank_id as bank_id_from, 
	ROUND( 
	p.landing_fee_p_kg_mtow*(ifnull(arr.ap_type_id, 1)/4)*
	round(((ifnull(ac1.pilots_count,1) + ifnull(ac1.crew_count,0) )*p.pax_weight_kg) + ifnull(fl.payload_kg,1) + ifnull(macr.empty_w_kg,1),0)
		,2) as amount,
    concat(IFNULL(arr.ap_iata, arr.ap_icao),' ', tt.tr_name, ' for flight ', fl.fl_name) as description,
    fl.al_id, fl.ac_id, arr.ap_id, fl.fl_id
    
INTO i_bank_id_from, d_amount, s_description, i_al_id, i_ac_id, i_d_ap_id, i_fl_id
FROM ams_ac.ams_al_flight fl
join ams_al.ams_bank b on b.al_id = fl.al_id
join ams_ac.ams_al_flight_log l on fl.fl_id = l.fl_id
join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id
join ams_ac.ams_aircraft ac1 on ac1.ac_id = ac.ac_id
join ams_ac.cfg_mac macr on macr.mac_id = ac1.mac_id
join ams_al.cfg_transaction_type tt on tt.tr_type_id = i_tr_type_id
join ams_ac.cfg_aircraft_flight_status acfs on acfs.acfs_id = l.acfs_id
join ams_al.v_al_callc_param_h p on p.al_id = ac1.owner_al_id
where l.fl_log_id = in_fl_log_id;
*/
SELECT  b.bank_id as bank_id_from, 
	ROUND( 
	p.landing_fee_p_kg_mtow*(ifnull(arr.ap_type_id, 1)/4)*
	round(((ifnull(ac1.pilots_count,1) + ifnull(ac1.crew_count,0) )*p.pax_weight_kg) + ifnull(fl.payload_kg,1) + ifnull(macr.empty_w_kg,1),0)
		,2) as amount,
    concat(IFNULL(arr.ap_iata, arr.ap_icao),' ', tt.tr_name, ' for flight ', fl.fl_name) as description,
    fl.al_id, fl.ac_id, arr.ap_id, fl.fl_id
    
INTO i_bank_id_from, d_amount, s_description, i_al_id, i_ac_id, i_d_ap_id, i_fl_id
FROM ams_ac.ams_al_flight_log l
join ams_ac.ams_al_flight fl on fl.fl_id = l.fl_id
join ams_al.ams_bank b on b.al_id = fl.al_id
join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id
join ams_ac.ams_aircraft ac1 on ac1.ac_id = ac.ac_id
join ams_ac.cfg_mac macr on macr.mac_id = ac1.mac_id
join ams_al.cfg_transaction_type tt on tt.tr_type_id = i_tr_type_id
join ams_ac.cfg_aircraft_flight_status acfs on acfs.acfs_id = l.acfs_id
join ams_al.v_al_callc_param_h p on p.al_id = ac1.owner_al_id
where l.fl_log_id = in_fl_log_id;

IF i_bank_id_from > 0 THEN
	INSERT INTO ams_al.ams_bank_transaction
	(bank_id_from, bank_id_to, amount, description, tr_type_id, ap_id, aircraft_id, fl_id, posted)
	VALUES(i_bank_id_from, i_ams_bank_id, d_amount, s_description, i_tr_type_id, i_d_ap_id, i_ac_id, i_fl_id, 0);
	
	SET o_tr_id = LAST_INSERT_ID();
	
	SET logNote=CONCAT('Transaction ', o_tr_id, ' from ',i_bank_id_from,' to ',i_ams_bank_id , ' for ',d_amount, ' $.' );
	call ams_wad.log_sp(spname, stTime, logNote);
	
    COMMIT;
END IF;

select o_tr_id from dual;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_ac_disembarking` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_ac_disembarking`(IN in_fl_log_id INT, out o_tr_id int)
BEGIN

DECLARE i_tr_type_id, i_bank_id_from, i_ams_bank_id, i_al_id, i_ac_id, i_d_ap_id, i_fl_id  INT;
DECLARE d_amount DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_description VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_tr_ac_disembarking', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_tr_ac_disembarking';
set countRows = 0;
set i_tr_type_id = 5; -- Disembarking
set i_ams_bank_id = 1;

SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

SELECT  b.bank_id as bank_id_from, 
	ROUND( 
	(p.boarding_fee_p_pax* ifnull(arr.ap_type_id, 1)/4)*
	(ac1.pilots_count + ifnull(ac1.crew_count,0) + ifnull(fl.pax, 0))
		,2) as amount,
    concat(IFNULL(arr.ap_iata, arr.ap_icao),' ', tt.tr_name, ' for flight ', fl.fl_name) as description,
    fl.al_id, fl.ac_id, arr.ap_id, fl.fl_id
    
INTO i_bank_id_from, d_amount, s_description, i_al_id, i_ac_id, i_d_ap_id, i_fl_id
FROM ams_ac.ams_al_flight fl
join ams_al.ams_bank b on b.al_id = fl.al_id
join ams_ac.ams_al_flight_log l on fl.fl_id = l.fl_id
join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id
join ams_ac.ams_aircraft ac1 on ac1.ac_id = ac.ac_id
join ams_ac.cfg_mac macr on macr.mac_id = ac1.mac_id
join ams_al.cfg_transaction_type tt on tt.tr_type_id = i_tr_type_id
join ams_ac.cfg_aircraft_flight_status acfs on acfs.acfs_id = l.acfs_id
join ams_al.v_al_callc_param_h p on p.al_id = ac1.owner_al_id
where l.fl_log_id = in_fl_log_id;

	INSERT INTO ams_al.ams_bank_transaction
	(bank_id_from, bank_id_to, amount, description, tr_type_id, ap_id, aircraft_id, fl_id, posted)
	VALUES(i_bank_id_from, i_ams_bank_id, d_amount, s_description, i_tr_type_id, i_d_ap_id, i_ac_id, i_fl_id, 0);
	
	SET o_tr_id = LAST_INSERT_ID();
	
	SET logNote=CONCAT('Transaction ', o_tr_id, ' from ',i_bank_id_from,' to ',i_ams_bank_id , ' for ',d_amount, ' $.' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
	
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_ac_fl_income` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_ac_fl_income`(IN in_fl_log_id INT, out o_tr_id int)
BEGIN

DECLARE i_tr_type_id, i_bank_id_from, i_ams_bank_id, i_al_id, i_ac_id, i_d_ap_id, i_fl_id  INT;
DECLARE d_amount DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_description VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_tr_ac_fl_income', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_tr_ac_fl_income';
set countRows = 0;
set i_tr_type_id = 14; -- Flight Income
set i_ams_bank_id = 1;

SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

SELECT  b.bank_id as bank_id_from, 
	ROUND( ifnull(flfp1.price,0),2) as amount,
    concat(IFNULL(arr.ap_iata, arr.ap_icao),' ', tt.tr_name, ' for flight ', fl.fl_name) as description,
    fl.al_id, fl.ac_id, arr.ap_id, fl.fl_id
    
INTO i_bank_id_from, d_amount, s_description, i_al_id, i_ac_id, i_d_ap_id, i_fl_id
FROM ams_ac.ams_al_flight fl
join ams_al.ams_bank b on b.al_id = fl.al_id
join ams_ac.ams_al_flight_log l on fl.fl_id = l.fl_id
join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id
join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id
join ams_ac.ams_aircraft ac1 on ac1.ac_id = ac.ac_id
join ams_ac.cfg_mac macr on macr.mac_id = ac1.mac_id
join ams_al.cfg_transaction_type tt on tt.tr_type_id = i_tr_type_id
join ams_ac.cfg_aircraft_flight_status acfs on acfs.acfs_id = l.acfs_id
join ams_al.v_al_callc_param_h p on p.al_id = ac1.owner_al_id
join(
select  flpl.fl_id, sum(ifnull(flpl.pax, 0)) as pax, 
	sum(ifnull(flpl.cargo_kg, 0)) as cargo_kg,
    sum(ifnull(flpl.payload_kg, 0)) as payload_kg,
    sum(ifnull(flpl.price, 0)) as price
from ams_ac.ams_al_flight_payload flpl
join ams_ac.ams_al_flight_log fll on fll.fl_id = flpl.fl_id
where fll.fl_log_id = in_fl_log_id
group by  flpl.fl_id) flfp1 on flfp1.fl_id = l.fl_id
where l.fl_log_id = in_fl_log_id;

	INSERT INTO ams_al.ams_bank_transaction
	(bank_id_from, bank_id_to, amount, description, tr_type_id, ap_id, aircraft_id, fl_id, posted)
	VALUES(i_ams_bank_id, i_bank_id_from, d_amount, s_description, i_tr_type_id, i_d_ap_id, i_ac_id, i_fl_id, 0);
	
	SET o_tr_id = LAST_INSERT_ID();
	
	SET logNote=CONCAT('Transaction ', o_tr_id, ' from ',i_bank_id_from,' to ',i_ams_bank_id , ' for ',d_amount, ' $.' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
	
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_ac_insurance_week` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_ac_insurance_week`(IN in_ac_id INT, out o_tr_id int)
BEGIN

DECLARE i_tr_type_id, i_bank_id_from, i_ams_bank_id, i_al_id, i_d_ap_id  INT;
DECLARE d_amount DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_description VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_tr_ac_insurance_week', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_tr_ac_insurance_week';
set countRows = 0;
set i_tr_type_id = 7; -- Insurance Week
set i_ams_bank_id = 1;

SET logNote=CONCAT('Start (',in_ac_id,') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

	SELECT b.bank_id as bank_id_from, act.insurance_cost_week as amount,
		concat(ac.registration,' ', tt.tr_name, ' for ', YEARWEEK(now())) as description,
		ac.owner_al_id as al_id
	INTO i_bank_id_from, d_amount, s_description, i_al_id
	FROM ams_ac.ams_aircraft ac
		join ams_ac.ams_aircraft_curr acc on acc.ac_id = ac.ac_id
		join ams_ac.cfg_mac m on m.mac_id = ac.mac_id
		join ams_al.ams_bank b on b.al_id = ac.owner_al_id
		join ams_ac.cfg_aircraft_type act on act.ac_type_id = m.ac_type_id
		join ams_al.cfg_transaction_type tt on tt.tr_type_id = i_tr_type_id
	where ac.ac_id=in_ac_id;

	INSERT INTO ams_al.ams_bank_transaction
	(bank_id_from, bank_id_to, amount, description, tr_type_id, ap_id, aircraft_id, fl_id, posted)
	VALUES(i_bank_id_from, i_ams_bank_id, d_amount, s_description, i_tr_type_id, null, in_ac_id, null, 0);
	
	SET o_tr_id = LAST_INSERT_ID();
	
	SET logNote=CONCAT('Transaction ', o_tr_id, ' from ',i_bank_id_from,' to ',i_ams_bank_id , ' for ',d_amount, ' $.' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    COMMIT;
    
    CALL ams_al.sp_tr_post();

    update ams_ac.ams_aircraft_curr
    set insurance_expiration_date = DATE_ADD(CURDATE(), INTERVAL 7 day)
    where ac_id = in_ac_id;
	
    COMMIT;
    
    SET logNote=CONCAT('Finish ', '<-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_ac_landing` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_ac_landing`(IN in_fl_log_id INT, out o_tr_id int)
BEGIN

DECLARE i_tr_type_id, i_bank_id_from, i_ams_bank_id, i_al_id, i_ac_id, i_d_ap_id, i_fl_id  INT;
DECLARE d_amount DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_description VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_tr_ac_landing', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_tr_ac_landing';
set countRows = 0;
set i_tr_type_id = 8; -- Landing and Taxy to Gate
set i_ams_bank_id = 1;

SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

SELECT  b.bank_id as bank_id_from, 
	ROUND( 
		(p.landing_fee_p_kg_mtow* ifnull(arr.ap_type_id, 1)/4)*
		(
			(( ac1.pilots_count + ifnull(ac1.crew_count,0) )*p.pax_weight_kg) +
			ifnull(fl.payload_kg,0) + 
			macr.empty_w_kg
		)
		,2) as amount,
    concat(IFNULL(arr.ap_iata, arr.ap_icao),' ', tt.tr_name, ' for flight ', fl.fl_name) as description,
    fl.al_id, fl.ac_id, arr.ap_id, fl.fl_id
    
	INTO i_bank_id_from, d_amount, s_description, i_al_id, i_ac_id, i_d_ap_id, i_fl_id
	FROM ams_ac.ams_al_flight fl
	join ams_al.ams_bank b on b.al_id = fl.al_id
	join ams_ac.ams_al_flight_log l on fl.fl_id = l.fl_id
	join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
	join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id
	join ams_ac.ams_aircraft ac1 on ac1.ac_id = ac.ac_id
	join ams_ac.cfg_mac macr on macr.mac_id = ac1.mac_id
	join ams_al.cfg_transaction_type tt on tt.tr_type_id = i_tr_type_id
	join ams_ac.cfg_aircraft_flight_status acfs on acfs.acfs_id = l.acfs_id
	join ams_al.v_al_callc_param_h p on p.al_id = ac1.owner_al_id
	where l.fl_log_id = in_fl_log_id;

	INSERT INTO ams_al.ams_bank_transaction
	(bank_id_from, bank_id_to, amount, description, tr_type_id, ap_id, aircraft_id, fl_id, posted)
	VALUES(i_bank_id_from, i_ams_bank_id, d_amount, s_description, i_tr_type_id, i_d_ap_id, i_ac_id, i_fl_id, 0);
	
	SET o_tr_id = LAST_INSERT_ID();
	COMMIT;
    
    SET logNote=CONCAT('Transaction ', o_tr_id, ' from ',i_bank_id_from,' to ',i_ams_bank_id , ' for ',d_amount, ' $.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    /*
    UPDATE ams_ac.ams_aircraft_curr ac
	SET ac.curr_ap_id = i_d_ap_id
	where ac.ac_id = i_ac_id;
    COMMIT;
	*/
    SET logNote=CONCAT('Updated in ams_aircraft_curr curr_ap_id:',i_d_ap_id,' ' );
	call ams_wad.log_sp(spname, stTime, logNote);
	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_ac_maintenance` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_ac_maintenance`(IN in_acmp_id INT, out o_tr_id int)
BEGIN

DECLARE i_tr_type_id, i_bank_id_from, i_ams_bank_id, i_al_id, i_ac_id, i_ap_id, i_fl_id  INT;
DECLARE d_amount DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_description VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_tr_ac_maintenance', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_tr_ac_maintenance';
set countRows = 0;
set i_tr_type_id = 9; -- Maintenance
set i_ams_bank_id = 1;

SET logNote=CONCAT('Start (',in_acmp_id,') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

SELECT  b.bank_id as bank_id_from, 
	ROUND( ifnull(mntp.price,0),2) as amount,
    mntp.description,
    mntp.al_id, mntp.ac_id, mntp.ap_id
    
INTO i_bank_id_from, d_amount, s_description, i_al_id, i_ac_id, i_ap_id
FROM ams_ac.ams_aircraft_maintenance_plan mntp
join ams_ac.ams_aircraft_log l on l.ac_id = mntp.ac_id
join ams_al.ams_bank b on b.al_id = mntp.al_id
-- join ams_al.cfg_transaction_type tt on tt.tr_type_id = i_tr_type_id
where mntp.acmp_id = in_acmp_id;

	INSERT INTO ams_al.ams_bank_transaction
	( bank_id_to, bank_id_from, amount, description, tr_type_id, ap_id, aircraft_id, fl_id, posted)
	VALUES(i_ams_bank_id, i_bank_id_from, d_amount, s_description, i_tr_type_id, i_ap_id, i_ac_id, null, 0);
	
	SET o_tr_id = LAST_INSERT_ID();
	
	SET logNote=CONCAT('Transaction ', o_tr_id, ' from ',i_bank_id_from,' to ',i_ams_bank_id , ' for ',d_amount, ' $.' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
	
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_ac_purchase` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_ac_purchase`(in in_ac_id int, in in_al_id int, out o_tr_id int)
BEGIN

DECLARE countRows, vtr_type_id INT;
DECLARE vap_id, vbank_id_from, vbank_id_to, vtr_id INT;
DECLARE vtr_name, val_name, vac_name, vapLabel VARCHAR(1000);
DECLARE stTime TIMESTAMP;
DECLARE vamount, vprice double;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('sp_tr_ac_purchase', stTime, logNote);
END;

DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call ams_wad.log_sp('sp_tr_ac_purchase', stTime, logNote);
END;

set vtr_type_id = 2; -- Aircraft Purchase
set stTime = current_timestamp();
set spname = 'sp_tr_ac_purchase';
set countRows = 0;

SET logNote=CONCAT('Start (', in_ac_id, ', ', in_al_id, ') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

	SELECT a.ap_id, b.bank_id, b.amount
	INTO vap_id, vbank_id_from, vamount
	FROM ams_al.ams_airline a
	join ams_al.ams_bank b on b.al_id = a.al_id
	where a.al_id = in_al_id;
    
    SET logNote=CONCAT('vap_id ', vap_id, ', vbank_id_from ', vbank_id_from,', vamount ', vamount,' .');
	-- call ams_wad.log_sp(spname, stTime, logNote);
        
	SELECT b.bank_id as bank_id_to, c.price_value,
    concat(a.al_name , ' (',a.iata, ') ') as al_name,
    concat(ac.registration , ' (',ac.ac_id, ') ') as ac_name, ap.apLabel
	INTO vbank_id_to, vprice, val_name, vac_name, vapLabel
	FROM ams_ac.ams_aircraft ac
    join ams_al.ams_airline a on a.al_id = ifnull(ac.owner_al_id, 7)
	join ams_al.ams_bank b on b.al_id = ifnull(ac.owner_al_id, 7)
	join ams_ac.ams_aircraft_curr c on c.ac_id = ac.ac_id
    join ams_wad.v_solr_airport ap on ap.apId = c.curr_ap_id
	where ac.ac_id=in_ac_id;
    
    SET logNote=CONCAT('vbank_id_to ', vbank_id_to, ', vprice ', vprice,', val_name ', val_name,' .');
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    SET logNote=CONCAT( '( vamount-vprice)= ', ( vamount-vprice),' .');
	-- call ams_wad.log_sp(spname, stTime, logNote);
	
    IF vprice < vamount THEN
		
        SELECT tr_name INTO vtr_name 
        FROM ams_al.cfg_transaction_type 
        where tr_type_id = vtr_type_id;
        
		SET logNote=CONCAT('Transaction ', vtr_name, ', vprice ', vprice,', tr_type_id ', vtr_type_id,' .');
		-- call ams_wad.log_sp(spname, stTime, logNote);
        
		SET logNote=CONCAT(vtr_name, ' ', vac_name, ' from ', val_name,' airline on ', vapLabel);
		call ams_wad.log_sp(spname, stTime, logNote);
        
        INSERT INTO ams_al.ams_bank_transaction
		(bank_id_from, bank_id_to, amount, description, tr_type_id, aircraft_id, posted)
		VALUES(vbank_id_from, vbank_id_to, vprice, logNote, vtr_type_id, in_ac_id, 0);
        
         SET vtr_id = LAST_INSERT_ID();
        
        SET logNote=CONCAT('inserted vtr_id ', vtr_id, ' for vbank_id_to ',vbank_id_to , '.' );
		-- call ams_wad.log_sp(spname, stTime, logNote);
        
        select vtr_id into o_tr_id from dual;
        
		SET logNote=CONCAT('inserted o_tr_id ', o_tr_id, ' for vtr_id ',vtr_id , '.' );
		-- call ams_wad.log_sp(spname, stTime, logNote);

        COMMIT;
	else
		SET logNote=CONCAT('Unavailable funds ',vamount, ' required ', vprice, '.' );
		call ams_wad.log_sp(spname, stTime, logNote);
        
    END IF;
	
	SET logNote=CONCAT('Finish ', '<-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    select o_tr_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_ac_refuling` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_ac_refuling`(IN in_fl_log_id INT, out o_tr_id int)
BEGIN

DECLARE i_tr_type_id, i_al_id, i_ac_id, i_bank_id_from, i_oil_l, i_ams_bank_id, i_d_ap_id, i_fl_id  INT;
DECLARE d_oli_price DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_description VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_tr_ac_refuling', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_tr_ac_refuling';
set countRows = 0;
set i_tr_type_id = 10; -- Refuling
set i_ams_bank_id = 1;

SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

SELECT  b.bank_id as bank_id_from, fl.al_id, fl.ac_id, fl.d_ap_id,
	ROUND(fl.oil_l*op.price_per_l,2) as oli_price, fl.oil_l,
    concat(tt.tr_name, ' ', fl.oil_l, ' L fuel for flight ', fl.fl_name) as description,
    fl.fl_id
INTO i_bank_id_from, i_al_id, i_ac_id, i_d_ap_id,  d_oli_price, i_oil_l, s_description, i_fl_id
FROM ams_ac.ams_al_flight_log l 
	join ams_ac.ams_al_flight fl on fl.fl_id = l.fl_id
	join ams_al.ams_bank b on b.al_id = fl.al_id
    join ams_al.cfg_transaction_type tt on tt.tr_type_id = 10 and 1=1
	join ams_al.cfg_oil_price op on op.oil_price_id = 1 and 1 = 1 
where l.fl_log_id = in_fl_log_id;

	INSERT INTO ams_al.ams_bank_transaction
	(bank_id_from, bank_id_to, amount, description, tr_type_id, ap_id, aircraft_id, fl_id, posted)
	VALUES(i_bank_id_from, i_ams_bank_id, d_oli_price, s_description, i_tr_type_id, i_d_ap_id, i_ac_id, i_fl_id, 0);
	
	SET o_tr_id = LAST_INSERT_ID();
	
	SET logNote=CONCAT('Transaction ', o_tr_id, ' from ',i_bank_id_from,' to ',i_ams_bank_id , ' for ',d_oli_price, ' $.' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
	
    COMMIT;
    
    SET logNote=CONCAT('Finish ', '<-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    select o_tr_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_ac_sell` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_ac_sell`(in in_ac_id int, out o_tr_id int)
BEGIN

DECLARE countRows, vtr_type_id INT;
DECLARE vap_id, vbank_id_from, vbank_id_to, vtr_id INT;
DECLARE vtr_name, val_name, vac_name, vapLabel VARCHAR(1000);
DECLARE stTime TIMESTAMP;
DECLARE vamount, vprice double;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('sp_tr_ac_sell', stTime, logNote);
END;

DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call ams_wad.log_sp('sp_tr_ac_sell', stTime, logNote);
END;

set vtr_type_id = 20; -- Aircraft Sell
set stTime = current_timestamp();
set spname = 'sp_tr_ac_sell';
set countRows = 0;

SET logNote=CONCAT('Start (', in_ac_id, ') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

	select count(*) INTO countRows
	FROM ams_ac.ams_aircraft ac
	join ams_ac.ams_aircraft_curr c on c.ac_id = ac.ac_id
	join ams_ac.ams_aircraft_log l on l.ac_id = ac.ac_id
	where ac.ac_id=in_ac_id and ac.ac_status_id=2 
	and ac.for_shedule_flights=0 and ac.owner_al_id is not null
	and l.acat_id = 6 and c.flight_in_queue=0;

	IF countRows > 0 THEN
		SELECT a.ap_id, b.bank_id, b.amount
		INTO vap_id, vbank_id_from, vamount
		FROM ams_al.ams_airline a
		join ams_al.ams_bank b on b.al_id = a.al_id
		where a.al_id = 7; -- system airline
		
		SET logNote=CONCAT('vap_id ', vap_id, ', vbank_id_from ', vbank_id_from,', vamount ', vamount,' .');
		call ams_wad.log_sp(spname, stTime, logNote);
			
		SELECT b.bank_id as bank_id_to, round((c.price_value*0.85),2) as vprice,
		concat(a.al_name , ' (',a.iata, ') ') as al_name,
		concat(ac.registration , ' (',ac.ac_id, ') ') as ac_name, ap.apLabel
		INTO vbank_id_to, vprice, val_name, vac_name, vapLabel
		FROM ams_ac.ams_aircraft ac
		join ams_al.ams_airline a on a.al_id = ifnull(ac.owner_al_id, 7)
		join ams_al.ams_bank b on b.al_id = ifnull(ac.owner_al_id, 7)
		join ams_ac.ams_aircraft_curr c on c.ac_id = ac.ac_id
		join ams_wad.v_solr_airport ap on ap.apId = c.curr_ap_id
		where ac.ac_id=in_ac_id;
		
		SET logNote=CONCAT('vbank_id_to ', vbank_id_to, ', vprice ', vprice,', val_name ', val_name,' .');
		call ams_wad.log_sp(spname, stTime, logNote);
		
		SET logNote=CONCAT( '( vamount-vprice)= ', ( vamount-vprice),' .');
		call ams_wad.log_sp(spname, stTime, logNote);
		
		IF vprice > 0 THEN
			
			SELECT tr_name INTO vtr_name 
			FROM ams_al.cfg_transaction_type 
			where tr_type_id = vtr_type_id;
			
			SET logNote=CONCAT('Transaction ', vtr_name, ', vprice ', vprice,', tr_type_id ', vtr_type_id,' .');
			call ams_wad.log_sp(spname, stTime, logNote);
			
			SET logNote=CONCAT(vtr_name, ' ', vac_name, ' from ', val_name,' airline on ', vapLabel);
			call ams_wad.log_sp(spname, stTime, logNote);
			
			INSERT INTO ams_al.ams_bank_transaction
			(bank_id_from, bank_id_to, amount, description, tr_type_id, aircraft_id, posted)
			VALUES(vbank_id_from, vbank_id_to, vprice, logNote, vtr_type_id, in_ac_id, 0);
			
			 SET vtr_id = LAST_INSERT_ID();
			
			SET logNote=CONCAT('inserted vtr_id ', vtr_id, ' for vbank_id_to ',vbank_id_to , '.' );
			call ams_wad.log_sp(spname, stTime, logNote);
			
			select vtr_id into o_tr_id from dual;
			
			SET logNote=CONCAT('inserted o_tr_id ', o_tr_id, ' for vtr_id ',vtr_id , '.' );
			-- call ams_wad.log_sp(spname, stTime, logNote);
			update ams_ac.ams_aircraft_curr
            set price_value = round( (price_value*0.9),2)
            where ac_id = in_ac_id;
			COMMIT;
		else
			SET logNote=CONCAT('Unavailable funds ',vamount, ' required ', vprice, '.' );
			call ams_wad.log_sp(spname, stTime, logNote);
		END IF;
        
	ELSE
		SET logNote=CONCAT('Aircraft is is busy can not sell.' );
		call ams_wad.log_sp(spname, stTime, logNote);
	END IF;
		
SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
    select o_tr_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_ac_takeoff` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_ac_takeoff`(IN in_fl_log_id INT, out o_tr_id int)
BEGIN

DECLARE i_tr_type_id, i_bank_id_from, i_ams_bank_id, i_al_id, i_ac_id, i_d_ap_id, i_fl_id  INT;
DECLARE d_amount DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_description VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_tr_ac_takeoff', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_tr_ac_takeoff';
set countRows = 0;
set i_tr_type_id = 12; -- Taxy and Takeoff
set i_ams_bank_id = 1;

SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

SELECT  b.bank_id as bank_id_from, 
	ROUND( 
		(p.landing_fee_p_kg_mtow* ifnull(dep.ap_type_id, 1)/4)*
		(
			(( ac1.pilots_count + ifnull(ac1.crew_count,0) )*p.pax_weight_kg) +
			ifnull(fl.payload_kg,0) + 
			macr.empty_w_kg
		)
		,2) as amount,
    concat(IFNULL(dep.ap_iata, dep.ap_icao),' ', tt.tr_name, ' for flight ', fl.fl_name) as description,
    fl.al_id, fl.ac_id, fl.d_ap_id, fl.fl_id
    
INTO i_bank_id_from, d_amount, s_description, i_al_id, i_ac_id, i_d_ap_id, i_fl_id
FROM ams_ac.ams_al_flight fl
join ams_al.ams_bank b on b.al_id = fl.al_id
join ams_ac.ams_al_flight_log l on fl.fl_id = l.fl_id
join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id
join ams_ac.ams_aircraft ac1 on ac1.ac_id = ac.ac_id
join ams_ac.cfg_mac macr on macr.mac_id = ac1.mac_id
join ams_al.cfg_transaction_type tt on tt.tr_type_id = i_tr_type_id
join ams_ac.cfg_aircraft_flight_status acfs on acfs.acfs_id = l.acfs_id
join ams_al.v_al_callc_param_h p on p.al_id = ac1.owner_al_id
where l.fl_log_id = in_fl_log_id;

	INSERT INTO ams_al.ams_bank_transaction
	(bank_id_from, bank_id_to, amount, description, tr_type_id, ap_id, aircraft_id, fl_id, posted)
	VALUES(i_bank_id_from, i_ams_bank_id, d_amount, s_description, i_tr_type_id, i_d_ap_id, i_ac_id, i_fl_id, 0);
	
	SET o_tr_id = LAST_INSERT_ID();
	
	SET logNote=CONCAT('Transaction ', o_tr_id, ' from ',i_bank_id_from,' to ',i_ams_bank_id , ' for ',d_amount, ' $.' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
	
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_al_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_al_create`(in in_bank_id_to int)
BEGIN

DECLARE countRows, vtr_type_id, val_id, vtr_id INT;
DECLARE vtr_name, val_name VARCHAR(1000);
DECLARE stTime TIMESTAMP;
DECLARE vamount double;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('sp_tr_al_create', stTime, logNote);
END;

set vtr_type_id = 1;
set stTime = current_timestamp();
set spname = 'sp_tr_al_create';
set countRows = 0;

SET logNote=CONCAT('Start', '->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

	SELECT tr_name, amount INTO vtr_name, vamount FROM ams_al.cfg_transaction_type where tr_type_id = vtr_type_id;
	SET logNote=CONCAT('Transaction ', vtr_name, ', amount ', vamount,', tr_type_id ', vtr_type_id,' .');
	call ams_wad.log_sp(spname, stTime, logNote);
    
    SELECT a.al_id, concat(a.al_name , ' (',a.iata, ') ')as al_name  INTO val_id, val_name 
    FROM ams_al.ams_bank b
	join ams_al.ams_airline a on a.al_id = b.al_id
	where b.bank_id = in_bank_id_to;
	SET logNote=CONCAT(vtr_name, ' ', val_name,' .');
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_al.ams_bank_transaction
	(bank_id_from, bank_id_to, amount, description, tr_type_id, posted)
	VALUES(1, in_bank_id_to, vamount, logNote, vtr_type_id, 0);
    
    SET vtr_id = LAST_INSERT_ID();
	
    COMMIT;

	SET logNote=CONCAT('inserted tr_id ', vtr_id, ' for in_bank_id_to ',in_bank_id_to , '' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

	SET logNote=CONCAT('Finish ', '<-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    select vtr_id from dual;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_al_hub_upkeep_week` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_al_hub_upkeep_week`(IN in_hub_id INT, out o_tr_id int)
BEGIN

DECLARE i_tr_type_id, i_bank_id_from, i_ams_bank_id, i_al_id, i_ap_id  INT;
DECLARE d_amount DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_description VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_tr_al_hub_upkeep_week', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_tr_al_hub_upkeep_week';
set countRows = 0;
set i_tr_type_id = 16; -- Hub Upkeep
set i_ams_bank_id = 1;

SET logNote=CONCAT('Start (',in_hub_id,') ->' );
call ams_wad.log_sp(spname, stTime, logNote);
 
	SELECT b.bank_id as bank_id_from, ROUND((tt.amount*ap.ap_type_id)*IF(al.ap_id = h.ap_id,  ppb.param_value, pph.param_value),2) as amount,
		concat(if(ap.ap_icao is null, ap.ap_icao, ap.ap_iata),' ', ap.ap_name, ' ',IF(al.ap_id = h.ap_id, 'Base ', 'Hub '), tt.tr_name, ' for week ', YEARWEEK(now())) as description,
		h.al_id as al_id, h.ap_id
	INTO i_bank_id_from, d_amount, s_description, i_al_id, i_ap_id
	FROM ams_al.ams_al_hub h
		join ams_wad.cfg_airport ap on ap.ap_id = h.ap_id
		join ams_al.ams_bank b on b.al_id = h.al_id
        join ams_al.ams_airline al on al.al_id = h.al_id
		join (select param_value FROM ams_al.cfg_callc_param where param_name = 'fbo_fee_discount_base' ) ppb on 1=1
        join (select param_value FROM ams_al.cfg_callc_param where param_name = 'fbo_fee_discount_hub' ) pph on 1=1
		join ams_al.cfg_transaction_type tt on tt.tr_type_id = i_tr_type_id
	where h.hub_id=in_hub_id;

	INSERT INTO ams_al.ams_bank_transaction
	(bank_id_from, bank_id_to, amount, description, tr_type_id, ap_id, aircraft_id, fl_id, posted)
	VALUES(i_bank_id_from, i_ams_bank_id, d_amount, s_description, i_tr_type_id, i_ap_id, null, null, 0);
	
	SET o_tr_id = LAST_INSERT_ID();
	
	SET logNote=CONCAT('Transaction ', o_tr_id, ' from ',i_bank_id_from,' to ',i_ams_bank_id , ' for ',d_amount, ' $.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    COMMIT;
    
    CALL ams_al.sp_tr_post();

    update ams_al.ams_al_hub
    -- set upkeep_day = DATE_ADD(CURDATE(), INTERVAL 7 day)
    set upkeep_day = DATE_ADD(concat(date_format(upkeep_day, '%Y-%m-%d'), ' ', date_format(upkeep_day, '%H:%i:00')), INTERVAL 7 day)
    where hub_id = in_hub_id;
	
    COMMIT;
    
    SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_ams_award_shedule_flights` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_ams_award_shedule_flights`(IN in_al_id INT, IN in_tr_type_id INT, out o_tr_id int)
BEGIN

DECLARE i_bank_id_to, i_ams_bank_id INT;
DECLARE i_tr_type_17, i_tr_type_18, i_tr_type_19 INT;
DECLARE d_amount DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_description VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_tr_ams_award_shedule_flights', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_tr_ams_award_shedule_flights';
set i_tr_type_17 = 17; -- AMS award - 1000 sheduled flights
set i_tr_type_18 = 18; -- AMS award - 500 sheduled flights
set i_tr_type_19 = 19; -- AMS award - 10,000 sheduled flights
set countRows = 0;
set i_ams_bank_id = 1;

SET logNote=CONCAT('Start (',in_al_id,', ',in_tr_type_id,') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

	IF in_tr_type_id = i_tr_type_18 THEN
		SELECT b.bank_id as bank_id_to, ROUND((tt.amount*(ifnull(alc.lf_shedule,0) div 500)),2) as amount,
			concat('Awarded for completion of ',(ifnull(alc.lf_shedule,0) div 500)*500 ,' scheduled flights', '.') as description
		INTO i_bank_id_to, d_amount, s_description
		FROM ams_al.ams_airline al
		left join ams_al.ams_airline_counters alc on alc.al_id = al.al_id
		join ams_al.ams_bank b on b.al_id = al.al_id
		join ams_wad.v_solr_airport ap on ap.apId = al.ap_id
		join ams_al.cfg_transaction_type tt on tt.tr_type_id = in_tr_type_id
		where al.al_id=in_al_id;
	END IF;
    IF in_tr_type_id = i_tr_type_17 THEN
		SELECT b.bank_id as bank_id_to, ROUND((tt.amount*(ifnull(alc.lf_shedule,0) div 1000)),2) as amount,
			concat('Awarded for completion of ',(ifnull(alc.lf_shedule,0) div 1000)*1000 ,' scheduled flights', '.') as description
		INTO i_bank_id_to, d_amount, s_description
		FROM ams_al.ams_airline al
		left join ams_al.ams_airline_counters alc on alc.al_id = al.al_id
		join ams_al.ams_bank b on b.al_id = al.al_id
		join ams_wad.v_solr_airport ap on ap.apId = al.ap_id
		join ams_al.cfg_transaction_type tt on tt.tr_type_id = in_tr_type_id
		where al.al_id=in_al_id;
	END IF;
    IF in_tr_type_id = i_tr_type_19 THEN
		SELECT b.bank_id as bank_id_to, ROUND((tt.amount*(ifnull(alc.lf_shedule,0) div 10000)),2) as amount,
			concat('Awarded for completion of ',(ifnull(alc.lf_shedule,0) div 10000)*10000 ,' scheduled flights', '.') as description
		INTO i_bank_id_to, d_amount, s_description
		FROM ams_al.ams_airline al
		left join ams_al.ams_airline_counters alc on alc.al_id = al.al_id
		join ams_al.ams_bank b on b.al_id = al.al_id
		join ams_wad.v_solr_airport ap on ap.apId = al.ap_id
		join ams_al.cfg_transaction_type tt on tt.tr_type_id = in_tr_type_id
		where al.al_id=in_al_id;
	END IF;

	INSERT INTO ams_al.ams_bank_transaction
	(bank_id_from, bank_id_to, amount, description, tr_type_id, ap_id, aircraft_id, fl_id, posted)
	VALUES(i_ams_bank_id, i_bank_id_to, d_amount, s_description, in_tr_type_id, null, null, null, 0);
	
	SET o_tr_id = LAST_INSERT_ID();
	
	SET logNote=CONCAT('Transaction ', o_tr_id, ' from ',i_ams_bank_id,' to ',i_bank_id_to , ' for ',d_amount, ' $.' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    -- CALL ams_al.sp_tr_post();
	
    SET logNote=CONCAT('Finish ', '<-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_flight_crew` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_flight_crew`(IN in_fl_log_id INT, out o_tr_id int)
BEGIN

DECLARE i_tr_type_id, i_bank_id_from, i_ams_bank_id, i_al_id, i_ac_id, i_d_ap_id, i_fl_id  INT;
DECLARE d_amount DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_description VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_tr_flight_crew', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_tr_flight_crew';
set countRows = 0;
set i_tr_type_id = 13; -- Flight Crew flight_crew
set i_ams_bank_id = 1;

SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

SELECT  b.bank_id as bank_id_from, 
	Round(
		((fl.distance_km/macr.cruise_speed_kmph)*(macr.cost_per_fh * p.cost_per_fh_factor))
        ,2) as amount,
    concat(IFNULL(dep.ap_iata, dep.ap_icao),' ', tt.tr_name, ' for flight ', fl.fl_name) as description,
    fl.al_id, fl.ac_id, fl.d_ap_id, fl.fl_id
    
INTO i_bank_id_from, d_amount, s_description, i_al_id, i_ac_id, i_d_ap_id, i_fl_id
FROM ams_ac.ams_al_flight fl
join ams_al.ams_bank b on b.al_id = fl.al_id
join ams_ac.ams_al_flight_log l on fl.fl_id = l.fl_id
join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id
join ams_ac.ams_aircraft ac1 on ac1.ac_id = ac.ac_id
join ams_ac.cfg_mac macr on macr.mac_id = ac1.mac_id
join ams_al.cfg_transaction_type tt on tt.tr_type_id = i_tr_type_id
join ams_ac.cfg_aircraft_flight_status acfs on acfs.acfs_id = l.acfs_id
join ams_al.v_al_callc_param_h p on p.al_id = ac1.owner_al_id
where l.fl_log_id = in_fl_log_id;

	INSERT INTO ams_al.ams_bank_transaction
	(bank_id_from, bank_id_to, amount, description, tr_type_id, ap_id, aircraft_id, fl_id, posted)
	VALUES(i_bank_id_from, i_ams_bank_id, d_amount, s_description, i_tr_type_id, i_d_ap_id, i_ac_id, i_fl_id, 0);
	
	SET o_tr_id = LAST_INSERT_ID();
	
	SET logNote=CONCAT('Transaction ', o_tr_id, ' from ',i_bank_id_from,' to ',i_ams_bank_id , ' for ',d_amount, ' $.' );
	call ams_wad.log_sp(spname, stTime, logNote);
	
    COMMIT;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_gov_support` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_gov_support`(IN in_al_id INT, out o_tr_id int)
BEGIN

DECLARE i_tr_type_id, i_bank_id_to, i_ams_bank_id INT;
DECLARE d_amount DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_description VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_tr_gov_support', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_tr_gov_support';
set countRows = 0;
set i_tr_type_id = 15; -- GOV Support
set i_ams_bank_id = 1;

SET logNote=CONCAT('Start (',in_al_id,') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

	SELECT b.bank_id as bank_id_to, tt.amount as amount,
		concat(ap.iso2,' ', tt.tr_name, ' for Q', quarter(now()), ' of ', year(now()), '.') as description
	INTO i_bank_id_to, d_amount, s_description
	FROM ams_al.ams_airline al
    join ams_al.ams_bank b on b.al_id = al.al_id
	join ams_wad.v_solr_airport ap on ap.apId = al.ap_id
	join ams_al.cfg_transaction_type tt on tt.tr_type_id = i_tr_type_id
	where al.al_id=in_al_id;

	INSERT INTO ams_al.ams_bank_transaction
	(bank_id_from, bank_id_to, amount, description, tr_type_id, ap_id, aircraft_id, fl_id, posted)
	VALUES(i_ams_bank_id, i_bank_id_to, d_amount, s_description, i_tr_type_id, null, null, null, 0);
	
	SET o_tr_id = LAST_INSERT_ID();
	
	SET logNote=CONCAT('Transaction ', o_tr_id, ' from ',i_ams_bank_id,' to ',i_bank_id_to , ' for ',d_amount, ' $.' );
	 call ams_wad.log_sp(spname, stTime, logNote);
    COMMIT;
    
    CALL ams_al.sp_tr_post();
	
    COMMIT;
    
    SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_tr_post` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_tr_post`()
BEGIN

DECLARE countRows, vbank_id_from, vbank_id_to, val_id, vtr_id INT;
DECLARE vtr_name, val_name VARCHAR(1000);
DECLARE stTime TIMESTAMP;
DECLARE vamount double;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;
DECLARE cursor_tr CURSOR FOR SELECT tr_id 
					FROM ams_al.ams_bank_transaction where posted = 0 order by tr_id desc limit 20;
                        
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_al.sp_tr_post', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_al.sp_tr_post';
set countRows = 0;

SET logNote=CONCAT('Start', '->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_tr;

	read_loop: LOOP
			FETCH cursor_tr INTO vtr_id;
		
        IF done THEN
			SET logNote=CONCAT('No records ', 'done' );
			-- call ams_wad.log_sp(spname, stTime, logNote);
			LEAVE read_loop;
		END IF;
        
        SET logNote=CONCAT('tr_id: ',vtr_id, ' get transaction data...');
		-- call ams_wad.log_sp(spname, stTime, logNote);
        SELECT bank_id_from, bank_id_to, ifnull(amount, 0) INTO vbank_id_from, vbank_id_to, vamount 
        FROM ams_al.ams_bank_transaction where tr_id = vtr_id;

        SET logNote=CONCAT('bank_id_from: ',vbank_id_from,', bank_id_to: ',vbank_id_to,', amount: ',vamount ,'.');
		-- call ams_wad.log_sp(spname, stTime, logNote);
        
        UPDATE ams_al.ams_bank b set amount = Round((b.amount - vamount),2) WHERE b.bank_id = vbank_id_from;
        commit;
		UPDATE ams_al.ams_bank b set amount = Round((b.amount + vamount),2) WHERE b.bank_id = vbank_id_to;
        commit;
        UPDATE ams_al.ams_bank_transaction b set posted = 1 WHERE b.tr_id = vtr_id; 
        commit;
		SET countRows = countRows+1;
		
     END LOOP;

	CLOSE cursor_tr;
    COMMIT;
    SET logNote=CONCAT('Posted ', countRows, ' transactions.');
	call ams_wad.log_sp(spname, stTime, logNote);

	SET logNote=CONCAT('Finish ', '<-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 14:16:34
