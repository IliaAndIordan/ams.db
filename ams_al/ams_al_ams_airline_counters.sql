CREATE DATABASE  IF NOT EXISTS `ams_al` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_al`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_al
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ams_airline_counters`
--

DROP TABLE IF EXISTS `ams_airline_counters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ams_airline_counters` (
  `al_id` int NOT NULL,
  `fl_transfer` int DEFAULT '0',
  `fl_charter` int DEFAULT '0',
  `lf_shedule` int DEFAULT '0',
  PRIMARY KEY (`al_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ams_airline_counters`
--

LOCK TABLES `ams_airline_counters` WRITE;
/*!40000 ALTER TABLE `ams_airline_counters` DISABLE KEYS */;
INSERT INTO `ams_airline_counters` VALUES (1,3162,5807,34660),(7,12,14,300),(8,99,361,8270),(10,126,347,8141),(11,0,0,0),(12,0,0,0),(13,0,0,0),(14,0,0,0);
/*!40000 ALTER TABLE `ams_airline_counters` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`iordanov_ams`@`%`*/ /*!50003 TRIGGER `ams_airline_counters_AFTER_UPDATE` AFTER UPDATE ON `ams_airline_counters` FOR EACH ROW BEGIN
	DECLARE i_tr_id,  i_tr_type_id int;
    DECLARE i_tr_type_17, i_tr_type_18, i_tr_type_19 INT;
    DECLARE stTime TIMESTAMP;
	DECLARE logNote VARCHAR(4000);
    set stTime = current_timestamp();
    set i_tr_type_17 = 17; -- AMS award - 1000 sheduled flights
	set i_tr_type_18 = 18; -- AMS award - 500 sheduled flights
	set i_tr_type_19 = 19; -- AMS award - 10,000 sheduled flights
	set i_tr_id=0;
    SET logNote=CONCAT('New lf_shedule: ', new.lf_shedule, ' old ',old.lf_shedule, ' .' );
	-- call ams_wad.log_sp('ams_airline_counters_AFTER_UPDATE', stTime, logNote);
	IF (new.lf_shedule <> old.lf_shedule) THEN
		IF (new.lf_shedule <= 30000) THEN
			SET logNote=CONCAT('Apply AMS Award 17: ', (new.lf_shedule div 1000)<>(old.lf_shedule div 1000), ' ?');
			-- call ams_wad.log_sp('ams_airline_counters_AFTER_UPDATE', stTime, logNote);
			IF((new.lf_shedule div 1000)<>(old.lf_shedule div 1000))THEN
				CALL ams_al.sp_tr_ams_award_shedule_flights(old.al_id, i_tr_type_17, i_tr_id);
			END IF;
            IF i_tr_id=0 AND((new.lf_shedule div 500)<>(old.lf_shedule div 500))THEN
				CALL ams_al.sp_tr_ams_award_shedule_flights(old.al_id, i_tr_type_18, i_tr_id);
			END IF;
		END IF;
        IF (new.lf_shedule > 30000) THEN 
			IF((new.lf_shedule div 10000)<>(old.lf_shedule div 10000))THEN
				CALL ams_al.sp_tr_ams_award_shedule_flights(old.al_id, i_tr_type_19, i_tr_id);
			END IF;
		END IF;
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 14:15:16
