CREATE DATABASE  IF NOT EXISTS `ams_al` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_al`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_al
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ams_user`
--

DROP TABLE IF EXISTS `ams_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ams_user` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `user_name` varchar(256) DEFAULT NULL,
  `user_email` varchar(512) NOT NULL,
  `user_pwd` varchar(50) NOT NULL,
  `user_role` int DEFAULT '3' COMMENT 'predefined roles 1-admin, 2 editor, 3 user, 4 - guest',
  `is_receive_emails` int DEFAULT '0',
  `ip_address` varchar(256) DEFAULT NULL,
  `adate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `udate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `PK_USER_ID` (`user_id`),
  UNIQUE KEY `user_email_UNIQUE` (`user_email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ams_user`
--

LOCK TABLES `ams_user` WRITE;
/*!40000 ALTER TABLE `ams_user` DISABLE KEYS */;
INSERT INTO `ams_user` VALUES (1,'AMS Admin','iziordanov@gmail.com','iordanov',1,1,'\"46.10.221.26\"','2020-12-27 23:58:08','2024-10-24 09:45:50'),(2,'D M Amerik','dmamerik@abv.bg','iordanov',3,0,'\"46.10.221.26\"','2021-09-26 20:02:13','2025-01-15 07:14:58'),(3,'Air Nort Macedonia','mka@abv.bg','iordanov',3,0,'\"46.10.221.26\"','2024-06-24 16:48:14','2024-10-22 08:22:25'),(8,'Air Albania','air_albania@abv.bg','iordanov',3,0,'\"46.10.221.26\"','2024-06-26 22:09:39','2024-10-23 12:41:49'),(9,NULL,'harder.philip.jr@gmail.com','Dbkl5678z',3,0,'{\"lat\":42.7034531999999984464011504314839839935302734375,\"lng\":23.279695100000001417583916918374598026275634765625,\"ipAddress\":\"46.10.221.26\",\"action\":\"user login\",\"time\":\"2024-09-18 19:53:16\"}','2024-08-25 10:09:33','2024-09-18 16:53:17'),(10,NULL,'gillesdebruyne99@gmail.com','wafwaf1999',3,0,'{\"lat\":42.703432800000001634543878026306629180908203125,\"lng\":23.279702700000001414082362316548824310302734375,\"ipAddress\":\"46.10.221.26\",\"action\":\"user login\",\"time\":\"2024-10-03 11:04:18\"}','2024-09-25 09:52:50','2024-10-03 08:04:19');
/*!40000 ALTER TABLE `ams_user` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`iordanov_ams`@`%`*/ /*!50003 TRIGGER `ams_user_AFTER_UPDATE` AFTER UPDATE ON `ams_user` FOR EACH ROW BEGIN

INSERT INTO `ams_al`.`ams_user_h`
(`user_id`,
`user_name`,
`user_email`,
`user_pwd`,
`user_role`,
`is_receive_emails`,
`ip_address`,
`adate`,
`udate`)
SELECT * from ams_al.ams_user where user_id=OLD.user_id;

END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 14:15:34
