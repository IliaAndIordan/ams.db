CREATE DATABASE  IF NOT EXISTS `ams_al` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_al`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_al
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfg_transaction_type`
--

DROP TABLE IF EXISTS `cfg_transaction_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfg_transaction_type` (
  `tr_type_id` int NOT NULL AUTO_INCREMENT,
  `tr_name` varchar(256) DEFAULT NULL,
  `tr_description` varchar(1024) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `fixed_price` int DEFAULT NULL,
  `tr_group` varchar(45) DEFAULT 'Expences',
  PRIMARY KEY (`tr_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfg_transaction_type`
--

LOCK TABLES `cfg_transaction_type` WRITE;
/*!40000 ALTER TABLE `cfg_transaction_type` DISABLE KEYS */;
INSERT INTO `cfg_transaction_type` VALUES (1,'Airline Create','Ammount of  money invested to launch a new airline.',2500000,1,'1 Airline'),(2,'Aircraft Purchase','Ammount of  money invested in aircraft purchase',NULL,0,'Aircraft'),(3,'Boarding',NULL,NULL,0,'Flight'),(4,'Cargo Load',NULL,NULL,0,'Flight'),(5,'Disembarking',NULL,NULL,0,'Flight'),(6,'Cargo Unload',NULL,NULL,0,'Flight'),(7,'Insurance Week',NULL,NULL,0,'Aircraft'),(8,'Landing and Taxy to Gate',NULL,NULL,0,'Flight'),(9,'Maintenance',NULL,NULL,0,'Aircraft'),(10,'Refuling',NULL,NULL,0,'Flight'),(11,'Tax Airport Week',NULL,NULL,0,'Aircraft'),(12,'Taxy and Takeoff',NULL,NULL,0,'Flight'),(13,'Flight Crew','Payments for Flight Crew: the people involved with flying a plane',NULL,0,'Flight'),(14,'Flight Income','Ticket sell and transported cargo payments',NULL,NULL,'Flight'),(15,'Government-Backed Support','Government-Backed Support and Finance for Business',2000000,1,'1 Airline'),(16,'Hub Upkeep Week','Hub Upkeep per week 240*apType',240,0,'1 Airline'),(17,'AMS award - 1000 sheduled flights','AMS award for completion of additional 1000 scheduled flights',2000000,1,'1 Airline'),(18,'AMS award - 500 sheduled flights','AMS award for completion of additional 500 scheduled flights',500000,1,'1 Airline'),(19,'AMS award - 10,000 sheduled flights','AMS award for completion of additional 10,000 scheduled flights',10000000,1,'1 Airline'),(20,'Aircraft Sell','Sell Aircraft for 85% of current aircraft value',NULL,NULL,'Aircraft');
/*!40000 ALTER TABLE `cfg_transaction_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 14:15:23
