CREATE DATABASE  IF NOT EXISTS `ams_al` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_al`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_al
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ams_al_hub`
--

DROP TABLE IF EXISTS `ams_al_hub`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ams_al_hub` (
  `hub_id` int NOT NULL AUTO_INCREMENT,
  `hub_name` varchar(256) DEFAULT NULL,
  `al_id` int DEFAULT NULL,
  `ap_id` int DEFAULT NULL,
  `upkeep_day` datetime DEFAULT CURRENT_TIMESTAMP,
  `adate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `udate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`hub_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ams_al_hub`
--

LOCK TABLES `ams_al_hub` WRITE;
/*!40000 ALTER TABLE `ams_al_hub` DISABLE KEYS */;
INSERT INTO `ams_al_hub` VALUES (1,'D M Amerik SOF',1,108,'2025-01-24 12:00:00','2023-10-21 08:21:58','2025-01-16 22:09:32'),(2,NULL,7,108,'2025-01-26 00:00:00','2023-10-21 08:21:58','2025-01-17 22:09:31'),(9,'Hub  VAR Varna',7,12,'2025-01-26 00:00:00','2024-04-26 07:56:58','2025-01-17 22:09:32'),(10,'Base  LWSK Skopje Int',8,199,'2025-01-28 18:29:00','2024-06-25 15:29:51','2025-01-20 22:09:31'),(11,'Base  LATI Mother Teresa',10,128,'2025-01-26 02:05:00','2024-06-26 22:22:14','2025-01-18 22:09:32'),(12,'Hub  LBWN Varna',1,12,'2025-01-26 20:08:00','2024-08-07 17:08:44','2025-01-18 22:09:33'),(15,'Base  RPLL Ninoy Aquino',11,272,'2025-01-22 19:49:00','2024-09-18 16:49:04','2025-01-14 22:09:31'),(16,'Hub  UGTB Tbilisi IA',1,219,'2025-01-22 09:49:00','2024-10-02 06:49:44','2025-01-14 22:09:32'),(17,'Hub  LUKK Chişinău',1,277,'2025-01-22 18:15:00','2024-10-30 16:15:22','2025-01-14 22:09:33'),(18,'Hub  LGAV  Eleftherios Venizelos Int',10,158,'2025-01-26 02:05:00','2024-11-13 09:45:54','2025-01-18 22:09:33'),(19,'Hub  LYBE Nikola Tesla',8,253,'2025-01-28 15:19:00','2024-12-10 13:19:49','2025-01-20 22:09:32'),(20,'Hub  LQSA Sarajevo IA',10,312,'2025-01-26 02:05:00','2024-12-10 14:23:42','2025-01-18 22:09:34'),(21,'Hub  UKBB Boryspil IA',10,261,'2025-01-27 18:36:00','2025-01-06 16:36:44','2025-01-19 22:09:31');
/*!40000 ALTER TABLE `ams_al_hub` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`iordanov_ams`@`%`*/ /*!50003 TRIGGER `ams_al_hub_AFTER_UPDATE` AFTER UPDATE ON `ams_al_hub` FOR EACH ROW BEGIN
INSERT INTO ams_al.ams_al_hub_h
( hub_id, hub_name, al_id, ap_id, upkeep_day)
SELECT hub_id, hub_name, al_id, ap_id, upkeep_day
 from ams_al.ams_al_hub where hub_id=OLD.hub_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`iordanov_ams`@`%`*/ /*!50003 TRIGGER `ams_al_hub_BEFORE_DELETE` BEFORE DELETE ON `ams_al_hub` FOR EACH ROW BEGIN
INSERT INTO ams_al.ams_al_hub_h
( hub_id, hub_name, al_id, ap_id, upkeep_day)
SELECT hub_id, concat(hub_name, ' (deleted)'), al_id, ap_id, upkeep_day
 from ams_al.ams_al_hub where hub_id=OLD.hub_id;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 14:15:28
