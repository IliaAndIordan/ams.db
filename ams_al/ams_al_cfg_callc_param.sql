CREATE DATABASE  IF NOT EXISTS `ams_al` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_al`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_al
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfg_callc_param`
--

DROP TABLE IF EXISTS `cfg_callc_param`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfg_callc_param` (
  `param_id` int NOT NULL AUTO_INCREMENT,
  `param_name` varchar(256) DEFAULT NULL,
  `param_value` double DEFAULT NULL,
  `measure_base` varchar(20) DEFAULT NULL,
  `measure` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`param_id`),
  KEY `IDX_PARAM_NAME` (`param_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfg_callc_param`
--

LOCK TABLES `cfg_callc_param` WRITE;
/*!40000 ALTER TABLE `cfg_callc_param` DISABLE KEYS */;
INSERT INTO `cfg_callc_param` VALUES (1,'pax_weight_kg',77,'kg',NULL),(2,'m_to_ft',3.28084,'m','ft'),(3,'kg_to_lbs',2.20462,'kg','lb'),(4,'l_to_us_gal',0.264172,'L','gal'),(5,'km_to_mile',0.621371,'km','ml'),(6,'km_to_nm',0.539957,'nm',NULL),(7,'landing_fee_p_kg_mtow',0.002,'$',NULL),(8,'ground_crew_fee_p_kg_mtow',0.004,'$',NULL),(9,'fbo_fee_discount_base',0.5,'$',NULL),(10,'boarding_fee_p_pax',1.16,'$',NULL),(11,'max_ticket_price_ppd_kg_per_km',0.0054,'$',NULL),(12,'max_ticket_price_cpd_kg_per_km',0.0027,'$',NULL),(13,'ticket_price_factor_b',1.8,'$',NULL),(14,'ticket_price_factor_f',3.2,'$',NULL),(15,'afr_min_aircraft_speed_kmph',120,'km/h',NULL),(16,'afr_max_aircraft_speed_kmph',850,'km/h',NULL),(17,'flight_charter_pct_from_max_price',0.85,'$',NULL),(18,'distance_unit',111.045,'km',NULL),(19,'km_per_h_to_knots',0.5399568034557235,'km/h','knots'),(32,'airport_trafic_local',0.46,'%',NULL),(33,'airport_trafic_regional',0.15,'%',NULL),(34,'airport_trafic_international',0.37,'%',NULL),(35,'cost_per_fh_factor',0.05,'$',NULL),(36,'ppd_b',0.1,'%',NULL),(37,'ppd_f',0.05,'%',NULL),(38,'ppd_f_km_min',1200,'km',NULL),(40,'calc_ac_speed_kmph',500,'km/h',NULL),(41,'fbo_fee_discount_hub',0.75,'$',NULL),(42,'flight_plan_add_days_in_advance',7,'days','days'),(43,'transfers_id',16,'number','number'),(44,'transfers_id_cpd',1,'number','number'),(45,'pcpd_month_since_adate',3,'number','number'),(46,'pcpd_days_after_udate',10,'number','number');
/*!40000 ALTER TABLE `cfg_callc_param` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 14:15:25
