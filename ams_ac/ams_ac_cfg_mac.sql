CREATE DATABASE  IF NOT EXISTS `ams_ac` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_ac`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_ac
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfg_mac`
--

DROP TABLE IF EXISTS `cfg_mac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfg_mac` (
  `mac_id` int NOT NULL AUTO_INCREMENT,
  `mfr_id` int DEFAULT NULL,
  `ac_type_id` int NOT NULL,
  `model` varchar(250) NOT NULL,
  `price` double NOT NULL COMMENT 'USD',
  `pilots` int DEFAULT NULL,
  `cruise_speed_kmph` int NOT NULL COMMENT 'Maximum Cruise Speed kmph',
  `cruise_altitude_m` int NOT NULL COMMENT 'Service Ceiling m',
  `take_off_m` int NOT NULL COMMENT 'Takeoff Distance m',
  `max_take_off_kg` double DEFAULT NULL COMMENT 'Maximum Takeoff Weight',
  `landing_m` int NOT NULL COMMENT 'Landing Distance m',
  `max_landing_kg` double DEFAULT NULL COMMENT 'Maximum Landing  Weight',
  `max_range_km` int NOT NULL COMMENT 'km',
  `empty_w_kg` double DEFAULT NULL COMMENT 'Empty Weight kg',
  `fuel_vol_l` int DEFAULT NULL COMMENT 'Usable Fuel Volume',
  `load_kg` double DEFAULT '0' COMMENT 'Useful Load kg',
  `wiki_link` varchar(1000) DEFAULT NULL,
  `ams_status_id` int DEFAULT NULL,
  `notes` varchar(2000) DEFAULT NULL,
  `fuel_consumption_lp100km` double NOT NULL COMMENT 'mpg',
  `cost_per_fh` int NOT NULL,
  `operation_time_min` int NOT NULL COMMENT 'min',
  `popularity` int DEFAULT '30',
  `max_seating` int NOT NULL COMMENT 'pax',
  `asmi_rate` int NOT NULL COMMENT 'Minimum monthly lease rate',
  `powerplant` varchar(1000) DEFAULT NULL,
  `production_start` datetime DEFAULT NULL,
  `production_rate_h` int DEFAULT NULL,
  `last_produced_on` datetime DEFAULT NULL,
  `number_build` int DEFAULT '0',
  `ap_id` int DEFAULT NULL,
  `udate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sumpax_weight_kg` double GENERATED ALWAYS AS (((`max_seating` + `pilots`) * 77)) STORED,
  `cargo_kg` double GENERATED ALWAYS AS ((`load_kg` - ((`max_seating` + `pilots`) * 77))) STORED,
  PRIMARY KEY (`mac_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfg_mac`
--

LOCK TABLES `cfg_mac` WRITE;
/*!40000 ALTER TABLE `cfg_mac` DISABLE KEYS */;
INSERT INTO `cfg_mac` (`mac_id`, `mfr_id`, `ac_type_id`, `model`, `price`, `pilots`, `cruise_speed_kmph`, `cruise_altitude_m`, `take_off_m`, `max_take_off_kg`, `landing_m`, `max_landing_kg`, `max_range_km`, `empty_w_kg`, `fuel_vol_l`, `load_kg`, `wiki_link`, `ams_status_id`, `notes`, `fuel_consumption_lp100km`, `cost_per_fh`, `operation_time_min`, `popularity`, `max_seating`, `asmi_rate`, `powerplant`, `production_start`, `production_rate_h`, `last_produced_on`, `number_build`, `ap_id`, `udate`) VALUES (1,1,1,'Cessna 182 Skylane',360900,1,269,5500,461,1406,411,1338,1720,804,237,412,'https://en.wikipedia.org/wiki/Cessna_182_Skylane',4,'Propellers: 3-bladed constant speed',13.78,185,5,45,3,45,' 1 × Lycoming IO-540-AB1A5 air-cooled flat-six, 230 hp (170 kW)','1955-12-30 18:00:00',84,'2025-01-18 03:54:43',297,27,'2025-01-18 01:54:43'),(2,3,1,'Cirrus SR22T',850000,1,395,7620,330,1633,359,1338,1943,929,348,426,'https://en.wikipedia.org/wiki/Cirrus_SR22',4,'Propellers: 3-bladed constant speed',17.91,289,5,45,3,45,' 1 × Continental IO-550-N 310 hp','2001-03-29 18:00:00',134,'2025-01-17 03:54:32',156,114,'2025-01-17 01:54:32'),(3,1,1,'Cessna Turbo STATIONAIR HD',450000,1,298,7925,600,1705,425,1633,1350,803,313,652,'https://en.wikipedia.org/wiki/Cessna_206',4,'Based on 450 annual owner-operated hours and $5.47-per-gallon fuel cost, the CESSNA 206H has total variable costs of $92,034.00, total fixed costs of $25,390.00, and an annual budget of $117,424.00. This breaks down to $260.94 per hour',23.19,260,5,4,5,4,'1 × Lycoming IO-540-AC1A naturally aspirated air-cooled flat-six engine, 300 hp (220 kW)','1962-08-30 12:00:00',168,'2025-01-20 03:54:32',27,27,'2025-01-20 01:54:32'),(4,2,2,'EMB 120 Brasilia',8500000,2,552,9085,1420,12500,1420,11250,1750,7070,3320,2774,'https://en.wikipedia.org/wiki/Embraer_EMB_120_Brasilia',4,'',189.71,2532,20,4,30,3,' Pratt & Whitney Canada PW118 4-bladed 4-bladed Hamilton Standard 14RF19','1983-07-26 09:00:00',445,'2025-01-18 03:54:59',50,115,'2025-01-18 01:54:59'),(7,1,2,'Cessna Caravan',2200000,1,344,7620,626,3990,495,3538,1980,2015,1257,969,'https://en.wikipedia.org/wiki/Cessna_208_Caravan',4,'',63.48,858,15,2,10,2,' 1 × Pratt & Whitney Canada PT6A-114A turboprop, 675 shp (503 kW)','1986-10-14 12:00:00',122,'2025-01-16 03:55:13',155,27,'2025-01-16 01:55:13'),(8,4,4,'Dassault Falcon 900',9790000,2,950,15000,1633,20640,1095,20185,7400,10256,10863,1694,'https://en.wikipedia.org/wiki/Dassault_Falcon_900',4,'The Falcon 900 took off for the first time on 21 September 1984. A total 500 Falcon 900s and 900 Bs have been delivered throughout the world.',146.8,5202,20,4,15,4,'3 × AlliedSignal TFE731-5BR-1C turbofans, 21.13 kN (4,750 lbf) ','1984-07-15 06:00:00',998,'2024-12-17 03:54:48',22,118,'2024-12-17 01:54:48'),(9,5,1,'BN-2T Islander',1800000,1,325,7620,365,2994,308,3855,1595,1866,620,1493,'https://en.wikipedia.org/wiki/Britten-Norman_BN-2_Islander',4,'British light utility aircraft and regional airliner',38.87,724,10,2,10,2,'Rolls Royce Model 250 B17 320hp','1965-06-13 00:00:00',390,'2025-01-06 03:54:38',48,119,'2025-01-06 01:54:38'),(10,2,4,'ERJ 145XR',21000000,2,854,11278,1340,24100,1430,20000,3706,12591,7467,5535,'https://en.wikipedia.org/wiki/Embraer_ERJ_family',4,'ERJ 145XR aircraft is equipped with Rolls-Royce AE 3007A1E engines. The high performance engines provide lower specific fuel consumption (SFC) and improved performance in hot and high conditions.',201.48,5856,25,4,50,1,' Rolls-Royce AE 3007-A1E ','1997-05-29 00:00:00',167,'2025-01-14 03:54:38',29,115,'2025-01-14 01:54:38'),(11,6,2,'ATR 42-600S (STOL)',26000000,2,535,6000,890,18600,810,18300,1259,10285,5625,8315,'https://en.wikipedia.org/wiki/ATR_42',4,'The ATR 42-600S is the STOL (short takeoff and landing) variant of the -600. The aircraft is capable of operating from runways as short as 800-metre-long (2,600 ft) with up to 34 passengers, and 890-metre-long (2,920 ft) fully seated (48 passengers) ',446.78,2879,25,4,48,4,'Pratt & Whitney Canada PW127XT-L','2022-11-09 08:00:00',183,'2025-01-14 03:54:48',20,235,'2025-01-14 01:54:48'),(12,8,2,'An-140',18500000,2,574,7600,1750,19150,1360,19100,1380,10110,5463,4670,'https://en.wikipedia.org/wiki/Antonov_An-140',4,'An-140 features a large and spacious cabin that offers plenty of room for all of the onboard passengers. There is space for 52 passengers sitting four in a row. With three different and clear zones, passengers can easily find the right spot for them.There are 26 seats in economy, 22 in business, and just four seats in the exclusive lounge.Additionally, there is plenty of room for cargo below the cabin and in the fuselage.',395.87,3230,20,2,52,2,'2x 2 × Pratt & Whitney PW127A or Klimov ','1997-03-02 00:00:00',5043,'2024-08-21 03:54:36',3,263,'2024-12-01 21:19:48'),(13,8,4,'An-148',26500000,2,870,12200,1885,43700,1900,43700,4400,22000,15063,9650,'https://en.wikipedia.org/wiki/Antonov_An-148',4,'Regional jet designed and built by Antonov of Ukraine. \nDevelopment of the aircraft was started in the 1990s, and its maiden flight took place on 17 December 2004. \nThe aircraft completed its certification programme on 26 February 2007.',342.34,200,30,3,85,3,'Progress D-436-148 ','2009-05-31 09:00:00',1460,'2024-11-28 03:54:33',4,263,'2024-12-01 21:21:47'),(14,7,1,'DHC-6 Twin Otter 300-G™',4500000,1,338,7620,366,5747,320,5579,1480,2653,1469,1919,'https://en.wikipedia.org/wiki/De_Havilland_Canada_DHC-6_Twin_Otter',4,'For over 50 years, the DHC-6 Twin Otter has stood alone as the most reliable and versatile aircraft in its class. After extensive consultation with customers, De Havilland Canada is poised and proud to take this iconic aircraft to new heights with the new DHC-6 Twin Otter Classic 300-G™.',99.26,1007,15,4,20,4,'PT6A-27','2010-02-15 10:00:00',525,'2025-01-20 03:54:38',9,271,'2025-01-20 01:54:38'),(15,7,1,'DASH 8-400',27000000,2,667,7620,1277,30481,1268,28123,2040,17819,6526,7441,'https://en.wikipedia.org/wiki/De_Havilland_Canada_Dash_8',4,'e Dash 8-400 is the highest capacity\nturboprop on the market today and has the lowest unit cost',319.9,2500,25,4,90,3,'PW150A, Propellers Dowty R408','1998-01-30 20:00:00',305,'2025-01-09 03:54:33',9,271,'2025-01-09 01:54:33'),(16,8,4,'An-158',30000000,2,870,12200,1900,43700,1800,43600,2500,35050,12050,8650,'https://en.wikipedia.org/wiki/Antonov_An-148',4,'The AN-158 is a twin-engine passenger aircraft designed by the Ukraine State Enterprise Antonov, and jointly manufactured by Antonov Serial Plant (Ukraine)',482,85,30,3,99,3,'Motor Sich D-436-148','2009-06-01 09:00:00',2796,'2024-12-02 03:54:32',2,263,'2024-12-02 01:54:32'),(17,2,4,'E175-E2',46800000,2,833,12000,1730,44600,1345,40000,3704,28000,10653,8078,'https://en.wikipedia.org/wiki/Embraer_E-Jet_E2_family',4,'Incorporating 4th generation fly-by-wire technology and state-of-the-art connectivity, the E175-E2 Profit Hunter sets a new benchmark in fuel efficiency and economics for this size of aircraft. (EMB 190-500) ',287.61,14900,30,4,90,4,'Pratt & Whitney PW1700G','2018-12-02 14:00:00',1800,'2025-01-05 03:54:49',23,115,'2025-01-05 01:54:49'),(18,2,5,'E190-E2',34000000,2,833,12000,1615,56400,1215,49050,5280,32500,17113,10210,'https://en.wikipedia.org/wiki/Embraer_E-Jet_E2_family',4,'',324.11,14850,30,4,114,4,'Pratt & Whitney PW1900G','2018-04-23 15:00:00',1011,'2025-01-15 03:54:34',4,115,'2025-01-15 01:54:34'),(19,1,4,'Citation CJ4 Gen3',9600000,1,835,13716,1039,7761,896,7500,4010,3874,3305,1243,'https://en.wikipedia.org/wiki/Cessna_CitationJet/M2',4,'(Model 525) Series of light business jets built by Cessna, and are part of the Citation family. Launched in October 1989, the first flight of the Model 525 was on April 29, 1991',82.42,3850,10,5,9,5,'','2022-12-03 10:00:00',56,'2025-01-20 03:54:45',18,27,'2025-01-20 01:54:45');
/*!40000 ALTER TABLE `cfg_mac` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`iordanov_ams`@`%`*/ /*!50003 TRIGGER `cfg_mac_AFTER_UPDATE` AFTER UPDATE ON `cfg_mac` FOR EACH ROW BEGIN
update ams_ac.cfg_mac_cabin c
join ams_ac.cfg_mac m on m.mac_id = c.mac_id
set c.mac_load_kg = m.load_kg;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 15:09:41
