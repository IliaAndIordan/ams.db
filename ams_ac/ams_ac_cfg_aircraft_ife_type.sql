CREATE DATABASE  IF NOT EXISTS `ams_ac` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_ac`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_ac
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfg_aircraft_ife_type`
--

DROP TABLE IF EXISTS `cfg_aircraft_ife_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfg_aircraft_ife_type` (
  `ife_id` int NOT NULL DEFAULT '0',
  `order` int DEFAULT NULL,
  `iname` varchar(256) CHARACTER SET cp1251 DEFAULT NULL,
  `description` varchar(1000) CHARACTER SET cp1251 DEFAULT NULL,
  `stars` int DEFAULT NULL,
  `price_instalation` double DEFAULT NULL,
  `price_per_pax` double DEFAULT NULL,
  `points` int DEFAULT NULL,
  PRIMARY KEY (`ife_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfg_aircraft_ife_type`
--

LOCK TABLES `cfg_aircraft_ife_type` WRITE;
/*!40000 ALTER TABLE `cfg_aircraft_ife_type` DISABLE KEYS */;
INSERT INTO `cfg_aircraft_ife_type` VALUES (1,1,'Base','Public Inflight Music',1,40,1,1),(2,3,'Base + Wi-Fi','Public Inflight Music + In-flight internet service.',3,250,12,15),(3,3,'Base TV |','Good quality TVs hanging in the middle of the aisle. 1 TV per 100 seats:',1,65,2,3),(4,4,'Base TV ||','High quality TVs in the middle of the aisle. 2 TV per 100 seats:',1,75,3,6),(5,5,'QualityTV |||','Excellent quality TVs hanging f in the middle of the aisle. 2 TV per 100 seats. One Vireo and One real-time flight information channels.',1,85,4,8),(6,6,'Quality TV ||| Personal Audio','TVs hanging in the middle of the aisle. 2 TV per 100 seats. Best quality of the TVs.<br/>A personal on demand music player for each seat. 15 on demand music chanels',2,266,5,10),(7,7,'Quality TV ||| Personal Audio and Video','Full Cabin Movie Projectors (2 TV screens per 100 seats) Audio on Demand and Video on Demand systems.<br/>A personal on demand music player for each seat. 15 music and 15 vireo chanels',3,465,11,15),(8,8,'Moving-map','Full Cabin Movie Projectors (2 TV screens per 100 seats) Audio on Demand and Video on Demand systems.<br/>A personal on demand music player for each seat. 15 music and 15 vireo chanels. Real-time flight information video channel.',3,822,18,30),(9,9,'Game system','Full Cabin Movie Projectors (2 TV screens per 100 seats) Audio on Demand and Video on Demand systems.<br/>A personal on demand music player for each seat. 15 music and 15 vireo chanels. Real-time flight information video channel.Personal Video Game',4,1069,22,40),(10,10,'Satillite Phone','Game system + Satellite telephones integrated into system. These are either found at strategic locations in the aircraft or integrated into the passenger remote control used for the individual in-flight entertainment. Passengers can use their credit card to make phone calls anywhere on the ground.',4,1200,30,50),(11,11,'Wi-Fi','Satillite Phone + In-flight internet service. It is provided either through a satellite network or an air-to-ground network. Allows passengers to connect to live Internet from the individual IFE units or their laptops via the in-flight Wi-Fi access',5,1600,38,60),(12,12,'In flight mobile phone coverage.','Wi-Fi + inflight mobile connectivity. The GSM network connects to the ground infrastructure via an Inmarsat SwiftBroadband satellite which provides consistent global coverage.',5,1800,50,80);
/*!40000 ALTER TABLE `cfg_aircraft_ife_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 15:09:25
