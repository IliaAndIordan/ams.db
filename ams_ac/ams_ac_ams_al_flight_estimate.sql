CREATE DATABASE  IF NOT EXISTS `ams_ac` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_ac`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_ac
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ams_al_flight_estimate`
--

DROP TABLE IF EXISTS `ams_al_flight_estimate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ams_al_flight_estimate` (
  `fle_id` int NOT NULL AUTO_INCREMENT,
  `flp_id` int DEFAULT NULL,
  `ch_id` int DEFAULT NULL,
  `route_id` int DEFAULT NULL,
  `fl_type_id` int DEFAULT NULL,
  `fl_num` int DEFAULT NULL,
  `fl_name` varchar(128) DEFAULT NULL,
  `fl_description` varchar(1024) DEFAULT NULL,
  `al_id` int DEFAULT NULL,
  `ac_id` int DEFAULT NULL,
  `d_ap_id` int DEFAULT NULL,
  `a_ap_id` int DEFAULT NULL,
  `distance_km` int DEFAULT NULL,
  `duration_min` int DEFAULT NULL,
  `oil_l` double DEFAULT NULL,
  `oli_price` double DEFAULT NULL,
  `landin_fee` double DEFAULT NULL,
  `ground_crew_fee` double DEFAULT NULL,
  `boarding_fee` double DEFAULT NULL,
  `fl_price` double DEFAULT NULL,
  `bank_id_from` int DEFAULT NULL,
  `pax` int DEFAULT '0',
  `payload_kg` int DEFAULT NULL,
  `mtow` double DEFAULT NULL,
  `dtime` timestamp NULL DEFAULT NULL,
  `atime` timestamp NULL DEFAULT NULL,
  `expences` double DEFAULT NULL,
  `income` double DEFAULT NULL,
  `revenue` double DEFAULT NULL,
  `adate` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`fle_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ams_al_flight_estimate`
--

LOCK TABLES `ams_al_flight_estimate` WRITE;
/*!40000 ALTER TABLE `ams_al_flight_estimate` DISABLE KEYS */;
/*!40000 ALTER TABLE `ams_al_flight_estimate` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 14:36:36
