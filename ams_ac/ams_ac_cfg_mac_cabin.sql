CREATE DATABASE  IF NOT EXISTS `ams_ac` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_ac`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_ac
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfg_mac_cabin`
--

DROP TABLE IF EXISTS `cfg_mac_cabin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfg_mac_cabin` (
  `cabin_id` int NOT NULL AUTO_INCREMENT,
  `mac_id` int DEFAULT NULL,
  `cabin_name` varchar(256) DEFAULT NULL,
  `seat_e_count` int DEFAULT NULL,
  `seat_e_type_id` int DEFAULT NULL,
  `seat_b_count` int DEFAULT NULL,
  `seat_b_type_id` int DEFAULT NULL,
  `seat_f_count` int DEFAULT NULL,
  `seat_f_type_id` int DEFAULT NULL,
  `seat_crew_count` int DEFAULT '0',
  `confort` double DEFAULT NULL,
  `price` double DEFAULT NULL,
  `max_ife_id` int DEFAULT '0',
  `default` int DEFAULT '0',
  `mac_load_kg` double DEFAULT NULL,
  `cargo_kg` double GENERATED ALWAYS AS ((ifnull(`mac_load_kg`,0) - ((((ifnull(`seat_e_count`,0) + ifnull(`seat_b_count`,0)) + ifnull(`seat_f_count`,0)) + ifnull(`seat_crew_count`,0)) * 77))) STORED,
  PRIMARY KEY (`cabin_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfg_mac_cabin`
--

LOCK TABLES `cfg_mac_cabin` WRITE;
/*!40000 ALTER TABLE `cfg_mac_cabin` DISABLE KEYS */;
INSERT INTO `cfg_mac_cabin` (`cabin_id`, `mac_id`, `cabin_name`, `seat_e_count`, `seat_e_type_id`, `seat_b_count`, `seat_b_type_id`, `seat_f_count`, `seat_f_type_id`, `seat_crew_count`, `confort`, `price`, `max_ife_id`, `default`, `mac_load_kg`) VALUES (1,1,'Default',3,3,0,3,0,3,1,3.3,25750,2,1,412),(2,3,'Club Six',5,2,0,3,0,3,1,2.5,33750,2,1,652),(3,1,'Executive',0,NULL,3,6,0,NULL,1,4,31750,2,0,412),(4,1,'Eurobusiness',0,NULL,3,7,0,NULL,1,4.6,49750,2,0,412),(5,2,'Economy WiFi',3,4,0,NULL,0,NULL,1,4,27250,2,1,426),(6,4,'Economy',30,1,0,NULL,0,NULL,2,2,126500,2,0,2774),(7,4,'Economy 29',28,2,0,NULL,0,NULL,1,2.7,120000,2,1,2774),(8,4,'Business',25,3,3,6,0,NULL,2,3.8,154000,2,0,2774),(9,7,'Commuter 8',8,3,0,NULL,0,NULL,1,3.3,49000,2,0,969),(10,7,'Club 7',0,NULL,7,7,0,NULL,1,4.6,100750,2,0,969),(11,7,'Commuter 9',9,2,0,NULL,0,NULL,1,2.6,48750,2,1,969),(12,8,'Business 14',0,NULL,14,8,0,NULL,1,5.6,304508,8,0,1694),(13,8,'Economy 14',14,4,0,NULL,0,NULL,1,3.8,79500,2,0,1694),(14,8,'Economy 15',15,4,0,NULL,0,NULL,1,3.5,81775,5,1,1694),(15,8,'Business 13',0,NULL,13,8,0,NULL,1,4.4,276458,6,0,1694),(16,9,'Commuter 9 Seat',9,1,0,NULL,0,NULL,1,1.3,42360,1,1,1493),(17,9,'6 Seat Club WiFi',0,NULL,6,6,0,NULL,1,3.9,52500,2,0,1493),(19,10,'Default 50',44,3,6,7,0,NULL,3,4.8,332100,8,0,5535),(20,11,'48 Seat Single Class',48,3,0,NULL,0,NULL,2,3.8,233000,2,1,8315),(21,12,'52 Seat Single Class BWifi',52,2,0,NULL,0,NULL,2,2.5,215420,4,1,4670),(22,12,'Combo 36 Seats + 1640 kg additional Cargo ',36,4,0,NULL,0,NULL,1,3.5,180060,4,0,4670),(23,13,'94 Seat Single Class',94,1,0,NULL,0,NULL,2,1.8,318990,4,0,9650),(24,13,'83 Seats Saravia',83,2,0,NULL,0,NULL,4,2.9,354555,4,1,9650),(25,13,'74 Seat Polet',74,3,0,NULL,0,NULL,4,4.2,372684,6,0,9650),(26,13,'77 Seat Two Clases',69,4,8,7,0,NULL,4,6.2,526794,8,0,9650),(28,14,'19 Seat E Plus Base',19,3,0,NULL,0,NULL,1,1.9,91760,1,0,1919),(29,14,'19 Seat E Plus Wi-Fi',19,3,0,NULL,0,NULL,1,3.5,95750,2,0,1919),(30,14,'20 Seats Slimline',20,2,0,NULL,0,NULL,1,1.6,85800,1,1,1919),(31,14,'10 Seat Single Row Wifi',10,4,0,NULL,0,NULL,1,4.1,62500,2,0,1919),(32,14,'10 Seat Club WiFi',0,NULL,10,8,0,NULL,1,5.3,217500,2,0,1919),(33,9,'6 Sear Club E',6,4,0,NULL,0,NULL,1,3.9,43500,2,0,1493),(34,15,'Single class 90 seats',90,1,0,NULL,0,NULL,3,2,320650,4,0,7441),(35,15,'Single Class 82 seats @ 30 inch pitch',82,2,0,NULL,0,NULL,3,2.7,336970,4,1,7441),(36,15,'Single Class 78 seats',78,3,0,NULL,0,NULL,2,3.1,347630,4,0,7441),(37,15,'Double class 78',68,3,10,7,0,NULL,4,5.2,513116,8,0,7441),(38,15,'Single Class 74 seats',74,4,0,NULL,0,NULL,3,5.7,436828,8,0,7441),(39,10,'Single Class 51',51,1,0,NULL,0,NULL,3,2,200335,4,0,5535),(40,10,'Single Class 50',50,2,0,NULL,0,NULL,2,2.5,208250,4,1,5535),(41,16,'Single Class 99',99,1,0,NULL,0,NULL,4,2.1,362415,4,0,8650),(42,16,'Sibgle Class 92',92,2,0,NULL,0,NULL,4,2.9,386820,4,0,8650),(43,16,'Double Class 10-79',79,3,10,7,0,NULL,4,4.4,516674,6,1,8650),(44,16,'Double Class 12-74',74,3,12,7,0,NULL,4,4.7,536990,7,0,8650),(45,17,'Two Class 80 Seats 12B 68E',68,3,12,7,0,NULL,4,5.2,538760,8,1,8078),(46,17,'Single class 82',82,4,0,NULL,0,NULL,3,5.7,479404,8,0,8078),(47,17,'Single Class 88',88,2,0,NULL,0,NULL,4,2.9,372480,4,0,8078),(48,17,'Single Class 90',90,1,0,NULL,0,NULL,4,2.1,334650,4,0,8078),(49,18,'Two Class 96 8 B and 88 E',88,3,8,8,0,NULL,5,6.1,685624,9,0,10210),(50,18,'Single Class 108 E two cabins',108,3,0,NULL,0,NULL,4,4.9,577776,8,0,10210),(51,18,'Single Class 110 E',110,2,0,NULL,0,NULL,4,3.8,532420,8,1,10210),(52,3,'Club Five',4,3,0,NULL,0,NULL,1,1.8,31160,1,0,652),(53,19,'Standard B7',0,NULL,7,8,0,NULL,1,7.6,166200,11,0,1243),(54,19,'Standard E8',8,5,0,NULL,0,NULL,1,7,67800,11,1,1243),(55,19,'Extended Refreshment Center B6',0,NULL,6,8,0,NULL,1,7.4,144600,11,0,1243);
/*!40000 ALTER TABLE `cfg_mac_cabin` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`iordanov_ams`@`%`*/ /*!50003 TRIGGER `cfg_mac_cabin_AFTER_UPDATE` AFTER UPDATE ON `cfg_mac_cabin` FOR EACH ROW BEGIN
update ams_ac.ams_aircraft a
join ams_ac.cfg_mac_cabin c on c.cabin_id = a.cabin_id
set a.cargo_kg = c.cargo_kg;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 15:09:41
