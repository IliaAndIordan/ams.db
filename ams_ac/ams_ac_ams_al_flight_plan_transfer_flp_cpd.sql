CREATE DATABASE  IF NOT EXISTS `ams_ac` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_ac`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_ac
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ams_al_flight_plan_transfer_flp_cpd`
--

DROP TABLE IF EXISTS `ams_al_flight_plan_transfer_flp_cpd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ams_al_flight_plan_transfer_flp_cpd` (
  `transfer_id` int NOT NULL DEFAULT '0',
  `pax_class_id` int DEFAULT NULL,
  `cpd_id` int NOT NULL DEFAULT '0',
  `d_ap_id` int DEFAULT NULL,
  `a_ap_id` int DEFAULT NULL,
  `dest_id` int DEFAULT NULL,
  `distance_km` double DEFAULT NULL,
  `flp_id_1` int DEFAULT NULL,
  `flp_id_2` int DEFAULT NULL,
  `flp_id_3` int DEFAULT NULL,
  `flp_id_4` int DEFAULT NULL,
  `fl_name_1` varchar(256) DEFAULT NULL,
  `fl_name_2` varchar(256) DEFAULT NULL,
  `fl_name_3` varchar(256) DEFAULT NULL,
  `fl_name_4` varchar(256) DEFAULT NULL,
  `cpd_payload_kg` double DEFAULT NULL,
  `cpd_price` double DEFAULT NULL,
  `cpd_note` varchar(256) DEFAULT NULL,
  `payload_diff_1` double DEFAULT NULL,
  `payload_diff_2` double DEFAULT NULL,
  `payload_diff_3` double DEFAULT NULL,
  `payload_diff_4` double DEFAULT NULL,
  `flpns_price_sum` double NOT NULL DEFAULT '0',
  `flpns_price` double DEFAULT NULL,
  `free_payload_1` double NOT NULL DEFAULT '0',
  `free_payload_2` double NOT NULL DEFAULT '0',
  `free_payload_3` double NOT NULL DEFAULT '0',
  `free_payload_4` double NOT NULL DEFAULT '0',
  `id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`),
  KEY `IDX_DEST_ID` (`dest_id`) /*!80000 INVISIBLE */,
  KEY `IDXCPPD_ID` (`cpd_id`),
  KEY `IDX_CPD_CLASS` (`cpd_id`,`pax_class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ams_al_flight_plan_transfer_flp_cpd`
--

LOCK TABLES `ams_al_flight_plan_transfer_flp_cpd` WRITE;
/*!40000 ALTER TABLE `ams_al_flight_plan_transfer_flp_cpd` DISABLE KEYS */;
/*!40000 ALTER TABLE `ams_al_flight_plan_transfer_flp_cpd` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 14:36:37
