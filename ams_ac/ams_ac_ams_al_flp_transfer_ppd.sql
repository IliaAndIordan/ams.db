CREATE DATABASE  IF NOT EXISTS `ams_ac` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_ac`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_ac
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ams_al_flp_transfer_ppd`
--

DROP TABLE IF EXISTS `ams_al_flp_transfer_ppd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ams_al_flp_transfer_ppd` (
  `ppdId` int DEFAULT NULL,
  `flpId` int DEFAULT NULL,
  `flpId2` int DEFAULT NULL,
  `flpId3` int DEFAULT NULL,
  `flpId4` int DEFAULT NULL,
  `depApId` int DEFAULT NULL,
  `arrApId` int DEFAULT NULL,
  `distanceKm` double DEFAULT NULL,
  `flName` varchar(128) DEFAULT NULL,
  `flName2` varchar(128) DEFAULT NULL,
  `flName3` varchar(128) DEFAULT NULL,
  `flName4` varchar(128) DEFAULT NULL,
  `remainPaxE` decimal(33,0) DEFAULT NULL,
  `remainPaxE2` decimal(33,0) DEFAULT NULL,
  `remainPaxE3` decimal(33,0) DEFAULT NULL,
  `remainPaxE4` decimal(33,0) DEFAULT NULL,
  `diffPrice` double DEFAULT NULL,
  `sumPrice` double DEFAULT NULL,
  `pPrice` double DEFAULT NULL,
  `paxClassId` int DEFAULT NULL,
  `pax` int DEFAULT NULL,
  `payloadKg` double DEFAULT NULL,
  `note` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ams_al_flp_transfer_ppd`
--

LOCK TABLES `ams_al_flp_transfer_ppd` WRITE;
/*!40000 ALTER TABLE `ams_al_flp_transfer_ppd` DISABLE KEYS */;
/*!40000 ALTER TABLE `ams_al_flp_transfer_ppd` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 15:09:25
