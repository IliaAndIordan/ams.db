CREATE DATABASE  IF NOT EXISTS `ams_ac` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_ac`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_ac
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `v_ams_al_flight_plan_transfers_ppd`
--

DROP TABLE IF EXISTS `v_ams_al_flight_plan_transfers_ppd`;
/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_transfers_ppd`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_al_flight_plan_transfers_ppd` AS SELECT 
 1 AS `transfers_id`,
 1 AS `ppdId`,
 1 AS `cpdId`,
 1 AS `flpId`,
 1 AS `flpId2`,
 1 AS `flpId3`,
 1 AS `flpId4`,
 1 AS `depApId`,
 1 AS `arrApId`,
 1 AS `distanceKm`,
 1 AS `flName`,
 1 AS `flName2`,
 1 AS `flName3`,
 1 AS `flName4`,
 1 AS `remainPax`,
 1 AS `remainPax2`,
 1 AS `remainPax3`,
 1 AS `remainPax4`,
 1 AS `remainPayloadKg`,
 1 AS `remainPayloadKg2`,
 1 AS `remainPayloadKg3`,
 1 AS `remainPayloadKg4`,
 1 AS `diffPrice`,
 1 AS `sumPrice`,
 1 AS `pPrice`,
 1 AS `paxClassId`,
 1 AS `pax`,
 1 AS `payloadKg`,
 1 AS `note`,
 1 AS `id`,
 1 AS `remain_pax_e`,
 1 AS `remain_pax_e2`,
 1 AS `remain_pax_e3`,
 1 AS `remain_pax_e4`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_flp_transfer_quadruple_cpd`
--

DROP TABLE IF EXISTS `v_flp_transfer_quadruple_cpd`;
/*!50001 DROP VIEW IF EXISTS `v_flp_transfer_quadruple_cpd`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_flp_transfer_quadruple_cpd` AS SELECT 
 1 AS `ppdId`,
 1 AS `flpId`,
 1 AS `flpId2`,
 1 AS `flpId3`,
 1 AS `flpId4`,
 1 AS `depApId`,
 1 AS `arrApId`,
 1 AS `distanceKm`,
 1 AS `flName`,
 1 AS `flName2`,
 1 AS `flName3`,
 1 AS `flName4`,
 1 AS `remainPayloadKg`,
 1 AS `remainPayloadKgE2`,
 1 AS `remainPayloadKgE3`,
 1 AS `remainPayloadKgE4`,
 1 AS `diffPrice`,
 1 AS `sumPrice`,
 1 AS `pPrice`,
 1 AS `paxClassId`,
 1 AS `pax`,
 1 AS `payloadKg`,
 1 AS `note`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_al_flight_queue_ac_flq_sum_flight_h`
--

DROP TABLE IF EXISTS `v_ams_al_flight_queue_ac_flq_sum_flight_h`;
/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_queue_ac_flq_sum_flight_h`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_al_flight_queue_ac_flq_sum_flight_h` AS SELECT 
 1 AS `fl_id`,
 1 AS `fl_type_id`,
 1 AS `fl_name`,
 1 AS `fl_description`,
 1 AS `fle_id`,
 1 AS `flp_id`,
 1 AS `ch_id`,
 1 AS `route_id`,
 1 AS `al_id`,
 1 AS `ac_id`,
 1 AS `d_ap_id`,
 1 AS `a_ap_id`,
 1 AS `distance_km`,
 1 AS `oil_l`,
 1 AS `flight_h`,
 1 AS `pax`,
 1 AS `payload_kg`,
 1 AS `dtime`,
 1 AS `atime`,
 1 AS `processed`,
 1 AS `flq_sum_flight_h`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_flp_transfer_tripple_cpd`
--

DROP TABLE IF EXISTS `v_flp_transfer_tripple_cpd`;
/*!50001 DROP VIEW IF EXISTS `v_flp_transfer_tripple_cpd`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_flp_transfer_tripple_cpd` AS SELECT 
 1 AS `ppdId`,
 1 AS `flpId`,
 1 AS `flpId2`,
 1 AS `flpId3`,
 1 AS `depApId`,
 1 AS `arrApId`,
 1 AS `distanceKm`,
 1 AS `flName`,
 1 AS `flName2`,
 1 AS `flName3`,
 1 AS `remainPayloadKg`,
 1 AS `remainPayloadKgE2`,
 1 AS `remainPayloadKgE3`,
 1 AS `diffPrice`,
 1 AS `sumPrice`,
 1 AS `pPrice`,
 1 AS `paxClassId`,
 1 AS `pax`,
 1 AS `payloadKg`,
 1 AS `note`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_ppd`
--

DROP TABLE IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd`;
/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_al_flight_plan_payload_add_n_transfers_ppd` AS SELECT 
 1 AS `transfers_id`,
 1 AS `ppdId`,
 1 AS `flpId`,
 1 AS `flpId2`,
 1 AS `flpId3`,
 1 AS `flpId4`,
 1 AS `depApId`,
 1 AS `arrApId`,
 1 AS `distanceKm`,
 1 AS `flName`,
 1 AS `flName2`,
 1 AS `flName3`,
 1 AS `flName4`,
 1 AS `remain_pax`,
 1 AS `remain_pax2`,
 1 AS `remain_pax3`,
 1 AS `remain_pax4`,
 1 AS `price_diff`,
 1 AS `price_sum`,
 1 AS `ppd_price`,
 1 AS `ppd_pax_class_id`,
 1 AS `ppd_pax`,
 1 AS `ppd_payload_kg`,
 1 AS `ppd_note`,
 1 AS `ppd_adate`,
 1 AS `id`,
 1 AS `adate`,
 1 AS `dtime`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_al_flp_group`
--

DROP TABLE IF EXISTS `v_al_flp_group`;
/*!50001 DROP VIEW IF EXISTS `v_al_flp_group`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_al_flp_group` AS SELECT 
 1 AS `grp_id`,
 1 AS `grp_name`,
 1 AS `al_id`,
 1 AS `ac_id_prototype`,
 1 AS `ap_id`,
 1 AS `max_range_km`,
 1 AS `min_rw_len_m`,
 1 AS `operation_time_min`,
 1 AS `cruise_speed_kmph`,
 1 AS `cruise_altitude_m`,
 1 AS `ac_id`,
 1 AS `is_active`,
 1 AS `start_date`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_al_flight_queue_max_dtime`
--

DROP TABLE IF EXISTS `v_ams_al_flight_queue_max_dtime`;
/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_queue_max_dtime`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_al_flight_queue_max_dtime` AS SELECT 
 1 AS `ac_id`,
 1 AS `flq_sum_flight_h`,
 1 AS `max_dtime`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_flight_plan_payload_sum`
--

DROP TABLE IF EXISTS `v_ams_flight_plan_payload_sum`;
/*!50001 DROP VIEW IF EXISTS `v_ams_flight_plan_payload_sum`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_flight_plan_payload_sum` AS SELECT 
 1 AS `flp_id`,
 1 AS `flpns_id`,
 1 AS `flp_status_id`,
 1 AS `ac_id`,
 1 AS `pax_class_id`,
 1 AS `flp_payload`,
 1 AS `cabin_payload`,
 1 AS `flp_free_payload`,
 1 AS `flpns_price`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_cpd_gui`
--

DROP TABLE IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_cpd_gui`;
/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_cpd_gui`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_al_flight_plan_payload_add_n_transfers_cpd_gui` AS SELECT 
 1 AS `transfers_id`,
 1 AS `cpdId`,
 1 AS `flpId`,
 1 AS `flpId2`,
 1 AS `flpId3`,
 1 AS `flpId4`,
 1 AS `depApId`,
 1 AS `arrApId`,
 1 AS `distanceKm`,
 1 AS `flName`,
 1 AS `flName2`,
 1 AS `flName3`,
 1 AS `flName4`,
 1 AS `remain_payload_kg`,
 1 AS `remain_payload_kg2`,
 1 AS `remain_payload_kg3`,
 1 AS `remain_payload_kg4`,
 1 AS `price_diff`,
 1 AS `price_sum`,
 1 AS `cpd_price`,
 1 AS `cpd_pax_class_id`,
 1 AS `cpd_payload_kg`,
 1 AS `cpd_note`,
 1 AS `cpd_adate`,
 1 AS `dtime`,
 1 AS `id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_cpd_gui_v01`
--

DROP TABLE IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_cpd_gui_v01`;
/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_cpd_gui_v01`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_al_flight_plan_payload_add_n_transfers_cpd_gui_v01` AS SELECT 
 1 AS `transfer_id`,
 1 AS `pax_class_id`,
 1 AS `cpd_id`,
 1 AS `d_ap_id`,
 1 AS `a_ap_id`,
 1 AS `dest_id`,
 1 AS `distance_km`,
 1 AS `flp_id_1`,
 1 AS `flp_id_2`,
 1 AS `flp_id_3`,
 1 AS `flp_id_4`,
 1 AS `fl_name_1`,
 1 AS `fl_name_2`,
 1 AS `fl_name_3`,
 1 AS `fl_name_4`,
 1 AS `cpd_payload_kg`,
 1 AS `cpd_price`,
 1 AS `cpd_note`,
 1 AS `adate`,
 1 AS `payload_diff_1`,
 1 AS `payload_diff_2`,
 1 AS `payload_diff_3`,
 1 AS `payload_diff_4`,
 1 AS `flpns_price_sum`,
 1 AS `flpns_price`,
 1 AS `free_payload_1`,
 1 AS `free_payload_2`,
 1 AS `free_payload_3`,
 1 AS `free_payload_4`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_al_flight_plan_transfer`
--

DROP TABLE IF EXISTS `v_ams_al_flight_plan_transfer`;
/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_transfer`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_al_flight_plan_transfer` AS SELECT 
 1 AS `transfer_id`,
 1 AS `pax_class_id`,
 1 AS `d_ap_id`,
 1 AS `a_ap_id`,
 1 AS `dest_id`,
 1 AS `distance_km`,
 1 AS `flp_id_1`,
 1 AS `flp_id_2`,
 1 AS `flp_id_3`,
 1 AS `flp_id_4`,
 1 AS `fl_name_1`,
 1 AS `fl_name_2`,
 1 AS `fl_name_3`,
 1 AS `fl_name_4`,
 1 AS `payload`,
 1 AS `ppd_price`,
 1 AS `flpns_price`,
 1 AS `ppd_note`,
 1 AS `free_payload_1`,
 1 AS `free_payload_2`,
 1 AS `free_payload_3`,
 1 AS `free_payload_4`,
 1 AS `flpns_price_sum`,
 1 AS `ppd_id`,
 1 AS `cpd_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_aircraft_cabin`
--

DROP TABLE IF EXISTS `v_ams_aircraft_cabin`;
/*!50001 DROP VIEW IF EXISTS `v_ams_aircraft_cabin`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_aircraft_cabin` AS SELECT 
 1 AS `ac_id`,
 1 AS `cabin_id`,
 1 AS `mac_id`,
 1 AS `pax_class_id`,
 1 AS `cabin_payload`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_ppd_add`
--

DROP TABLE IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd_add`;
/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd_add`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_al_flight_plan_payload_add_n_transfers_ppd_add` AS SELECT 
 1 AS `transfers_id`,
 1 AS `ppdId`,
 1 AS `flpId`,
 1 AS `flpId2`,
 1 AS `flpId3`,
 1 AS `flpId4`,
 1 AS `depApId`,
 1 AS `arrApId`,
 1 AS `distanceKm`,
 1 AS `flName`,
 1 AS `flName2`,
 1 AS `flName3`,
 1 AS `flName4`,
 1 AS `remain_pax`,
 1 AS `remain_pax2`,
 1 AS `remain_pax3`,
 1 AS `remain_pax4`,
 1 AS `price_diff`,
 1 AS `price_sum`,
 1 AS `ppd_price`,
 1 AS `ppd_pax_class_id`,
 1 AS `ppd_pax`,
 1 AS `ppd_payload_kg`,
 1 AS `ppd_note`,
 1 AS `ppd_adate`,
 1 AS `id`,
 1 AS `adate`,
 1 AS `dtime`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_flp_transfer_double`
--

DROP TABLE IF EXISTS `v_flp_transfer_double`;
/*!50001 DROP VIEW IF EXISTS `v_flp_transfer_double`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_flp_transfer_double` AS SELECT 
 1 AS `ppdId`,
 1 AS `flpId`,
 1 AS `flpId2`,
 1 AS `depApId`,
 1 AS `arrApId`,
 1 AS `distanceKm`,
 1 AS `flName`,
 1 AS `flName2`,
 1 AS `remainPaxE`,
 1 AS `remainPaxE2`,
 1 AS `diffPrice`,
 1 AS `sumPrice`,
 1 AS `pPrice`,
 1 AS `paxClassId`,
 1 AS `pax`,
 1 AS `payloadKg`,
 1 AS `note`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v03`
--

DROP TABLE IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v03`;
/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v03`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v03` AS SELECT 
 1 AS `transfer_id`,
 1 AS `pax_class_id`,
 1 AS `ppd_id`,
 1 AS `dep_ap_id`,
 1 AS `arr_ap_id`,
 1 AS `dest_id`,
 1 AS `distance_km`,
 1 AS `flp_id_1`,
 1 AS `flp_id_2`,
 1 AS `flp_id_3`,
 1 AS `flp_id_4`,
 1 AS `fl_name_1`,
 1 AS `fl_name_2`,
 1 AS `fl_name_3`,
 1 AS `fl_name_4`,
 1 AS `ppd_pax`,
 1 AS `ppd_payload_kg`,
 1 AS `ppd_price`,
 1 AS `ppd_note`,
 1 AS `adate`,
 1 AS `pax_diff_1`,
 1 AS `pax_diff_2`,
 1 AS `pax_diff_3`,
 1 AS `pax_diff_4`,
 1 AS `flpns_price_sum`,
 1 AS `flpns_price`,
 1 AS `free_payload_1`,
 1 AS `free_payload_2`,
 1 AS `free_payload_3`,
 1 AS `free_payload_4`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_cpd_add`
--

DROP TABLE IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_cpd_add`;
/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_cpd_add`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_al_flight_plan_payload_add_n_transfers_cpd_add` AS SELECT 
 1 AS `transfers_id`,
 1 AS `cpdId`,
 1 AS `flpId`,
 1 AS `flpId2`,
 1 AS `flpId3`,
 1 AS `flpId4`,
 1 AS `depApId`,
 1 AS `arrApId`,
 1 AS `distanceKm`,
 1 AS `flName`,
 1 AS `flName2`,
 1 AS `flName3`,
 1 AS `flName4`,
 1 AS `remain_payload_kg`,
 1 AS `remain_payload_kg2`,
 1 AS `remain_payload_kg3`,
 1 AS `remain_payload_kg4`,
 1 AS `price_diff`,
 1 AS `price_sum`,
 1 AS `cpd_price`,
 1 AS `cpd_pax_class_id`,
 1 AS `cpd_payload_kg`,
 1 AS `cpd_note`,
 1 AS `cpd_adate`,
 1 AS `dtime`,
 1 AS `id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_al_flight_plan_transfers_pcpd_price`
--

DROP TABLE IF EXISTS `v_ams_al_flight_plan_transfers_pcpd_price`;
/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_transfers_pcpd_price`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_al_flight_plan_transfers_pcpd_price` AS SELECT 
 1 AS `transfer_id`,
 1 AS `pax_class_id`,
 1 AS `d_ap_id`,
 1 AS `a_ap_id`,
 1 AS `dest_id`,
 1 AS `distance_km`,
 1 AS `flp_id_1`,
 1 AS `flp_id_2`,
 1 AS `flp_id_3`,
 1 AS `flp_id_4`,
 1 AS `fl_name_1`,
 1 AS `fl_name_2`,
 1 AS `fl_name_3`,
 1 AS `fl_name_4`,
 1 AS `payload`,
 1 AS `ppd_price`,
 1 AS `flpns_price`,
 1 AS `ppd_note`,
 1 AS `free_payload_1`,
 1 AS `free_payload_2`,
 1 AS `free_payload_3`,
 1 AS `free_payload_4`,
 1 AS `flpns_price_sum`,
 1 AS `ppd_id`,
 1 AS `cpd_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui`
--

DROP TABLE IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui`;
/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui` AS SELECT 
 1 AS `transfers_id`,
 1 AS `ppdId`,
 1 AS `flpId`,
 1 AS `flpId2`,
 1 AS `flpId3`,
 1 AS `flpId4`,
 1 AS `depApId`,
 1 AS `arrApId`,
 1 AS `distanceKm`,
 1 AS `flName`,
 1 AS `flName2`,
 1 AS `flName3`,
 1 AS `flName4`,
 1 AS `remain_pax`,
 1 AS `remain_pax2`,
 1 AS `remain_pax3`,
 1 AS `remain_pax4`,
 1 AS `price_diff`,
 1 AS `price_sum`,
 1 AS `ppd_price`,
 1 AS `ppd_pax_class_id`,
 1 AS `ppd_pax`,
 1 AS `ppd_payload_kg`,
 1 AS `ppd_note`,
 1 AS `ppd_adate`,
 1 AS `id`,
 1 AS `adate`,
 1 AS `dtime`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_flight_plan_payload_sum_class`
--

DROP TABLE IF EXISTS `v_ams_flight_plan_payload_sum_class`;
/*!50001 DROP VIEW IF EXISTS `v_ams_flight_plan_payload_sum_class`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_flight_plan_payload_sum_class` AS SELECT 
 1 AS `flp_id`,
 1 AS `flpns_id`,
 1 AS `fl_name`,
 1 AS `flp_status_id`,
 1 AS `ac_id`,
 1 AS `d_ap_id`,
 1 AS `a_ap_id`,
 1 AS `dtime`,
 1 AS `atime`,
 1 AS `pax_class_id`,
 1 AS `flpns_price`,
 1 AS `payloads_count`,
 1 AS `cabin_payload`,
 1 AS `flp_payload_sum`,
 1 AS `flp_price_sum`,
 1 AS `cabin_id`,
 1 AS `mac_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v01`
--

DROP TABLE IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v01`;
/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v01`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v01` AS SELECT 
 1 AS `transfer_id`,
 1 AS `pax_class_id`,
 1 AS `ppd_id`,
 1 AS `dep_ap_id`,
 1 AS `arr_ap_id`,
 1 AS `dest_id`,
 1 AS `distance_km`,
 1 AS `flp_id_1`,
 1 AS `flp_id_2`,
 1 AS `flp_id_3`,
 1 AS `flp_id_4`,
 1 AS `fl_name_1`,
 1 AS `fl_name_2`,
 1 AS `fl_name_3`,
 1 AS `fl_name_4`,
 1 AS `ppd_pax`,
 1 AS `ppd_payload_kg`,
 1 AS `ppd_price`,
 1 AS `ppd_note`,
 1 AS `adate`,
 1 AS `pax_diff_1`,
 1 AS `pax_diff_2`,
 1 AS `pax_diff_3`,
 1 AS `pax_diff_4`,
 1 AS `flpns_price_sum`,
 1 AS `flpns_price`,
 1 AS `free_payload_1`,
 1 AS `free_payload_2`,
 1 AS `free_payload_3`,
 1 AS `free_payload_4`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_cpd`
--

DROP TABLE IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_cpd`;
/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_cpd`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_al_flight_plan_payload_add_n_transfers_cpd` AS SELECT 
 1 AS `transfers_id`,
 1 AS `cpdId`,
 1 AS `flpId`,
 1 AS `flpId2`,
 1 AS `flpId3`,
 1 AS `flpId4`,
 1 AS `depApId`,
 1 AS `arrApId`,
 1 AS `distanceKm`,
 1 AS `flName`,
 1 AS `flName2`,
 1 AS `flName3`,
 1 AS `flName4`,
 1 AS `remain_payload_kg`,
 1 AS `remain_payload_kg2`,
 1 AS `remain_payload_kg3`,
 1 AS `remain_payload_kg4`,
 1 AS `price_diff`,
 1 AS `price_sum`,
 1 AS `cpd_price`,
 1 AS `cpd_pax_class_id`,
 1 AS `cpd_payload_kg`,
 1 AS `cpd_note`,
 1 AS `cpd_adate`,
 1 AS `dtime`,
 1 AS `id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_flp_transfer_double_cpd`
--

DROP TABLE IF EXISTS `v_flp_transfer_double_cpd`;
/*!50001 DROP VIEW IF EXISTS `v_flp_transfer_double_cpd`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_flp_transfer_double_cpd` AS SELECT 
 1 AS `ppdId`,
 1 AS `flpId`,
 1 AS `flpId2`,
 1 AS `depApId`,
 1 AS `arrApId`,
 1 AS `distanceKm`,
 1 AS `flName`,
 1 AS `flName2`,
 1 AS `remainPayloadKg`,
 1 AS `remainPayloadKgE2`,
 1 AS `diffPrice`,
 1 AS `sumPrice`,
 1 AS `pPrice`,
 1 AS `paxClassId`,
 1 AS `pax`,
 1 AS `payloadKg`,
 1 AS `note`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_al_flight_plan_transfers_cpd`
--

DROP TABLE IF EXISTS `v_ams_al_flight_plan_transfers_cpd`;
/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_transfers_cpd`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_al_flight_plan_transfers_cpd` AS SELECT 
 1 AS `transfers_id`,
 1 AS `ppdId`,
 1 AS `cpdId`,
 1 AS `flpId`,
 1 AS `flpId2`,
 1 AS `flpId3`,
 1 AS `flpId4`,
 1 AS `depApId`,
 1 AS `arrApId`,
 1 AS `distanceKm`,
 1 AS `flName`,
 1 AS `flName2`,
 1 AS `flName3`,
 1 AS `flName4`,
 1 AS `remainPax`,
 1 AS `remainPax2`,
 1 AS `remainPax3`,
 1 AS `remainPax4`,
 1 AS `remainPayloadKg`,
 1 AS `remainPayloadKg2`,
 1 AS `remainPayloadKg3`,
 1 AS `remainPayloadKg4`,
 1 AS `diffPrice`,
 1 AS `sumPrice`,
 1 AS `pPrice`,
 1 AS `paxClassId`,
 1 AS `pax`,
 1 AS `payloadKg`,
 1 AS `note`,
 1 AS `id`,
 1 AS `remain_payload_kg`,
 1 AS `remain_payload_kg2`,
 1 AS `remain_payload_kg3`,
 1 AS `remain_payload_kg4`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_al_flight_plan_payload_sum`
--

DROP TABLE IF EXISTS `v_ams_al_flight_plan_payload_sum`;
/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_sum`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_al_flight_plan_payload_sum` AS SELECT 
 1 AS `flp_id`,
 1 AS `flpns_id`,
 1 AS `fl_name`,
 1 AS `flp_status_id`,
 1 AS `ac_id`,
 1 AS `d_ap_id`,
 1 AS `a_ap_id`,
 1 AS `dtime`,
 1 AS `atime`,
 1 AS `payloads_count`,
 1 AS `sum_pax_e`,
 1 AS `sum_pax_b`,
 1 AS `sum_pax_f`,
 1 AS `sum_payload_kg`,
 1 AS `sum_price`,
 1 AS `remain_pax_e`,
 1 AS `remain_pax_b`,
 1 AS `remain_pax_f`,
 1 AS `remain_payload_kg`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_ac_mnt_next`
--

DROP TABLE IF EXISTS `v_ams_ac_mnt_next`;
/*!50001 DROP VIEW IF EXISTS `v_ams_ac_mnt_next`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_ac_mnt_next` AS SELECT 
 1 AS `ac_id`,
 1 AS `forSheduleFlights`,
 1 AS `is_home_ap`,
 1 AS `acat_id`,
 1 AS `acat_name`,
 1 AS `plane_stand_time_h`,
 1 AS `next_flq_fl_id`,
 1 AS `next_flq_dtime`,
 1 AS `flight_h_to_maintenance`,
 1 AS `is_in_mnt`,
 1 AS `current_acmp_id`,
 1 AS `next_amt_id`,
 1 AS `next_amt_name`,
 1 AS `mnt_start_time`,
 1 AS `mtn_time_h`,
 1 AS `mnt_end_time`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v02`
--

DROP TABLE IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v02`;
/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v02`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v02` AS SELECT 
 1 AS `transfers_id`,
 1 AS `ppdId`,
 1 AS `flpId`,
 1 AS `flpId2`,
 1 AS `flpId3`,
 1 AS `flpId4`,
 1 AS `depApId`,
 1 AS `arrApId`,
 1 AS `distanceKm`,
 1 AS `flName`,
 1 AS `flName2`,
 1 AS `flName3`,
 1 AS `flName4`,
 1 AS `remain_pax`,
 1 AS `remain_pax2`,
 1 AS `remain_pax3`,
 1 AS `remain_pax4`,
 1 AS `price_diff`,
 1 AS `price_sum`,
 1 AS `ppd_price`,
 1 AS `ppd_pax_class_id`,
 1 AS `ppd_pax`,
 1 AS `ppd_payload_kg`,
 1 AS `ppd_note`,
 1 AS `ppd_adate`,
 1 AS `id`,
 1 AS `adate`,
 1 AS `dtime`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_flp_transfer_tripple`
--

DROP TABLE IF EXISTS `v_flp_transfer_tripple`;
/*!50001 DROP VIEW IF EXISTS `v_flp_transfer_tripple`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_flp_transfer_tripple` AS SELECT 
 1 AS `ppdId`,
 1 AS `flpId`,
 1 AS `flpId2`,
 1 AS `flpId3`,
 1 AS `depApId`,
 1 AS `arrApId`,
 1 AS `distanceKm`,
 1 AS `flName`,
 1 AS `flName2`,
 1 AS `flName3`,
 1 AS `remainPaxE`,
 1 AS `remainPaxE2`,
 1 AS `remainPaxE3`,
 1 AS `diffPrice`,
 1 AS `sumPrice`,
 1 AS `pPrice`,
 1 AS `paxClassId`,
 1 AS `pax`,
 1 AS `payloadKg`,
 1 AS `note`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_ams_ac`
--

DROP TABLE IF EXISTS `v_ams_ac`;
/*!50001 DROP VIEW IF EXISTS `v_ams_ac`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_ams_ac` AS SELECT 
 1 AS `acId`,
 1 AS `registration`,
 1 AS `macId`,
 1 AS `ownerAlId`,
 1 AS `homeApId`,
 1 AS `adate`,
 1 AS `udate`,
 1 AS `cabinId`,
 1 AS `pilots`,
 1 AS `crew`,
 1 AS `paxF`,
 1 AS `paxB`,
 1 AS `paxE`,
 1 AS `cargoKg`,
 1 AS `costPerFh`,
 1 AS `acStatusId`,
 1 AS `forShaduleFlighs`,
 1 AS `currApId`,
 1 AS `flightHours`,
 1 AS `distanceKm`,
 1 AS `state`,
 1 AS `price`,
 1 AS `opTimeMin`,
 1 AS `aCheckFh`,
 1 AS `bCheckFh`,
 1 AS `cCheckFh`,
 1 AS `dCheckFh`,
 1 AS `stars`,
 1 AS `points`,
 1 AS `flight_in_queue`,
 1 AS `flight_h_in_queue`,
 1 AS `ac_last_flq_atime`,
 1 AS `homeAp`,
 1 AS `currAp`,
 1 AS `homeApCode`,
 1 AS `currApCode`,
 1 AS `cabinName`,
 1 AS `confort`,
 1 AS `acTypeId`,
 1 AS `fuelConsumptionLp100km`,
 1 AS `maxRangeKm`,
 1 AS `minRunwayM`,
 1 AS `cruiseSpeedKmph`,
 1 AS `acActionTypeId`,
 1 AS `logFlId`,
 1 AS `logAdate`,
 1 AS `logDurationMin`,
 1 AS `logFlightH`,
 1 AS `logDistanceKm`,
 1 AS `logUdate`,
 1 AS `flInQueue`,
 1 AS `forSheduleFlights`,
 1 AS `grpId`,
 1 AS `flight_h_to_maintenance`,
 1 AS `next_amt_id`,
 1 AS `last_flq_fl_id`,
 1 AS `last_flq_a_ap_id`,
 1 AS `last_flq_atime`,
 1 AS `flq_sum_flight_h`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_flp_transfer_quadruple`
--

DROP TABLE IF EXISTS `v_flp_transfer_quadruple`;
/*!50001 DROP VIEW IF EXISTS `v_flp_transfer_quadruple`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_flp_transfer_quadruple` AS SELECT 
 1 AS `ppdId`,
 1 AS `flpId`,
 1 AS `flpId2`,
 1 AS `flpId3`,
 1 AS `flpId4`,
 1 AS `depApId`,
 1 AS `arrApId`,
 1 AS `distanceKm`,
 1 AS `flName`,
 1 AS `flName2`,
 1 AS `flName3`,
 1 AS `flName4`,
 1 AS `remainPaxE`,
 1 AS `remainPaxE2`,
 1 AS `remainPaxE3`,
 1 AS `remainPaxE4`,
 1 AS `diffPrice`,
 1 AS `sumPrice`,
 1 AS `pPrice`,
 1 AS `paxClassId`,
 1 AS `pax`,
 1 AS `payloadKg`,
 1 AS `note`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `v_ams_al_flight_plan_transfers_ppd`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_transfers_ppd`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_al_flight_plan_transfers_ppd` AS select `tr`.`transfers_id` AS `transfers_id`,`tr`.`ppdId` AS `ppdId`,`tr`.`cpdId` AS `cpdId`,`tr`.`flpId` AS `flpId`,`tr`.`flpId2` AS `flpId2`,`tr`.`flpId3` AS `flpId3`,`tr`.`flpId4` AS `flpId4`,`tr`.`depApId` AS `depApId`,`tr`.`arrApId` AS `arrApId`,`tr`.`distanceKm` AS `distanceKm`,`tr`.`flName` AS `flName`,`tr`.`flName2` AS `flName2`,`tr`.`flName3` AS `flName3`,`tr`.`flName4` AS `flName4`,`tr`.`remainPax` AS `remainPax`,`tr`.`remainPax2` AS `remainPax2`,`tr`.`remainPax3` AS `remainPax3`,`tr`.`remainPax4` AS `remainPax4`,`tr`.`remainPayloadKg` AS `remainPayloadKg`,`tr`.`remainPayloadKg2` AS `remainPayloadKg2`,`tr`.`remainPayloadKg3` AS `remainPayloadKg3`,`tr`.`remainPayloadKg4` AS `remainPayloadKg4`,`tr`.`diffPrice` AS `diffPrice`,`tr`.`sumPrice` AS `sumPrice`,`tr`.`pPrice` AS `pPrice`,`tr`.`paxClassId` AS `paxClassId`,`tr`.`pax` AS `pax`,`tr`.`payloadKg` AS `payloadKg`,`tr`.`note` AS `note`,`tr`.`id` AS `id`,`flp`.`remain_pax_e` AS `remain_pax_e`,`flp2`.`remain_pax_e` AS `remain_pax_e2`,`flp3`.`remain_pax_e` AS `remain_pax_e3`,`flp4`.`remain_pax_e` AS `remain_pax_e4` from (((((((((`ams_al_flight_plan_transfers` `tr` join `ams_al_flight_plan` `flp` on((`flp`.`flp_id` = `tr`.`flpId`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpns_id` = `flp`.`flpns_id`))) left join `ams_al_flight_plan` `flp2` on((`flp2`.`flp_id` = `tr`.`flpId2`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns2` on((`flpns2`.`flpns_id` = `flp2`.`flpns_id`))) left join `ams_al_flight_plan` `flp3` on((`flp3`.`flp_id` = `tr`.`flpId3`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns3` on((`flpns3`.`flpns_id` = `flp3`.`flpns_id`))) left join `ams_al_flight_plan` `flp4` on((`flp4`.`flp_id` = `tr`.`flpId4`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns4` on((`flpns4`.`flpns_id` = `flp4`.`flpns_id`))) join (select `ams_al`.`cfg_callc_param`.`param_value` AS `transfers_id` from `ams_al`.`cfg_callc_param` where (`ams_al`.`cfg_callc_param`.`param_id` = 43)) `p` on((1 = 1))) where ((`tr`.`transfers_id` = `p`.`transfers_id`) and (ifnull(`flp`.`flp_status_id`,1) = 1) and (ifnull(`flp2`.`flp_status_id`,1) = 1) and (ifnull(`flp3`.`flp_status_id`,1) = 1) and (ifnull(`flp4`.`flp_status_id`,1) = 1) and (((`tr`.`paxClassId` = 1) and (`tr`.`pax` <= ifnull(`flp`.`remain_pax_e`,`tr`.`pax`)) and (`tr`.`pax` <= ifnull(`flp2`.`remain_pax_e`,`tr`.`pax`)) and (`tr`.`pax` <= ifnull(`flp3`.`remain_pax_e`,`tr`.`pax`)) and (`tr`.`pax` <= ifnull(`flp4`.`remain_pax_e`,`tr`.`pax`))) or ((`tr`.`paxClassId` = 2) and (`tr`.`pax` <= ifnull(`flp`.`remain_pax_b`,`tr`.`pax`)) and (`tr`.`pax` <= ifnull(`flp2`.`remain_pax_b`,`tr`.`pax`)) and (`tr`.`pax` <= ifnull(`flp3`.`remain_pax_b`,`tr`.`pax`)) and (`tr`.`pax` <= ifnull(`flp4`.`remain_pax_b`,`tr`.`pax`))) or ((`tr`.`paxClassId` = 3) and (`tr`.`pax` <= ifnull(`flp`.`remain_pax_f`,`tr`.`pax`)) and (`tr`.`pax` <= ifnull(`flp2`.`remain_pax_f`,`tr`.`pax`)) and (`tr`.`pax` <= ifnull(`flp3`.`remain_pax_f`,`tr`.`pax`)) and (`tr`.`pax` <= ifnull(`flp4`.`remain_pax_f`,`tr`.`pax`))))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_flp_transfer_quadruple_cpd`
--

/*!50001 DROP VIEW IF EXISTS `v_flp_transfer_quadruple_cpd`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb3 */;
/*!50001 SET character_set_results     = utf8mb3 */;
/*!50001 SET collation_connection      = utf8mb3_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_flp_transfer_quadruple_cpd` AS select `p`.`cpd_id` AS `ppdId`,`flp`.`flp_id` AS `flpId`,`flp2`.`flp_id` AS `flpId2`,`flp3`.`flp_id` AS `flpId3`,`flp4`.`flp_id` AS `flpId4`,`pd`.`dep_ap_id` AS `depApId`,`pd`.`arr_ap_id` AS `arrApId`,`pd`.`distance_km` AS `distanceKm`,`flp`.`fl_name` AS `flName`,`flp2`.`fl_name` AS `flName2`,`flp3`.`fl_name` AS `flName3`,`flp4`.`fl_name` AS `flName4`,`flp`.`remain_payload_kg` AS `remainPayloadKg`,`flp2`.`remain_payload_kg` AS `remainPayloadKgE2`,`flp3`.`remain_payload_kg` AS `remainPayloadKgE3`,`flp4`.`remain_payload_kg` AS `remainPayloadKgE4`,round((`p`.`max_ticket_price` - ((((`p`.`payload_kg` * `flpns`.`price_c_p_kg`) + (`p`.`payload_kg` * `flpns2`.`price_c_p_kg`)) + (`p`.`payload_kg` * `flpns3`.`price_c_p_kg`)) + (`p`.`payload_kg` * `flpns4`.`price_c_p_kg`))),2) AS `diffPrice`,round(((((`p`.`payload_kg` * `flpns`.`price_c_p_kg`) + (`p`.`payload_kg` * `flpns2`.`price_c_p_kg`)) + (`p`.`payload_kg` * `flpns3`.`price_c_p_kg`)) + (`p`.`payload_kg` * `flpns4`.`price_c_p_kg`)),2) AS `sumPrice`,round(`p`.`max_ticket_price`,2) AS `pPrice`,`p`.`pax_class_id` AS `paxClassId`,0 AS `pax`,`p`.`payload_kg` AS `payloadKg`,`p`.`note` AS `note` from ((((((((((`ams_al_flight_plan_payload_pct_full` `flp` join `ams_al`.`ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpns_id` = `flp`.`flpns_id`))) join `v_ams_ac` `ac` on((`ac`.`acId` = `flp`.`ac_id`))) join `ams_al_flight_plan_payload_pct_full` `flp2` on((`flp2`.`d_ap_id` = `flp`.`a_ap_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns2` on((`flpns2`.`flpns_id` = `flp2`.`flpns_id`))) join `ams_al_flight_plan_payload_pct_full` `flp3` on((`flp3`.`d_ap_id` = `flp2`.`a_ap_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns3` on((`flpns3`.`flpns_id` = `flp3`.`flpns_id`))) join `ams_al_flight_plan_payload_pct_full` `flp4` on((`flp4`.`d_ap_id` = `flp3`.`a_ap_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns4` on((`flpns4`.`flpns_id` = `flp4`.`flpns_id`))) join `ams_wad`.`ams_airport_cpd` `p` on((`p`.`dep_ap_id` = `flp`.`d_ap_id`))) join `ams_wad`.`cfg_airport_destination` `pd` on((`pd`.`dest_id` = `p`.`dest_id`))) where ((`flp`.`flp_status_id` = 1) and (`flp`.`remain_payload_kg` > 0) and (`flp2`.`remain_payload_kg` > 0) and (`flp3`.`remain_payload_kg` > 0) and (`flp4`.`remain_payload_kg` > 0) and (`flp2`.`flp_id` <> `flp`.`flp_id`) and (`flp2`.`a_ap_id` <> `flp`.`d_ap_id`) and (`flp3`.`flp_id` <> `flp2`.`flp_id`) and (`flp3`.`flp_id` <> `flp`.`flp_id`) and (`flp3`.`a_ap_id` <> `flp2`.`d_ap_id`) and (`flp3`.`a_ap_id` <> `flp`.`d_ap_id`) and (`flp4`.`flp_id` <> `flp3`.`flp_id`) and (`flp4`.`flp_id` <> `flp2`.`flp_id`) and (`flp4`.`flp_id` <> `flp`.`flp_id`) and (`flp4`.`a_ap_id` <> `flp3`.`d_ap_id`) and (`flp4`.`a_ap_id` <> `flp2`.`d_ap_id`) and (`flp4`.`a_ap_id` <> `flp`.`d_ap_id`) and (`pd`.`arr_ap_id` = `flp3`.`a_ap_id`) and (timestampdiff(MINUTE,`flp`.`atime`,`flp2`.`dtime`) > `ac`.`opTimeMin`) and (timestampdiff(MINUTE,`flp2`.`atime`,`flp3`.`dtime`) > `ac`.`opTimeMin`) and (timestampdiff(MINUTE,`flp3`.`atime`,`flp4`.`dtime`) > `ac`.`opTimeMin`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_al_flight_queue_ac_flq_sum_flight_h`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_queue_ac_flq_sum_flight_h`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_al_flight_queue_ac_flq_sum_flight_h` AS select `q`.`fl_id` AS `fl_id`,`q`.`fl_type_id` AS `fl_type_id`,`q`.`fl_name` AS `fl_name`,`q`.`fl_description` AS `fl_description`,`q`.`fle_id` AS `fle_id`,`q`.`flp_id` AS `flp_id`,`q`.`ch_id` AS `ch_id`,`q`.`route_id` AS `route_id`,`q`.`al_id` AS `al_id`,`q`.`ac_id` AS `ac_id`,`q`.`d_ap_id` AS `d_ap_id`,`q`.`a_ap_id` AS `a_ap_id`,`q`.`distance_km` AS `distance_km`,`q`.`oil_l` AS `oil_l`,`q`.`flight_h` AS `flight_h`,`q`.`pax` AS `pax`,`q`.`payload_kg` AS `payload_kg`,`q`.`dtime` AS `dtime`,`q`.`atime` AS `atime`,`q`.`processed` AS `processed`,`qm`.`flq_sum_flight_h` AS `flq_sum_flight_h` from (`ams_al_flight_queue` `q` join `v_ams_al_flight_queue_max_dtime` `qm` on((`qm`.`ac_id` = `q`.`ac_id`))) where (`q`.`dtime` = `qm`.`max_dtime`) group by `q`.`ac_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_flp_transfer_tripple_cpd`
--

/*!50001 DROP VIEW IF EXISTS `v_flp_transfer_tripple_cpd`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb3 */;
/*!50001 SET character_set_results     = utf8mb3 */;
/*!50001 SET collation_connection      = utf8mb3_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_flp_transfer_tripple_cpd` AS select `p`.`cpd_id` AS `ppdId`,`flp`.`flp_id` AS `flpId`,`flp2`.`flp_id` AS `flpId2`,`flp3`.`flp_id` AS `flpId3`,`pd`.`dep_ap_id` AS `depApId`,`pd`.`arr_ap_id` AS `arrApId`,`pd`.`distance_km` AS `distanceKm`,`flp`.`fl_name` AS `flName`,`flp2`.`fl_name` AS `flName2`,`flp3`.`fl_name` AS `flName3`,`flp`.`remain_payload_kg` AS `remainPayloadKg`,`flp2`.`remain_payload_kg` AS `remainPayloadKgE2`,`flp3`.`remain_payload_kg` AS `remainPayloadKgE3`,round((`p`.`max_ticket_price` - (((`p`.`payload_kg` * `flpns`.`price_c_p_kg`) + (`p`.`payload_kg` * `flpns2`.`price_c_p_kg`)) + (`p`.`payload_kg` * `flpns3`.`price_c_p_kg`))),2) AS `diffPrice`,round((((`p`.`payload_kg` * `flpns`.`price_c_p_kg`) + (`p`.`payload_kg` * `flpns2`.`price_c_p_kg`)) + (`p`.`payload_kg` * `flpns3`.`price_c_p_kg`)),2) AS `sumPrice`,round(`p`.`max_ticket_price`,2) AS `pPrice`,`p`.`pax_class_id` AS `paxClassId`,0 AS `pax`,`p`.`payload_kg` AS `payloadKg`,`p`.`note` AS `note` from ((((((((`ams_al_flight_plan_payload_pct_full` `flp` join `ams_al`.`ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpns_id` = `flp`.`flpns_id`))) join `v_ams_ac` `ac` on((`ac`.`acId` = `flp`.`ac_id`))) join `ams_al_flight_plan_payload_pct_full` `flp2` on((`flp2`.`d_ap_id` = `flp`.`a_ap_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns2` on((`flpns2`.`flpns_id` = `flp2`.`flpns_id`))) join `ams_al_flight_plan_payload_pct_full` `flp3` on((`flp3`.`d_ap_id` = `flp2`.`a_ap_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns3` on((`flpns3`.`flpns_id` = `flp3`.`flpns_id`))) join `ams_wad`.`ams_airport_cpd` `p` on((`p`.`dep_ap_id` = `flp`.`d_ap_id`))) join `ams_wad`.`cfg_airport_destination` `pd` on((`pd`.`dest_id` = `p`.`dest_id`))) where ((`flp`.`flp_status_id` = 1) and (`flp`.`remain_payload_kg` > 0) and (`flp2`.`remain_payload_kg` > 0) and (`flp3`.`remain_payload_kg` > 0) and (`flp2`.`flp_id` <> `flp`.`flp_id`) and (`flp2`.`a_ap_id` <> `flp`.`d_ap_id`) and (`flp3`.`flp_id` <> `flp2`.`flp_id`) and (`flp3`.`flp_id` <> `flp`.`flp_id`) and (`flp3`.`a_ap_id` <> `flp2`.`d_ap_id`) and (`flp3`.`a_ap_id` <> `flp`.`d_ap_id`) and (`pd`.`arr_ap_id` = `flp3`.`a_ap_id`) and (timestampdiff(MINUTE,`flp`.`atime`,`flp2`.`dtime`) > `ac`.`opTimeMin`) and (timestampdiff(MINUTE,`flp2`.`atime`,`flp3`.`dtime`) > `ac`.`opTimeMin`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_ppd`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_al_flight_plan_payload_add_n_transfers_ppd` AS select `tr`.`transfers_id` AS `transfers_id`,`tr`.`ppd_id` AS `ppdId`,`tr`.`flp_id` AS `flpId`,`tr`.`flp_id2` AS `flpId2`,`tr`.`flp_id3` AS `flpId3`,`tr`.`flp_id4` AS `flpId4`,`tr`.`dep_ap_id` AS `depApId`,`tr`.`arr_ap_id` AS `arrApId`,`tr`.`distance_km` AS `distanceKm`,`tr`.`fl_name` AS `flName`,`tr`.`fl_name2` AS `flName2`,`tr`.`fl_name3` AS `flName3`,`tr`.`fl_name4` AS `flName4`,(case when (`tr`.`ppd_pax_class_id` = 1) then `flp`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp`.`remain_pax_f` else NULL end) AS `remain_pax`,(case when (`tr`.`ppd_pax_class_id` = 1) then `flp2`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp2`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp2`.`remain_pax_f` else NULL end) AS `remain_pax2`,(case when (`tr`.`ppd_pax_class_id` = 1) then `flp3`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp3`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp3`.`remain_pax_f` else NULL end) AS `remain_pax3`,(case when (`tr`.`ppd_pax_class_id` = 1) then `flp4`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp4`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp4`.`remain_pax_f` else NULL end) AS `remain_pax4`,(case when (`tr`.`ppd_pax_class_id` = 1) then round((`tr`.`ppd_price` - ((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_e`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_e`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_e`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_e`,0)))),2) when (`tr`.`ppd_pax_class_id` = 2) then round((`tr`.`ppd_price` - ((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_b`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_b`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_b`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_b`,0)))),2) when (`tr`.`ppd_pax_class_id` = 3) then round((`tr`.`ppd_price` - ((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_f`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_f`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_f`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_f`,0)))),2) else NULL end) AS `price_diff`,(case when (`tr`.`ppd_pax_class_id` = 1) then round(((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_e`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_e`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_e`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_e`,0))),2) when (`tr`.`ppd_pax_class_id` = 2) then round(((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_b`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_b`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_b`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_b`,0))),2) when (`tr`.`ppd_pax_class_id` = 3) then round(((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_f`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_f`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_f`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_f`,0))),2) else NULL end) AS `price_sum`,`tr`.`ppd_price` AS `ppd_price`,`tr`.`ppd_pax_class_id` AS `ppd_pax_class_id`,`tr`.`ppd_pax` AS `ppd_pax`,`tr`.`ppd_payload_kg` AS `ppd_payload_kg`,`tr`.`ppd_note` AS `ppd_note`,`tr`.`ppd_adate` AS `ppd_adate`,`tr`.`id` AS `id`,`tr`.`adate` AS `adate`,least(ifnull(`flp`.`dtime`,now()),ifnull(`flp2`.`dtime`,now()),ifnull(`flp3`.`dtime`,now()),ifnull(`flp4`.`dtime`,now())) AS `dtime` from (((((((((`ams_al_flight_plan_transfers_ppd` `tr` join `ams_al_flight_plan_tmp` `flp` on((`flp`.`flp_id` = `tr`.`flp_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpns_id` = `flp`.`flpns_id`))) left join `ams_al_flight_plan_tmp` `flp2` on((`flp2`.`flp_id` = `tr`.`flp_id2`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns2` on((`flpns2`.`flpns_id` = `flp2`.`flpns_id`))) left join `ams_al_flight_plan_tmp` `flp3` on((`flp3`.`flp_id` = `tr`.`flp_id3`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns3` on((`flpns3`.`flpns_id` = `flp3`.`flpns_id`))) left join `ams_al_flight_plan_tmp` `flp4` on((`flp4`.`flp_id` = `tr`.`flp_id4`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns4` on((`flpns4`.`flpns_id` = `flp4`.`flpns_id`))) join (select `ams_al`.`cfg_callc_param`.`param_value` AS `transfers_id` from `ams_al`.`cfg_callc_param` where (`ams_al`.`cfg_callc_param`.`param_id` = 43)) `p` on((1 = 1))) where ((`tr`.`transfers_id` = `p`.`transfers_id`) and (ifnull(`flp`.`flp_status_id`,1) = 1) and (ifnull(`flp2`.`flp_status_id`,1) = 1) and (ifnull(`flp3`.`flp_status_id`,1) = 1) and (ifnull(`flp4`.`flp_status_id`,1) = 1) and (ifnull((case when (`tr`.`ppd_pax_class_id` = 1) then `flp`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp`.`remain_pax_f` else NULL end),`tr`.`ppd_pax`) >= `tr`.`ppd_pax`) and (ifnull((case when (`tr`.`ppd_pax_class_id` = 1) then `flp2`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp2`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp2`.`remain_pax_f` else NULL end),`tr`.`ppd_pax`) >= `tr`.`ppd_pax`) and (ifnull((case when (`tr`.`ppd_pax_class_id` = 1) then `flp3`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp3`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp3`.`remain_pax_f` else NULL end),`tr`.`ppd_pax`) >= `tr`.`ppd_pax`) and (ifnull((case when (`tr`.`ppd_pax_class_id` = 1) then `flp4`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp4`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp4`.`remain_pax_f` else NULL end),`tr`.`ppd_pax`) >= `tr`.`ppd_pax`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_al_flp_group`
--

/*!50001 DROP VIEW IF EXISTS `v_al_flp_group`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb3 */;
/*!50001 SET character_set_results     = utf8mb3 */;
/*!50001 SET collation_connection      = utf8mb3_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_al_flp_group` AS select `g`.`grp_id` AS `grp_id`,`g`.`grp_name` AS `grp_name`,`g`.`al_id` AS `al_id`,`g`.`ac_id_prototype` AS `ac_id_prototype`,`g`.`ap_id` AS `ap_id`,`m`.`max_range_km` AS `max_range_km`,`m`.`take_off_m` AS `min_rw_len_m`,`m`.`operation_time_min` AS `operation_time_min`,`m`.`cruise_speed_kmph` AS `cruise_speed_kmph`,`m`.`cruise_altitude_m` AS `cruise_altitude_m`,`g`.`ac_id` AS `ac_id`,`g`.`is_active` AS `is_active`,`g`.`start_date` AS `start_date` from ((`ams_al`.`ams_al_flp_group` `g` join `ams_aircraft` `a` on((`a`.`ac_id` = `g`.`ac_id_prototype`))) join `cfg_mac` `m` on((`m`.`mac_id` = `a`.`mac_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_al_flight_queue_max_dtime`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_queue_max_dtime`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_al_flight_queue_max_dtime` AS select `ams_al_flight_queue`.`ac_id` AS `ac_id`,round(sum(`ams_al_flight_queue`.`flight_h`),2) AS `flq_sum_flight_h`,max(`ams_al_flight_queue`.`dtime`) AS `max_dtime` from `ams_al_flight_queue` group by `ams_al_flight_queue`.`ac_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_flight_plan_payload_sum`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_flight_plan_payload_sum`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_flight_plan_payload_sum` AS select `flp`.`flp_id` AS `flp_id`,`flp`.`flpns_id` AS `flpns_id`,`flp`.`flp_status_id` AS `flp_status_id`,`acc`.`ac_id` AS `ac_id`,`acc`.`pax_class_id` AS `pax_class_id`,sum(if((`acc`.`pax_class_id` = 4),ifnull(`pl`.`payload_kg`,0),ifnull(`pl`.`pax`,0))) AS `flp_payload`,`acc`.`cabin_payload` AS `cabin_payload`,round((`acc`.`cabin_payload` - sum(if((`acc`.`pax_class_id` = 4),ifnull(`pl`.`payload_kg`,0),ifnull(`pl`.`pax`,0)))),2) AS `flp_free_payload`,(case when (`acc`.`pax_class_id` = 1) then ifnull(`flpns`.`price_e`,0) when (`acc`.`pax_class_id` = 2) then ifnull(`flpns`.`price_b`,0) when (`acc`.`pax_class_id` = 3) then ifnull(`flpns`.`price_f`,0) when (`acc`.`pax_class_id` = 4) then round(ifnull(`flpns`.`price_c_p_kg`,0),4) else 0 end) AS `flpns_price` from (((`ams_al_flight_plan` `flp` join `v_ams_aircraft_cabin` `acc` on((`acc`.`ac_id` = `flp`.`ac_id`))) left join `ams_al_flight_plan_payload` `pl` on(((`pl`.`flp_id` = `flp`.`flp_id`) and (`pl`.`pax_class_id` = `acc`.`pax_class_id`)))) join `ams_al`.`ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpns_id` = `flp`.`flpns_id`))) where (`flp`.`flp_status_id` < 2) group by `flp`.`flp_id`,`acc`.`pax_class_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_cpd_gui`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_cpd_gui`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_al_flight_plan_payload_add_n_transfers_cpd_gui` AS select `tr`.`transfers_id` AS `transfers_id`,`tr`.`cpd_id` AS `cpdId`,`tr`.`flp_id` AS `flpId`,`tr`.`flp_id2` AS `flpId2`,`tr`.`flp_id3` AS `flpId3`,`tr`.`flp_id4` AS `flpId4`,`tr`.`dep_ap_id` AS `depApId`,`tr`.`arr_ap_id` AS `arrApId`,`tr`.`distance_km` AS `distanceKm`,`tr`.`fl_name` AS `flName`,`tr`.`fl_name2` AS `flName2`,`tr`.`fl_name3` AS `flName3`,`tr`.`fl_name4` AS `flName4`,ifnull(`flp`.`remain_payload_kg`,0) AS `remain_payload_kg`,ifnull(`flp2`.`remain_payload_kg`,0) AS `remain_payload_kg2`,ifnull(`flp3`.`remain_payload_kg`,0) AS `remain_payload_kg3`,ifnull(`flp4`.`remain_payload_kg`,0) AS `remain_payload_kg4`,round((`tr`.`cpd_price` - (`tr`.`cpd_payload_kg` * (((ifnull(`flpns`.`price_c_p_kg`,0) + ifnull(`flpns2`.`price_c_p_kg`,0)) + ifnull(`flpns3`.`price_c_p_kg`,0)) + ifnull(`flpns4`.`price_c_p_kg`,0)))),2) AS `price_diff`,round(((((`tr`.`cpd_payload_kg` * ifnull(`flpns`.`price_c_p_kg`,0)) + (`tr`.`cpd_payload_kg` * ifnull(`flpns2`.`price_c_p_kg`,0))) + (`tr`.`cpd_payload_kg` * ifnull(`flpns3`.`price_c_p_kg`,0))) + (`tr`.`cpd_payload_kg` * ifnull(`flpns4`.`price_c_p_kg`,0))),2) AS `price_sum`,`tr`.`cpd_price` AS `cpd_price`,`tr`.`cpd_pax_class_id` AS `cpd_pax_class_id`,`tr`.`cpd_payload_kg` AS `cpd_payload_kg`,`tr`.`cpd_note` AS `cpd_note`,`tr`.`cpd_adate` AS `cpd_adate`,least(ifnull(`flp`.`dtime`,now()),ifnull(`flp2`.`dtime`,now()),ifnull(`flp3`.`dtime`,now()),ifnull(`flp4`.`dtime`,now())) AS `dtime`,`tr`.`id` AS `id` from ((((((((`ams_al_flight_plan_transfers_cpd` `tr` join `ams_al_flight_plan` `flp` on((`flp`.`flp_id` = `tr`.`flp_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpns_id` = `flp`.`flpns_id`))) left join `ams_al_flight_plan` `flp2` on((`flp2`.`flp_id` = `tr`.`flp_id2`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns2` on((`flpns2`.`flpns_id` = `flp2`.`flpns_id`))) left join `ams_al_flight_plan` `flp3` on((`flp3`.`flp_id` = `tr`.`flp_id3`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns3` on((`flpns3`.`flpns_id` = `flp3`.`flpns_id`))) left join `ams_al_flight_plan` `flp4` on((`flp4`.`flp_id` = `tr`.`flp_id4`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns4` on((`flpns4`.`flpns_id` = `flp4`.`flpns_id`))) where ((ifnull(`flp`.`flp_status_id`,1) = 1) and (ifnull(`flp2`.`flp_status_id`,1) = 1) and (ifnull(`flp3`.`flp_status_id`,1) = 1) and (ifnull(`flp4`.`flp_status_id`,1) = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_cpd_gui_v01`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_cpd_gui_v01`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_al_flight_plan_payload_add_n_transfers_cpd_gui_v01` AS select `tr`.`transfer_id` AS `transfer_id`,`p`.`pax_class_id` AS `pax_class_id`,`p`.`cpd_id` AS `cpd_id`,`tr`.`d_ap_id` AS `d_ap_id`,`tr`.`a_ap_id` AS `a_ap_id`,`tr`.`dest_id` AS `dest_id`,`tr`.`distance_km` AS `distance_km`,`tr`.`flp_id_1` AS `flp_id_1`,`tr`.`flp_id_2` AS `flp_id_2`,`tr`.`flp_id_3` AS `flp_id_3`,`tr`.`flp_id_4` AS `flp_id_4`,`tr`.`fl_name_1` AS `fl_name_1`,`tr`.`fl_name_2` AS `fl_name_2`,`tr`.`fl_name_3` AS `fl_name_3`,`tr`.`fl_name_4` AS `fl_name_4`,`p`.`payload_kg` AS `cpd_payload_kg`,round(`p`.`max_ticket_price`,2) AS `cpd_price`,`p`.`note` AS `cpd_note`,`p`.`adate` AS `adate`,(if((`tr`.`flp_id_1` is null),`p`.`payload_kg`,ifnull(`flp1`.`flp_free_payload`,-(1))) - `p`.`payload_kg`) AS `payload_diff_1`,(if((`tr`.`flp_id_2` is null),`p`.`payload_kg`,ifnull(`flp2`.`flp_free_payload`,-(1))) - `p`.`payload_kg`) AS `payload_diff_2`,(if((`tr`.`flp_id_3` is null),`p`.`payload_kg`,ifnull(`flp3`.`flp_free_payload`,-(1))) - `p`.`payload_kg`) AS `payload_diff_3`,(if((`tr`.`flp_id_4` is null),`p`.`payload_kg`,ifnull(`flp4`.`flp_free_payload`,-(1))) - `p`.`payload_kg`) AS `payload_diff_4`,(((ifnull(`flp1`.`flpns_price`,0) + ifnull(`flp2`.`flpns_price`,0)) + ifnull(`flp3`.`flpns_price`,0)) + ifnull(`flp4`.`flpns_price`,0)) AS `flpns_price_sum`,round((`p`.`payload_kg` * (((ifnull(`flp1`.`flpns_price`,0) + ifnull(`flp2`.`flpns_price`,0)) + ifnull(`flp3`.`flpns_price`,0)) + ifnull(`flp4`.`flpns_price`,0))),2) AS `flpns_price`,ifnull(`flp1`.`flp_free_payload`,0) AS `free_payload_1`,ifnull(`flp2`.`flp_free_payload`,0) AS `free_payload_2`,ifnull(`flp3`.`flp_free_payload`,0) AS `free_payload_3`,ifnull(`flp4`.`flp_free_payload`,0) AS `free_payload_4` from (((((`ams_al_flight_plan_transfer` `tr` join `ams_airport_cpd` `p` on((`p`.`dest_id` = `tr`.`dest_id`))) left join `v_ams_flight_plan_payload_sum` `flp1` on(((`flp1`.`flp_id` = `tr`.`flp_id_1`) and (`flp1`.`pax_class_id` = `p`.`pax_class_id`)))) left join `v_ams_flight_plan_payload_sum` `flp2` on(((`flp2`.`flp_id` = `tr`.`flp_id_2`) and (`flp2`.`pax_class_id` = `p`.`pax_class_id`)))) left join `v_ams_flight_plan_payload_sum` `flp3` on(((`flp3`.`flp_id` = `tr`.`flp_id_3`) and (`flp3`.`pax_class_id` = `p`.`pax_class_id`)))) left join `v_ams_flight_plan_payload_sum` `flp4` on(((`flp4`.`flp_id` = `tr`.`flp_id_4`) and (`flp4`.`pax_class_id` = `p`.`pax_class_id`)))) where (((if((`tr`.`flp_id_1` is null),`p`.`payload_kg`,ifnull(`flp1`.`flp_free_payload`,-(1))) - `p`.`payload_kg`) >= 0) and ((if((`tr`.`flp_id_2` is null),`p`.`payload_kg`,ifnull(`flp2`.`flp_free_payload`,-(1))) - `p`.`payload_kg`) >= 0) and ((if((`tr`.`flp_id_3` is null),`p`.`payload_kg`,ifnull(`flp3`.`flp_free_payload`,-(1))) - `p`.`payload_kg`) >= 0) and ((if((`tr`.`flp_id_4` is null),`p`.`payload_kg`,ifnull(`flp4`.`flp_free_payload`,-(1))) - `p`.`payload_kg`) >= 0)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_al_flight_plan_transfer`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_transfer`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_al_flight_plan_transfer` AS select `tr`.`transfer_id` AS `transfer_id`,`flp1`.`pax_class_id` AS `pax_class_id`,`tr`.`d_ap_id` AS `d_ap_id`,`tr`.`a_ap_id` AS `a_ap_id`,`tr`.`dest_id` AS `dest_id`,`tr`.`distance_km` AS `distance_km`,`tr`.`flp_id_1` AS `flp_id_1`,`tr`.`flp_id_2` AS `flp_id_2`,`tr`.`flp_id_3` AS `flp_id_3`,`tr`.`flp_id_4` AS `flp_id_4`,`tr`.`fl_name_1` AS `fl_name_1`,`tr`.`fl_name_2` AS `fl_name_2`,`tr`.`fl_name_3` AS `fl_name_3`,`tr`.`fl_name_4` AS `fl_name_4`,if((`flp1`.`pax_class_id` = 4),ifnull(`c`.`payload_kg`,0),ifnull(`p`.`pax`,0)) AS `payload`,round(if((`flp1`.`pax_class_id` = 4),ifnull(`c`.`max_ticket_price`,0),ifnull(`p`.`max_ticket_price`,0)),2) AS `ppd_price`,round((if((`flp1`.`pax_class_id` = 4),ifnull(`c`.`payload_kg`,0),ifnull(`p`.`pax`,0)) * (((ifnull(`flp1`.`flpns_price`,0) + ifnull(`flp2`.`flpns_price`,0)) + ifnull(`flp3`.`flpns_price`,0)) + ifnull(`flp4`.`flpns_price`,0))),2) AS `flpns_price`,if((`flp1`.`pax_class_id` = 4),ifnull(`c`.`note`,0),ifnull(`p`.`note`,0)) AS `ppd_note`,round((`flp1`.`cabin_payload` - `flp1`.`flp_payload_sum`),2) AS `free_payload_1`,round((`flp2`.`cabin_payload` - `flp2`.`flp_payload_sum`),2) AS `free_payload_2`,round((`flp3`.`cabin_payload` - `flp3`.`flp_payload_sum`),2) AS `free_payload_3`,round((`flp4`.`cabin_payload` - `flp4`.`flp_payload_sum`),2) AS `free_payload_4`,round((((ifnull(`flp1`.`flpns_price`,0) + ifnull(`flp2`.`flpns_price`,0)) + ifnull(`flp3`.`flpns_price`,0)) + ifnull(`flp4`.`flpns_price`,0)),2) AS `flpns_price_sum`,`p`.`ppd_id` AS `ppd_id`,`c`.`cpd_id` AS `cpd_id` from ((((((`ams_al_flight_plan_transfer` `tr` left join `v_ams_flight_plan_payload_sum_class` `flp1` on((`flp1`.`flp_id` = `tr`.`flp_id_1`))) left join `v_ams_flight_plan_payload_sum_class` `flp2` on(((`flp2`.`flp_id` = `tr`.`flp_id_2`) and (`flp2`.`pax_class_id` = `flp1`.`pax_class_id`)))) left join `v_ams_flight_plan_payload_sum_class` `flp3` on(((`flp3`.`flp_id` = `tr`.`flp_id_3`) and (`flp3`.`pax_class_id` = `flp1`.`pax_class_id`)))) left join `v_ams_flight_plan_payload_sum_class` `flp4` on(((`flp4`.`flp_id` = `tr`.`flp_id_4`) and (`flp4`.`pax_class_id` = `flp1`.`pax_class_id`)))) left join `ams_airport_ppd` `p` on(((`p`.`dest_id` = `tr`.`dest_id`) and (`p`.`pax_class_id` = `flp1`.`pax_class_id`)))) left join `ams_airport_cpd` `c` on(((`c`.`dest_id` = `tr`.`dest_id`) and (`c`.`pax_class_id` = `flp1`.`pax_class_id`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_aircraft_cabin`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_aircraft_cabin`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_aircraft_cabin` AS select `ac`.`ac_id` AS `ac_id`,`c`.`cabin_id` AS `cabin_id`,`c`.`mac_id` AS `mac_id`,`pc`.`pax_class_id` AS `pax_class_id`,(case when (`pc`.`pax_class_id` = 1) then `c`.`seat_e_count` when (`pc`.`pax_class_id` = 2) then `c`.`seat_b_count` when (`pc`.`pax_class_id` = 3) then `c`.`seat_f_count` when (`pc`.`pax_class_id` = 4) then `c`.`cargo_kg` else 0 end) AS `cabin_payload` from ((`ams_aircraft` `ac` join `cfg_mac_cabin` `c` on((`c`.`cabin_id` = `ac`.`cabin_id`))) join `ams_wad`.`cfg_pax_class` `pc` on((1 = 1))) having (`cabin_payload` > 0) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_ppd_add`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd_add`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_al_flight_plan_payload_add_n_transfers_ppd_add` AS select `tr`.`transfers_id` AS `transfers_id`,`tr`.`ppd_id` AS `ppdId`,`tr`.`flp_id` AS `flpId`,`tr`.`flp_id2` AS `flpId2`,`tr`.`flp_id3` AS `flpId3`,`tr`.`flp_id4` AS `flpId4`,`tr`.`dep_ap_id` AS `depApId`,`tr`.`arr_ap_id` AS `arrApId`,`tr`.`distance_km` AS `distanceKm`,`tr`.`fl_name` AS `flName`,`tr`.`fl_name2` AS `flName2`,`tr`.`fl_name3` AS `flName3`,`tr`.`fl_name4` AS `flName4`,(case when (`tr`.`ppd_pax_class_id` = 1) then `flp`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp`.`remain_pax_f` else NULL end) AS `remain_pax`,(case when (`tr`.`ppd_pax_class_id` = 1) then `flp2`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp2`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp2`.`remain_pax_f` else NULL end) AS `remain_pax2`,(case when (`tr`.`ppd_pax_class_id` = 1) then `flp3`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp3`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp3`.`remain_pax_f` else NULL end) AS `remain_pax3`,(case when (`tr`.`ppd_pax_class_id` = 1) then `flp4`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp4`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp4`.`remain_pax_f` else NULL end) AS `remain_pax4`,(case when (`tr`.`ppd_pax_class_id` = 1) then round((`tr`.`ppd_price` - ((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_e`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_e`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_e`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_e`,0)))),2) when (`tr`.`ppd_pax_class_id` = 2) then round((`tr`.`ppd_price` - ((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_b`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_b`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_b`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_b`,0)))),2) when (`tr`.`ppd_pax_class_id` = 3) then round((`tr`.`ppd_price` - ((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_f`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_f`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_f`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_f`,0)))),2) else NULL end) AS `price_diff`,(case when (`tr`.`ppd_pax_class_id` = 1) then round(((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_e`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_e`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_e`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_e`,0))),2) when (`tr`.`ppd_pax_class_id` = 2) then round(((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_b`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_b`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_b`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_b`,0))),2) when (`tr`.`ppd_pax_class_id` = 3) then round(((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_f`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_f`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_f`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_f`,0))),2) else NULL end) AS `price_sum`,`tr`.`ppd_price` AS `ppd_price`,`tr`.`ppd_pax_class_id` AS `ppd_pax_class_id`,`tr`.`ppd_pax` AS `ppd_pax`,`tr`.`ppd_payload_kg` AS `ppd_payload_kg`,`tr`.`ppd_note` AS `ppd_note`,`tr`.`ppd_adate` AS `ppd_adate`,`tr`.`id` AS `id`,`tr`.`adate` AS `adate`,least(ifnull(`flp`.`dtime`,now()),ifnull(`flp2`.`dtime`,now()),ifnull(`flp3`.`dtime`,now()),ifnull(`flp4`.`dtime`,now())) AS `dtime` from (((((((((`ams_al_flight_plan_transfers_ppd_add` `tr` join `ams_al_flight_plan_tmp` `flp` on((`flp`.`flp_id` = `tr`.`flp_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpns_id` = `flp`.`flpns_id`))) left join `ams_al_flight_plan_tmp` `flp2` on((`flp2`.`flp_id` = `tr`.`flp_id2`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns2` on((`flpns2`.`flpns_id` = `flp2`.`flpns_id`))) left join `ams_al_flight_plan_tmp` `flp3` on((`flp3`.`flp_id` = `tr`.`flp_id3`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns3` on((`flpns3`.`flpns_id` = `flp3`.`flpns_id`))) left join `ams_al_flight_plan_tmp` `flp4` on((`flp4`.`flp_id` = `tr`.`flp_id4`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns4` on((`flpns4`.`flpns_id` = `flp4`.`flpns_id`))) join (select `ams_al`.`cfg_callc_param`.`param_value` AS `transfers_id` from `ams_al`.`cfg_callc_param` where (`ams_al`.`cfg_callc_param`.`param_id` = 43)) `p` on((1 = 1))) where ((`tr`.`transfers_id` = `p`.`transfers_id`) and (ifnull(`flp`.`flp_status_id`,1) = 1) and (ifnull(`flp2`.`flp_status_id`,1) = 1) and (ifnull(`flp3`.`flp_status_id`,1) = 1) and (ifnull(`flp4`.`flp_status_id`,1) = 1) and (ifnull((case when (`tr`.`ppd_pax_class_id` = 1) then `flp`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp`.`remain_pax_f` else NULL end),`tr`.`ppd_pax`) >= `tr`.`ppd_pax`) and (ifnull((case when (`tr`.`ppd_pax_class_id` = 1) then `flp2`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp2`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp2`.`remain_pax_f` else NULL end),`tr`.`ppd_pax`) >= `tr`.`ppd_pax`) and (ifnull((case when (`tr`.`ppd_pax_class_id` = 1) then `flp3`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp3`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp3`.`remain_pax_f` else NULL end),`tr`.`ppd_pax`) >= `tr`.`ppd_pax`) and (ifnull((case when (`tr`.`ppd_pax_class_id` = 1) then `flp4`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp4`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp4`.`remain_pax_f` else NULL end),`tr`.`ppd_pax`) >= `tr`.`ppd_pax`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_flp_transfer_double`
--

/*!50001 DROP VIEW IF EXISTS `v_flp_transfer_double`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb3 */;
/*!50001 SET character_set_results     = utf8mb3 */;
/*!50001 SET collation_connection      = utf8mb3_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_flp_transfer_double` AS select `p`.`ppd_id` AS `ppdId`,`flp`.`flp_id` AS `flpId`,`flp2`.`flp_id` AS `flpId2`,`pd`.`dep_ap_id` AS `depApId`,`pd`.`arr_ap_id` AS `arrApId`,`pd`.`distance_km` AS `distanceKm`,`flp`.`fl_name` AS `flName`,`flp2`.`fl_name` AS `flName2`,`flp`.`remain_pax_e` AS `remainPaxE`,`flp2`.`remain_pax_e` AS `remainPaxE2`,round((`p`.`max_ticket_price` - ((`p`.`pax` * `flpns`.`price_e`) + (`p`.`pax` * `flpns2`.`price_e`))),2) AS `diffPrice`,round(((`p`.`pax` * `flpns`.`price_e`) + (`p`.`pax` * `flpns2`.`price_e`)),2) AS `sumPrice`,round(`p`.`max_ticket_price`,2) AS `pPrice`,`p`.`pax_class_id` AS `paxClassId`,`p`.`pax` AS `pax`,`p`.`payload_kg` AS `payloadKg`,`p`.`note` AS `note` from ((((((`ams_al_flight_plan_payload_pct_full` `flp` join `ams_al`.`ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpns_id` = `flp`.`flpns_id`))) join `v_ams_ac` `ac` on((`ac`.`acId` = `flp`.`ac_id`))) join `ams_al_flight_plan_payload_pct_full` `flp2` on((`flp2`.`d_ap_id` = `flp`.`a_ap_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns2` on((`flpns2`.`flpns_id` = `flp2`.`flpns_id`))) join `ams_wad`.`ams_airport_ppd` `p` on((`p`.`dep_ap_id` = `flp`.`d_ap_id`))) join `ams_wad`.`cfg_airport_destination` `pd` on((`pd`.`dest_id` = `p`.`dest_id`))) where ((`flp`.`flp_status_id` = 1) and (`flp`.`remain_pax_e` > 0) and (`flp2`.`remain_pax_e` > 0) and (`flp2`.`flp_id` <> `flp`.`flp_id`) and (`flp2`.`a_ap_id` <> `flp`.`d_ap_id`) and (`pd`.`arr_ap_id` = `flp2`.`a_ap_id`) and (timestampdiff(MINUTE,`flp`.`atime`,`flp2`.`dtime`) > `ac`.`opTimeMin`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v03`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v03`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v03` AS select `tr`.`transfer_id` AS `transfer_id`,`p`.`pax_class_id` AS `pax_class_id`,`p`.`ppd_id` AS `ppd_id`,`d`.`dep_ap_id` AS `dep_ap_id`,`d`.`arr_ap_id` AS `arr_ap_id`,`tr`.`dest_id` AS `dest_id`,`tr`.`distance_km` AS `distance_km`,`tr`.`flp_id_1` AS `flp_id_1`,`tr`.`flp_id_2` AS `flp_id_2`,`tr`.`flp_id_3` AS `flp_id_3`,`tr`.`flp_id_4` AS `flp_id_4`,`tr`.`fl_name_1` AS `fl_name_1`,`tr`.`fl_name_2` AS `fl_name_2`,`tr`.`fl_name_3` AS `fl_name_3`,`tr`.`fl_name_4` AS `fl_name_4`,`p`.`pax` AS `ppd_pax`,`p`.`payload_kg` AS `ppd_payload_kg`,round(`p`.`max_ticket_price`,2) AS `ppd_price`,`p`.`note` AS `ppd_note`,`p`.`adate` AS `adate`,(if((`tr`.`flp_id_1` is null),`p`.`pax`,(ifnull(`flp1`.`cabin_payload`,0) - ifnull(`flp1`.`flp_payload_sum`,0))) - `p`.`pax`) AS `pax_diff_1`,(if((`tr`.`flp_id_2` is null),`p`.`pax`,(ifnull(`flp2`.`cabin_payload`,0) - ifnull(`flp2`.`flp_payload_sum`,0))) - `p`.`pax`) AS `pax_diff_2`,(if((`tr`.`flp_id_3` is null),`p`.`pax`,(ifnull(`flp3`.`cabin_payload`,0) - ifnull(`flp3`.`flp_payload_sum`,0))) - `p`.`pax`) AS `pax_diff_3`,(if((`tr`.`flp_id_4` is null),`p`.`pax`,(ifnull(`flp4`.`cabin_payload`,0) - ifnull(`flp4`.`flp_payload_sum`,0))) - `p`.`pax`) AS `pax_diff_4`,(((ifnull(`flp1`.`flpns_price`,0) + ifnull(`flp2`.`flpns_price`,0)) + ifnull(`flp3`.`flpns_price`,0)) + ifnull(`flp4`.`flpns_price`,0)) AS `flpns_price_sum`,round((`p`.`pax` * (((ifnull(`flp1`.`flpns_price`,0) + ifnull(`flp2`.`flpns_price`,0)) + ifnull(`flp3`.`flpns_price`,0)) + ifnull(`flp4`.`flpns_price`,0))),2) AS `flpns_price`,(ifnull(`flp1`.`cabin_payload`,0) - ifnull(`flp1`.`flp_payload_sum`,0)) AS `free_payload_1`,(ifnull(`flp2`.`cabin_payload`,0) - ifnull(`flp2`.`flp_payload_sum`,0)) AS `free_payload_2`,(ifnull(`flp3`.`cabin_payload`,0) - ifnull(`flp3`.`flp_payload_sum`,0)) AS `free_payload_3`,(ifnull(`flp4`.`cabin_payload`,0) - ifnull(`flp4`.`flp_payload_sum`,0)) AS `free_payload_4` from ((((((`ams_al_flight_plan_transfer` `tr` join `ams_wad`.`ams_airport_ppd` `p` on((`p`.`dest_id` = `tr`.`dest_id`))) join `ams_wad`.`cfg_airport_destination` `d` on((`d`.`dest_id` = `tr`.`dest_id`))) left join `v_ams_flight_plan_payload_sum_class` `flp1` on(((`flp1`.`flp_id` = `tr`.`flp_id_1`) and (`flp1`.`pax_class_id` = `p`.`pax_class_id`)))) left join `v_ams_flight_plan_payload_sum_class` `flp2` on(((`flp2`.`flp_id` = `tr`.`flp_id_2`) and (`flp2`.`pax_class_id` = `p`.`pax_class_id`)))) left join `v_ams_flight_plan_payload_sum_class` `flp3` on(((`flp3`.`flp_id` = `tr`.`flp_id_3`) and (`flp3`.`pax_class_id` = `p`.`pax_class_id`)))) left join `v_ams_flight_plan_payload_sum_class` `flp4` on(((`flp4`.`flp_id` = `tr`.`flp_id_4`) and (`flp4`.`pax_class_id` = `p`.`pax_class_id`)))) where (((if((`tr`.`flp_id_1` is null),`p`.`pax`,ifnull(`flp1`.`cabin_payload`,0)) - `p`.`pax`) >= 0) and ((if((`tr`.`flp_id_2` is null),`p`.`pax`,ifnull(`flp2`.`cabin_payload`,0)) - `p`.`pax`) >= 0) and ((if((`tr`.`flp_id_3` is null),`p`.`pax`,ifnull(`flp3`.`cabin_payload`,0)) - `p`.`pax`) >= 0) and ((if((`tr`.`flp_id_4` is null),`p`.`pax`,ifnull(`flp4`.`cabin_payload`,0)) - `p`.`pax`) >= 0)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_cpd_add`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_cpd_add`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_al_flight_plan_payload_add_n_transfers_cpd_add` AS select `tr`.`transfers_id` AS `transfers_id`,`tr`.`cpd_id` AS `cpdId`,`tr`.`flp_id` AS `flpId`,`tr`.`flp_id2` AS `flpId2`,`tr`.`flp_id3` AS `flpId3`,`tr`.`flp_id4` AS `flpId4`,`tr`.`dep_ap_id` AS `depApId`,`tr`.`arr_ap_id` AS `arrApId`,`tr`.`distance_km` AS `distanceKm`,`tr`.`fl_name` AS `flName`,`tr`.`fl_name2` AS `flName2`,`tr`.`fl_name3` AS `flName3`,`tr`.`fl_name4` AS `flName4`,ifnull(`flp`.`remain_payload_kg`,0) AS `remain_payload_kg`,ifnull(`flp2`.`remain_payload_kg`,0) AS `remain_payload_kg2`,ifnull(`flp3`.`remain_payload_kg`,0) AS `remain_payload_kg3`,ifnull(`flp4`.`remain_payload_kg`,0) AS `remain_payload_kg4`,round((`tr`.`cpd_price` - (`tr`.`cpd_payload_kg` * (((ifnull(`flpns`.`price_c_p_kg`,0) + ifnull(`flpns2`.`price_c_p_kg`,0)) + ifnull(`flpns3`.`price_c_p_kg`,0)) + ifnull(`flpns4`.`price_c_p_kg`,0)))),2) AS `price_diff`,round(((((`tr`.`cpd_payload_kg` * ifnull(`flpns`.`price_c_p_kg`,0)) + (`tr`.`cpd_payload_kg` * ifnull(`flpns2`.`price_c_p_kg`,0))) + (`tr`.`cpd_payload_kg` * ifnull(`flpns3`.`price_c_p_kg`,0))) + (`tr`.`cpd_payload_kg` * ifnull(`flpns4`.`price_c_p_kg`,0))),2) AS `price_sum`,`tr`.`cpd_price` AS `cpd_price`,`tr`.`cpd_pax_class_id` AS `cpd_pax_class_id`,`tr`.`cpd_payload_kg` AS `cpd_payload_kg`,`tr`.`cpd_note` AS `cpd_note`,`tr`.`cpd_adate` AS `cpd_adate`,`tr`.`dtime` AS `dtime`,`tr`.`id` AS `id` from (((((((((`ams_al_flight_plan_transfers_cpd_add` `tr` join `ams_al_flight_plan_tmp` `flp` on((`flp`.`flp_id` = `tr`.`flp_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpns_id` = `flp`.`flpns_id`))) left join `ams_al_flight_plan_tmp` `flp2` on((`flp2`.`flp_id` = `tr`.`flp_id2`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns2` on((`flpns2`.`flpns_id` = `flp2`.`flpns_id`))) left join `ams_al_flight_plan_tmp` `flp3` on((`flp3`.`flp_id` = `tr`.`flp_id3`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns3` on((`flpns3`.`flpns_id` = `flp3`.`flpns_id`))) left join `ams_al_flight_plan_tmp` `flp4` on((`flp4`.`flp_id` = `tr`.`flp_id4`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns4` on((`flpns4`.`flpns_id` = `flp4`.`flpns_id`))) join (select `ams_al`.`cfg_callc_param`.`param_value` AS `transfers_id` from `ams_al`.`cfg_callc_param` where (`ams_al`.`cfg_callc_param`.`param_id` = 44)) `p` on((1 = 1))) where ((`tr`.`transfers_id` = `p`.`transfers_id`) and (ifnull(`flp`.`flp_status_id`,1) = 1) and (ifnull(`flp2`.`flp_status_id`,1) = 1) and (ifnull(`flp3`.`flp_status_id`,1) = 1) and (ifnull(`flp4`.`flp_status_id`,1) = 1) and (ifnull(`flp`.`remain_payload_kg`,`tr`.`cpd_payload_kg`) >= `tr`.`cpd_payload_kg`) and (ifnull(`flp2`.`remain_payload_kg`,`tr`.`cpd_payload_kg`) >= `tr`.`cpd_payload_kg`) and (ifnull(`flp3`.`remain_payload_kg`,`tr`.`cpd_payload_kg`) >= `tr`.`cpd_payload_kg`) and (ifnull(`flp4`.`remain_payload_kg`,`tr`.`cpd_payload_kg`) >= `tr`.`cpd_payload_kg`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_al_flight_plan_transfers_pcpd_price`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_transfers_pcpd_price`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_al_flight_plan_transfers_pcpd_price` AS select `tr`.`transfer_id` AS `transfer_id`,`flp1`.`pax_class_id` AS `pax_class_id`,`tr`.`d_ap_id` AS `d_ap_id`,`tr`.`a_ap_id` AS `a_ap_id`,`tr`.`dest_id` AS `dest_id`,`tr`.`distance_km` AS `distance_km`,`tr`.`flp_id_1` AS `flp_id_1`,`tr`.`flp_id_2` AS `flp_id_2`,`tr`.`flp_id_3` AS `flp_id_3`,`tr`.`flp_id_4` AS `flp_id_4`,`tr`.`fl_name_1` AS `fl_name_1`,`tr`.`fl_name_2` AS `fl_name_2`,`tr`.`fl_name_3` AS `fl_name_3`,`tr`.`fl_name_4` AS `fl_name_4`,if((`flp1`.`pax_class_id` = 4),ifnull(`c`.`payload_kg`,0),ifnull(`p`.`pax`,0)) AS `payload`,round(if((`flp1`.`pax_class_id` = 4),ifnull(`c`.`max_ticket_price`,0),ifnull(`p`.`max_ticket_price`,0)),2) AS `ppd_price`,round((if((`flp1`.`pax_class_id` = 4),ifnull(`c`.`payload_kg`,0),ifnull(`p`.`pax`,0)) * (((ifnull(`flp1`.`flpns_price`,0) + ifnull(`flp2`.`flpns_price`,0)) + ifnull(`flp3`.`flpns_price`,0)) + ifnull(`flp4`.`flpns_price`,0))),2) AS `flpns_price`,if((`flp1`.`pax_class_id` = 4),ifnull(`c`.`note`,0),ifnull(`p`.`note`,0)) AS `ppd_note`,ifnull(`flp1`.`flp_free_payload`,0) AS `free_payload_1`,ifnull(`flp2`.`flp_free_payload`,0) AS `free_payload_2`,ifnull(`flp3`.`flp_free_payload`,0) AS `free_payload_3`,ifnull(`flp4`.`flp_free_payload`,0) AS `free_payload_4`,(((ifnull(`flp1`.`flpns_price`,0) + ifnull(`flp2`.`flpns_price`,0)) + ifnull(`flp3`.`flpns_price`,0)) + ifnull(`flp4`.`flpns_price`,0)) AS `flpns_price_sum`,`p`.`ppd_id` AS `ppd_id`,`c`.`cpd_id` AS `cpd_id` from ((((((`ams_al_flight_plan_transfer` `tr` left join `v_ams_flight_plan_payload_sum` `flp1` on((`flp1`.`flp_id` = `tr`.`flp_id_1`))) left join `ams_airport_ppd` `p` on(((`p`.`dest_id` = `tr`.`dest_id`) and (`p`.`pax_class_id` = `flp1`.`pax_class_id`)))) left join `ams_airport_cpd` `c` on(((`c`.`dest_id` = `tr`.`dest_id`) and (`c`.`pax_class_id` = `flp1`.`pax_class_id`)))) left join `v_ams_flight_plan_payload_sum` `flp2` on(((`flp2`.`flp_id` = `tr`.`flp_id_2`) and (`flp2`.`pax_class_id` = `flp1`.`pax_class_id`)))) left join `v_ams_flight_plan_payload_sum` `flp3` on(((`flp3`.`flp_id` = `tr`.`flp_id_3`) and (`flp3`.`pax_class_id` = `flp1`.`pax_class_id`)))) left join `v_ams_flight_plan_payload_sum` `flp4` on(((`flp4`.`flp_id` = `tr`.`flp_id_4`) and (`flp4`.`pax_class_id` = `flp1`.`pax_class_id`)))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui` AS select `tr`.`transfers_id` AS `transfers_id`,`tr`.`ppd_id` AS `ppdId`,`tr`.`flp_id` AS `flpId`,`tr`.`flp_id2` AS `flpId2`,`tr`.`flp_id3` AS `flpId3`,`tr`.`flp_id4` AS `flpId4`,`tr`.`dep_ap_id` AS `depApId`,`tr`.`arr_ap_id` AS `arrApId`,`tr`.`distance_km` AS `distanceKm`,`tr`.`fl_name` AS `flName`,`tr`.`fl_name2` AS `flName2`,`tr`.`fl_name3` AS `flName3`,`tr`.`fl_name4` AS `flName4`,(case when (`tr`.`ppd_pax_class_id` = 1) then `flp`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp`.`remain_pax_f` else NULL end) AS `remain_pax`,(case when (`tr`.`ppd_pax_class_id` = 1) then `flp2`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp2`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp2`.`remain_pax_f` else NULL end) AS `remain_pax2`,(case when (`tr`.`ppd_pax_class_id` = 1) then `flp3`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp3`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp3`.`remain_pax_f` else NULL end) AS `remain_pax3`,(case when (`tr`.`ppd_pax_class_id` = 1) then `flp4`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp4`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp4`.`remain_pax_f` else NULL end) AS `remain_pax4`,(case when (`tr`.`ppd_pax_class_id` = 1) then round((`tr`.`ppd_price` - ((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_e`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_e`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_e`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_e`,0)))),2) when (`tr`.`ppd_pax_class_id` = 2) then round((`tr`.`ppd_price` - ((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_b`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_b`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_b`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_b`,0)))),2) when (`tr`.`ppd_pax_class_id` = 3) then round((`tr`.`ppd_price` - ((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_f`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_f`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_f`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_f`,0)))),2) else NULL end) AS `price_diff`,(case when (`tr`.`ppd_pax_class_id` = 1) then round(((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_e`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_e`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_e`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_e`,0))),2) when (`tr`.`ppd_pax_class_id` = 2) then round(((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_b`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_b`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_b`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_b`,0))),2) when (`tr`.`ppd_pax_class_id` = 3) then round(((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_f`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_f`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_f`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_f`,0))),2) else NULL end) AS `price_sum`,`tr`.`ppd_price` AS `ppd_price`,`tr`.`ppd_pax_class_id` AS `ppd_pax_class_id`,`tr`.`ppd_pax` AS `ppd_pax`,`tr`.`ppd_payload_kg` AS `ppd_payload_kg`,`tr`.`ppd_note` AS `ppd_note`,`tr`.`ppd_adate` AS `ppd_adate`,`tr`.`id` AS `id`,`tr`.`adate` AS `adate`,least(ifnull(`flp`.`dtime`,now()),ifnull(`flp2`.`dtime`,now()),ifnull(`flp3`.`dtime`,now()),ifnull(`flp4`.`dtime`,now())) AS `dtime` from (((((((((`ams_al_flight_plan_transfers_ppd` `tr` join `ams_al_flight_plan` `flp` on((`flp`.`flp_id` = `tr`.`flp_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpns_id` = `flp`.`flpns_id`))) left join `ams_al_flight_plan_tmp` `flp2` on((`flp2`.`flp_id` = `tr`.`flp_id2`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns2` on((`flpns2`.`flpns_id` = `flp2`.`flpns_id`))) left join `ams_al_flight_plan_tmp` `flp3` on((`flp3`.`flp_id` = `tr`.`flp_id3`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns3` on((`flpns3`.`flpns_id` = `flp3`.`flpns_id`))) left join `ams_al_flight_plan_tmp` `flp4` on((`flp4`.`flp_id` = `tr`.`flp_id4`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns4` on((`flpns4`.`flpns_id` = `flp4`.`flpns_id`))) join (select `ams_al`.`cfg_callc_param`.`param_value` AS `transfers_id` from `ams_al`.`cfg_callc_param` where (`ams_al`.`cfg_callc_param`.`param_id` = 43)) `p` on((1 = 1))) where ((`tr`.`transfers_id` = `p`.`transfers_id`) and (ifnull(`flp`.`flp_status_id`,1) = 1) and (ifnull(`flp2`.`flp_status_id`,1) = 1) and (ifnull(`flp3`.`flp_status_id`,1) = 1) and (ifnull(`flp4`.`flp_status_id`,1) = 1) and (ifnull((case when (`tr`.`ppd_pax_class_id` = 1) then `flp`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp`.`remain_pax_f` else NULL end),`tr`.`ppd_pax`) >= `tr`.`ppd_pax`) and (ifnull((case when (`tr`.`ppd_pax_class_id` = 1) then `flp2`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp2`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp2`.`remain_pax_f` else NULL end),`tr`.`ppd_pax`) >= `tr`.`ppd_pax`) and (ifnull((case when (`tr`.`ppd_pax_class_id` = 1) then `flp3`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp3`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp3`.`remain_pax_f` else NULL end),`tr`.`ppd_pax`) >= `tr`.`ppd_pax`) and (ifnull((case when (`tr`.`ppd_pax_class_id` = 1) then `flp4`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp4`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp4`.`remain_pax_f` else NULL end),`tr`.`ppd_pax`) >= `tr`.`ppd_pax`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_flight_plan_payload_sum_class`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_flight_plan_payload_sum_class`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_flight_plan_payload_sum_class` AS select `flp`.`flp_id` AS `flp_id`,`flp`.`flpns_id` AS `flpns_id`,`flp`.`fl_name` AS `fl_name`,`flp`.`flp_status_id` AS `flp_status_id`,`flp`.`ac_id` AS `ac_id`,`flp`.`d_ap_id` AS `d_ap_id`,`flp`.`a_ap_id` AS `a_ap_id`,`flp`.`dtime` AS `dtime`,`flp`.`atime` AS `atime`,`acc`.`pax_class_id` AS `pax_class_id`,(case when (`acc`.`pax_class_id` = 1) then ifnull(`flpns`.`price_e`,0) when (`acc`.`pax_class_id` = 2) then ifnull(`flpns`.`price_b`,0) when (`acc`.`pax_class_id` = 3) then ifnull(`flpns`.`price_f`,0) when (`acc`.`pax_class_id` = 4) then round(ifnull(`flpns`.`price_c_p_kg`,0),4) else 0 end) AS `flpns_price`,count(`p`.`flppl_id`) AS `payloads_count`,`acc`.`cabin_payload` AS `cabin_payload`,round(sum(if((`acc`.`pax_class_id` = 4),round(ifnull(`p`.`payload_kg`,0),2),ifnull(`p`.`pax`,0))),2) AS `flp_payload_sum`,round(sum(ifnull(`p`.`price`,0)),2) AS `flp_price_sum`,`acc`.`cabin_id` AS `cabin_id`,`acc`.`mac_id` AS `mac_id` from (((`ams_al_flight_plan` `flp` join `ams_al`.`ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpns_id` = `flp`.`flpns_id`))) join `v_ams_aircraft_cabin` `acc` on((`acc`.`ac_id` = `flp`.`ac_id`))) left join `ams_al_flight_plan_payload` `p` on(((`p`.`flp_id` = `flp`.`flp_id`) and (`p`.`pax_class_id` = `acc`.`pax_class_id`)))) group by `flp`.`flp_id`,`acc`.`pax_class_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v01`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v01`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v01` AS select `tr`.`transfer_id` AS `transfer_id`,`p`.`pax_class_id` AS `pax_class_id`,`p`.`ppd_id` AS `ppd_id`,`d`.`dep_ap_id` AS `dep_ap_id`,`d`.`arr_ap_id` AS `arr_ap_id`,`tr`.`dest_id` AS `dest_id`,`tr`.`distance_km` AS `distance_km`,`tr`.`flp_id_1` AS `flp_id_1`,`tr`.`flp_id_2` AS `flp_id_2`,`tr`.`flp_id_3` AS `flp_id_3`,`tr`.`flp_id_4` AS `flp_id_4`,`tr`.`fl_name_1` AS `fl_name_1`,`tr`.`fl_name_2` AS `fl_name_2`,`tr`.`fl_name_3` AS `fl_name_3`,`tr`.`fl_name_4` AS `fl_name_4`,`p`.`pax` AS `ppd_pax`,`p`.`payload_kg` AS `ppd_payload_kg`,round(`p`.`max_ticket_price`,2) AS `ppd_price`,`p`.`note` AS `ppd_note`,`p`.`adate` AS `adate`,(if((`tr`.`flp_id_1` is null),`p`.`pax`,(ifnull(`flp1`.`cabin_payload`,0) - ifnull(`flp1`.`flp_payload_sum`,0))) - `p`.`pax`) AS `pax_diff_1`,(if((`tr`.`flp_id_2` is null),`p`.`pax`,(ifnull(`flp2`.`cabin_payload`,0) - ifnull(`flp2`.`flp_payload_sum`,0))) - `p`.`pax`) AS `pax_diff_2`,(if((`tr`.`flp_id_3` is null),`p`.`pax`,(ifnull(`flp3`.`cabin_payload`,0) - ifnull(`flp3`.`flp_payload_sum`,0))) - `p`.`pax`) AS `pax_diff_3`,(if((`tr`.`flp_id_4` is null),`p`.`pax`,(ifnull(`flp4`.`cabin_payload`,0) - ifnull(`flp4`.`flp_payload_sum`,0))) - `p`.`pax`) AS `pax_diff_4`,(((ifnull(`flp1`.`flpns_price`,0) + ifnull(`flp2`.`flpns_price`,0)) + ifnull(`flp3`.`flpns_price`,0)) + ifnull(`flp4`.`flpns_price`,0)) AS `flpns_price_sum`,round((`p`.`pax` * (((ifnull(`flp1`.`flpns_price`,0) + ifnull(`flp2`.`flpns_price`,0)) + ifnull(`flp3`.`flpns_price`,0)) + ifnull(`flp4`.`flpns_price`,0))),2) AS `flpns_price`,(ifnull(`flp1`.`cabin_payload`,0) - ifnull(`flp1`.`flp_payload_sum`,0)) AS `free_payload_1`,(ifnull(`flp2`.`cabin_payload`,0) - ifnull(`flp2`.`flp_payload_sum`,0)) AS `free_payload_2`,(ifnull(`flp3`.`cabin_payload`,0) - ifnull(`flp3`.`flp_payload_sum`,0)) AS `free_payload_3`,(ifnull(`flp4`.`cabin_payload`,0) - ifnull(`flp4`.`flp_payload_sum`,0)) AS `free_payload_4` from ((((((`ams_al_flight_plan_transfer` `tr` join `ams_wad`.`ams_airport_ppd` `p` on((`p`.`dest_id` = `tr`.`dest_id`))) join `ams_wad`.`cfg_airport_destination` `d` on((`d`.`dest_id` = `tr`.`dest_id`))) left join `v_ams_flight_plan_payload_sum_class` `flp1` on(((`flp1`.`flp_id` = `tr`.`flp_id_1`) and (`flp1`.`pax_class_id` = `p`.`pax_class_id`)))) left join `v_ams_flight_plan_payload_sum_class` `flp2` on(((`flp2`.`flp_id` = `tr`.`flp_id_2`) and (`flp2`.`pax_class_id` = `p`.`pax_class_id`)))) left join `v_ams_flight_plan_payload_sum_class` `flp3` on(((`flp3`.`flp_id` = `tr`.`flp_id_3`) and (`flp3`.`pax_class_id` = `p`.`pax_class_id`)))) left join `v_ams_flight_plan_payload_sum_class` `flp4` on(((`flp4`.`flp_id` = `tr`.`flp_id_4`) and (`flp4`.`pax_class_id` = `p`.`pax_class_id`)))) where (((if((`tr`.`flp_id_1` is null),`p`.`pax`,(ifnull(`flp1`.`cabin_payload`,0) - ifnull(`flp1`.`flp_payload_sum`,0))) - `p`.`pax`) >= 0) and ((if((`tr`.`flp_id_2` is null),`p`.`pax`,(ifnull(`flp2`.`cabin_payload`,0) - ifnull(`flp2`.`flp_payload_sum`,0))) - `p`.`pax`) >= 0) and ((if((`tr`.`flp_id_3` is null),`p`.`pax`,(ifnull(`flp3`.`cabin_payload`,0) - ifnull(`flp3`.`flp_payload_sum`,0))) - `p`.`pax`) >= 0) and ((if((`tr`.`flp_id_4` is null),`p`.`pax`,(ifnull(`flp4`.`cabin_payload`,0) - ifnull(`flp4`.`flp_payload_sum`,0))) - `p`.`pax`) >= 0)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_cpd`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_cpd`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_al_flight_plan_payload_add_n_transfers_cpd` AS select `tr`.`transfers_id` AS `transfers_id`,`tr`.`cpd_id` AS `cpdId`,`tr`.`flp_id` AS `flpId`,`tr`.`flp_id2` AS `flpId2`,`tr`.`flp_id3` AS `flpId3`,`tr`.`flp_id4` AS `flpId4`,`tr`.`dep_ap_id` AS `depApId`,`tr`.`arr_ap_id` AS `arrApId`,`tr`.`distance_km` AS `distanceKm`,`tr`.`fl_name` AS `flName`,`tr`.`fl_name2` AS `flName2`,`tr`.`fl_name3` AS `flName3`,`tr`.`fl_name4` AS `flName4`,ifnull(`flp`.`remain_payload_kg`,0) AS `remain_payload_kg`,ifnull(`flp2`.`remain_payload_kg`,0) AS `remain_payload_kg2`,ifnull(`flp3`.`remain_payload_kg`,0) AS `remain_payload_kg3`,ifnull(`flp4`.`remain_payload_kg`,0) AS `remain_payload_kg4`,round((`tr`.`cpd_price` - (`tr`.`cpd_payload_kg` * (((ifnull(`flpns`.`price_c_p_kg`,0) + ifnull(`flpns2`.`price_c_p_kg`,0)) + ifnull(`flpns3`.`price_c_p_kg`,0)) + ifnull(`flpns4`.`price_c_p_kg`,0)))),2) AS `price_diff`,round(((((`tr`.`cpd_payload_kg` * ifnull(`flpns`.`price_c_p_kg`,0)) + (`tr`.`cpd_payload_kg` * ifnull(`flpns2`.`price_c_p_kg`,0))) + (`tr`.`cpd_payload_kg` * ifnull(`flpns3`.`price_c_p_kg`,0))) + (`tr`.`cpd_payload_kg` * ifnull(`flpns4`.`price_c_p_kg`,0))),2) AS `price_sum`,`tr`.`cpd_price` AS `cpd_price`,`tr`.`cpd_pax_class_id` AS `cpd_pax_class_id`,`tr`.`cpd_payload_kg` AS `cpd_payload_kg`,`tr`.`cpd_note` AS `cpd_note`,`tr`.`cpd_adate` AS `cpd_adate`,least(ifnull(`flp`.`dtime`,now()),ifnull(`flp2`.`dtime`,now()),ifnull(`flp3`.`dtime`,now()),ifnull(`flp4`.`dtime`,now())) AS `dtime`,`tr`.`id` AS `id` from (((((((((`ams_al_flight_plan_transfers_cpd` `tr` join `ams_al_flight_plan_tmp` `flp` on((`flp`.`flp_id` = `tr`.`flp_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpns_id` = `flp`.`flpns_id`))) left join `ams_al_flight_plan_tmp` `flp2` on((`flp2`.`flp_id` = `tr`.`flp_id2`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns2` on((`flpns2`.`flpns_id` = `flp2`.`flpns_id`))) left join `ams_al_flight_plan_tmp` `flp3` on((`flp3`.`flp_id` = `tr`.`flp_id3`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns3` on((`flpns3`.`flpns_id` = `flp3`.`flpns_id`))) left join `ams_al_flight_plan_tmp` `flp4` on((`flp4`.`flp_id` = `tr`.`flp_id4`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns4` on((`flpns4`.`flpns_id` = `flp4`.`flpns_id`))) join (select `ams_al`.`cfg_callc_param`.`param_value` AS `transfers_id` from `ams_al`.`cfg_callc_param` where (`ams_al`.`cfg_callc_param`.`param_id` = 44)) `p` on((1 = 1))) where ((`tr`.`transfers_id` = `p`.`transfers_id`) and (ifnull(`flp`.`flp_status_id`,1) = 1) and (ifnull(`flp2`.`flp_status_id`,1) = 1) and (ifnull(`flp3`.`flp_status_id`,1) = 1) and (ifnull(`flp4`.`flp_status_id`,1) = 1) and (ifnull(`flp`.`remain_payload_kg`,`tr`.`cpd_payload_kg`) >= `tr`.`cpd_payload_kg`) and (ifnull(`flp2`.`remain_payload_kg`,`tr`.`cpd_payload_kg`) >= `tr`.`cpd_payload_kg`) and (ifnull(`flp3`.`remain_payload_kg`,`tr`.`cpd_payload_kg`) >= `tr`.`cpd_payload_kg`) and (ifnull(`flp4`.`remain_payload_kg`,`tr`.`cpd_payload_kg`) >= `tr`.`cpd_payload_kg`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_flp_transfer_double_cpd`
--

/*!50001 DROP VIEW IF EXISTS `v_flp_transfer_double_cpd`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb3 */;
/*!50001 SET character_set_results     = utf8mb3 */;
/*!50001 SET collation_connection      = utf8mb3_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_flp_transfer_double_cpd` AS select `p`.`cpd_id` AS `ppdId`,`flp`.`flp_id` AS `flpId`,`flp2`.`flp_id` AS `flpId2`,`pd`.`dep_ap_id` AS `depApId`,`pd`.`arr_ap_id` AS `arrApId`,`pd`.`distance_km` AS `distanceKm`,`flp`.`fl_name` AS `flName`,`flp2`.`fl_name` AS `flName2`,`flp`.`remain_payload_kg` AS `remainPayloadKg`,`flp2`.`remain_payload_kg` AS `remainPayloadKgE2`,round((`p`.`max_ticket_price` - ((`p`.`payload_kg` * `flpns`.`price_c_p_kg`) + (`p`.`payload_kg` * `flpns2`.`price_c_p_kg`))),2) AS `diffPrice`,round(((`p`.`payload_kg` * `flpns`.`price_c_p_kg`) + (`p`.`payload_kg` * `flpns2`.`price_c_p_kg`)),2) AS `sumPrice`,round(`p`.`max_ticket_price`,2) AS `pPrice`,`p`.`pax_class_id` AS `paxClassId`,0 AS `pax`,`p`.`payload_kg` AS `payloadKg`,`p`.`note` AS `note` from ((((((`ams_al_flight_plan_payload_pct_full` `flp` join `ams_al`.`ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpns_id` = `flp`.`flpns_id`))) join `v_ams_ac` `ac` on((`ac`.`acId` = `flp`.`ac_id`))) join `ams_al_flight_plan_payload_pct_full` `flp2` on((`flp2`.`d_ap_id` = `flp`.`a_ap_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns2` on((`flpns2`.`flpns_id` = `flp2`.`flpns_id`))) join `ams_wad`.`ams_airport_cpd` `p` on((`p`.`dep_ap_id` = `flp`.`d_ap_id`))) join `ams_wad`.`cfg_airport_destination` `pd` on((`pd`.`dest_id` = `p`.`dest_id`))) where ((`flp`.`flp_status_id` = 1) and (`flp`.`remain_payload_kg` > 0) and (`flp2`.`remain_payload_kg` > 0) and (`flp2`.`flp_id` <> `flp`.`flp_id`) and (`flp2`.`a_ap_id` <> `flp`.`d_ap_id`) and (`pd`.`arr_ap_id` = `flp2`.`a_ap_id`) and (timestampdiff(MINUTE,`flp`.`atime`,`flp2`.`dtime`) > `ac`.`opTimeMin`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_al_flight_plan_transfers_cpd`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_transfers_cpd`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_al_flight_plan_transfers_cpd` AS select `tr`.`transfers_id` AS `transfers_id`,`tr`.`ppdId` AS `ppdId`,`tr`.`cpdId` AS `cpdId`,`tr`.`flpId` AS `flpId`,`tr`.`flpId2` AS `flpId2`,`tr`.`flpId3` AS `flpId3`,`tr`.`flpId4` AS `flpId4`,`tr`.`depApId` AS `depApId`,`tr`.`arrApId` AS `arrApId`,`tr`.`distanceKm` AS `distanceKm`,`tr`.`flName` AS `flName`,`tr`.`flName2` AS `flName2`,`tr`.`flName3` AS `flName3`,`tr`.`flName4` AS `flName4`,`tr`.`remainPax` AS `remainPax`,`tr`.`remainPax2` AS `remainPax2`,`tr`.`remainPax3` AS `remainPax3`,`tr`.`remainPax4` AS `remainPax4`,`tr`.`remainPayloadKg` AS `remainPayloadKg`,`tr`.`remainPayloadKg2` AS `remainPayloadKg2`,`tr`.`remainPayloadKg3` AS `remainPayloadKg3`,`tr`.`remainPayloadKg4` AS `remainPayloadKg4`,`tr`.`diffPrice` AS `diffPrice`,`tr`.`sumPrice` AS `sumPrice`,`tr`.`pPrice` AS `pPrice`,`tr`.`paxClassId` AS `paxClassId`,`tr`.`pax` AS `pax`,`tr`.`payloadKg` AS `payloadKg`,`tr`.`note` AS `note`,`tr`.`id` AS `id`,`flp`.`remain_payload_kg` AS `remain_payload_kg`,`flp2`.`remain_payload_kg` AS `remain_payload_kg2`,`flp3`.`remain_payload_kg` AS `remain_payload_kg3`,`flp4`.`remain_payload_kg` AS `remain_payload_kg4` from (((((((((`ams_al_flight_plan_transfers` `tr` join `ams_al_flight_plan` `flp` on((`flp`.`flp_id` = `tr`.`flpId`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpns_id` = `flp`.`flpns_id`))) left join `ams_al_flight_plan` `flp2` on((`flp2`.`flp_id` = `tr`.`flpId2`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns2` on((`flpns2`.`flpns_id` = `flp2`.`flpns_id`))) left join `ams_al_flight_plan` `flp3` on((`flp3`.`flp_id` = `tr`.`flpId3`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns3` on((`flpns3`.`flpns_id` = `flp3`.`flpns_id`))) left join `ams_al_flight_plan` `flp4` on((`flp4`.`flp_id` = `tr`.`flpId4`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns4` on((`flpns4`.`flpns_id` = `flp4`.`flpns_id`))) left join (select `ams_al`.`cfg_callc_param`.`param_value` AS `transfers_id` from `ams_al`.`cfg_callc_param` where (`ams_al`.`cfg_callc_param`.`param_id` = 43)) `p` on((1 = 1))) where ((`tr`.`transfers_id` = `p`.`transfers_id`) and (`tr`.`paxClassId` = 4) and (ifnull(`flp`.`flp_status_id`,1) = 1) and (ifnull(`flp2`.`flp_status_id`,1) = 1) and (ifnull(`flp3`.`flp_status_id`,1) = 1) and (ifnull(`flp4`.`flp_status_id`,1) = 1) and (ifnull(`flp`.`remain_payload_kg`,`tr`.`payloadKg`) >= `tr`.`payloadKg`) and (ifnull(`flp2`.`remain_payload_kg`,`tr`.`payloadKg`) >= `tr`.`payloadKg`) and (ifnull(`flp3`.`remain_payload_kg`,`tr`.`payloadKg`) >= `tr`.`payloadKg`) and (ifnull(`flp4`.`remain_payload_kg`,`tr`.`payloadKg`) >= `tr`.`payloadKg`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_al_flight_plan_payload_sum`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_sum`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_al_flight_plan_payload_sum` AS select `flp`.`flp_id` AS `flp_id`,`flp`.`flpns_id` AS `flpns_id`,`flp`.`fl_name` AS `fl_name`,`flp`.`flp_status_id` AS `flp_status_id`,`flp`.`ac_id` AS `ac_id`,`flp`.`d_ap_id` AS `d_ap_id`,`flp`.`a_ap_id` AS `a_ap_id`,`flp`.`dtime` AS `dtime`,`flp`.`atime` AS `atime`,count(`p`.`flppl_id`) AS `payloads_count`,sum(if((ifnull(`p`.`pax_class_id`,1) = 1),ifnull(`p`.`pax`,0),0)) AS `sum_pax_e`,sum(if((ifnull(`p`.`pax_class_id`,2) = 2),ifnull(`p`.`pax`,0),0)) AS `sum_pax_b`,sum(if((ifnull(`p`.`pax_class_id`,3) = 3),ifnull(`p`.`pax`,0),0)) AS `sum_pax_f`,round(sum(ifnull(`p`.`payload_kg`,0)),2) AS `sum_payload_kg`,round(sum(ifnull(`p`.`price`,0)),2) AS `sum_price`,(ifnull(`flp`.`available_pax_e`,0) - sum(if((`p`.`pax_class_id` = 1),ifnull(`p`.`pax`,0),0))) AS `remain_pax_e`,(ifnull(`flp`.`available_pax_b`,0) - sum(if((`p`.`pax_class_id` = 2),ifnull(`p`.`pax`,0),0))) AS `remain_pax_b`,(ifnull(`flp`.`available_pax_f`,0) - sum(if((`p`.`pax_class_id` = 3),ifnull(`p`.`pax`,0),0))) AS `remain_pax_f`,round((ifnull(`flp`.`available_cargo_kg`,0) - sum(ifnull(`p`.`payload_kg`,0))),2) AS `remain_payload_kg` from (`ams_al_flight_plan` `flp` left join `ams_al_flight_plan_payload` `p` on((`p`.`flp_id` = `flp`.`flp_id`))) group by `flp`.`flp_id`,`p`.`pax_class_id` order by `flp`.`flp_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_ac_mnt_next`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_ac_mnt_next`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb3 */;
/*!50001 SET character_set_results     = utf8mb3 */;
/*!50001 SET collation_connection      = utf8mb3_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_ac_mnt_next` AS select `acv`.`acId` AS `ac_id`,`acv`.`forSheduleFlights` AS `forSheduleFlights`,if((`acv`.`homeApId` = `acv`.`currApId`),1,0) AS `is_home_ap`,`acv`.`acActionTypeId` AS `acat_id`,`act`.`acat_name` AS `acat_name`,round(((unix_timestamp(ifnull(`flq`.`dtime`,now())) - unix_timestamp(now())) / 3600),2) AS `plane_stand_time_h`,`flq`.`fl_id` AS `next_flq_fl_id`,`flq`.`dtime` AS `next_flq_dtime`,`acv`.`flight_h_to_maintenance` AS `flight_h_to_maintenance`,if((`acv`.`acActionTypeId` = 31),1,0) AS `is_in_mnt`,`mtnp`.`acmp_id` AS `current_acmp_id`,`acv`.`next_amt_id` AS `next_amt_id`,`mt`.`amt_name` AS `next_amt_name`,from_unixtime((unix_timestamp(now()) + (2 * 60))) AS `mnt_start_time`,`mt`.`time_h` AS `mtn_time_h`,from_unixtime((unix_timestamp(now()) + ((`mt`.`time_h` * 60) * 60))) AS `mnt_end_time` from ((((`v_ams_ac` `acv` left join (select `flq1`.`fl_id` AS `fl_id`,`flq1`.`fl_type_id` AS `fl_type_id`,`flq1`.`fl_name` AS `fl_name`,`flq1`.`fl_description` AS `fl_description`,`flq1`.`fle_id` AS `fle_id`,`flq1`.`flp_id` AS `flp_id`,`flq1`.`ch_id` AS `ch_id`,`flq1`.`route_id` AS `route_id`,`flq1`.`al_id` AS `al_id`,`flq1`.`ac_id` AS `ac_id`,`flq1`.`d_ap_id` AS `d_ap_id`,`flq1`.`a_ap_id` AS `a_ap_id`,`flq1`.`distance_km` AS `distance_km`,`flq1`.`oil_l` AS `oil_l`,`flq1`.`flight_h` AS `flight_h`,`flq1`.`pax` AS `pax`,`flq1`.`payload_kg` AS `payload_kg`,`flq1`.`dtime` AS `dtime`,`flq1`.`atime` AS `atime`,`flq1`.`processed` AS `processed` from (`ams_al_flight_queue` `flq1` join (select `ams_al_flight_queue`.`ac_id` AS `ac_id`,min(`ams_al_flight_queue`.`dtime`) AS `mdtime` from `ams_al_flight_queue` group by `ams_al_flight_queue`.`ac_id`) `minflqdtime` on(((`flq1`.`ac_id` = `minflqdtime`.`ac_id`) and (`flq1`.`dtime` = `minflqdtime`.`mdtime`))))) `flq` on((`flq`.`ac_id` = `acv`.`acId`))) join `cfg_aircraft_action_type` `act` on((`act`.`acat_id` = `acv`.`acActionTypeId`))) left join `ams_aircraft_maintenance_plan` `mtnp` on((`mtnp`.`ac_id` = `acv`.`acId`))) join `cfg_aircraft_maintenance_type` `mt` on((`mt`.`amt_id` = `acv`.`next_amt_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v02`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v02`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_al_flight_plan_payload_add_n_transfers_ppd_gui_v02` AS select `tr`.`transfers_id` AS `transfers_id`,`tr`.`ppd_id` AS `ppdId`,`tr`.`flp_id` AS `flpId`,`tr`.`flp_id2` AS `flpId2`,`tr`.`flp_id3` AS `flpId3`,`tr`.`flp_id4` AS `flpId4`,`tr`.`dep_ap_id` AS `depApId`,`tr`.`arr_ap_id` AS `arrApId`,`tr`.`distance_km` AS `distanceKm`,`tr`.`fl_name` AS `flName`,`tr`.`fl_name2` AS `flName2`,`tr`.`fl_name3` AS `flName3`,`tr`.`fl_name4` AS `flName4`,(case when (`tr`.`ppd_pax_class_id` = 1) then `flp`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp`.`remain_pax_f` else NULL end) AS `remain_pax`,(case when (`tr`.`ppd_pax_class_id` = 1) then `flp2`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp2`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp2`.`remain_pax_f` else NULL end) AS `remain_pax2`,(case when (`tr`.`ppd_pax_class_id` = 1) then `flp3`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp3`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp3`.`remain_pax_f` else NULL end) AS `remain_pax3`,(case when (`tr`.`ppd_pax_class_id` = 1) then `flp4`.`remain_pax_e` when (`tr`.`ppd_pax_class_id` = 2) then `flp4`.`remain_pax_b` when (`tr`.`ppd_pax_class_id` = 3) then `flp4`.`remain_pax_f` else NULL end) AS `remain_pax4`,(case when (`tr`.`ppd_pax_class_id` = 1) then round((`tr`.`ppd_price` - ((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_e`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_e`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_e`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_e`,0)))),2) when (`tr`.`ppd_pax_class_id` = 2) then round((`tr`.`ppd_price` - ((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_b`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_b`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_b`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_b`,0)))),2) when (`tr`.`ppd_pax_class_id` = 3) then round((`tr`.`ppd_price` - ((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_f`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_f`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_f`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_f`,0)))),2) else NULL end) AS `price_diff`,(case when (`tr`.`ppd_pax_class_id` = 1) then round(((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_e`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_e`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_e`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_e`,0))),2) when (`tr`.`ppd_pax_class_id` = 2) then round(((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_b`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_b`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_b`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_b`,0))),2) when (`tr`.`ppd_pax_class_id` = 3) then round(((((`tr`.`ppd_pax` * ifnull(`flpns`.`price_f`,0)) + (`tr`.`ppd_pax` * ifnull(`flpns2`.`price_f`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns3`.`price_f`,0))) + (`tr`.`ppd_pax` * ifnull(`flpns4`.`price_f`,0))),2) else NULL end) AS `price_sum`,`tr`.`ppd_price` AS `ppd_price`,`tr`.`ppd_pax_class_id` AS `ppd_pax_class_id`,`tr`.`ppd_pax` AS `ppd_pax`,`tr`.`ppd_payload_kg` AS `ppd_payload_kg`,`tr`.`ppd_note` AS `ppd_note`,`tr`.`ppd_adate` AS `ppd_adate`,`tr`.`id` AS `id`,`tr`.`adate` AS `adate`,least(ifnull(`flp`.`dtime`,now()),ifnull(`flp2`.`dtime`,now()),ifnull(`flp3`.`dtime`,now()),ifnull(`flp4`.`dtime`,now())) AS `dtime` from (((((((((`ams_al_flight_plan_transfers_ppd` `tr` join `ams_al_flight_plan` `flp` on((`flp`.`flp_id` = `tr`.`flp_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpns_id` = `flp`.`flpns_id`))) left join `ams_al_flight_plan` `flp2` on((`flp2`.`flp_id` = `tr`.`flp_id2`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns2` on((`flpns2`.`flpns_id` = `flp2`.`flpns_id`))) left join `ams_al_flight_plan` `flp3` on((`flp3`.`flp_id` = `tr`.`flp_id3`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns3` on((`flpns3`.`flpns_id` = `flp3`.`flpns_id`))) left join `ams_al_flight_plan` `flp4` on((`flp4`.`flp_id` = `tr`.`flp_id4`))) left join `ams_al`.`ams_al_flp_number_schedule` `flpns4` on((`flpns4`.`flpns_id` = `flp4`.`flpns_id`))) join (select `ams_al`.`cfg_callc_param`.`param_value` AS `transfers_id` from `ams_al`.`cfg_callc_param` where (`ams_al`.`cfg_callc_param`.`param_id` = 43)) `p` on((1 = 1))) where ((`tr`.`transfers_id` = `p`.`transfers_id`) and (ifnull(`flp`.`flp_status_id`,1) = 1) and (ifnull(`flp2`.`flp_status_id`,1) = 1) and (ifnull(`flp3`.`flp_status_id`,1) = 1) and (ifnull(`flp4`.`flp_status_id`,1) = 1)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_flp_transfer_tripple`
--

/*!50001 DROP VIEW IF EXISTS `v_flp_transfer_tripple`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb3 */;
/*!50001 SET character_set_results     = utf8mb3 */;
/*!50001 SET collation_connection      = utf8mb3_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_flp_transfer_tripple` AS select `p`.`ppd_id` AS `ppdId`,`flp`.`flp_id` AS `flpId`,`flp2`.`flp_id` AS `flpId2`,`flp3`.`flp_id` AS `flpId3`,`pd`.`dep_ap_id` AS `depApId`,`pd`.`arr_ap_id` AS `arrApId`,`pd`.`distance_km` AS `distanceKm`,`flp`.`fl_name` AS `flName`,`flp2`.`fl_name` AS `flName2`,`flp3`.`fl_name` AS `flName3`,`flp`.`remain_pax_e` AS `remainPaxE`,`flp2`.`remain_pax_e` AS `remainPaxE2`,`flp3`.`remain_pax_e` AS `remainPaxE3`,round((`p`.`max_ticket_price` - (((`p`.`pax` * `flpns`.`price_e`) + (`p`.`pax` * `flpns2`.`price_e`)) + (`p`.`pax` * `flpns3`.`price_e`))),2) AS `diffPrice`,round((((`p`.`pax` * `flpns`.`price_e`) + (`p`.`pax` * `flpns2`.`price_e`)) + (`p`.`pax` * `flpns3`.`price_e`)),2) AS `sumPrice`,round(`p`.`max_ticket_price`,2) AS `pPrice`,`p`.`pax_class_id` AS `paxClassId`,`p`.`pax` AS `pax`,`p`.`payload_kg` AS `payloadKg`,`p`.`note` AS `note` from ((((((((`ams_al_flight_plan_payload_pct_full` `flp` join `ams_al`.`ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpns_id` = `flp`.`flpns_id`))) join `v_ams_ac` `ac` on((`ac`.`acId` = `flp`.`ac_id`))) join `ams_al_flight_plan_payload_pct_full` `flp2` on((`flp2`.`d_ap_id` = `flp`.`a_ap_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns2` on((`flpns2`.`flpns_id` = `flp2`.`flpns_id`))) join `ams_al_flight_plan_payload_pct_full` `flp3` on((`flp3`.`d_ap_id` = `flp2`.`a_ap_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns3` on((`flpns3`.`flpns_id` = `flp3`.`flpns_id`))) join `ams_wad`.`ams_airport_ppd` `p` on((`p`.`dep_ap_id` = `flp`.`d_ap_id`))) join `ams_wad`.`cfg_airport_destination` `pd` on((`pd`.`dest_id` = `p`.`dest_id`))) where ((`flp`.`flp_status_id` = 1) and (`flp2`.`remain_pax_e` > 0) and (`flp2`.`flp_status_id` = 1) and (`flp2`.`flp_id` <> `flp`.`flp_id`) and (`flp2`.`a_ap_id` <> `flp`.`d_ap_id`) and (`flp3`.`remain_pax_e` > 0) and (`flp3`.`flp_status_id` = 1) and (`flp3`.`flp_id` <> `flp2`.`flp_id`) and (`flp3`.`a_ap_id` <> `flp2`.`d_ap_id`) and (`flp3`.`flp_id` <> `flp`.`flp_id`) and (`flp3`.`a_ap_id` <> `flp`.`d_ap_id`) and (`pd`.`arr_ap_id` = `flp3`.`a_ap_id`) and (timestampdiff(MINUTE,`flp`.`atime`,`flp2`.`dtime`) > `ac`.`opTimeMin`) and (timestampdiff(MINUTE,`flp2`.`atime`,`flp3`.`dtime`) > `ac`.`opTimeMin`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_ams_ac`
--

/*!50001 DROP VIEW IF EXISTS `v_ams_ac`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_ams_ac` AS select `a`.`ac_id` AS `acId`,`a`.`registration` AS `registration`,`a`.`mac_id` AS `macId`,`a`.`owner_al_id` AS `ownerAlId`,`a`.`home_ap_id` AS `homeApId`,`a`.`adate` AS `adate`,`a`.`udate` AS `udate`,`a`.`cabin_id` AS `cabinId`,`a`.`pilots_count` AS `pilots`,`a`.`crew_count` AS `crew`,`a`.`pax_f` AS `paxF`,`a`.`pax_b` AS `paxB`,`a`.`pax_e` AS `paxE`,`a`.`cargo_kg` AS `cargoKg`,`a`.`cost_per_fh` AS `costPerFh`,`a`.`ac_status_id` AS `acStatusId`,`a`.`for_shedule_flights` AS `forShaduleFlighs`,`ac`.`curr_ap_id` AS `currApId`,`ac`.`flight_hours` AS `flightHours`,`ac`.`distance_km` AS `distanceKm`,`ac`.`state_value` AS `state`,`ac`.`price_value` AS `price`,`ac`.`op_time_min` AS `opTimeMin`,`ac`.`a_check_fh` AS `aCheckFh`,`ac`.`b_check_fh` AS `bCheckFh`,`ac`.`c_check_fh` AS `cCheckFh`,`ac`.`d_check_fh` AS `dCheckFh`,`ac`.`stars` AS `stars`,`ac`.`points` AS `points`,ifnull(`ac`.`flight_in_queue`,0) AS `flight_in_queue`,ifnull(`ac`.`flight_h_in_queue`,0) AS `flight_h_in_queue`,ifnull(`ac`.`last_flq_atime`,unix_timestamp(now())) AS `ac_last_flq_atime`,`ams_wad`.`h`.`apLabel` AS `homeAp`,`ams_wad`.`c`.`apLabel` AS `currAp`,if(((`ams_wad`.`h`.`iata` is null) or (`ams_wad`.`h`.`iata` = '')),`ams_wad`.`h`.`icao`,`ams_wad`.`h`.`iata`) AS `homeApCode`,if(((`ams_wad`.`c`.`iata` is null) or (`ams_wad`.`c`.`iata` = '')),`ams_wad`.`c`.`icao`,`ams_wad`.`c`.`iata`) AS `currApCode`,`cb`.`cabin_name` AS `cabinName`,`cb`.`confort` AS `confort`,`m`.`ac_type_id` AS `acTypeId`,`m`.`fuel_consumption_lp100km` AS `fuelConsumptionLp100km`,`m`.`max_range_km` AS `maxRangeKm`,if((`m`.`landing_m` > `m`.`take_off_m`),`m`.`landing_m`,`m`.`take_off_m`) AS `minRunwayM`,`m`.`cruise_speed_kmph` AS `cruiseSpeedKmph`,`l`.`acat_id` AS `acActionTypeId`,`l`.`fl_id` AS `logFlId`,`l`.`adate` AS `logAdate`,`l`.`duration_min` AS `logDurationMin`,`l`.`flight_h` AS `logFlightH`,`l`.`distance_km` AS `logDistanceKm`,`l`.`udate` AS `logUdate`,`ac`.`flight_in_queue` AS `flInQueue`,`a`.`for_shedule_flights` AS `forSheduleFlights`,`grp`.`grp_id` AS `grpId`,(case when (ifnull(`ac`.`flight_hours`,0) <= ifnull(`ac`.`a_check_fh`,0)) then round((ifnull(`ac`.`a_check_fh`,0) - ifnull(`ac`.`flight_hours`,0)),2) when ((ifnull(`ac`.`flight_hours`,0) > ifnull(`ac`.`a_check_fh`,0)) and (ifnull(`ac`.`flight_hours`,0) <= ifnull(`ac`.`b_check_fh`,0))) then round((ifnull(`ac`.`b_check_fh`,0) - ifnull(`ac`.`flight_hours`,0)),2) when ((ifnull(`ac`.`flight_hours`,0) > ifnull(`ac`.`b_check_fh`,0)) and (ifnull(`ac`.`flight_hours`,0) <= ifnull(`ac`.`c_check_fh`,0))) then round((ifnull(`ac`.`c_check_fh`,0) - ifnull(`ac`.`flight_hours`,0)),2) when ((ifnull(`ac`.`flight_hours`,0) > ifnull(`ac`.`c_check_fh`,0)) and (ifnull(`ac`.`flight_hours`,0) <= ifnull(`ac`.`d_check_fh`,0))) then round((ifnull(`ac`.`d_check_fh`,0) - ifnull(`ac`.`flight_hours`,0)),2) end) AS `flight_h_to_maintenance`,(case when (ifnull(`ac`.`flight_hours`,0) <= ifnull(`ac`.`a_check_fh`,0)) then 1 when ((ifnull(`ac`.`flight_hours`,0) > ifnull(`ac`.`a_check_fh`,0)) and (ifnull(`ac`.`flight_hours`,0) <= ifnull(`ac`.`b_check_fh`,0))) then 2 when ((ifnull(`ac`.`flight_hours`,0) > ifnull(`ac`.`b_check_fh`,0)) and (ifnull(`ac`.`flight_hours`,0) <= ifnull(`ac`.`c_check_fh`,0))) then 3 when ((ifnull(`ac`.`flight_hours`,0) > ifnull(`ac`.`c_check_fh`,0)) and (ifnull(`ac`.`flight_hours`,0) <= ifnull(`ac`.`d_check_fh`,0))) then 4 end) AS `next_amt_id`,`qa`.`fl_id` AS `last_flq_fl_id`,`qa`.`a_ap_id` AS `last_flq_a_ap_id`,ifnull(`qa`.`atime`,unix_timestamp(now())) AS `last_flq_atime`,ifnull(`qa`.`flq_sum_flight_h`,0) AS `flq_sum_flight_h` from ((((((((`ams_aircraft` `a` join `cfg_mac` `m` on((`m`.`mac_id` = `a`.`mac_id`))) join `ams_aircraft_curr` `ac` on((`ac`.`ac_id` = `a`.`ac_id`))) join `ams_aircraft_log` `l` on((`l`.`ac_id` = `a`.`ac_id`))) join `cfg_mac_cabin` `cb` on((`cb`.`cabin_id` = `a`.`cabin_id`))) join `ams_wad`.`v_solr_airport` `h` on((`ams_wad`.`h`.`apId` = `a`.`home_ap_id`))) join `ams_wad`.`v_solr_airport` `c` on((`ams_wad`.`c`.`apId` = `ac`.`curr_ap_id`))) left join `v_ams_al_flight_queue_ac_flq_sum_flight_h` `qa` on((`qa`.`ac_id` = `a`.`ac_id`))) left join `ams_al`.`ams_al_flp_group` `grp` on((`grp`.`ac_id` = `a`.`ac_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_flp_transfer_quadruple`
--

/*!50001 DROP VIEW IF EXISTS `v_flp_transfer_quadruple`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb3 */;
/*!50001 SET character_set_results     = utf8mb3 */;
/*!50001 SET collation_connection      = utf8mb3_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_flp_transfer_quadruple` AS select `p`.`ppd_id` AS `ppdId`,`flp`.`flp_id` AS `flpId`,`flp2`.`flp_id` AS `flpId2`,`flp3`.`flp_id` AS `flpId3`,`flp4`.`flp_id` AS `flpId4`,`pd`.`dep_ap_id` AS `depApId`,`pd`.`arr_ap_id` AS `arrApId`,`pd`.`distance_km` AS `distanceKm`,`flp`.`fl_name` AS `flName`,`flp2`.`fl_name` AS `flName2`,`flp3`.`fl_name` AS `flName3`,`flp4`.`fl_name` AS `flName4`,`flp`.`remain_pax_e` AS `remainPaxE`,`flp2`.`remain_pax_e` AS `remainPaxE2`,`flp3`.`remain_pax_e` AS `remainPaxE3`,`flp4`.`remain_pax_e` AS `remainPaxE4`,round((`p`.`max_ticket_price` - ((((`p`.`pax` * `flpns`.`price_e`) + (`p`.`pax` * `flpns2`.`price_e`)) + (`p`.`pax` * `flpns3`.`price_e`)) + (`p`.`pax` * `flpns4`.`price_e`))),2) AS `diffPrice`,round(((((`p`.`pax` * `flpns`.`price_e`) + (`p`.`pax` * `flpns2`.`price_e`)) + (`p`.`pax` * `flpns3`.`price_e`)) + (`p`.`pax` * `flpns4`.`price_e`)),2) AS `sumPrice`,round(`p`.`max_ticket_price`,2) AS `pPrice`,`p`.`pax_class_id` AS `paxClassId`,`p`.`pax` AS `pax`,`p`.`payload_kg` AS `payloadKg`,`p`.`note` AS `note` from ((((((((((`ams_al_flight_plan_payload_pct_full` `flp` join `ams_al`.`ams_al_flp_number_schedule` `flpns` on((`flpns`.`flpns_id` = `flp`.`flpns_id`))) join `v_ams_ac` `ac` on((`ac`.`acId` = `flp`.`ac_id`))) join `ams_al_flight_plan_payload_pct_full` `flp2` on((`flp2`.`d_ap_id` = `flp`.`a_ap_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns2` on((`flpns2`.`flpns_id` = `flp2`.`flpns_id`))) join `ams_al_flight_plan_payload_pct_full` `flp3` on((`flp3`.`d_ap_id` = `flp2`.`a_ap_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns3` on((`flpns3`.`flpns_id` = `flp3`.`flpns_id`))) join `ams_al_flight_plan_payload_pct_full` `flp4` on((`flp4`.`d_ap_id` = `flp3`.`a_ap_id`))) join `ams_al`.`ams_al_flp_number_schedule` `flpns4` on((`flpns4`.`flpns_id` = `flp4`.`flpns_id`))) join `ams_wad`.`ams_airport_ppd` `p` on((`p`.`dep_ap_id` = `flp`.`d_ap_id`))) join `ams_wad`.`cfg_airport_destination` `pd` on((`pd`.`dest_id` = `p`.`dest_id`))) where ((`flp`.`flp_status_id` = 1) and (`flp2`.`remain_pax_e` > 0) and (`flp3`.`remain_pax_e` > 0) and (`flp4`.`remain_pax_e` > 0) and (`flp2`.`flp_status_id` = 1) and (`flp2`.`flp_id` <> `flp`.`flp_id`) and (`flp2`.`a_ap_id` <> `flp`.`d_ap_id`) and (`flp3`.`flp_status_id` = 1) and (`flp3`.`flp_id` <> `flp2`.`flp_id`) and (`flp3`.`a_ap_id` <> `flp2`.`d_ap_id`) and (`flp3`.`flp_id` <> `flp`.`flp_id`) and (`flp3`.`a_ap_id` <> `flp`.`d_ap_id`) and (`flp4`.`flp_status_id` = 1) and (`flp4`.`flp_id` <> `flp3`.`flp_id`) and (`flp4`.`a_ap_id` <> `flp3`.`d_ap_id`) and (`flp4`.`flp_id` <> `flp2`.`flp_id`) and (`flp4`.`a_ap_id` <> `flp2`.`d_ap_id`) and (`flp4`.`flp_id` <> `flp`.`flp_id`) and (`flp4`.`a_ap_id` <> `flp`.`d_ap_id`) and (`pd`.`arr_ap_id` = `flp4`.`a_ap_id`) and (timestampdiff(MINUTE,`flp`.`atime`,`flp2`.`dtime`) > `ac`.`opTimeMin`) and (timestampdiff(MINUTE,`flp2`.`atime`,`flp3`.`dtime`) > `ac`.`opTimeMin`) and (timestampdiff(MINUTE,`flp3`.`atime`,`flp4`.`dtime`) > `ac`.`opTimeMin`)) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Dumping events for database 'ams_ac'
--

--
-- Dumping routines for database 'ams_ac'
--
/*!50003 DROP PROCEDURE IF EXISTS `sp_acat_certificate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acat_certificate`(in in_ac_id int)
BEGIN

DECLARE countRows, i_acat_id, vduration_min, vdistance_km, vflight_h INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acat_certificate', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acat_certificate';
set i_acat_id = 1; -- Airworthiness Certificate
set countRows = 0;

SET logNote=CONCAT('Start (', in_ac_id ,') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

		select count(*) INTO countRows
        from ams_ac.ams_aircraft_log where ac_id = in_ac_id;
        
		SELECT ifnull(duration_min, 0), ifnull(distance_km, 0), ifnull(flight_h,0) 
        INTO vduration_min, vdistance_km, vflight_h
        from ams_ac.cfg_aircraft_action_type where acat_id=i_acat_id;
        
        if countRows = 0 THEN
			SET logNote=CONCAT('Inserted Airworthiness Certificate record for ac_id ', in_ac_id , ' ' );
            
			INSERT INTO ams_ac.ams_aircraft_log
			(ac_id, acat_id, duration_min, flight_h, distance_km, processed)
			values(in_ac_id, i_acat_id, vduration_min, vflight_h, vdistance_km, 0);
		ELSE
        
			UPDATE ams_ac.ams_aircraft_log
            set acat_id = i_acat_id, duration_min = vduration_min, 
            flight_h = vflight_h, distance_km =  vdistance_km, processed = 0
            where ac_id = in_ac_id;
            SET logNote=CONCAT('Updated Airworthiness Certificate record for ac_id ', in_ac_id , ' ' );
		END IF;
        
        set countRows = 0;
		-- call ams_wad.log_sp(spname, stTime, logNote);
        
        call ams_ac.sp_acat_process(in_ac_id);
        
        COMMIT;


SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_acat_flight` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acat_flight`(in in_ac_id int, in in_fl_type_id int, in in_fl_id int)
BEGIN

DECLARE countRows, vacat_id, vaclog_id, vduration_min, vdistance_km INT;
DECLARE stTime TIMESTAMP;
DECLARE vflight_h double;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acat_flight', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acat_flight';
set vacat_id = 5; -- Flight
set countRows = 0;

IF (in_fl_type_id = 3) THEN
	set vacat_id = 5; -- 4 Transfer Flight
END IF;

SET logNote=CONCAT('Start (', in_ac_id ,',', in_fl_type_id,') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

		SELECT ifnull(duration_min, 0), ifnull(distance_km, 0), ifnull(flight_h,0) 
        INTO vduration_min, vdistance_km, vflight_h
        from ams_ac.cfg_aircraft_action_type where acat_id=vacat_id;
        
        select count(*) INTO countRows
        from ams_ac.ams_aircraft_log where ac_id = in_ac_id;
        
         if countRows = 0 THEN
			SET logNote=CONCAT('Inserted Flight record for ac_id ', in_ac_id , ' ' );
            
			INSERT INTO ams_ac.ams_aircraft_log
			(ac_id, acat_id, duration_min, flight_h, distance_km, processed, fl_id)
			values(in_ac_id, vacat_id, vduration_min, vflight_h, vdistance_km, 0, in_fl_id);
		ELSE
        
			UPDATE ams_ac.ams_aircraft_log
            set acat_id = vacat_id, duration_min = vduration_min, 
            flight_h = vflight_h, 
            distance_km =  vdistance_km, 
            processed = 0, fl_id = in_fl_id,
            adate = current_timestamp(), acmp_id = null, tr_id = null
            where ac_id = in_ac_id;
            SET logNote=CONCAT('Updated Flight record for ac_id ', in_ac_id , ' ' );
		END IF;
        
        set countRows = 0;
		call ams_wad.log_sp(spname, stTime, logNote);
        
        COMMIT;


SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_acat_mnt_check` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acat_mnt_check`(in in_ac_id int, in in_acmp_id int)
BEGIN

DECLARE countRows, i_acat_id INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acat_mnt_check', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acat_mnt_check';
set i_acat_id = 31; -- Maintenance Check
set countRows = 0;

SET logNote=CONCAT('Start (', in_ac_id ,',', in_acmp_id,') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

        select count(*) INTO countRows
        from ams_ac.ams_aircraft_log where ac_id = in_ac_id;
        
         if countRows = 0 THEN
			
			INSERT INTO ams_ac.ams_aircraft_log
			(ac_id, acat_id, duration_min, flight_h, distance_km, processed, acmp_id)
			SELECT  mntp.ac_id, i_acat_id as acat_id, (mt.time_h * 60) as duration_min, 
			0 as flight_h, 0 as distance_km, 0 as processed, mntp.acmp_id 
			FROM ams_ac.ams_aircraft_maintenance_plan mntp
			join ams_ac.cfg_aircraft_maintenance_type mt on mt.amt_id = mntp.amt_id
			join ams_ac.ams_aircraft ac on ac.ac_id = mntp.ac_id
			join ams_ac.ams_aircraft_log l on l.ac_id = mntp.ac_id
			where mntp.acmp_id=in_acmp_id;
            
            SET logNote=CONCAT('Inserted Flight record for ac_id ', in_ac_id , ' ' );
		ELSE
			 UPDATE ams_ac.ams_aircraft_log l
            join ams_ac.ams_aircraft_maintenance_plan mntp on mntp.ac_id = l.ac_id
			join ams_ac.cfg_aircraft_maintenance_type mt on mt.amt_id = mntp.amt_id
			join ams_ac.ams_aircraft ac on ac.ac_id = mntp.ac_id
            set l.acat_id  = i_acat_id, l.duration_min = (mt.time_h * 60), 
            l.tr_id = null, l.fl_id = null,
			l.flight_h = 0, l.distance_km = 0, l.processed = 0, l.acmp_id = mntp.acmp_id,
            l.adate = current_timestamp()
			where mntp.acmp_id=in_acmp_id;
            
            SET logNote=CONCAT('Updated Flight record for ac_id ', in_ac_id , ' ' );
		END IF;
        
        set countRows = 0;
		call ams_wad.log_sp(spname, stTime, logNote);
        
        COMMIT;


SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_acat_park` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acat_park`(in in_ac_id int)
BEGIN

DECLARE countRows, vacat_id, vaclog_id, vduration_min, vdistance_km INT;
DECLARE stTime TIMESTAMP;
DECLARE vflight_h double;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acat_park', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acat_park';
set vacat_id = 6; -- Plane stand
set countRows = 0;

SET logNote=CONCAT('Start (', in_ac_id ,') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

	select count(*) INTO countRows
	from ams_ac.ams_aircraft_log where ac_id = in_ac_id;

	SELECT ifnull(duration_min, 0), ifnull(distance_km, 0), ifnull(flight_h,0) 
	INTO vduration_min, vdistance_km, vflight_h
	from ams_ac.cfg_aircraft_action_type where acat_id=vacat_id;
    
    if countRows = 0 THEN
		SET logNote=CONCAT('Inserted Plane stand record for ac_id ', in_ac_id , ' ' );
		
		INSERT INTO ams_ac.ams_aircraft_log
		(ac_id, acat_id, duration_min, flight_h, distance_km, processed)
		values(in_ac_id, vacat_id, vduration_min, vflight_h, vdistance_km, 0);
	ELSE
	
		UPDATE ams_ac.ams_aircraft_log
		set acat_id = vacat_id, duration_min = vduration_min, 
		flight_h = vflight_h, distance_km =  vdistance_km, processed = 0,
        adate = current_timestamp(), acmp_id = null, tr_id = null, fl_id=null
		where ac_id = in_ac_id;
		SET logNote=CONCAT('Updated Plane stand record for ac_id ', in_ac_id , ' ' );
        COMMIT;
	END IF;
        
	set countRows = 0;
	-- call ams_wad.log_sp(spname, stTime, logNote);
	
	call ams_ac.sp_acat_process(in_ac_id);
	
   
	COMMIT;
    -- Update ac status current
    CALL ams_ac.sp_ams_aircraft_curr_update_ac(in_ac_id);

SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_acat_process` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acat_process`(in in_ac_id int)
BEGIN

DECLARE countRows, vac_id, vacat_id, vtr_id, vfl_id, vdistance_km, vflight_h INT;
DECLARE i_idx INT;
DECLARE stTime TIMESTAMP;
DECLARE d_price_value, d_state_value DOUBLE;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acat_process', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acat_process';
set countRows = 0;

SET logNote=CONCAT('Start (', in_ac_id ,') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);
IF in_ac_id>0 THEN
	SELECT ac_id, acat_id, tr_id, fl_id, flight_h, distance_km
    INTO vac_id, vacat_id, vtr_id, vfl_id, vflight_h, vdistance_km
    FROM ams_ac.ams_aircraft_log
    WHERE ac_id = in_ac_id;
    
    SET logNote=CONCAT('Update aircraft rirr for ac_id:',in_ac_id, ', acat_id:', vacat_id , '' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    SET d_state_value = 0;
    SET d_price_value = 0;
    select round((acc.after_check_condition-((10*round((ifnull(acc.flight_hours, 0)/(acc.a_check_fh)),2)))/100),2) as state_value
    INTO d_state_value
    FROM ams_ac.ams_aircraft_curr acc
    where acc.ac_id = in_ac_id;
     SET logNote=CONCAT('get d_state_value ', d_state_value , '' );
     call ams_wad.log_sp(spname, stTime, logNote);
     
    SELECT round(((acc.state_value/100) * m.price),2) as price_value
    INTO d_price_value
    FROM ams_ac.ams_aircraft_curr acc
	join ams_ac.ams_aircraft ac on ac.ac_id = acc.ac_id
	join ams_ac.cfg_mac m on m.mac_id = ac.mac_id  
	where acc.ac_id = in_ac_id;
    SET logNote=CONCAT('get d_price_value ', d_price_value , '' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    set countRows = 0;
    SET i_idx = 1;
    SELECT count(*) INTO countRows
	FROM performance_schema.table_handles 
	WHERE EXTERNAL_LOCK IS NOT NULL 
	and OBJECT_SCHEMA='ams_ac' and OBJECT_NAME='ams_aircraft_curr';
	SET logNote=CONCAT(i_idx, ' ams_aircraft_curr locked:', countRows , '' );
    call ams_wad.log_sp(spname, stTime, logNote);
    WHILE countRows > 0 DO
		SELECT SLEEP(0.2);
        SET i_idx = i_idx + 1;
        SELECT count(*) INTO countRows
		FROM performance_schema.table_handles 
		WHERE EXTERNAL_LOCK IS NOT NULL 
		and OBJECT_SCHEMA='ams_ac' and OBJECT_NAME='ams_aircraft_curr';
    END WHILE;
	SET logNote=CONCAT(i_idx, ' ams_aircraft_curr locked:', countRows , '' );
	call ams_wad.log_sp(spname, stTime, logNote);
	UPDATE ams_aircraft_curr ac
	join ams_ac.ams_aircraft_log l on l.ac_id = ac.ac_id
	SET ac.flight_hours = ifnull(ac.flight_hours,0) + ifnull(l.flight_h,0),
	ac.distance_km = (ifnull(ac.distance_km,0) + ifnull(l.distance_km,0)),
	ac.price_value = d_price_value,
	ac.state_value = d_state_value
	where l.ac_id = in_ac_id;
	
	SET countRows =  ROW_COUNT();
    COMMIT;
    SET logNote=CONCAT('updated ', countRows ,' rows in ams_aircraft_curr 1 for ac_id ', in_ac_id , '' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	UPDATE ams_ac.ams_aircraft_log set processed=1 where ac_id = in_ac_id;
    COMMIT;
    SET logNote=CONCAT('updated ams_aircraft_log for ac_id ', in_ac_id , '' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    -- CALL ams_ac.sp_ams_aircraft_curr_update_ac(in_ac_id);

	SET logNote=CONCAT('sp_ams_aircraft_curr_update_ac for ac_id ', in_ac_id , '' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
END IF;    
SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_acat_purchase` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acat_purchase`(in in_ac_id int, in in_al_id int)
BEGIN

DECLARE countRows, vacat_id, vaclog_id, vduration_min, vdistance_km INT;
DECLARE vtr_id, vap_id, vbank_id_from, vbank_id_to  INT;
DECLARE stTime TIMESTAMP;
DECLARE vflight_h, vamount, vprice double;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acat_purchase', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acat_purchase';
set vacat_id = 2; -- Aircraft Purchase
set countRows = 0;
set vtr_id = 0;

SET logNote=CONCAT('Start (', in_ac_id ,', ', in_al_id, ') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

		CALL ams_al.sp_tr_ac_purchase(in_ac_id, in_al_id, vtr_id);
		
        SET logNote = concat('Transaction vtr_id: ', vtr_id, '.');
		call ams_wad.log_sp(spname, stTime, logNote);
            
        
        if vtr_id > 0 THEN
        
			select count(*) INTO countRows
			from ams_ac.ams_aircraft_log where ac_id = in_ac_id;
        
            SELECT ifnull(duration_min, 0), ifnull(distance_km, 0), ifnull(flight_h,0) 
			INTO vduration_min, vdistance_km, vflight_h
			from ams_ac.cfg_aircraft_action_type where acat_id=vacat_id;
            SET logNote=CONCAT('Values  vduration_min:', vduration_min , ',  vdistance_km: ', vdistance_km, ',  vflight_h: ', vflight_h );
			call ams_wad.log_sp(spname, stTime, logNote);
			if countRows = 0 THEN
				SET logNote=CONCAT('Inserted Aircraft Purchase record for ac_id ', in_ac_id , ' ' );
				
				INSERT INTO ams_ac.ams_aircraft_log
				(ac_id, acat_id, duration_min, flight_h, distance_km, processed, tr_id)
				values(in_ac_id, vacat_id, vduration_min, vflight_h, vdistance_km, 0, vtr_id);
			ELSE
			
				UPDATE ams_ac.ams_aircraft_log
				set acat_id = vacat_id, duration_min = vduration_min, 
				flight_h = vflight_h, distance_km =  vdistance_km, processed = 0,
                tr_id = vtr_id
				where ac_id = in_ac_id;
				SET logNote=CONCAT('Updated Aircraft Purchase record for ac_id ', in_ac_id , ' ' );
			END IF;
            call ams_wad.log_sp(spname, stTime, logNote);
            
			set countRows = 0;
             
			SELECT a.ap_id INTO vap_id
			FROM ams_al.ams_airline a
			where a.al_id = in_al_id;
            SET logNote=CONCAT('vap_id ', vap_id , ' ' );
            call ams_wad.log_sp(spname, stTime, logNote);
            
            UPDATE ams_ac.ams_aircraft set 
            home_ap_id = vap_id, 
            owner_al_id = in_al_id,
            ac_status_id = 2
            where ac_id = in_ac_id;
            COMMIT;
            
            SET logNote=CONCAT('Ac ', in_ac_id , ' updated owner_al_id: ', in_al_id );
			call ams_wad.log_sp(spname, stTime, logNote);
            
            SET logNote=CONCAT('before sp_acat_process-> ', in_ac_id , ' ' );
            call ams_wad.log_sp(spname, stTime, logNote);
            call ams_ac.sp_acat_process(in_ac_id);
            
            
            SET logNote=CONCAT('before sp_tr_post-> ', in_ac_id , ' ' );
            call ams_wad.log_sp(spname, stTime, logNote);
            CALL ams_al.sp_tr_post();
        
			COMMIT;
            
            call ams_ac.sp_acat_park(in_ac_id);
            
        END IF;

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_acat_sell` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acat_sell`(in in_ac_id int)
BEGIN

DECLARE countRows, vacat_id, vaclog_id, vduration_min, vdistance_km INT;
DECLARE vtr_id, vap_id, vbank_id_from, vbank_id_to  INT;
DECLARE stTime TIMESTAMP;
DECLARE vflight_h, vamount, vprice double;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acat_sell', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acat_sell';
set vacat_id = 20; -- Aircraft Sell
set countRows = 0;
set vtr_id = 0;

SET logNote=CONCAT('Start (', in_ac_id ,', ', in_al_id, ') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

		CALL ams_al.sp_tr_ac_sell(in_ac_id, vtr_id);
		
        SET logNote = concat('Transaction vtr_id: ', vtr_id, '.');
		call ams_wad.log_sp(spname, stTime, logNote);
            
        
        if vtr_id > 0 THEN
        
			select count(*) INTO countRows
			from ams_ac.ams_aircraft_log where ac_id = in_ac_id;
        
            SELECT ifnull(duration_min, 0), ifnull(distance_km, 0), ifnull(flight_h,0) 
			INTO vduration_min, vdistance_km, vflight_h
			from ams_ac.cfg_aircraft_action_type where acat_id=vacat_id;
            SET logNote=CONCAT('Values  vduration_min:', vduration_min , ',  vdistance_km: ', vdistance_km, ',  vflight_h: ', vflight_h );
			call ams_wad.log_sp(spname, stTime, logNote);
			if countRows = 0 THEN
				SET logNote=CONCAT('Inserted Aircraft Purchase record for ac_id ', in_ac_id , ' ' );
				
				INSERT INTO ams_ac.ams_aircraft_log
				(ac_id, acat_id, duration_min, flight_h, distance_km, processed, tr_id)
				values(in_ac_id, vacat_id, vduration_min, vflight_h, vdistance_km, 0, vtr_id);
			ELSE
			
				UPDATE ams_ac.ams_aircraft_log
				set acat_id = vacat_id, duration_min = vduration_min, 
				flight_h = vflight_h, distance_km =  vdistance_km, processed = 0,
                tr_id = vtr_id
				where ac_id = in_ac_id;
				SET logNote=CONCAT('Updated Aircraft Sell record for ac_id ', in_ac_id , ' ' );
			END IF;
            call ams_wad.log_sp(spname, stTime, logNote);
            
			set countRows = 0;
            UPDATE ams_ac.ams_aircraft set 
            owner_al_id = null,
            ac_status_id = 1
            where ac_id = in_ac_id;
            COMMIT;
            
            SET logNote=CONCAT('Ac ', in_ac_id , ' updated owner_al_id: null' );
			call ams_wad.log_sp(spname, stTime, logNote);
            
            SET logNote=CONCAT('before sp_acat_process-> ', in_ac_id , ' ' );
            call ams_wad.log_sp(spname, stTime, logNote);
            call ams_ac.sp_acat_process(in_ac_id);
            
            
            SET logNote=CONCAT('before sp_tr_post-> ', in_ac_id , ' ' );
            call ams_wad.log_sp(spname, stTime, logNote);
            CALL ams_al.sp_tr_post();
        
			COMMIT;
            
            call ams_ac.sp_acat_park(in_ac_id);
            
        END IF;

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_acfs_boarding` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acfs_boarding`(IN in_fl_log_id INT)
BEGIN


DECLARE i_op_time_sec, i_fl_id, i_acfs_id, i_ac_id, i_bank_id_from, i_oil_l, o_tr_id  INT;
DECLARE d_oli_price DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_fl_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acfs_boarding', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acfs_boarding';
set countRows = 0;
set i_acfs_id = 4; -- Boarding

	SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

	CALL ams_al.sp_tr_ac_boarding(in_fl_log_id, o_tr_id);

	SET logNote=CONCAT('o_tr_id :',o_tr_id,' ' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

	update ams_ac.ams_al_flight_log l 
		join ams_ac.ams_al_flight fl on fl.fl_id = l.fl_id
        join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
		join ams_al.ams_bank b on b.al_id = fl.al_id
        join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id
		join ams_al.cfg_oil_price op on op.oil_price_id = 1 and 1 = 1 
		left join ams_ac.cfg_aircraft_flight_status acfsn on acfsn.acfs_id = (l.acfs_id +1)
	set l.acfs_id = acfsn.acfs_id,
		l.fl_log_description = concat(IFNULL(dep.ap_iata, dep.ap_icao),' ', acfsn.acfs_name, ' for ', fl.fl_name),
		l.fl_time_min = 0, l.fl_distance_km = 0, 
		l.op_time_sec = ((ac.op_time_min*60) * ifnull(acfsn.acfs_op_time_part, 0)),
		l.tr_id = o_tr_id
	where l.fl_log_id = in_fl_log_id;
	COMMIT;

	SET logNote=CONCAT('Finish ', '<-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_acfs_cargo_load` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acfs_cargo_load`(IN in_fl_log_id INT)
BEGIN


DECLARE i_op_time_sec, i_fl_id, i_acfs_id, i_ac_id, i_bank_id_from, i_oil_l, o_tr_id  INT;
DECLARE d_oli_price DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_fl_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acfs_cargo_load', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acfs_cargo_load';
set countRows = 0;
set i_acfs_id = 3; -- Cargo Load

	SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

	CALL ams_al.sp_tr_ac_cargo_load(in_fl_log_id, o_tr_id);

	SET logNote=CONCAT('o_tr_id :',o_tr_id,' ' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

	update ams_ac.ams_al_flight_log l 
		join ams_ac.ams_al_flight fl on fl.fl_id = l.fl_id
        join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
		join ams_al.ams_bank b on b.al_id = fl.al_id
        join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id
		join ams_al.cfg_oil_price op on op.oil_price_id = 1 and 1 = 1 
		left join ams_ac.cfg_aircraft_flight_status acfsn on acfsn.acfs_id = (l.acfs_id +1)
	set l.acfs_id = acfsn.acfs_id,
		l.fl_log_description = concat(IFNULL(dep.ap_iata, dep.ap_icao),' ',acfsn.acfs_name, ' for ', fl.fl_name),
		l.fl_time_min = 0, l.fl_distance_km = 0, 
		l.op_time_sec = ((ac.op_time_min*60) * ifnull(acfsn.acfs_op_time_part, 0)),
		l.tr_id = o_tr_id
	where l.fl_log_id = in_fl_log_id;
	COMMIT;

	SET logNote=CONCAT('Finish ', '<-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_acfs_cargo_unload` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acfs_cargo_unload`(IN in_fl_log_id INT)
BEGIN


DECLARE i_op_time_sec, i_fl_id, i_acfs_id, i_ac_id, i_bank_id_from, i_oil_l, o_tr_id  INT;
DECLARE d_oli_price DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_fl_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acfs_cargo_unload', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acfs_cargo_unload';
set countRows = 0;
set i_acfs_id = 9; -- Cargo Unload

	SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
	set o_tr_id = 0;
	CALL ams_al.sp_tr_ac_cargo_unload(in_fl_log_id, o_tr_id);
    
	IF o_tr_id > 0 then
		SET logNote=CONCAT('o_tr_id :',o_tr_id,' ' );
		-- call ams_wad.log_sp(spname, stTime, logNote);

		update ams_ac.ams_al_flight_log l 
			join ams_ac.ams_al_flight fl on fl.fl_id = l.fl_id
			join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
			join ams_al.ams_bank b on b.al_id = fl.al_id
			join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id
			join ams_al.cfg_oil_price op on op.oil_price_id = 1 and 1 = 1 
			left join ams_ac.cfg_aircraft_flight_status acfsn on acfsn.acfs_id = i_acfs_id
		set l.acfs_id = acfsn.acfs_id,
			l.fl_log_description = concat(IFNULL(arr.ap_iata, arr.ap_icao),' ', acfsn.acfs_name, ' for ', fl.fl_name),
			l.op_time_sec = ((ac.op_time_min*60) * ifnull(acfsn.acfs_op_time_part, 0)),
			l.tr_id = o_tr_id
		where l.fl_log_id = in_fl_log_id;

		-- CALL ams_al.sp_tr_post();
		COMMIT;
	END IF;
    
	SET logNote=CONCAT('Finish ', '<-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_acfs_disembarking` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acfs_disembarking`(IN in_fl_log_id INT)
BEGIN


DECLARE i_op_time_sec, i_fl_id, i_acfs_id, i_ac_id, i_bank_id_from, i_oil_l, o_tr_id  INT;
DECLARE d_oli_price DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_fl_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acfs_disembarking', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acfs_disembarking';
set countRows = 0;
set i_acfs_id = 8; -- Disembarking

	SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

	CALL ams_al.sp_tr_ac_disembarking(in_fl_log_id, o_tr_id);

	SET logNote=CONCAT('o_tr_id :',o_tr_id,' ' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

	update ams_ac.ams_al_flight_log l 
		join ams_ac.ams_al_flight fl on fl.fl_id = l.fl_id
        join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
		join ams_al.ams_bank b on b.al_id = fl.al_id
        join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id
		join ams_al.cfg_oil_price op on op.oil_price_id = 1 and 1 = 1 
		left join ams_ac.cfg_aircraft_flight_status acfsn on acfsn.acfs_id = (l.acfs_id +1)
	set l.acfs_id = acfsn.acfs_id,
		l.fl_log_description = concat(IFNULL(arr.ap_iata, arr.ap_icao),' ', acfsn.acfs_name, ' for ', fl.fl_name),
		l.op_time_sec = ((ac.op_time_min*60) * ifnull(acfsn.acfs_op_time_part, 0)),
		l.tr_id = o_tr_id
	where l.fl_log_id = in_fl_log_id;
	COMMIT;

	SET logNote=CONCAT('Finish ', '<-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_acfs_finish` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acfs_finish`(IN in_fl_log_id INT)
BEGIN


DECLARE i_op_time_sec, i_fl_id, i_acfs_id, i_ac_id, i_bank_id_from, i_oil_l, o_tr_id  INT;
DECLARE i_aclog_id INT;
DECLARE d_expences, d_income DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_fl_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acfs_finish', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acfs_finish';
set countRows = 0;
set i_acfs_id = 10; -- Finished

	SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
	select fl.ac_id  into i_ac_id
    FROM  ams_ac.ams_al_flight_log fll
    join ams_ac.ams_al_flight fl on fl.fl_id = fll.fl_id
    where fll.fl_log_id = in_fl_log_id;
    
	SET logNote=CONCAT('i_ac_id: ',i_ac_id,' ' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    select count(*) INTO countRows
    from ams_ac.ams_al_flight_payload flpl
    join ams_ac.ams_al_flight_log l on l.fl_id = flpl.fl_id
    where l.fl_log_id = in_fl_log_id;
    -- set flight income in transaction
    IF countRows > 0 then
		CALL ams_al.sp_tr_ac_fl_income(in_fl_log_id, o_tr_id);
		SET logNote=CONCAT('Flight income o_tr_id: ',ifnull(o_tr_id,'todo'),' ' );
		call ams_wad.log_sp(spname, stTime, logNote);
	END IF;
   
   update ams_ac.ams_al_flight_log l 
		join ams_ac.ams_al_flight fl on fl.fl_id = l.fl_id
        join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
		join ams_al.ams_bank b on b.al_id = fl.al_id
        join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id
        left join ams_ac.cfg_aircraft_flight_status acfsn on acfsn.acfs_id = i_acfs_id
		set l.acfs_id = acfsn.acfs_id,
		l.fl_log_description = concat(IFNULL(arr.ap_iata, arr.ap_icao),' Flight income for ', fl.fl_name),
		l.tr_id = o_tr_id
	where l.fl_log_id = in_fl_log_id;
    COMMIT;
    
    update ams_ac.ams_aircraft_log acl 
	join ams_ac.ams_al_flight_log l on l.fl_id = acl.fl_id
	join ams_ac.ams_al_flight fl on fl.fl_id = l.fl_id
	set acl.distance_km = fl.distance_km,
    acl.tr_id = o_tr_id, acl.fl_id = fl.fl_id,
	acl.flight_h = Round((UNIX_TIMESTAMP(ifnull(fl.atime_real,current_timestamp())) - UNIX_TIMESTAMP(ifnull(fl.dtime_real,fl.dtime)))/(60*60),2),
	acl.duration_min =  Round(( UNIX_TIMESTAMP(current_timestamp) - UNIX_TIMESTAMP(ifnull(acl.adate,current_timestamp())))/(60),0)
	where l.fl_log_id = in_fl_log_id ;
   
    select 
	sum(if(b.bank_id = tr.bank_id_from,  ifnull(tr.amount,0), 0)) as Expences,
	sum(if(b.bank_id = tr.bank_id_to,  ifnull(tr.amount,0), 0)) as Income
    INTO d_expences, d_income
	from  ams_al.ams_bank_transaction tr
	join ams_ac.ams_al_flight fl on fl.fl_id = tr.fl_id
	join ams_ac.ams_al_flight_log l on l.fl_id = fl.fl_id
	join ams_al.ams_bank b on b.al_id = fl.al_id
	where l.fl_log_id = in_fl_log_id;
    
    SET logNote=CONCAT('d_expences:',d_expences,',  d_income:', d_income );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
	update ams_ac.ams_al_flight fl
    join ams_ac.ams_al_flight_log l on l.fl_id = fl.fl_id
    set fl.expences = round(d_expences*-1),
    fl.income =  round(d_income, 2),
    fl.revenue = round(d_income - d_expences, 2)
    where l.fl_log_id = in_fl_log_id;

	COMMIT;
    
	call ams_ac.sp_acat_process(i_ac_id);
     
	CALL ams_al.sp_tr_post();
	COMMIT;

	SET logNote=CONCAT('Finish ', '<-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_acfs_history` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acfs_history`(IN in_fl_log_id INT)
BEGIN


DECLARE i_rows, i_acfs_id, i_ac_id  INT;
DECLARE i_fl_id INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_fl_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acfs_history', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acfs_history';
set countRows = 0;
set i_acfs_id = 10; -- Finished

	SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	select fl.ac_id, fl.fl_id  into i_ac_id, i_fl_id
    FROM  ams_ac.ams_al_flight_log fll
    join ams_ac.ams_al_flight fl on fl.fl_id = fll.fl_id
    where fll.fl_log_id = in_fl_log_id;
    
	SET logNote=CONCAT('ac_id: ',i_ac_id,', fl_id: ',i_fl_id,' ' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    IF i_fl_id > 0 THEN
    INSERT INTO ams_al.ams_al_ap_pc_tranfered
		(al_id, ap_id, fl_id, pax_f, pax_b, pax_e, cargo_kg)
	SELECT f.al_id, f.d_ap_id, p.fl_id,
		sum(if(p.payload_type = 'F', ifnull(p.pax, 0),0 )) as pax_f,
		sum(if(p.payload_type = 'B', ifnull(p.pax,0),0 )) as pax_b,
		sum(if(p.payload_type = 'E', ifnull(p.pax,0),0 )) as pax_e,
		sum(if(p.payload_type = 'C', ifnull(p.cargo_kg,0),0 )) as cargo_kg			
	from ams_ac.ams_al_flight_payload p
	join ams_ac.ams_al_flight f on f.fl_id = p.fl_id
	where p.fl_id = i_fl_id
	group by p.fl_id;
	COMMIT;
    
	INSERT INTO ams_ac.ams_al_flight_h
			(fl_id, fl_type_id, fl_name, fl_description,
			fle_id, flp_id, route_id, al_id, ac_id,
			d_ap_id, a_ap_id, distance_km, oil_l, pax,
			payload_kg, dtime, atime, dtime_real, atime_real,
			delay_min, expences, income, revenue, ch_id)
		select fl_id, fl_type_id, fl_name, fl_description,
			fle_id, flp_id, route_id, al_id, ac_id,
			d_ap_id, a_ap_id, distance_km, oil_l, pax,
			payload_kg, dtime, atime, dtime_real, atime_real,
			delay_min, expences, income, revenue, ch_id
		from ams_ac.ams_al_flight where fl_id = i_fl_id;
        
        COMMIT;
        SET logNote=CONCAT(' ->  FLH Payload Insert i_fl_id: ',i_fl_id, ' .');
		-- call ams_wad.log_sp(spname, stTime, logNote);
        INSERT INTO ams_ac.ams_al_flight_h_payload
			(flpl_id, fl_id, ch_id,
			payload_type, pax, cargo_kg, payload_kg,
			description, price, adate)
        SELECT flpl_id, fl_id, ch_id,
			payload_type, pax, cargo_kg, payload_kg,
			description, price, adate
        from ams_ac.ams_al_flight_payload where fl_id = i_fl_id; 
        
        COMMIT;
        
        SET i_rows = 0;
        SELECT COUNT(*) INTO i_rows FROM ams_ac.ams_al_flight_h where fl_id = i_fl_id; 
        SET logNote=CONCAT(' ->  COUNT FLH : ',i_rows, ' .');
		-- call ams_wad.log_sp(spname, stTime, logNote);
        IF i_rows > 0 THEN
        -- INSERT INTO ams_ac.ams_al_flight_h select * from ams_ac.ams_al_flight where fl_id = i_fl_id;
			UPDATE ams_ac.ams_al_flight_log l  set l.acfs_id = 11 where l.fl_log_id = in_fl_log_id;
			COMMIT;
			DELETE FROM ams_ac.ams_al_flight_log where fl_log_id = in_fl_log_id;
            COMMIT;
			DELETE FROM ams_ac.ams_al_flight where fl_id = i_fl_id;
            COMMIT;
            DELETE FROM ams_ac.ams_al_flight_payload where fl_id = i_fl_id;
			COMMIT;
			
            call ams_ac.sp_acat_park(i_ac_id); 
			
        
			SET logNote=CONCAT('Flight finished and archived: i_fl_id:',i_fl_id);
			call ams_wad.log_sp(spname, stTime, logNote);
            
            call ams_ac.sp_ac_mnt_flight_check(i_ac_id);
        END IF;
        
		IF i_rows = 0 THEN
			SET logNote=CONCAT('ERROR Can not copy flight to history fl_id: ',i_fl_id);
			call ams_wad.log_sp(spname, stTime, logNote);
        END IF;
        
	SET logNote=CONCAT('Flight i_fl_id: ', i_fl_id , ', processed.');
    
	call ams_ac.sp_acat_process(i_ac_id);
     
	CALL ams_al.sp_tr_post();
	COMMIT;
END IF;
	SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_acfs_in_flight` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acfs_in_flight`(IN in_fl_log_id INT)
BEGIN


DECLARE i_op_time_sec, i_fl_id, i_acfs_id, i_ac_id, i_bank_id_from, i_oil_l, o_tr_id  INT;
DECLARE d_oli_price DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_fl_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acfs_in_flight', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acfs_in_flight';
set countRows = 0;
set i_acfs_id = 6; -- In Flight

	SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

	CALL ams_al.sp_tr_flight_crew(in_fl_log_id, o_tr_id);

	SET logNote=CONCAT('o_tr_id :',o_tr_id,' ' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

	update ams_ac.ams_al_flight_log l 
		join ams_ac.ams_al_flight fl on fl.fl_id = l.fl_id
        join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
		join ams_al.ams_bank b on b.al_id = fl.al_id
        join ams_ac.ams_aircraft ac1 on ac1.ac_id = ac.ac_id
		join ams_ac.cfg_mac macr on macr.mac_id = ac1.mac_id
        join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id
		join ams_al.cfg_oil_price op on op.oil_price_id = 1 and 1 = 1 
		left join ams_ac.cfg_aircraft_flight_status acfsn on acfsn.acfs_id = (l.acfs_id +1)
	set l.acfs_id = acfsn.acfs_id,
		l.fl_log_description = concat(IFNULL(dep.ap_iata, dep.ap_icao),' ', acfsn.acfs_name, ' for ', fl.fl_name),
		l.fl_time_min = 0, l.fl_distance_km = 0, 
		l.op_time_sec = Round(((fl.distance_km/macr.cruise_speed_kmph)*60*60) ,0),
		l.tr_id = o_tr_id
	where l.fl_log_id = in_fl_log_id;
    COMMIT;
    update ams_ac.ams_al_flight fl
    join ams_ac.ams_al_flight_log l on l.fl_id = fl.fl_id
    set fl.dtime_real = current_timestamp() 
    where l.fl_log_id = in_fl_log_id;
	COMMIT;

	SET logNote=CONCAT('Finish ', '<-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_acfs_landing` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acfs_landing`(IN in_fl_log_id INT)
BEGIN


DECLARE i_op_time_sec, i_fl_id, i_acfs_id, i_bank_id_from, i_oil_l, o_tr_id  INT;
DECLARE i_curr_ap_id, i_ac_id, i_idx INT;
DECLARE d_oli_price DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_fl_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acfs_landing', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acfs_landing';
set countRows = 0;
set i_acfs_id = 7; -- Landing and Taxy to Gate

	SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
	call ams_wad.log_sp(spname, stTime, logNote);

	CALL ams_al.sp_tr_ac_landing(in_fl_log_id, o_tr_id);

	SET logNote=CONCAT('o_tr_id :',o_tr_id,' ' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	select fl_id into i_fl_id from ams_ac.ams_al_flight_log where fl_log_id = in_fl_log_id;
    SET logNote=CONCAT('i_fl_id :',i_fl_id,' ' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	update ams_ac.ams_al_flight_log l 
		join ams_ac.ams_al_flight fl on fl.fl_id = l.fl_id
        join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
		join ams_al.ams_bank b on b.al_id = fl.al_id
        join ams_ac.ams_aircraft ac1 on ac1.ac_id = ac.ac_id
		join ams_ac.cfg_mac macr on macr.mac_id = ac1.mac_id
        join ams_wad.cfg_airport arr on arr.ap_id = fl.a_ap_id
		join ams_al.cfg_oil_price op on op.oil_price_id = 1 and 1 = 1 
		left join ams_ac.cfg_aircraft_flight_status acfsn on acfsn.acfs_id = (l.acfs_id +1)
	set l.acfs_id = acfsn.acfs_id,
		l.fl_log_description = concat(IFNULL(arr.ap_iata, arr.ap_icao),' ', acfsn.acfs_name, ' for ', fl.fl_name),
		l.fl_time_min = 0, l.fl_distance_km = 0, 
		l.op_time_sec = Round(((ac.op_time_min*60) * ifnull(acfsn.acfs_op_time_part, 0)),0),
		l.tr_id = o_tr_id
	where l.fl_log_id = in_fl_log_id;
    commit;
    
    update ams_ac.ams_al_flight fl
    set fl.atime_real = current_timestamp(),
    fl.delay_min =  round(( (UNIX_TIMESTAMP(current_timestamp()) - if(fl.atime is null, 0, UNIX_TIMESTAMP(fl.atime)) )/60),0)   
    where fl.fl_id = i_fl_id;
    COMMIT;
    
    SET logNote=CONCAT('Updated atime_real ams_al_flight:',in_fl_log_id,' ' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    select a_ap_id, ac_id INTO i_curr_ap_id, i_ac_id from ams_ac.ams_al_flight where fl_id = i_fl_id;
    SET logNote=CONCAT(' i_curr_ap_id:',i_curr_ap_id,' i_ac_id:',i_ac_id,' ' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    set countRows = 0;
    SET i_idx = 1;
    SELECT count(*) INTO countRows
	FROM performance_schema.table_handles 
	WHERE EXTERNAL_LOCK IS NOT NULL 
	and OBJECT_SCHEMA='ams_ac' and OBJECT_NAME='ams_aircraft_curr';
	SET logNote=CONCAT(i_idx, ' ams_aircraft_curr locked:', countRows , '' );
    -- call ams_wad.log_sp(spname, stTime, logNote);
    WHILE countRows > 0 DO
		SELECT SLEEP(0.2);
        SET i_idx = i_idx + 1;
        SELECT count(*) INTO countRows
		FROM performance_schema.table_handles 
		WHERE EXTERNAL_LOCK IS NOT NULL 
		and OBJECT_SCHEMA='ams_ac' and OBJECT_NAME='ams_aircraft_curr';
    END WHILE;
    SET logNote=CONCAT(i_idx, ' ams_aircraft_curr locked:', countRows , '' );
	call ams_wad.log_sp(spname, stTime, logNote);
    UPDATE ams_ac.ams_aircraft_curr ac
	SET ac.curr_ap_id = i_curr_ap_id
	where ac.ac_id = i_ac_id;
    SET countRows =  ROW_COUNT();
    COMMIT;
    SET logNote=CONCAT('updated ', countRows ,' rows in ams_aircraft_curr 1 for ac_id ', i_ac_id , '' );
	call ams_wad.log_sp(spname, stTime, logNote);

	UPDATE ams_ac.ams_aircraft_log acl
	join ams_ac.ams_al_flight fl on fl.ac_id = acl.ac_id
	join ams_ac.ams_al_flight_log fll on fll.fl_id = fl.fl_id
    SET acl.fl_id = fl.fl_id, 
	acl.distance_km = fl.distance_km,
	acl.flight_h = round((UNIX_TIMESTAMP(current_timestamp()) - if(fl.dtime_real is null, 0, UNIX_TIMESTAMP(fl.dtime_real)))/(60*60),2) 
	where fll.fl_log_id = in_fl_log_id;
	COMMIT;

	SET logNote=CONCAT('Finish ', '<-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_acfs_refuling` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acfs_refuling`(IN in_fl_log_id INT)
BEGIN


DECLARE   i_acfs_id, o_tr_id  INT;
DECLARE d_oli_price DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_fl_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acfs_refuling', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acfs_refuling';
set countRows = 0;
set i_acfs_id = 2; -- Refuling

	SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

	CALL ams_al.sp_tr_ac_refuling(in_fl_log_id, o_tr_id);

	SET logNote=CONCAT('o_tr_id :',o_tr_id,' ' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

	update ams_ac.ams_al_flight_log l 
		join ams_ac.ams_al_flight fl on fl.fl_id = l.fl_id
        join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
		join ams_al.ams_bank b on b.al_id = fl.al_id
		join ams_al.cfg_oil_price op on op.oil_price_id = 1 and 1 = 1 
		left join ams_ac.cfg_aircraft_flight_status acfsn on acfsn.acfs_id = (l.acfs_id +1)
	set l.acfs_id = acfsn.acfs_id,
		l.fl_log_description = concat(acfsn.acfs_name, ' ', fl.oil_l, ' L fuel for ', fl.fl_name),
		l.fl_time_min = 0, l.fl_distance_km = 0, 
		l.op_time_sec = ((ac.op_time_min*60) * ifnull(acfsn.acfs_op_time_part, 0)),
		l.tr_id = o_tr_id
	where l.fl_log_id = in_fl_log_id;
	COMMIT;

	SET logNote=CONCAT('Finish ', '<-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_acfs_takeoff` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acfs_takeoff`(IN in_fl_log_id INT)
BEGIN


DECLARE i_op_time_sec, i_fl_id, i_acfs_id, i_ac_id, i_bank_id_from, i_oil_l, o_tr_id  INT;
DECLARE d_oli_price DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_fl_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acfs_takeoff', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acfs_takeoff';
set countRows = 0;
set i_acfs_id = 5; -- Taxy and Takeoff

	SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

	CALL ams_al.sp_tr_ac_takeoff(in_fl_log_id, o_tr_id);

	SET logNote=CONCAT('o_tr_id :',o_tr_id,' ' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

	update ams_ac.ams_al_flight_log l 
		join ams_ac.ams_al_flight fl on fl.fl_id = l.fl_id
        join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
		join ams_al.ams_bank b on b.al_id = fl.al_id
        join ams_wad.cfg_airport dep on dep.ap_id = fl.d_ap_id
		join ams_al.cfg_oil_price op on op.oil_price_id = 1 and 1 = 1 
		left join ams_ac.cfg_aircraft_flight_status acfsn on acfsn.acfs_id = (l.acfs_id +1)
	set l.acfs_id = acfsn.acfs_id,
		l.fl_log_description = concat(IFNULL(dep.ap_iata, dep.ap_icao),' ', acfsn.acfs_name, ' for ', fl.fl_name),
		l.fl_time_min = 0, l.fl_distance_km = 0, 
		l.op_time_sec = ((ac.op_time_min*60) * ifnull(acfsn.acfs_op_time_part, 0)),
		l.tr_id = o_tr_id
	where l.fl_log_id = in_fl_log_id;
	COMMIT;

	SET logNote=CONCAT('Finish ', '<-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_acfs_trip_to_landing` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_acfs_trip_to_landing`(IN in_fl_log_id INT)
BEGIN


DECLARE i_fl_time_left_sec, i_fl_time_min, i_fl_distance_km INT;
DECLARE countRows, i_acfs_id, i_delay_min INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_acfs_trip_to_landing', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_acfs_trip_to_landing';
set countRows = 0;
SET i_delay_min = 0;
set i_acfs_id = 6; -- In Flight

	SET logNote=CONCAT('Start (',in_fl_log_id,') ->' );
	call ams_wad.log_sp(spname, stTime, logNote);

		SELECT l.op_time_sec - (UNIX_TIMESTAMP(current_timestamp()) - if(fl.dtime_real is null, 0, UNIX_TIMESTAMP(fl.dtime_real))) as fl_time_left_sec,
		round((UNIX_TIMESTAMP(current_timestamp()) - if(fl.dtime_real is null, 0, UNIX_TIMESTAMP(fl.dtime_real)))/60,0) as fl_time_min,
		round(((UNIX_TIMESTAMP(current_timestamp()) - if(fl.dtime_real is null, 0, UNIX_TIMESTAMP(fl.dtime_real)))/(60*60)) *macr.cruise_speed_kmph,0) as fl_distance_km,
        round(
			(UNIX_TIMESTAMP(date_add( now(), interval (((fl.distance_km - l.fl_distance_km)/macr.cruise_speed_kmph)*60) minute) )
			-UNIX_TIMESTAMP(fl.atime))/60
            ,0) as delay_min
        INTO i_fl_time_left_sec, i_fl_time_min, i_fl_distance_km, i_delay_min
		FROM  ams_ac.ams_al_flight_log l 
		join ams_ac.ams_al_flight fl on fl.fl_id = l.fl_id
		join ams_ac.ams_aircraft ac1 on ac1.ac_id = fl.ac_id
		join ams_ac.cfg_mac macr on macr.mac_id = ac1.mac_id
		where l.fl_log_id=in_fl_log_id;
        
        SET logNote=CONCAT(' -> i_fl_time_min: ', i_fl_time_min, ', distance traveled ',i_fl_distance_km, ' km.');
		call ams_wad.log_sp(spname, stTime, logNote);
        
        IF i_fl_time_left_sec > 100 THEN
			UPDATE ams_ac.ams_al_flight_log l 
            set l.fl_time_min = i_fl_time_min,
            l.fl_distance_km = i_fl_distance_km,
            l.delay_min = i_delay_min
            where l.fl_log_id=in_fl_log_id;
            commit;
            
            update ams_ac.ams_aircraft_log acl
            join ams_ac.ams_al_flight fl on fl.ac_id = acl.ac_id
            join ams_ac.ams_al_flight_log fll on fll.fl_id = fl.fl_id
            set acl.fl_id = fl.fl_id, acl.distance_km = i_fl_distance_km,
            acl.duration_min = i_fl_time_min
            where fll.fl_log_id=in_fl_log_id;
            
            commit;
		ELSE
			CALL ams_ac.sp_acfs_landing(in_fl_log_id);
        END IF;
        
	
	SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ac_mnt_flight_check` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ac_mnt_flight_check`(IN in_ac_id INT)
BEGIN

DECLARE i_amt_id, i_acat_plane_stand INT;
DECLARE countRows INT;
DECLARE stTime, i_st_time TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_ac_mnt_flight_check', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_ac_mnt_flight_check';
set countRows = 0;
SET i_acat_plane_stand = 6;

SET logNote=CONCAT('Start -> ac_id:', in_ac_id );
call ams_wad.log_sp(spname, stTime, logNote);
        
select count(*) INTO countRows
FROM ams_ac.ams_aircraft_curr acc
left join  ams_ac.ams_aircraft_log acl on acl.ac_id = acc.ac_id
join ams_ac.ams_aircraft a on a.ac_id = acc.ac_id
where acc.ac_id = in_ac_id and acc.next_mnt_id is null 
and acc.flight_h_to_next_amt_id<1 and acc.curr_ap_id = a.home_ap_id
and acl.acat_id=i_acat_plane_stand; 

SET logNote=CONCAT('Check maintenance for ac_id:', in_ac_id , ' , countRows:', countRows );
call ams_wad.log_sp(spname, stTime, logNote);

	IF countRows > 0 THEN
		SET i_amt_id = 0;
        SET i_st_time = current_timestamp();
        select ac.next_amt_id INTO i_amt_id
		FROM ams_ac.ams_aircraft_curr ac where ac.ac_id = in_ac_id; 

		SET logNote=CONCAT('Start maintenance for ac_id:', in_ac_id ,', next amt_id:', i_amt_id , ' ->' );
		call ams_wad.log_sp(spname, stTime, logNote);
		
		call ams_ac.sp_ac_mnt_plan_create(in_ac_id,i_amt_id, i_st_time);
		CALL ams_ac.sp_ams_aircraft_curr_update_ac(in_ac_id);
		COMMIT;
		
		SET logNote=CONCAT('Finish ', '<-' );
		call ams_wad.log_sp(spname, stTime, logNote);
    END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ac_mnt_plan_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ac_mnt_plan_create`(IN in_ac_id INT, IN in_amt_id INT, IN in_st_time int)
BEGIN

DECLARE i_ac_status_operational, i_acat_planeStand, i_acmp_id, i_distance_km INT;
DECLARE i_fl_id, i_pax, i_ac_id INT;
DECLARE i_payload_kg DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_ac_mnt_plan_create', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_ac_mnt_plan_create';
set countRows = 0;
SET logNote=CONCAT('Start (', in_ac_id , ') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

SET i_distance_km = 0;
SET i_ac_status_operational = 2;
SET i_acat_planeStand = 6;

IF in_st_time is null then
	select UNIX_TIMESTAMP(current_timestamp()) + (op_time_min * 60) as st_time
    INTO in_st_time
	from ams_ac.ams_aircraft_curr where ac_id = in_ac_id;
END IF;

	SET logNote=CONCAT('in_st_time: ', in_st_time , ' ' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    
	INSERT INTO ams_ac.ams_aircraft_maintenance_plan
		(ac_id, amt_id, description,
		ap_id, is_home_ap, price, al_id,
		st_time, end_time)
	SELECT  ac.ac_id, mt.amt_id, 
		concat(ac.registration, ' ',  mt.amt_name) as description,
		acc.curr_ap_id as ap_id, if(ac.home_ap_id = acc.curr_ap_id, 1, 0) as is_home_ap,
		round((m.price * mt.price_facktor)* if(ac.home_ap_id = acc.curr_ap_id, 0.5, 1),2) as price,
		ac.owner_al_id, from_unixtime(in_st_time) as st_time, 
        from_unixtime((in_st_time + (mt.time_h * 60 * 60) )) as end_time
	FROM ams_ac.ams_aircraft ac
		join ams_ac.cfg_mac m on m.mac_id = ac.mac_id
		join ams_ac.ams_aircraft_curr acc on acc.ac_id = ac.ac_id
		join ams_ac.ams_aircraft_log l on l.ac_id = ac.ac_id
		join ams_wad.cfg_airport a on a.ap_id = acc.curr_ap_id
		join ams_ac.cfg_aircraft_maintenance_type mt on mt.amt_id = in_amt_id
	where ac.ac_id = in_ac_id;
    
    SET i_acmp_id = LAST_INSERT_ID();
    COMMIT;
    SET logNote=CONCAT('i_acmp_id: ', i_acmp_id , ' ' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	call ams_ac.sp_ams_aircraft_curr_update_ac(in_ac_id);
    
    SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    select i_acmp_id as acmp_id from dual;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ac_mnt_plan_create_cabin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ac_mnt_plan_create_cabin`(IN in_ac_id INT, IN in_cabin_id INT, IN in_st_time int)
BEGIN

DECLARE i_ac_status_operational, i_acat_planeStand, i_acmp_id, i_distance_km INT;
DECLARE i_fl_id, i_pax, i_ac_id, i_mnt_id INT;
DECLARE i_payload_kg DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_ac_mnt_plan_create_cabin', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_ac_mnt_plan_create_cabin';
set countRows = 0;
SET logNote=CONCAT('Start (', in_ac_id , ', ', in_cabin_id, ') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

SET i_mnt_id = 5; -- Cabin Configuration Change
SET i_distance_km = 0;
SET i_ac_status_operational = 2;
SET i_acat_planeStand = 6;

IF in_st_time is null then
	select UNIX_TIMESTAMP(current_timestamp()) + (op_time_min * 60) as st_time
    INTO in_st_time
	from ams_ac.ams_aircraft_curr where ac_id = in_ac_id;
END IF;

	SET logNote=CONCAT('in_st_time: ', in_st_time , ' ' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	INSERT INTO ams_ac.ams_aircraft_maintenance_plan
		(ac_id, amt_id, cabin_id, description,
		ap_id, is_home_ap, price, al_id,
		st_time, end_time)
	SELECT  ac.ac_id, mt.amt_id, p.in_cabin_id,
		concat(ac.registration, ' ',IFNULL(a.ap_iata, a.ap_icao), ' ',  mt.amt_name) as description,
		acc.curr_ap_id as ap_id, if(ac.home_ap_id = acc.curr_ap_id, 1, 0) as is_home_ap,
		round(((m.price * mt.price_facktor)+(c.price))* if(ac.home_ap_id = acc.curr_ap_id, 0.5, 1),2) as price,
		ac.owner_al_id, from_unixtime(p.in_st_time) as st_time, 
        from_unixtime((p.in_st_time + (mt.time_h * 60 * 60) )) as end_time
	FROM ams_ac.ams_aircraft ac
    join (select in_st_time as in_st_time, 
		in_ac_id as in_ac_id, i_mnt_id as i_amt_id, in_cabin_id as in_cabin_id from dual) p on 1=1
		join ams_ac.cfg_mac m on m.mac_id = ac.mac_id
		join ams_ac.ams_aircraft_curr acc on acc.ac_id = ac.ac_id
		join ams_ac.ams_aircraft_log l on l.ac_id = ac.ac_id
		join ams_wad.cfg_airport a on a.ap_id = acc.curr_ap_id
		join ams_ac.cfg_aircraft_maintenance_type mt on mt.amt_id = p.i_amt_id
        join ams_ac.cfg_mac_cabin c on c.cabin_id = p.in_cabin_id
	where ac.ac_id = p.in_ac_id ;

    SET i_acmp_id = LAST_INSERT_ID();
    COMMIT;
    SET logNote=CONCAT('i_acmp_id: ', i_acmp_id , ' ' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	call ams_ac.sp_ams_aircraft_curr_update_ac(in_ac_id);
    
    SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    select i_acmp_id as acmp_id from dual;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ac_mnt_plan_finish` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ac_mnt_plan_finish`()
BEGIN

DECLARE i_acmp_id, i_ac_id, i_amt_id, i_ac_status_operational, i_acat_planeStand, o_tr_id INT;
DECLARE i_fl_id, i_pax INT;
DECLARE i_payload_kg DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_mt CURSOR FOR SELECT mntp.acmp_id,  mntp.ac_id, mntp.amt_id
	FROM ams_ac.ams_aircraft_maintenance_plan mntp
    join ams_ac.ams_aircraft_log l on l.ac_id = mntp.ac_id
	join ams_ac.ams_aircraft ac on ac.ac_id = mntp.ac_id
	where l.processed = 0 and ac.ac_status_id = 2 and l.acat_id = 31
	and ((UNIX_TIMESTAMP(l.udate )+(l.duration_min * 60)) - UNIX_TIMESTAMP(current_timestamp()))<30;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;


DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_ac_mnt_plan_finish', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_ac_mnt_plan_finish';
set countRows = 0;
SET i_ac_status_operational = 2;
SET i_acat_planeStand = 6;

SET logNote=CONCAT('Start (' , ') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_mt;
read_loop: LOOP
		FETCH cursor_mt INTO i_acmp_id, i_ac_id, i_amt_id;
	
	IF done THEN
		SET logNote=CONCAT('No ac in maintenance found ', 'done' );
		-- call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
	
	SET logNote=CONCAT('i_acmp_id: ',i_acmp_id, ', i_ac_id: ',i_ac_id, ', type: ',i_amt_id,  ' amt_id ...');
	call ams_wad.log_sp(spname, stTime, logNote);
    
    call ams_al.sp_tr_ac_maintenance(i_acmp_id, o_tr_id);
    
    SET logNote=CONCAT('Transaction o_tr_id: ',ifnull(o_tr_id,'todo'),' ' );
	call ams_wad.log_sp(spname, stTime, logNote);
    UPDATE ams_ac.ams_aircraft_maintenance_plan set tr_id = o_tr_id where acmp_id = i_acmp_id;
	UPDATE ams_ac.ams_aircraft_log l set l.tr_id = o_tr_id where l.acmp_id=i_acmp_id;
    
    update ams_ac.ams_aircraft_curr acc 
    join ams_ac.ams_aircraft_maintenance_plan mntp on mntp.ac_id = acc.ac_id
    join ams_ac.cfg_aircraft_maintenance_type mta on mta.amt_id = 1
	join ams_ac.cfg_aircraft_maintenance_type mtb on mtb.amt_id = 2
	join ams_ac.cfg_aircraft_maintenance_type mtc on mtc.amt_id = 3
	join ams_ac.cfg_aircraft_maintenance_type mtd on mtd.amt_id = 4
    set acc.a_check_fh = round(IF(mntp.amt_id >= 1, acc.flight_hours,acc.a_check_fh) + (IF(mntp.amt_id >= 1, 1,0)*mta.period_h),0),
    acc.b_check_fh = round(IF(mntp.amt_id >= 2, acc.flight_hours,acc.b_check_fh) + (IF(mntp.amt_id >= 2, 1,0)*mtb.period_h),0),
    acc.c_check_fh = round(IF(mntp.amt_id >= 3, acc.flight_hours,acc.c_check_fh) + (IF(mntp.amt_id >= 3, 1,0)*mtc.period_h),0),
    acc.d_check_fh = round(IF(mntp.amt_id = 4, acc.flight_hours,acc.d_check_fh) + (IF(mntp.amt_id = 4, 1,0)*mtd.period_h),0),
    acc.after_check_condition = round((100 - ifnull(acc.a_check_fh, 100)/1000), 2),
    acc.next_amt_id = (
	case when ((ifnull(acc.flight_hours,0)+mta.period_h) <= round(IF(mntp.amt_id >= 1, acc.flight_hours,acc.a_check_fh) + (IF(mntp.amt_id >= 1, 1,0)*mta.period_h),0)) then 1 
		when ((ifnull(acc.flight_hours,0)+mta.period_h) >  round(IF(mntp.amt_id >= 1, acc.flight_hours,acc.a_check_fh) + (IF(mntp.amt_id >= 1, 1,0)*mta.period_h),0) 
			and ((ifnull(acc.flight_hours,0)+mta.period_h) <= round(IF(mntp.amt_id >= 2, acc.flight_hours,acc.b_check_fh) + (IF(mntp.amt_id >= 2, 1,0)*mtb.period_h),0))) then 2 
		when ((ifnull(acc.flight_hours,0)+mta.period_h) > round(IF(mntp.amt_id >= 2, acc.flight_hours,acc.b_check_fh) + (IF(mntp.amt_id >= 2, 1,0)*mtb.period_h),0) 
			and ((ifnull(acc.flight_hours,0)+mta.period_h) <= round(IF(mntp.amt_id >= 3, acc.flight_hours,acc.c_check_fh) + (IF(mntp.amt_id >= 3, 1,0)*mtc.period_h),0))) then 3 
		when (((ifnull(acc.flight_hours,0)+mta.period_h) > round(IF(mntp.amt_id >= 3, acc.flight_hours,acc.c_check_fh) + (IF(mntp.amt_id >= 3, 1,0)*mtc.period_h),0)) 
			and (ifnull(acc.flight_hours,0)+mta.period_h) <= round(IF(mntp.amt_id = 4, acc.flight_hours,acc.d_check_fh) + (IF(mntp.amt_id = 4, 1,0)*mtd.period_h),0)) then 4 
	end
	)
    where mntp.acmp_id=i_acmp_id;
    SET countRows =  ROW_COUNT();
	COMMIT;
    SET logNote=CONCAT('Updated ',countRows,' row in ams_aircraft_curr after maintenance.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    -- Cabin Configuration Change
    IF i_amt_id = 5 THEN 
		UPDATE ams_ac.ams_aircraft a
        join ams_ac.ams_aircraft_maintenance_plan mntp on mntp.ac_id = a.ac_id
        join ams_ac.cfg_mac_cabin c on c.cabin_id = mntp.cabin_id
        set a.cabin_id = mntp.cabin_id,
        a.crew_count = c.seat_crew_count,
        a.pax_f = c.seat_f_count,
        a.pax_b = c.seat_b_count,
        a.pax_e = c.seat_e_count,
        a.cargo_kg = c.cargo_kg
        where mntp.acmp_id=i_acmp_id; 
        
        SET countRows =  ROW_COUNT();
		COMMIT;
        SET logNote=CONCAT('Updated ',countRows,' row in ams_aircraft after cabin layout change.' );
		call ams_wad.log_sp(spname, stTime, logNote);
    END IF;
	call ams_ac.sp_acat_process(i_ac_id);
    
    insert into ams_ac.ams_aircraft_maintenance_plan_h 
    select * from ams_ac.ams_aircraft_maintenance_plan
    where acmp_id=i_acmp_id;
    COMMIT;
    
    delete from ams_ac.ams_aircraft_maintenance_plan
    where acmp_id=i_acmp_id;
    COMMIT;
    
    call ams_ac.sp_acat_park(i_ac_id);
        
	SET logNote=CONCAT('Maintenance finished and archived AC: ',i_ac_id);
	call ams_wad.log_sp(spname, stTime, logNote);
    
    SET countRows = countRows+1;
    
 END LOOP;
CLOSE cursor_mt;

IF countRows > 0 THEN
	SET logNote=CONCAT('Finished  ', countRows, ' meintenance plan records.');
	call ams_wad.log_sp(spname, stTime, logNote);
END IF;

SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ac_mnt_plan_start` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ac_mnt_plan_start`()
BEGIN

DECLARE i_acmp_id, i_ac_id, i_amt_id, i_ac_status_operational, i_acat_planeStand INT;
DECLARE i_fl_id, i_pax INT;
DECLARE i_payload_kg DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_mt CURSOR FOR SELECT mntp.acmp_id,  mntp.ac_id, mntp.amt_id
	FROM ams_ac.ams_aircraft_maintenance_plan mntp
	join ams_ac.ams_aircraft ac on ac.ac_id = mntp.ac_id
	join ams_ac.ams_aircraft_log l on l.ac_id = mntp.ac_id
	where mntp.processed = 0 and ac.ac_status_id = 2 and l.acat_id = 6
	and ((if(mntp.st_time is null, 0, UNIX_TIMESTAMP(mntp.st_time))) - UNIX_TIMESTAMP(current_timestamp()))<180;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;


DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_ac_mnt_plan_start', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_ac_mnt_plan_start';
set countRows = 0;
SET i_ac_status_operational = 2;
SET i_acat_planeStand = 6;

SET logNote=CONCAT('Start (' , ') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_mt;
read_loop: LOOP
		FETCH cursor_mt INTO i_acmp_id, i_ac_id, i_amt_id;
	
	IF done THEN
		SET logNote=CONCAT('No maintenance in queue found ', 'done' );
		-- call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
	
	SET logNote=CONCAT('i_acmp_id: ',i_acmp_id, ', i_ac_id: ',i_ac_id, ', type: ',i_amt_id,  ' amt_id ...');
	call ams_wad.log_sp(spname, stTime, logNote);
    
  
	update ams_ac.ams_aircraft_maintenance_plan mtp
	join ams_ac.ams_aircraft ac on ac.ac_id = mtp.ac_id
	join ams_ac.cfg_mac m on m.mac_id = ac.mac_id
	join ams_ac.ams_aircraft_curr acc on acc.ac_id = ac.ac_id
    join ams_wad.cfg_airport ap on ap.ap_id = acc.curr_ap_id
	join ams_ac.cfg_aircraft_maintenance_type mt on mt.amt_id = mtp.amt_id
	set mtp.ap_id = acc.curr_ap_id,
    mtp.description = concat(mtp.description, ' ',if(ac.home_ap_id = acc.curr_ap_id, '50% discount for maintenance at home airport.', 'maintenance.') ),
	mtp.is_home_ap = if(ac.home_ap_id = acc.curr_ap_id, 1, 0),
	mtp.price = round((m.price * mt.price_facktor)* if(ac.home_ap_id = acc.curr_ap_id, 0.5, 1),2)
    where mtp.acmp_id = i_acmp_id;
    
    commit;
    SET logNote=CONCAT('Updated price and airport for maintenance: ',i_acmp_id,'.');
	call ams_wad.log_sp(spname, stTime, logNote);

    call ams_ac.sp_acat_mnt_check(i_ac_id, i_acmp_id);
    
    update ams_ac.ams_aircraft_maintenance_plan 
    set processed=1 where acmp_id = i_acmp_id;
    COMMIT;
    
    CALL ams_ac.sp_ams_aircraft_curr_update_ac(i_ac_id);
    SET countRows = countRows+1;
    
 END LOOP;
CLOSE cursor_mt;
IF countRows>0 THEN
	SET logNote=CONCAT('Processed  ', countRows, ' meintenance plan records.');
	call ams_wad.log_sp(spname, stTime, logNote);
END IF;
	
SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_create`()
BEGIN

DECLARE i_flpns_id, i_fl_type_id, i_flp_id INT;
DECLARE countRows, i_flpns_count, i_day, i_yw INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_flpns CURSOR FOR SELECT flpns.flpns_id
				FROM ams_al.ams_al_flp_group g
				join ams_al.ams_al_flp_number_schedule flpns on flpns.grp_id = g.grp_id
				join (SELECT param_value FROM ams_al.cfg_callc_param where param_name='flight_plan_add_days_in_advance' ) days on 1=1
				left join ams_ac.ams_al_flight_plan flp on flp.flpns_id = flpns.flpns_id and flp.week = WEEKOFYEAR(ADDDATE(curdate(),days.param_value)) 
				where g.ac_id is not null and g.is_active<>0 and g.start_date < now() and 
					flpns.wd_id = DAYOFWEEK(ADDDATE(curdate(),days.param_value)) and flp.flp_id is null
				order by flpns.dtime_h, flpns.dtime_min, flpns.atime_h, flpns.atime_min limit 10;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_create', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_create';
set countRows = 0;
set i_flpns_count = 0;
set i_fl_type_id = 1; -- Schedule
set i_flpns_id = 0;

	SELECT DAYOFWEEK(ADDDATE(curdate(),days.param_value)) as i_day, 
		WEEKOFYEAR(ADDDATE(curdate(),days.param_value)) as i_yw 
        INTO i_day, i_yw
    from (SELECT param_value FROM ams_al.cfg_callc_param where param_name='flight_plan_add_days_in_advance' ) days;
    
SET logNote=CONCAT('Start () -> day:', i_day, ', week: ', i_yw);
call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_flpns;

read_loop: LOOP
	
    FETCH cursor_flpns INTO i_flpns_id;
	
	IF done THEN
		SET logNote=CONCAT('No flpns_id in queue found ', 'done' );
		 call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
	
	SET logNote=CONCAT(' i_flpns_id: ',i_flpns_id, ' prepare...');
	call ams_wad.log_sp(spname, stTime, logNote);
	
    INSERT INTO ams_ac.ams_al_flight_plan
	(week, fl_type_id, flpns_id, fl_name, fl_number,
	route_id, dest_id, al_id, ac_id, d_ap_id, a_ap_id, distance_km,
	oil_l, flight_h, dtime, atime, available_pax_f, available_pax_b, available_pax_e, available_cargo_kg)
    SELECT WEEKOFYEAR(ADDDATE(curdate(),days.param_value)) as week, 
		i_fl_type_id as fl_type_id, 
		flpns.flpns_id, 
        concat(a.iata, ' ',  LPAD(flpn.flpn_number, 4, '0'), ' ',
		ifnull(dep.ap_iata,  dep.ap_icao) ,' ',
		ifnull(arr.ap_iata, arr.ap_icao) ) as fl_name,
        flpn.flpn_number, flpn.route_id, flpn.dest_id, flpns.al_id, g.ac_id,
        flpn.dep_ap_id, flpn.arr_ap_id, d.distance_km, 
        ROUND((d.distance_km /100)* macr.fuel_consumption_lp100km,2) as oil_l,
        Round(((d.distance_km/macr.cruise_speed_kmph)),2) as flight_h,
        DATE_ADD( date_format(ADDDATE(curdate(),days.param_value), '%Y-%m-%d 00:00:00') ,INTERVAL (( flpns.dtime_h*60) + flpns.dtime_min) MINUTE)  as dtime,
        DATE_ADD( date_format(ADDDATE(curdate(),days.param_value), '%Y-%m-%d 00:00:00') ,INTERVAL (( (flpn.flight_h + flpns.dtime_h)*60) + flpns.dtime_min) MINUTE)  as atime,
        -- DATE_ADD( date_format(ADDDATE(curdate(),days.param_value), '%Y-%m-%d 00:00:00') ,INTERVAL (( flpns.atime_h*60) + flpns.atime_min) MINUTE)  as atime,
        ac.pax_f as available_pax_f, ac.pax_b as available_pax_b, ac.pax_e as available_pax_e, ac.cargo_kg as available_cargo_kg
	FROM ams_al.ams_al_flp_number_schedule flpns
	join ams_al.ams_al_flp_group g on g.grp_id = flpns.grp_id
	join ams_al.ams_al_flp_number flpn on flpn.flpn_id = flpns.flpn_id
	join ams_al.ams_airline a on a.al_id = flpns.al_id
	join ams_ac.ams_aircraft ac on ac.ac_id = g.ac_id
	join ams_ac.cfg_mac macr on ac.mac_id = macr.mac_id
	left join ams_wad.cfg_airport_destination d on d.dest_id = flpn.dest_id
	join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
	join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id
	join (SELECT param_value FROM ams_al.cfg_callc_param where param_name='flight_plan_add_days_in_advance' ) days on 1=1
	where flpns.flpns_id = i_flpns_id;
	
    SET i_flp_id = LAST_INSERT_ID();
    commit;
    SET logNote=CONCAT('inserted i_flp_id: ', i_flp_id);
	call ams_wad.log_sp(spname, stTime, logNote);
    
    call ams_ac.sp_al_flight_plan_create_add_transfer(i_flp_id);
    
	SET i_flpns_count = i_flpns_count+1;
        
	
 END LOOP;
 COMMIT;
 
 CALL ams_ac.sp_al_flight_plan_transfer_clean();

CLOSE cursor_flpns;
COMMIT;

SET logNote=CONCAT('inserted i_flpns_count ', i_flpns_count);
call ams_wad.log_sp(spname, stTime, logNote);

update ams_ac.ams_al_flight_plan flp
left JOIN 
	(select flp_id, 
	SUM(if(pax is null, 0, if(pax_class_id=1, pax, 0))) as load_pax_e,
	SUM(if(pax is null, 0, if(pax_class_id=2, pax, 0))) as load_pax_b,
	SUM(if(pax is null, 0, if(pax_class_id=3, pax, 0))) as load_pax_f,
	SUM(if(payload_kg is null, 0, if(pax_class_id=4, payload_kg, 0))) as load_cargo_kg
	FROM ams_ac.ams_al_flight_plan_payload group by flp_id) p ON p.flp_id = flp.flp_id
set flp.remain_pax_e = (flp.available_pax_e - ifnull(p.load_pax_e, 0)),
flp.remain_pax_b = (flp.available_pax_b - ifnull(p.load_pax_b, 0)),
flp.remain_pax_f = (flp.available_pax_f - ifnull(p.load_pax_f,0)),
flp.remain_payload_kg = Round((flp.available_cargo_kg - ifnull(p.load_cargo_kg,0)),2);
commit;

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_create_02` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_create_02`()
BEGIN

DECLARE i_flpns_id, i_fl_type_id, i_flp_id INT;
DECLARE i_week, i_wd_id, i_wd_diff INT;
DECLARE countRows, i_flpns_count, i_day, i_yw INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_flpns CURSOR FOR  SELECT flpns.flpns_id, 
				WEEKOFYEAR(ADDDATE(curdate(),abs(flpns.wd_id - DAYOFWEEK(curdate())))) as i_week, flpns.wd_id,
				-- DATE_ADD( date_format(ADDDATE(curdate(),if((flpns.wd_id - DAYOFWEEK(curdate()))>=0,(flpns.wd_id - DAYOFWEEK(curdate())), (7+(flpns.wd_id - DAYOFWEEK(curdate()))))), '%Y-%m-%d 00:00:00') ,INTERVAL (( flpns.dtime_h*60) + flpns.dtime_min) MINUTE)  as dtime,
				if(
                 (flpns.wd_id - DAYOFWEEK(curdate()))>=0,
                 (flpns.wd_id - DAYOFWEEK(curdate())), (7+(flpns.wd_id - DAYOFWEEK(curdate())))) wd_diff 
				FROM ams_al.ams_al_flp_group g
				join ams_al.ams_al_flp_number_schedule flpns on flpns.grp_id = g.grp_id
                join ams_al.ams_al_flp_number flpn on flpn.flpn_id = flpns.flpn_id
				join (SELECT param_value FROM ams_al.cfg_callc_param where param_name='flight_plan_add_days_in_advance' ) days on 1=1
				left join ams_ac.ams_al_flight_plan flp on flp.flpns_id = flpns.flpns_id and DAYOFWEEK(flp.dtime) = flpns.wd_id
				where g.ac_id is not null and g.is_active<>0 and g.start_date < now()
					and if(
                 (flpns.wd_id - DAYOFWEEK(curdate()))>=0,
                 (flpns.wd_id - DAYOFWEEK(curdate())), (7+(flpns.wd_id - DAYOFWEEK(curdate())))) >= 0
					and (
                    DATE_ADD(now(),INTERVAL 36 HOUR) < DATE_ADD( date_format(ADDDATE(curdate(),if(
                 (flpns.wd_id - DAYOFWEEK(curdate()))>=0,
                 (flpns.wd_id - DAYOFWEEK(curdate())), (7+(flpns.wd_id - DAYOFWEEK(curdate()))))), '%Y-%m-%d 00:00:00') ,INTERVAL (( flpns.dtime_h*60) + flpns.dtime_min) MINUTE)
					and DATE_ADD( date_format(ADDDATE(curdate(),if(
                 (flpns.wd_id - DAYOFWEEK(curdate()))>=0,
                 (flpns.wd_id - DAYOFWEEK(curdate())), (7+(flpns.wd_id - DAYOFWEEK(curdate()))))), '%Y-%m-%d 00:00:00') ,INTERVAL (( flpns.dtime_h*60) + flpns.dtime_min) MINUTE) < DATE_ADD(now(),INTERVAL days.param_value Day)
                    ) and flp.flp_id is null
				order by DATE_ADD( date_format(ADDDATE(curdate(),if(
                 (flpns.wd_id - DAYOFWEEK(curdate()))>=0,
                 (flpns.wd_id - DAYOFWEEK(curdate())), (7+(flpns.wd_id - DAYOFWEEK(curdate()))))), '%Y-%m-%d 00:00:00') ,INTERVAL (( flpns.dtime_h*60) + flpns.dtime_min) MINUTE) , 
					flpns.dtime_h, flpns.dtime_min, flpns.atime_h, flpns.atime_min  limit 10;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_create', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_create';
set countRows = 0;
set i_flpns_count = 0;
set i_fl_type_id = 1; -- Schedule
set i_flpns_id = 0;

	SELECT DAYOFWEEK(ADDDATE(curdate(),days.param_value)) as i_day, 
		WEEKOFYEAR(ADDDATE(curdate(),days.param_value)) as i_yw 
        INTO i_day, i_yw
    from (SELECT param_value FROM ams_al.cfg_callc_param where param_name='flight_plan_add_days_in_advance' ) days;
    
SET logNote=CONCAT('Start () -> day:', i_day, ', week: ', i_yw);
call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_flpns;

read_loop: LOOP
	
    FETCH cursor_flpns INTO i_flpns_id, i_week, i_wd_id, i_wd_diff;
	
	IF done THEN
		SET logNote=CONCAT('No flpns_id in queue found ', 'done' );
		 call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
	
	SET logNote=CONCAT(' i_flpns_id: ',i_flpns_id, ', wd_id: ',i_wd_id, ', wd_diff: ',i_wd_diff,' prepare...');
	call ams_wad.log_sp(spname, stTime, logNote);
	
    INSERT INTO ams_ac.ams_al_flight_plan
	(week, fl_type_id, flpns_id, fl_name, fl_number,
	route_id, dest_id, al_id, ac_id, d_ap_id, a_ap_id, distance_km,
	oil_l, flight_h, dtime, atime, available_pax_f, available_pax_b, available_pax_e, available_cargo_kg)
    
SELECT p.weekOfYear as week, 
		i_fl_type_id as fl_type_id, 
		flpns.flpns_id, 
        concat(a.iata, ' ',  LPAD(flpn.flpn_number, 4, '0'), ' ',
		ifnull(dep.ap_iata,  dep.ap_icao) ,' ',
		ifnull(arr.ap_iata, arr.ap_icao) ) as fl_name,
        flpn.flpn_number, flpn.route_id, flpn.dest_id, flpns.al_id, g.ac_id,
        flpn.dep_ap_id, flpn.arr_ap_id, d.distance_km, 
        ROUND((d.distance_km /100)* macr.fuel_consumption_lp100km,2) as oil_l,
        Round(((d.distance_km/macr.cruise_speed_kmph)),2) as flight_h,
        DATE_ADD( date_format(ADDDATE(curdate(),p.wd_diff), '%Y-%m-%d 00:00:00') ,INTERVAL (( flpns.dtime_h*60) + flpns.dtime_min) MINUTE) as dtime,
        DATE_ADD( date_format(ADDDATE(curdate(),p.wd_diff), '%Y-%m-%d 00:00:00') ,INTERVAL (( (flpn.flight_h + flpns.dtime_h)*60) + flpns.dtime_min) MINUTE)  as atime,
        ac.pax_f as available_pax_f, ac.pax_b as available_pax_b, ac.pax_e as available_pax_e, ac.cargo_kg as available_cargo_kg
	FROM ams_al.ams_al_flp_number_schedule flpns
    join (select i_flpns_id as flpns_id, i_week as weekOfYear, i_wd_id as wd_id, i_wd_diff as wd_diff from dual) p on p.flpns_id=flpns.flpns_id
	join ams_al.ams_al_flp_group g on g.grp_id = flpns.grp_id
	join ams_al.ams_al_flp_number flpn on flpn.flpn_id = flpns.flpn_id
	join ams_al.ams_airline a on a.al_id = flpns.al_id
	join ams_ac.ams_aircraft ac on ac.ac_id = g.ac_id
	join ams_ac.cfg_mac macr on ac.mac_id = macr.mac_id
	left join ams_wad.cfg_airport_destination d on d.dest_id = flpn.dest_id
	join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
	join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id
	join (SELECT param_value FROM ams_al.cfg_callc_param where param_name='flight_plan_add_days_in_advance' ) days on 1=1;
	
    SET i_flp_id = LAST_INSERT_ID();
    
    SET logNote=CONCAT('inserted i_flp_id: ', i_flp_id);
	call ams_wad.log_sp(spname, stTime, logNote);
    
	SET i_flpns_count = i_flpns_count+1;
        
	
 END LOOP;
 
CLOSE cursor_flpns;
COMMIT;

SET logNote=CONCAT('inserted i_flpns_count ', i_flpns_count);
call ams_wad.log_sp(spname, stTime, logNote);

update ams_ac.ams_al_flight_plan flp
left JOIN 
	(select flp_id, 
	SUM(if(pax is null, 0, if(pax_class_id=1, pax, 0))) as load_pax_e,
	SUM(if(pax is null, 0, if(pax_class_id=2, pax, 0))) as load_pax_b,
	SUM(if(pax is null, 0, if(pax_class_id=3, pax, 0))) as load_pax_f,
	SUM(if(payload_kg is null, 0, if(pax_class_id=4, payload_kg, 0))) as load_cargo_kg
	FROM ams_ac.ams_al_flight_plan_payload group by flp_id) p ON p.flp_id = flp.flp_id
set flp.remain_pax_e = (flp.available_pax_e - ifnull(p.load_pax_e, 0)),
flp.remain_pax_b = (flp.available_pax_b - ifnull(p.load_pax_b, 0)),
flp.remain_pax_f = (flp.available_pax_f - ifnull(p.load_pax_f,0)),
flp.remain_payload_kg = Round((flp.available_cargo_kg - ifnull(p.load_cargo_kg,0)),2);
commit;

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_create_add_transfer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_create_add_transfer`(IN in_flp_id INT)
BEGIN

DECLARE countRows, totalRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote, sqlScr, qry VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_create_add_transfer', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_create_add_transfer';
set countRows = 0;
set totalRows = 0;

SET logNote=CONCAT('Start ', ' ->' );
call ams_wad.log_sp(spname, stTime, logNote);


select count(*) INTO countRows 
from ams_ac.ams_al_flight_plan_transfer
where (flp_id_1 = in_flp_id	or flp_id_2 = in_flp_id	or flp_id_3 = in_flp_id	or flp_id_4 = in_flp_id) for share;
	
IF countRows > 0 THEN
	SET logNote=CONCAT('Found ', FORMAT(countRows, 0), ' rows for in_flp_id: ',in_flp_id,' in flight_plan_transfer table to delete' );
	call ams_wad.log_sp(spname, stTime, logNote);
     
	delete from ams_ac.ams_al_flight_plan_transfer
	where (flp_id_1 = in_flp_id	or flp_id_2 = in_flp_id	or flp_id_3 = in_flp_id	or flp_id_4 = in_flp_id);
    SET countRows =  ROW_COUNT();
    commit;
    SET logNote=CONCAT('Deleted ', FORMAT(countRows, 0), ' rows from flight_plan_transfer table.' );
	call ams_wad.log_sp(spname, stTime, logNote);
END IF;
	
	SET logNote=CONCAT(' in_flp_id: ',in_flp_id, ' .');
	call ams_wad.log_sp(spname, stTime, logNote);
	
    INSERT INTO ams_ac.ams_al_flight_plan_transfer
		(flp_id_1, flp_id_2, 
		d_ap_id, a_ap_id, dest_id, 
		fl_name_1, fl_name_2,
        tr_time_min_1,
		distance_km, flight_h)
    (SELECT flp2.flp_id as flp_id_1,  flp1.flp_id as flp_id_2, 
		d.dep_ap_id as d_ap_id, d.arr_ap_id as a_ap_id,
		d.dest_id,
		flp2.fl_name as fl_name_1,  flp1.fl_name as fl_name_2, 
		TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) as tr_time_min_1,
		(ifnull(flp1.distance_km,0) + ifnull(flp2.distance_km,0)) as distance_km,
		round((ifnull(flp1.flight_h,0) + ifnull(flp2.flight_h,0)),2) as flight_h
	FROM ams_ac.ams_al_flight_plan flp1
		JOIN ams_al.ams_al_flp_number_schedule flpns1 ON flpns1.flpns_id = flp1.flpns_id
		left JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.a_ap_id = flp1.d_ap_id)) and flp2.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) > 19)
		join ams_wad.cfg_airport_destination d on d.dep_ap_id = flp2.d_ap_id and d.arr_ap_id = flp1.a_ap_id
	where flp1.flp_id = in_flp_id FOR SHARE);
	SET countRows =  ROW_COUNT();
    set totalRows = totalRows + countRows;
    commit;
    SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' double transfers.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    SET countRows = 0;
    INSERT INTO ams_ac.ams_al_flight_plan_transfer
		(flp_id_1, flp_id_2, flp_id_3,
		d_ap_id, a_ap_id, dest_id, 
		fl_name_1, fl_name_2, fl_name_3,
        tr_time_min_1, tr_time_min_2,
		distance_km, flight_h)
    (SELECT flp3.flp_id as flp_id_1, flp2.flp_id as flp_id_2,  flp1.flp_id as flp_id_3, 
		d.dep_ap_id as d_ap_id, d.arr_ap_id as a_ap_id,
		d.dest_id,
		flp3.fl_name as fl_name_1, flp2.fl_name as fl_name_2,  flp1.fl_name as fl_name_3, 
		TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) as tr_time_min_1,
        TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) as tr_time_min_2,
		(ifnull(flp3.distance_km,0) + ifnull(flp2.distance_km,0) + ifnull(flp1.distance_km,0) ) as distance_km,
		round((ifnull(flp3.flight_h,0) + ifnull(flp2.flight_h,0) + ifnull(flp1.flight_h,0)),2) as flight_h
	FROM ams_ac.ams_al_flight_plan flp1
		left JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.a_ap_id = flp1.d_ap_id)) and flp2.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) > 19)
        left JOIN ams_ac.ams_al_flight_plan flp3 ON ((flp3.a_ap_id = flp2.d_ap_id)) and flp3.d_ap_id <> flp2.a_ap_id and flp3.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) > 19)
		join ams_wad.cfg_airport_destination d on d.dep_ap_id = flp3.d_ap_id and d.arr_ap_id = flp1.a_ap_id
        where flp1.flp_id = in_flp_id FOR SHARE);
		SET countRows =  ROW_COUNT();
		set totalRows = totalRows + countRows;
		commit;
		SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' triple transfers.' );
		call ams_wad.log_sp(spname, stTime, logNote);
        
		SET countRows = 0;
        INSERT INTO ams_ac.ams_al_flight_plan_transfer
			(flp_id_1, flp_id_2, flp_id_3, flp_id_4,
			d_ap_id, a_ap_id, dest_id, 
			fl_name_1, fl_name_2, fl_name_3, fl_name_4,
			tr_time_min_1, tr_time_min_2, tr_time_min_3,
			distance_km, flight_h)
		(SELECT flp4.flp_id as flp_id_1, flp3.flp_id as flp_id_2, flp2.flp_id as flp_id_3,  flp1.flp_id as flp_id_4, 
			d.dep_ap_id as d_ap_id, d.arr_ap_id as a_ap_id,
			d.dest_id,
			flp4.fl_name as fl_name_1, flp3.fl_name as fl_name_2, flp2.fl_name as fl_name_3,  flp1.fl_name as fl_name_4, 
			TIMESTAMPDIFF(MINUTE, flp4.atime, flp3.dtime) as tr_time_min_1,
			TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) as tr_time_min_2,
			TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) as tr_time_min_3,
			(ifnull(flp4.distance_km,0) + ifnull(flp3.distance_km,0) + ifnull(flp2.distance_km,0) + ifnull(flp1.distance_km,0) ) as distance_km,
			round((ifnull(flp4.flight_h,0) +ifnull(flp3.flight_h,0) + ifnull(flp2.flight_h,0) + ifnull(flp1.flight_h,0)),2) as flight_h
		FROM ams_ac.ams_al_flight_plan flp1
			left JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.a_ap_id = flp1.d_ap_id)) and flp2.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) > 19)
			left JOIN ams_ac.ams_al_flight_plan flp3 ON ((flp3.a_ap_id = flp2.d_ap_id)) and flp3.d_ap_id <> flp2.a_ap_id and flp3.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) > 19)
			left JOIN ams_ac.ams_al_flight_plan flp4 ON ((flp4.a_ap_id = flp3.d_ap_id)) and flp4.d_ap_id <> flp3.a_ap_id and flp4.d_ap_id <> flp2.a_ap_id and flp4.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp4.atime, flp3.dtime) > 19)
			join ams_wad.cfg_airport_destination d on d.dep_ap_id = flp4.d_ap_id and d.arr_ap_id = flp1.a_ap_id
        where flp1.flp_id = in_flp_id FOR SHARE);
		SET countRows =  ROW_COUNT();
		set totalRows = totalRows + countRows;
		commit;
		SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' quadruple transfers.' );
		call ams_wad.log_sp(spname, stTime, logNote);
    
    SET logNote=CONCAT(FORMAT(totalRows, 0), ' transfers for flp_id ', in_flp_id, '.' );
	call ams_wad.log_sp(spname, stTime, logNote);
     
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_create_add_transfer_all` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_create_add_transfer_all`()
BEGIN

DECLARE i_flp_id INT;
DECLARE countRows, i_row_count INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_flpns CURSOR FOR SELECT flp_id 
					FROM ams_ac.ams_al_flight_plan
					where flp_status_id <2;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_create_add_transfer_all', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_create_add_transfer_all';
set countRows = 0;
set i_flp_id = 0;
set i_row_count = 0;

SET logNote=CONCAT('Start () ');
call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_flpns;

read_loop: LOOP
	
    FETCH cursor_flpns INTO i_flp_id;
	
	IF done THEN
		SET logNote=CONCAT('No i_flp_id in queue found ', 'done' );
		 call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
	SET i_row_count = i_row_count+1;
    call sp_al_flight_plan_create_add_transfer(i_flp_id);
    
	SET logNote=CONCAT(FORMAT(i_row_count, 0), ' transfer added for i_flp_id: ',i_flp_id, '.');
	call ams_wad.log_sp(spname, stTime, logNote);
        
 END LOOP;
 
CLOSE cursor_flpns;
COMMIT;

SET logNote=CONCAT('Pricessed', FORMAT(i_row_count, 0),' flp rows. ');
call ams_wad.log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_create_add_transfer_v01` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_create_add_transfer_v01`(IN in_flp_id INT)
BEGIN

DECLARE countRows, totalRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote, sqlScr, qry VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_create_add_transfer', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_create_add_transfer';
set countRows = 0;
set totalRows = 0;

SET logNote=CONCAT('Start ', ' ->' );
call ams_wad.log_sp(spname, stTime, logNote);


select count(*) INTO countRows 
from ams_ac.ams_al_flight_plan_transfer
where (flp_id_1 = in_flp_id	or flp_id_2 = in_flp_id	or flp_id_3 = in_flp_id	or flp_id_4 = in_flp_id);
	
IF countRows > 0 THEN
	SET logNote=CONCAT('Found ', FORMAT(countRows, 0), ' rows for in_flp_id: ',in_flp_id,' in flight_plan_transfer table to delete' );
	call ams_wad.log_sp(spname, stTime, logNote);
     
	delete from ams_ac.ams_al_flight_plan_transfer
	where (flp_id_1 = in_flp_id	or flp_id_2 = in_flp_id	or flp_id_3 = in_flp_id	or flp_id_4 = in_flp_id);
    SET countRows =  ROW_COUNT();
    commit;
    SET logNote=CONCAT('Deleted ', FORMAT(countRows, 0), ' rows from flight_plan_transfer table.' );
	call ams_wad.log_sp(spname, stTime, logNote);
END IF;
	
	SET logNote=CONCAT(' in_flp_id: ',in_flp_id, ' .');
	call ams_wad.log_sp(spname, stTime, logNote);
	
    INSERT INTO ams_ac.ams_al_flight_plan_transfer
		(flp_id_1, flp_id_2, 
		d_ap_id, a_ap_id, dest_id, 
		fl_name_1, fl_name_2,
        tr_time_min_1,
		distance_km, flight_h)
    (SELECT flp2.flp_id as flp_id_1,  flp1.flp_id as flp_id_2, 
		d.dep_ap_id as d_ap_id, d.arr_ap_id as a_ap_id,
		d.dest_id,
		flp2.fl_name as fl_name_1,  flp1.fl_name as fl_name_2, 
		TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) as tr_time_min_1,
		(ifnull(flp1.distance_km,0) + ifnull(flp2.distance_km,0)) as distance_km,
		round((ifnull(flp1.flight_h,0) + ifnull(flp2.flight_h,0)),2) as flight_h
	FROM ams_ac.ams_al_flight_plan flp1
		JOIN ams_al.ams_al_flp_number_schedule flpns1 ON flpns1.flpns_id = flp1.flpns_id
		left JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.a_ap_id = flp1.d_ap_id)) and flp2.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) > 19)
		join ams_wad.cfg_airport_destination d on d.dep_ap_id = flp2.d_ap_id and d.arr_ap_id = flp1.a_ap_id
	where flp1.flp_id = in_flp_id FOR SHARE);
	SET countRows =  ROW_COUNT();
    set totalRows = totalRows + countRows;
    commit;
    SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' double transfers.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    SET countRows = 0;
    INSERT INTO ams_ac.ams_al_flight_plan_transfer
		(flp_id_1, flp_id_2, 
		d_ap_id, a_ap_id, dest_id, 
		fl_name_1, fl_name_2,
        tr_time_min_1,
		distance_km, flight_h)
    (SELECT flp2.flp_id as flp_id_1,  flp1.flp_id as flp_id_2, 
		d.dep_ap_id as d_ap_id, d.arr_ap_id as a_ap_id,
		d.dest_id,
		flp2.fl_name as fl_name_1,  flp1.fl_name as fl_name_2, 
		TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) as tr_time_min_1,
		(ifnull(flp1.distance_km,0) + ifnull(flp2.distance_km,0)) as distance_km,
		round((ifnull(flp1.flight_h,0) + ifnull(flp2.flight_h,0)),2) as flight_h
	FROM ams_ac.ams_al_flight_plan flp1
		JOIN ams_al.ams_al_flp_number_schedule flpns1 ON flpns1.flpns_id = flp1.flpns_id
		left JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.a_ap_id = flp1.d_ap_id)) and flp2.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) > 19)
		join ams_wad.cfg_airport_destination d on d.dep_ap_id = flp2.d_ap_id and d.arr_ap_id = flp1.a_ap_id
	where flp2.flp_id = in_flp_id FOR SHARE);
	SET countRows =  ROW_COUNT();
    set totalRows = totalRows + countRows;
    commit;
    SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' double transfers flp2.' );
	call ams_wad.log_sp(spname, stTime, logNote);
	
    INSERT INTO ams_ac.ams_al_flight_plan_transfer
		(flp_id_1, flp_id_2, flp_id_3,
		d_ap_id, a_ap_id, dest_id, 
		fl_name_1, fl_name_2, fl_name_3,
        tr_time_min_1, tr_time_min_2,
		distance_km, flight_h)
    (SELECT flp3.flp_id as flp_id_1, flp2.flp_id as flp_id_2,  flp1.flp_id as flp_id_3, 
		d.dep_ap_id as d_ap_id, d.arr_ap_id as a_ap_id,
		d.dest_id,
		flp3.fl_name as fl_name_1, flp2.fl_name as fl_name_2,  flp1.fl_name as fl_name_3, 
		TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) as tr_time_min_1,
        TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) as tr_time_min_2,
		(ifnull(flp3.distance_km,0) + ifnull(flp2.distance_km,0) + ifnull(flp1.distance_km,0) ) as distance_km,
		round((ifnull(flp3.flight_h,0) + ifnull(flp2.flight_h,0) + ifnull(flp1.flight_h,0)),2) as flight_h
	FROM ams_ac.ams_al_flight_plan flp1
		left JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.a_ap_id = flp1.d_ap_id)) and flp2.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) > 19)
        left JOIN ams_ac.ams_al_flight_plan flp3 ON ((flp3.a_ap_id = flp2.d_ap_id)) and flp3.d_ap_id <> flp2.a_ap_id and flp3.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) > 19)
		join ams_wad.cfg_airport_destination d on d.dep_ap_id = flp3.d_ap_id and d.arr_ap_id = flp1.a_ap_id
        where flp1.flp_id = in_flp_id FOR SHARE);
		SET countRows =  ROW_COUNT();
		set totalRows = totalRows + countRows;
		commit;
		SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' triple transfers.' );
		call ams_wad.log_sp(spname, stTime, logNote);
	
		INSERT INTO ams_ac.ams_al_flight_plan_transfer
			(flp_id_1, flp_id_2, flp_id_3,
			d_ap_id, a_ap_id, dest_id, 
			fl_name_1, fl_name_2, fl_name_3,
			tr_time_min_1, tr_time_min_2,
			distance_km, flight_h)
		(SELECT flp3.flp_id as flp_id_1, flp2.flp_id as flp_id_2,  flp1.flp_id as flp_id_3, 
			d.dep_ap_id as d_ap_id, d.arr_ap_id as a_ap_id,
			d.dest_id,
			flp3.fl_name as fl_name_1, flp2.fl_name as fl_name_2,  flp1.fl_name as fl_name_3, 
			TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) as tr_time_min_1,
			TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) as tr_time_min_2,
			(ifnull(flp3.distance_km,0) + ifnull(flp2.distance_km,0) + ifnull(flp1.distance_km,0) ) as distance_km,
			round((ifnull(flp3.flight_h,0) + ifnull(flp2.flight_h,0) + ifnull(flp1.flight_h,0)),2) as flight_h
		FROM ams_ac.ams_al_flight_plan flp1
			left JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.a_ap_id = flp1.d_ap_id)) and flp2.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) > 19)
			left JOIN ams_ac.ams_al_flight_plan flp3 ON ((flp3.a_ap_id = flp2.d_ap_id)) and flp3.d_ap_id <> flp2.a_ap_id and flp3.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) > 19)
			join ams_wad.cfg_airport_destination d on d.dep_ap_id = flp3.d_ap_id and d.arr_ap_id = flp1.a_ap_id
        where flp2.flp_id = in_flp_id FOR SHARE);
		SET countRows =  ROW_COUNT();
		set totalRows = totalRows + countRows;
		commit;
		SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' triple transfers. flp2' );
		call ams_wad.log_sp(spname, stTime, logNote);
		
        INSERT INTO ams_ac.ams_al_flight_plan_transfer
			(flp_id_1, flp_id_2, flp_id_3,
			d_ap_id, a_ap_id, dest_id, 
			fl_name_1, fl_name_2, fl_name_3,
			tr_time_min_1, tr_time_min_2,
			distance_km, flight_h)
		(SELECT flp3.flp_id as flp_id_1, flp2.flp_id as flp_id_2,  flp1.flp_id as flp_id_3, 
			d.dep_ap_id as d_ap_id, d.arr_ap_id as a_ap_id,
			d.dest_id,
			flp3.fl_name as fl_name_1, flp2.fl_name as fl_name_2,  flp1.fl_name as fl_name_3, 
			TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) as tr_time_min_1,
			TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) as tr_time_min_2,
			(ifnull(flp3.distance_km,0) + ifnull(flp2.distance_km,0) + ifnull(flp1.distance_km,0) ) as distance_km,
			round((ifnull(flp3.flight_h,0) + ifnull(flp2.flight_h,0) + ifnull(flp1.flight_h,0)),2) as flight_h
		FROM ams_ac.ams_al_flight_plan flp1
			left JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.a_ap_id = flp1.d_ap_id)) and flp2.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) > 19)
			left JOIN ams_ac.ams_al_flight_plan flp3 ON ((flp3.a_ap_id = flp2.d_ap_id)) and flp3.d_ap_id <> flp2.a_ap_id and flp3.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) > 19)
			join ams_wad.cfg_airport_destination d on d.dep_ap_id = flp3.d_ap_id and d.arr_ap_id = flp1.a_ap_id
        where flp3.flp_id = in_flp_id FOR SHARE);
		SET countRows =  ROW_COUNT();
		set totalRows = totalRows + countRows;
		commit;
		SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' triple transfers. flp3' );
		call ams_wad.log_sp(spname, stTime, logNote);
		
        
        INSERT INTO ams_ac.ams_al_flight_plan_transfer
			(flp_id_1, flp_id_2, flp_id_3, flp_id_4,
			d_ap_id, a_ap_id, dest_id, 
			fl_name_1, fl_name_2, fl_name_3, fl_name_4,
			tr_time_min_1, tr_time_min_2, tr_time_min_3,
			distance_km, flight_h)
		(SELECT flp4.flp_id as flp_id_1, flp3.flp_id as flp_id_2, flp2.flp_id as flp_id_3,  flp1.flp_id as flp_id_4, 
			d.dep_ap_id as d_ap_id, d.arr_ap_id as a_ap_id,
			d.dest_id,
			flp4.fl_name as fl_name_1, flp3.fl_name as fl_name_2, flp2.fl_name as fl_name_3,  flp1.fl_name as fl_name_4, 
			TIMESTAMPDIFF(MINUTE, flp4.atime, flp3.dtime) as tr_time_min_1,
			TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) as tr_time_min_2,
			TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) as tr_time_min_3,
			(ifnull(flp4.distance_km,0) + ifnull(flp3.distance_km,0) + ifnull(flp2.distance_km,0) + ifnull(flp1.distance_km,0) ) as distance_km,
			round((ifnull(flp4.flight_h,0) +ifnull(flp3.flight_h,0) + ifnull(flp2.flight_h,0) + ifnull(flp1.flight_h,0)),2) as flight_h
		FROM ams_ac.ams_al_flight_plan flp1
			left JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.a_ap_id = flp1.d_ap_id)) and flp2.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) > 19)
			left JOIN ams_ac.ams_al_flight_plan flp3 ON ((flp3.a_ap_id = flp2.d_ap_id)) and flp3.d_ap_id <> flp2.a_ap_id and flp3.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) > 19)
			left JOIN ams_ac.ams_al_flight_plan flp4 ON ((flp4.a_ap_id = flp3.d_ap_id)) and flp4.d_ap_id <> flp3.a_ap_id and flp4.d_ap_id <> flp2.a_ap_id and flp4.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp4.atime, flp3.dtime) > 19)
			join ams_wad.cfg_airport_destination d on d.dep_ap_id = flp4.d_ap_id and d.arr_ap_id = flp1.a_ap_id
        where flp1.flp_id = in_flp_id FOR SHARE);
		SET countRows =  ROW_COUNT();
		set totalRows = totalRows + countRows;
		commit;
		SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' quadruple transfers.' );
		call ams_wad.log_sp(spname, stTime, logNote);
		
        INSERT INTO ams_ac.ams_al_flight_plan_transfer
			(flp_id_1, flp_id_2, flp_id_3, flp_id_4,
			d_ap_id, a_ap_id, dest_id, 
			fl_name_1, fl_name_2, fl_name_3, fl_name_4,
			tr_time_min_1, tr_time_min_2, tr_time_min_3,
			distance_km, flight_h)
		(SELECT flp4.flp_id as flp_id_1, flp3.flp_id as flp_id_2, flp2.flp_id as flp_id_3,  flp1.flp_id as flp_id_4, 
			d.dep_ap_id as d_ap_id, d.arr_ap_id as a_ap_id,
			d.dest_id,
			flp4.fl_name as fl_name_1, flp3.fl_name as fl_name_2, flp2.fl_name as fl_name_3,  flp1.fl_name as fl_name_4, 
			TIMESTAMPDIFF(MINUTE, flp4.atime, flp3.dtime) as tr_time_min_1,
			TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) as tr_time_min_2,
			TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) as tr_time_min_3,
			(ifnull(flp4.distance_km,0) + ifnull(flp3.distance_km,0) + ifnull(flp2.distance_km,0) + ifnull(flp1.distance_km,0) ) as distance_km,
			round((ifnull(flp4.flight_h,0) +ifnull(flp3.flight_h,0) + ifnull(flp2.flight_h,0) + ifnull(flp1.flight_h,0)),2) as flight_h
		FROM ams_ac.ams_al_flight_plan flp1
			left JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.a_ap_id = flp1.d_ap_id)) and flp2.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) > 19)
			left JOIN ams_ac.ams_al_flight_plan flp3 ON ((flp3.a_ap_id = flp2.d_ap_id)) and flp3.d_ap_id <> flp2.a_ap_id and flp3.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) > 19)
			left JOIN ams_ac.ams_al_flight_plan flp4 ON ((flp4.a_ap_id = flp3.d_ap_id)) and flp4.d_ap_id <> flp3.a_ap_id and flp4.d_ap_id <> flp2.a_ap_id and flp4.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp4.atime, flp3.dtime) > 19)
			join ams_wad.cfg_airport_destination d on d.dep_ap_id = flp4.d_ap_id and d.arr_ap_id = flp1.a_ap_id
        where flp2.flp_id = in_flp_id FOR SHARE);
		SET countRows =  ROW_COUNT();
		set totalRows = totalRows + countRows;
		commit;
		SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' quadruple transfers. flp2' );
		call ams_wad.log_sp(spname, stTime, logNote);
		INSERT INTO ams_ac.ams_al_flight_plan_transfer
			(flp_id_1, flp_id_2, flp_id_3, flp_id_4,
			d_ap_id, a_ap_id, dest_id, 
			fl_name_1, fl_name_2, fl_name_3, fl_name_4,
			tr_time_min_1, tr_time_min_2, tr_time_min_3,
			distance_km, flight_h)
		(SELECT flp4.flp_id as flp_id_1, flp3.flp_id as flp_id_2, flp2.flp_id as flp_id_3,  flp1.flp_id as flp_id_4, 
			d.dep_ap_id as d_ap_id, d.arr_ap_id as a_ap_id,
			d.dest_id,
			flp4.fl_name as fl_name_1, flp3.fl_name as fl_name_2, flp2.fl_name as fl_name_3,  flp1.fl_name as fl_name_4, 
			TIMESTAMPDIFF(MINUTE, flp4.atime, flp3.dtime) as tr_time_min_1,
			TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) as tr_time_min_2,
			TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) as tr_time_min_3,
			(ifnull(flp4.distance_km,0) + ifnull(flp3.distance_km,0) + ifnull(flp2.distance_km,0) + ifnull(flp1.distance_km,0) ) as distance_km,
			round((ifnull(flp4.flight_h,0) +ifnull(flp3.flight_h,0) + ifnull(flp2.flight_h,0) + ifnull(flp1.flight_h,0)),2) as flight_h
		FROM ams_ac.ams_al_flight_plan flp1
			left JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.a_ap_id = flp1.d_ap_id)) and flp2.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) > 19)
			left JOIN ams_ac.ams_al_flight_plan flp3 ON ((flp3.a_ap_id = flp2.d_ap_id)) and flp3.d_ap_id <> flp2.a_ap_id and flp3.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) > 19)
			left JOIN ams_ac.ams_al_flight_plan flp4 ON ((flp4.a_ap_id = flp3.d_ap_id)) and flp4.d_ap_id <> flp3.a_ap_id and flp4.d_ap_id <> flp2.a_ap_id and flp4.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp4.atime, flp3.dtime) > 19)
			join ams_wad.cfg_airport_destination d on d.dep_ap_id = flp4.d_ap_id and d.arr_ap_id = flp1.a_ap_id
        where flp3.flp_id = in_flp_id  FOR SHARE);
		SET countRows =  ROW_COUNT();
		set totalRows = totalRows + countRows;
		commit;
		SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' quadruple transfers. flp3' );
		call ams_wad.log_sp(spname, stTime, logNote);
        
		INSERT INTO ams_ac.ams_al_flight_plan_transfer
			(flp_id_1, flp_id_2, flp_id_3, flp_id_4,
			d_ap_id, a_ap_id, dest_id, 
			fl_name_1, fl_name_2, fl_name_3, fl_name_4,
			tr_time_min_1, tr_time_min_2, tr_time_min_3,
			distance_km, flight_h)
		(SELECT flp4.flp_id as flp_id_1, flp3.flp_id as flp_id_2, flp2.flp_id as flp_id_3,  flp1.flp_id as flp_id_4, 
			d.dep_ap_id as d_ap_id, d.arr_ap_id as a_ap_id,
			d.dest_id,
			flp4.fl_name as fl_name_1, flp3.fl_name as fl_name_2, flp2.fl_name as fl_name_3,  flp1.fl_name as fl_name_4, 
			TIMESTAMPDIFF(MINUTE, flp4.atime, flp3.dtime) as tr_time_min_1,
			TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) as tr_time_min_2,
			TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) as tr_time_min_3,
			(ifnull(flp4.distance_km,0) + ifnull(flp3.distance_km,0) + ifnull(flp2.distance_km,0) + ifnull(flp1.distance_km,0) ) as distance_km,
			round((ifnull(flp4.flight_h,0) +ifnull(flp3.flight_h,0) + ifnull(flp2.flight_h,0) + ifnull(flp1.flight_h,0)),2) as flight_h
		FROM ams_ac.ams_al_flight_plan flp1
			left JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.a_ap_id = flp1.d_ap_id)) and flp2.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp2.atime, flp1.dtime) > 19)
			left JOIN ams_ac.ams_al_flight_plan flp3 ON ((flp3.a_ap_id = flp2.d_ap_id)) and flp3.d_ap_id <> flp2.a_ap_id and flp3.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp3.atime, flp2.dtime) > 19)
			left JOIN ams_ac.ams_al_flight_plan flp4 ON ((flp4.a_ap_id = flp3.d_ap_id)) and flp4.d_ap_id <> flp3.a_ap_id and flp4.d_ap_id <> flp2.a_ap_id and flp4.d_ap_id <> flp1.a_ap_id and ( TIMESTAMPDIFF(MINUTE, flp4.atime, flp3.dtime) > 19)
			join ams_wad.cfg_airport_destination d on d.dep_ap_id = flp4.d_ap_id and d.arr_ap_id = flp1.a_ap_id
        where flp4.flp_id = in_flp_id FOR SHARE);
        
		SET countRows =  ROW_COUNT();
		set totalRows = totalRows + countRows;
		commit;
		SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' quadruple transfers. flp4' );
		call ams_wad.log_sp(spname, stTime, logNote);
    
    SET logNote=CONCAT(FORMAT(totalRows, 0), ' transfers for flp_id ', in_flp_id, '.' );
	call ams_wad.log_sp(spname, stTime, logNote);
     
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_create_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_create_day`(IN in_days INT)
BEGIN

DECLARE i_flpns_id, i_fl_type_id, i_flp_id INT;
DECLARE countRows, i_flpns_count, i_day, i_yw INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_flpns CURSOR FOR SELECT flpns.flpns_id
					FROM ams_al.ams_al_flp_group g
					join ams_al.ams_al_flp_number_schedule flpns on flpns.grp_id = g.grp_id
					join (SELECT in_days as param_value from dual ) days on 1=1
					left join ams_ac.ams_al_flight_plan flp on flp.flpns_id = flpns.flpns_id and flp.week = WEEKOFYEAR(ADDDATE(curdate(),days.param_value)) 
					where g.ac_id is not null and g.is_active<>0 and g.start_date < now() and 
					flpns.wd_id = DAYOFWEEK(ADDDATE(curdate(),days.param_value)) and flp.flp_id is null
				order by flpns.wd_id, flpns.dtime_h, flpns.dtime_min, flpns.atime_h, flpns.atime_min;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_create_day', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_create_day';
set countRows = 0;
set i_flpns_count = 0;
set i_fl_type_id = 1; -- Schedule
set i_flpns_id = 0;
    
SET logNote=CONCAT('Start () -> days_in_advance:', in_days);
call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_flpns;

read_loop: LOOP
	
    FETCH cursor_flpns INTO i_flpns_id;
	
	IF done THEN
		SET logNote=CONCAT('No flpns_id in queue found ', 'done' );
		 call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
	
	SET logNote=CONCAT(' i_flpns_id: ',i_flpns_id, ' prepare...');
	-- call ams_wad.log_sp(spname, stTime, logNote);
	
    INSERT INTO ams_ac.ams_al_flight_plan
	(week, fl_type_id, flpns_id, fl_name, fl_number,
	route_id, dest_id, al_id, ac_id, d_ap_id, a_ap_id, distance_km,
	oil_l, flight_h, dtime, atime, available_pax_f, available_pax_b, available_pax_e, available_cargo_kg)
    SELECT WEEKOFYEAR(ADDDATE(curdate(),days.param_value)) as week, 
		i_fl_type_id as fl_type_id, 
		flpns.flpns_id, 
        concat(a.iata, ' ',  LPAD(flpn.flpn_number, 4, '0'), ' ',
		ifnull(dep.ap_iata,  dep.ap_icao) ,' ',
		ifnull(arr.ap_iata, arr.ap_icao) ) as fl_name,
        flpn.flpn_number, flpn.route_id, flpn.dest_id, flpns.al_id, g.ac_id,
        flpn.dep_ap_id, flpn.arr_ap_id, d.distance_km, 
        ROUND((d.distance_km /100)* macr.fuel_consumption_lp100km,2) as oil_l,
        Round(((d.distance_km/macr.cruise_speed_kmph)),2) as flight_h,
        DATE_ADD( date_format(ADDDATE(curdate(),days.param_value), '%Y-%m-%d 00:00:00') ,INTERVAL (( flpns.dtime_h*60) + flpns.dtime_min) MINUTE)  as dtime,
        DATE_ADD( date_format(ADDDATE(curdate(),days.param_value), '%Y-%m-%d 00:00:00') ,INTERVAL (( (flpn.flight_h + flpns.dtime_h)*60) + flpns.dtime_min) MINUTE)  as atime,
        -- DATE_ADD( date_format(ADDDATE(curdate(),days.param_value), '%Y-%m-%d 00:00:00') ,INTERVAL (( flpns.atime_h*60) + flpns.atime_min) MINUTE)  as atime,
        ac.pax_f as available_pax_f, ac.pax_b as available_pax_b, ac.pax_e as available_pax_e, ac.cargo_kg as available_cargo_kg
	FROM ams_al.ams_al_flp_number_schedule flpns
	join ams_al.ams_al_flp_group g on g.grp_id = flpns.grp_id
	join ams_al.ams_al_flp_number flpn on flpn.flpn_id = flpns.flpn_id
	join ams_al.ams_airline a on a.al_id = flpns.al_id
	join ams_ac.ams_aircraft ac on ac.ac_id = g.ac_id
	join ams_ac.cfg_mac macr on ac.mac_id = macr.mac_id
	left join ams_wad.cfg_airport_destination d on d.dest_id = flpn.dest_id
	join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
	join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id
	join (SELECT in_days as param_value from dual ) days on 1=1
	where flpns.flpns_id = i_flpns_id;
	
    SET i_flp_id = LAST_INSERT_ID();
    
    SET logNote=CONCAT(i_flpns_count,' -> inserted i_flp_id: ', i_flp_id);
	-- call ams_wad.log_sp(spname, stTime, logNote);
    COMMIT;
    
	SET i_flpns_count = i_flpns_count+1;
        
	
 END LOOP;
 
CLOSE cursor_flpns;

SET logNote=CONCAT('inserted ', i_flpns_count, ' flp records for days_in_advance:', in_days);
call ams_wad.log_sp(spname, stTime, logNote);

call ams_ac.sp_ams_al_flight_plan_update();

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_create_v01` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_create_v01`()
BEGIN

DECLARE i_flpns_id, i_fl_type_id, i_flp_id INT;
DECLARE countRows, i_flpns_count, i_day, i_yw INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_flpns CURSOR FOR SELECT flpns.flpns_id
				FROM ams_al.ams_al_flp_group g
				join ams_al.ams_al_flp_number_schedule flpns on flpns.grp_id = g.grp_id
				join (SELECT param_value FROM ams_al.cfg_callc_param where param_name='flight_plan_add_days_in_advance' ) days on 1=1
				left join ams_ac.ams_al_flight_plan flp on flp.flpns_id = flpns.flpns_id and flp.week = WEEKOFYEAR(ADDDATE(curdate(),days.param_value)) 
				where g.ac_id is not null and g.is_active<>0 and 
					flpns.wd_id = DAYOFWEEK(ADDDATE(curdate(),days.param_value)) and flp.flp_id is null
				order by flpns.dtime_h, flpns.dtime_min, flpns.atime_h, flpns.atime_min limit 10;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_create', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_create';
set countRows = 0;
set i_flpns_count = 0;
set i_fl_type_id = 1; -- Schedule
set i_flpns_id = 0;

	SELECT DAYOFWEEK(ADDDATE(curdate(),days.param_value)) as i_day, 
		WEEKOFYEAR(ADDDATE(curdate(),days.param_value)) as i_yw 
        INTO i_day, i_yw
    from (SELECT param_value FROM ams_al.cfg_callc_param where param_name='flight_plan_add_days_in_advance' ) days;
    
SET logNote=CONCAT('Start () -> day:', i_day, ', week: ', i_yw);
call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_flpns;

read_loop: LOOP
	
    FETCH cursor_flpns INTO i_flpns_id;
	
	IF done THEN
		SET logNote=CONCAT('No flpns_id in queue found ', 'done' );
		 call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
	
	SET logNote=CONCAT(' i_flpns_id: ',i_flpns_id, ' prepare...');
	call ams_wad.log_sp(spname, stTime, logNote);
	
    INSERT INTO ams_ac.ams_al_flight_plan
	(week, fl_type_id, flpns_id, fl_name, fl_number,
	route_id, dest_id, al_id, ac_id, d_ap_id, a_ap_id, distance_km,
	oil_l, flight_h, dtime, atime, available_pax_f, available_pax_b, available_pax_e, available_cargo_kg)
    SELECT WEEKOFYEAR(ADDDATE(curdate(),days.param_value)) as week, 
		i_fl_type_id as fl_type_id, 
		flpns.flpns_id, 
        concat(a.iata, ' ',  LPAD(flpn.flpn_number, 4, '0'), ' ',
		ifnull(dep.ap_iata,  dep.ap_icao) ,' ',
		ifnull(arr.ap_iata, arr.ap_icao) ) as fl_name,
        flpn.flpn_number, flpn.route_id, flpn.dest_id, flpns.al_id, g.ac_id,
        flpn.dep_ap_id, flpn.arr_ap_id, d.distance_km, 
        ROUND((d.distance_km /100)* macr.fuel_consumption_lp100km,2) as oil_l,
        Round(((d.distance_km/macr.cruise_speed_kmph)),2) as flight_h,
        DATE_ADD( date_format(ADDDATE(curdate(),days.param_value), '%Y-%m-%d 00:00:00') ,INTERVAL (( flpns.dtime_h*60) + flpns.dtime_min) MINUTE)  as dtime,
        DATE_ADD( date_format(ADDDATE(curdate(),days.param_value), '%Y-%m-%d 00:00:00') ,INTERVAL (( (flpn.flight_h + flpns.dtime_h)*60) + flpns.dtime_min) MINUTE)  as atime,
        -- DATE_ADD( date_format(ADDDATE(curdate(),days.param_value), '%Y-%m-%d 00:00:00') ,INTERVAL (( flpns.atime_h*60) + flpns.atime_min) MINUTE)  as atime,
        ac.pax_f as available_pax_f, ac.pax_b as available_pax_b, ac.pax_e as available_pax_e, ac.cargo_kg as available_cargo_kg
	FROM ams_al.ams_al_flp_number_schedule flpns
	join ams_al.ams_al_flp_group g on g.grp_id = flpns.grp_id
	join ams_al.ams_al_flp_number flpn on flpn.flpn_id = flpns.flpn_id
	join ams_al.ams_airline a on a.al_id = flpns.al_id
	join ams_ac.ams_aircraft ac on ac.ac_id = g.ac_id
	join ams_ac.cfg_mac macr on ac.mac_id = macr.mac_id
	left join ams_wad.cfg_airport_destination d on d.dest_id = flpn.dest_id
	join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
	join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id
	join (SELECT param_value FROM ams_al.cfg_callc_param where param_name='flight_plan_add_days_in_advance' ) days on 1=1
	where flpns.flpns_id = i_flpns_id;
	
    SET i_flp_id = LAST_INSERT_ID();
    
    SET logNote=CONCAT('inserted i_flp_id: ', i_flp_id);
	call ams_wad.log_sp(spname, stTime, logNote);
    
	SET i_flpns_count = i_flpns_count+1;
        
	
 END LOOP;
 
CLOSE cursor_flpns;
COMMIT;

SET logNote=CONCAT('inserted i_flpns_count ', i_flpns_count);
call ams_wad.log_sp(spname, stTime, logNote);

update ams_ac.ams_al_flight_plan flp
left JOIN 
	(select flp_id, 
	SUM(if(pax is null, 0, if(pax_class_id=1, pax, 0))) as load_pax_e,
	SUM(if(pax is null, 0, if(pax_class_id=2, pax, 0))) as load_pax_b,
	SUM(if(pax is null, 0, if(pax_class_id=3, pax, 0))) as load_pax_f,
	SUM(if(payload_kg is null, 0, if(pax_class_id=4, payload_kg, 0))) as load_cargo_kg
	FROM ams_ac.ams_al_flight_plan_payload group by flp_id) p ON p.flp_id = flp.flp_id
set flp.remain_pax_e = (flp.available_pax_e - ifnull(p.load_pax_e, 0)),
flp.remain_pax_b = (flp.available_pax_b - ifnull(p.load_pax_b, 0)),
flp.remain_pax_f = (flp.available_pax_f - ifnull(p.load_pax_f,0)),
flp.remain_payload_kg = Round((flp.available_cargo_kg - ifnull(p.load_cargo_kg,0)),2);
commit;

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_delete_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_delete_id`(IN in_flp_id INT)
BEGIN

DECLARE i_ac_status_operational, i_acat_planeStand INT;
DECLARE i_fl_id, i_ac_id, i_stat_add_to_flq INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_delete_id', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_delete_id';
set countRows = 0;
SET logNote=CONCAT('Start in_flp_id:', in_flp_id , ' ->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET i_stat_add_to_flq = 4;

	select count(*) INTO countRows from ams_ac.ams_al_flight_plan 
    where flp_id =  in_flp_id and flp_status_id <> i_stat_add_to_flq;
    
	IF countRows > 0 THEN
		set countRows = 0;
		SET SQL_SAFE_UPDATES = 0;
		insert into ams_ac.ams_al_flight_plan_h
		select * from ams_ac.ams_al_flight_plan 
		where flp_id = in_flp_id;
		commit;
		insert into ams_ac.ams_al_flight_plan_payload_h
		select * from ams_ac.ams_al_flight_plan_payload 
		where flp_id = in_flp_id;
		commit;
		insert into ams_ac.ams_al_flight_pcpd_h
		select pcpd.* 
		from ams_ac.ams_al_flight_pcpd pcpd
		join ams_ac.ams_al_flight_plan_payload pl on pl.pcpd_id = pcpd.pcpd_id
		where pl.flp_id = in_flp_id;
		commit;
		
		DELETE pcpd from ams_ac.ams_al_flight_pcpd pcpd
		join ams_ac.ams_al_flight_plan_payload pl on pl.pcpd_id = pcpd.pcpd_id
		where pl.flp_id = in_flp_id;
		delete from ams_al_flight_plan where flp_id = in_flp_id;
		delete from ams_al_flight_plan_payload where flp_id = in_flp_id;
		commit;
		SET logNote=CONCAT('deleted records from in ams_al_flight_plan ', in_flp_id);
		call ams_wad.log_sp(spname, stTime, logNote);
		
        CALL ams_ac.sp_ams_aircraft_curr_update_ac(i_ac_id);
	
	END IF;

	SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add';
set countRows = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);

-- call ams_ac.sp_ams_al_flight_plan_update();
-- call ams_ac.sp_al_flight_plan_payload_add_n();
call sp_al_flight_plan_payload_add_d_flp();

SET logNote=CONCAT('Start add_n_single ' );
call ams_wad.log_sp(spname, stTime, logNote);
call ams_ac.sp_al_flight_plan_payload_add_n_single();

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_c` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_c`()
BEGIN

DECLARE i_ppd_id, i_cpd_id, i_tr_id INT;
DECLARE i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4, i_flp_status_id INT;
DECLARE countRows, i_transfers_id, ppdRows, cpdRows INT;
DECLARE i_cicles, i_atime INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_c', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_c';
set i_flp_status_id = 2; -- Flight is sold out
set countRows = 0;
set ppdRows = 0;
set cpdRows = 0;
set i_ppd_id = 0;
set i_cpd_id = 0;
set i_cicles = 50;
set i_atime = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);
-- SET SQL_SAFE_UPDATES = 0;
-- Update ams_al_flight_plan_payload_sum
call sp_al_flight_plan_payload_add_c_payload_sum();
commit;
-- Insert PPD records
call sp_al_flight_plan_payload_add_c_ppd();
commit;
-- Insert CPD records
call sp_al_flight_plan_payload_add_c_cpd();
commit;
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_cpd_double` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_cpd_double`(IN in_flp_id INT, IN in_cpd_id INT, IN in_flp_id_2 INT)
BEGIN


DECLARE i_pcpd_id, i_flppl_id INT;
DECLARE i_pcpd_id_2, i_flppl_id_2 INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_cpd_double', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_cpd_double';
set countRows = 0;
set i_pcpd_id = 0;

SET logNote=CONCAT('Start () ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

select count(*) into countRows from ams_wad.ams_airport_cpd p where p.cpd_id = in_cpd_id;

IF countRows>0 THEN

	INSERT INTO ams_ac.ams_al_flight_pcpd
		(cpd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	select 
		p.cpd_id, p.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 1, 
		0, p.pax_class_id, p.ppd_type_id, p.payload_kg, (p.payload_kg*flpns.price_c_p_kg),
        concat(p.note, ', #1 of 2 transfer flights')
		from ams_wad.ams_airport_cpd p 
		join ams_ac.ams_al_flight_plan flp on 1=1 
        join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where flp.flp_id = in_flp_id and p.cpd_id = in_cpd_id;
    
    SET i_pcpd_id = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_pcpd_id: ', i_pcpd_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_pcpd
		(cpd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	select 
		p.cpd_id, p.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 2, 
		0, p.pax_class_id, p.ppd_type_id, p.payload_kg, (p.payload_kg*flpns.price_c_p_kg),
        concat(p.note, ', #2 of 2 transfer flights')
		from ams_wad.ams_airport_cpd p 
		join ams_ac.ams_al_flight_plan flp on 1=1 
        join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where flp.flp_id = in_flp_id_2 and p.cpd_id = in_cpd_id;
    
    SET i_pcpd_id_2 = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_pcpd_id_2: ', i_pcpd_id_2 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
     select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.payload_kg* flpns.price_c_p_kg) as price
	from ams_al_flight_pcpd pc
		join ams_ac.ams_al_flight_plan flp on 1=1  
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where  flp.flp_id = in_flp_id  and pc.pcpd_id = i_pcpd_id;
    
    SET i_flppl_id = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_flppl_id: ', i_flppl_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
     select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.payload_kg* flpns.price_c_p_kg) as price
	from ams_al_flight_pcpd pc
		join ams_ac.ams_al_flight_plan flp on 1=1  
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where  flp.flp_id = in_flp_id_2  and pc.pcpd_id = i_pcpd_id;
    
    SET i_flppl_id_2 = LAST_INSERT_ID();
    COMMIT;
    
    SET logNote=CONCAT('Inserted i_flppl_id_2: ', i_flppl_id_2 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
	DELETE FROM ams_wad.ams_airport_cpd_for_flp where cpd_id = in_cpd_id;
    DELETE FROM ams_wad.ams_airport_cpd where cpd_id = in_cpd_id;
    COMMIT;
	SET logNote=CONCAT(' deleted from ams_airport_cpd in_cpd_id: ', in_cpd_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id);
	call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id_2);
	COMMIT;

END IF;

SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_cpd_quadruple` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_cpd_quadruple`(IN in_flp_id INT, IN in_cpd_id INT, IN in_flp_id_2 INT, IN in_flp_id_3 INT, IN in_flp_id_4 INT)
BEGIN


DECLARE i_pcpd_id, i_flppl_id INT;
DECLARE i_pcpd_id_2, i_flppl_id_2 INT;
DECLARE i_pcpd_id_3, i_flppl_id_3 INT;
DECLARE i_pcpd_id_4, i_flppl_id_4 INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_cpd_quadruple', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_cpd_quadruple';
set countRows = 0;
set i_pcpd_id = 0;

SET logNote=CONCAT('Start () ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

select count(*) into countRows 
from ams_wad.ams_airport_cpd p 
where p.cpd_id = in_cpd_id;

SET logNote=CONCAT('in_cpd_id: ', in_cpd_id, ', countRows:', countRows );
-- call ams_wad.log_sp(spname, stTime, logNote);

IF countRows > 0 THEN

	INSERT INTO ams_ac.ams_al_flight_pcpd
		(cpd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	select 
		p.cpd_id, p.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 1, 
		0, p.pax_class_id, p.ppd_type_id, p.payload_kg, (p.payload_kg*flpns.price_c_p_kg),
        concat(p.note, ', #1 of 4 transfer flights')
		from ams_wad.ams_airport_cpd p 
		join ams_ac.ams_al_flight_plan flp on 1=1 
        join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where flp.flp_id = in_flp_id and p.cpd_id = in_cpd_id;
    
    SET i_pcpd_id = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_pcpd_id: ', i_pcpd_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_pcpd
		(cpd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	select 
		p.cpd_id, p.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 2, 
		0, p.pax_class_id, p.ppd_type_id, p.payload_kg, (p.payload_kg*flpns.price_c_p_kg),
        concat(p.note, ', #2 of 4 transfer flights')
		from ams_wad.ams_airport_cpd p 
		join ams_ac.ams_al_flight_plan flp on 1=1 
        join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where flp.flp_id = in_flp_id_2 and p.cpd_id = in_cpd_id;
    
    SET i_pcpd_id_2 = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_pcpd_id_2: ', i_pcpd_id_2 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_pcpd
		(cpd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	select 
		p.cpd_id, p.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 3, 
		0, p.pax_class_id, p.ppd_type_id, p.payload_kg, (p.payload_kg*flpns.price_c_p_kg),
        concat(p.note, ', #3 of 4 transfer flights')
		from ams_wad.ams_airport_cpd p 
		join ams_ac.ams_al_flight_plan flp on 1=1 
        join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where flp.flp_id = in_flp_id_3 and p.cpd_id = in_cpd_id;
    
    SET i_pcpd_id_3 = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_pcpd_id_3: ', i_pcpd_id_3 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_pcpd
		(cpd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	select 
		p.cpd_id, p.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 4, 
		0, p.pax_class_id, p.ppd_type_id, p.payload_kg, (p.payload_kg*flpns.price_c_p_kg),
        concat(p.note, ', #4 of 4 transfer flights')
		from ams_wad.ams_airport_cpd p 
		join ams_ac.ams_al_flight_plan flp on 1=1 
        join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where flp.flp_id = in_flp_id_4 and p.cpd_id = in_cpd_id;
    
    SET i_pcpd_id_4 = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_pcpd_id_3: ', i_pcpd_id_3 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
     select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.payload_kg* flpns.price_c_p_kg) as price
	from ams_al_flight_pcpd pc
		join ams_ac.ams_al_flight_plan flp on 1=1  
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where  flp.flp_id = in_flp_id  and pc.pcpd_id = i_pcpd_id;
    
    SET i_flppl_id = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_flppl_id: ', i_flppl_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
     select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.payload_kg* flpns.price_c_p_kg) as price
	from ams_al_flight_pcpd pc
		join ams_ac.ams_al_flight_plan flp on 1=1  
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where  flp.flp_id = in_flp_id_2  and pc.pcpd_id = i_pcpd_id_2;
    
    SET i_flppl_id_2 = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_flppl_id_2: ', i_flppl_id_2 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
     select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.payload_kg* flpns.price_c_p_kg) as price
	from ams_al_flight_pcpd pc
		join ams_ac.ams_al_flight_plan flp on 1=1  
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where  flp.flp_id = in_flp_id_3  and pc.pcpd_id = i_pcpd_id_3;
    
    SET i_flppl_id_3 = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_flppl_id_3: ', i_flppl_id_3 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
     select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.payload_kg* flpns.price_c_p_kg) as price
	from ams_al_flight_pcpd pc
		join ams_ac.ams_al_flight_plan flp on 1=1  
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where  flp.flp_id = in_flp_id_4  and pc.pcpd_id = i_pcpd_id_3;
    
    SET i_flppl_id_4 = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_flppl_id_4: ', i_flppl_id_4 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    COMMIT;
    
    DELETE FROM ams_wad.ams_airport_cpd_for_flp where cpd_id = in_cpd_id;
    DELETE FROM ams_wad.ams_airport_cpd where cpd_id = in_cpd_id;
    COMMIT;
    
	SET logNote=CONCAT(' deleted from ams_airport_cpd in_cpd_id: ', in_cpd_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id);
	call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id_2);
	call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id_3);
	call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id_4);
	commit;
    
	SET logNote=CONCAT(' sp_ams_al_flight_plan_update_id compleated: ', in_cpd_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
END IF;


SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_cpd_single` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_cpd_single`(IN in_flp_id INT, IN in_cpd_id INT)
BEGIN


DECLARE i_pcpd_id, i_flppl_id INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_cpd_single', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_cpd_single';
set countRows = 0;
set i_pcpd_id = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);

select count(*) into countRows from ams_wad.ams_airport_cpd p where p.cpd_id = in_cpd_id;

IF countRows>0 THEN

	INSERT INTO ams_ac.ams_al_flight_pcpd
		(cpd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	select 
		p.cpd_id, p.dest_id, p.dep_ap_id, flp.a_ap_id, flp.al_id, p.sequence, 
		0, p.pax_class_id, p.ppd_type_id, p.payload_kg, p.max_ticket_price,
        p.note
		from ams_wad.ams_airport_cpd p 
		left join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
		join ams_ac.ams_al_flight_plan flp on flp.dest_id = p.dest_id and flp.flp_id = in_flp_id 
	where p.cpd_id = in_cpd_id;
    
    SET i_pcpd_id = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_pcpd_id: ', i_pcpd_id );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
     select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.payload_kg* flpns.price_c_p_kg) as price
	from ams_al_flight_pcpd pc
		join ams_ac.ams_al_flight_plan flp on flp.dest_id = pc.dest_id and flp.flp_id = in_flp_id 
		left join ams_wad.cfg_airport_destination d on d.dest_id = pc.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where pc.pcpd_id = i_pcpd_id;
    
    SET i_flppl_id = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_flppl_id: ', i_flppl_id );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    COMMIT;
    DELETE FROM ams_wad.ams_airport_cpd_for_flp where cpd_id = in_cpd_id;
    DELETE FROM ams_wad.ams_airport_cpd where cpd_id = in_cpd_id;
    COMMIT;
	SET logNote=CONCAT(' deleted from ams_airport_cpd in_cpd_id: ', in_cpd_id );
	call ams_wad.log_sp(spname, stTime, logNote);
    call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id);
    
END IF;


SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_cpd_triple` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_cpd_triple`(IN in_flp_id INT, IN in_cpd_id INT, IN in_flp_id_2 INT, IN in_flp_id_3 INT)
BEGIN


DECLARE i_pcpd_id, i_flppl_id INT;
DECLARE i_pcpd_id_2, i_flppl_id_2 INT;
DECLARE i_pcpd_id_3, i_flppl_id_3 INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_cpd_triple', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_cpd_triple';
set countRows = 0;
set i_pcpd_id = 0;

SET logNote=CONCAT('Start (', in_flp_id, ', ',in_cpd_id, ', ',in_flp_id_2, ', ', in_flp_id_3 ,') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

select count(*) into countRows from ams_wad.ams_airport_cpd p where p.cpd_id = in_cpd_id;

IF countRows>0 THEN

	INSERT INTO ams_ac.ams_al_flight_pcpd
		(cpd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	select 
		p.cpd_id, p.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 1, 
		0, p.pax_class_id, p.ppd_type_id, p.payload_kg, (p.payload_kg*flpns.price_c_p_kg),
        concat(p.note, ', #1 of 3 transfer flights')
		from (select * from ams_wad.ams_airport_cpd where cpd_id = in_cpd_id for share) p 
		join (select * from ams_ac.ams_al_flight_plan where flp_id = in_flp_id for share) flp on 1=1 
        join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where flp.flp_id = in_flp_id and p.cpd_id = in_cpd_id;
    
    SET i_pcpd_id = LAST_INSERT_ID();
    COMMIT;
    SET logNote=CONCAT('Inserted i_pcpd_id: ', i_pcpd_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_pcpd
		(cpd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	select 
		p.cpd_id, p.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 2, 
		0, p.pax_class_id, p.ppd_type_id, p.payload_kg, (p.payload_kg*flpns.price_c_p_kg),
        concat(p.note, ', #2 of 3 transfer flights')
		from (select * from ams_wad.ams_airport_cpd where cpd_id = in_cpd_id for share) p 
		join (select * from ams_ac.ams_al_flight_plan where flp_id = in_flp_id_2 for share) flp on 1=1 
        join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where flp.flp_id = in_flp_id_2 and p.cpd_id = in_cpd_id;
    
    SET i_pcpd_id_2 = LAST_INSERT_ID();
    COMMIT;
    SET logNote=CONCAT('Inserted i_pcpd_id_2: ', i_pcpd_id_2 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_pcpd
		(cpd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	select 
		p.cpd_id, p.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 2, 
		0, p.pax_class_id, p.ppd_type_id, p.payload_kg, (p.payload_kg*flpns.price_c_p_kg),
        concat(p.note, ', #3 of 3 transfer flights')
		from (select * from ams_wad.ams_airport_cpd where cpd_id = in_cpd_id for share) p 
		join (select * from ams_ac.ams_al_flight_plan where flp_id = in_flp_id_3 for share) flp on 1=1 
        join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where flp.flp_id = in_flp_id_3 and p.cpd_id = in_cpd_id;
    
    SET i_pcpd_id_3 = LAST_INSERT_ID();
    COMMIT;
    SET logNote=CONCAT('Inserted i_pcpd_id_3: ', i_pcpd_id_3 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
     select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.payload_kg* flpns.price_c_p_kg) as price
	from ams_al_flight_pcpd pc
		join (select * from ams_ac.ams_al_flight_plan where flp_id = in_flp_id for share) flp on 1=1  
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where  flp.flp_id = in_flp_id  and pc.pcpd_id = i_pcpd_id;
    
    SET i_flppl_id = LAST_INSERT_ID();
    COMMIT;
    SET logNote=CONCAT('Inserted i_flppl_id: ', i_flppl_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
     select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.payload_kg* flpns.price_c_p_kg) as price
	from ams_al_flight_pcpd pc
		join (select * from ams_ac.ams_al_flight_plan where flp_id = in_flp_id_2 for share) flp on 1=1  
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where  flp.flp_id = in_flp_id_2  and pc.pcpd_id = i_pcpd_id_2;
    
    SET i_flppl_id_2 = LAST_INSERT_ID();
    COMMIT;
    SET logNote=CONCAT('Inserted i_flppl_id_2: ', i_flppl_id_2 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
     select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.payload_kg* flpns.price_c_p_kg) as price
	from ams_al_flight_pcpd pc
		join (select * from ams_ac.ams_al_flight_plan where flp_id = in_flp_id_3 for share) flp on 1=1  
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where  flp.flp_id = in_flp_id_3  and pc.pcpd_id = i_pcpd_id_3;
    
    SET i_flppl_id_3 = LAST_INSERT_ID();
    COMMIT;
    SET logNote=CONCAT('Inserted i_flppl_id_3: ', i_flppl_id_3 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
	
    DELETE FROM ams_wad.ams_airport_cpd_for_flp where cpd_id = in_cpd_id;
    DELETE FROM ams_wad.ams_airport_cpd where cpd_id = in_cpd_id;
    COMMIT;
	SET logNote=CONCAT(' deleted from ams_airport_cpd in_cpd_id: ', in_cpd_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id);
	call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id_2);
	call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id_3);
	commit;

END IF;


SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_c_cpd` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_c_cpd`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_c_cpd', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_c_cpd';
set countRows = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);
-- SET SQL_SAFE_UPDATES = 0;
-- Update ams_al_flight_plan_payload_sum
	truncate table ams_ac.ams_al_flight_plan_transfer_cpd;
    SET countRows =  ROW_COUNT();
	commit;
    SET logNote=CONCAT('Truncated ', FORMAT(countRows, 0), ' rows from ams_ac.ams_al_flight_plan_transfer_cpd.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    truncate table ams_ac.ams_airport_cpd_for_c;
	commit;
	insert into ams_ac.ams_airport_cpd_for_c
	select * from ams_wad.ams_airport_cpd;
     SET countRows =  ROW_COUNT();
	commit;
    SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' rows from ams_ac.ams_airport_cpd_for_c.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	INSERT INTO ams_ac.ams_al_flight_plan_transfer_cpd
		(transfer_id, pax_class_id, cpd_id, d_ap_id, a_ap_id, dest_id, distance_km,
			flp_id_1, flp_id_2, flp_id_3, flp_id_4,
			fl_name_1, fl_name_2, fl_name_3, fl_name_4,
			ppd_pax, ppd_payload_kg, ppd_price, ppd_note,
			payload_diff_1,	payload_diff_2,	payload_diff_3,	payload_diff_4,
			flpns_price_sum, flpns_price, 
            free_payload_1, free_payload_2, free_payload_3, free_payload_4)
	SELECT tr.transfer_id,  p.pax_class_id, p.cpd_id,
		tr.d_ap_id, tr.a_ap_id, tr.dest_id, tr.distance_km, 
		tr.flp_id_1, tr.flp_id_2, tr.flp_id_3, tr.flp_id_4,
		tr.fl_name_1, tr.fl_name_2,tr.fl_name_3,tr.fl_name_4,
		0 AS ppd_pax,
		p.payload_kg AS ppd_payload_kg,
		ROUND(p.max_ticket_price, 2) AS ppd_price,
		p.note AS ppd_note,
		 (if(tr.flp_id_1 is null, p.payload_kg, ifnull(flp1.flp_free_payload,-1))-p.payload_kg) as payload_diff_1,
		 (if(tr.flp_id_2 is null, p.payload_kg, ifnull(flp2.flp_free_payload,-1))-p.payload_kg) as payload_diff_2,
		 (if(tr.flp_id_3 is null, p.payload_kg, ifnull(flp3.flp_free_payload,-1))-p.payload_kg) as payload_diff_3,
		 (if(tr.flp_id_4 is null, p.payload_kg, ifnull(flp4.flp_free_payload,-1))-p.payload_kg) as payload_diff_4,
		(ifnull(flp1.flpns_price,0) + ifnull(flp2.flpns_price,0) + ifnull(flp3.flpns_price,0) + ifnull(flp4.flpns_price,0)) as flpns_price_sum,
		round((p.payload_kg * (ifnull(flp1.flpns_price,0) + ifnull(flp2.flpns_price,0) + ifnull(flp3.flpns_price,0) + ifnull(flp4.flpns_price,0))),2) as flpns_price,
		ifnull(flp1.flp_free_payload,0) as free_payload_1, ifnull(flp2.flp_free_payload,0) as free_payload_2, 
		ifnull(flp3.flp_free_payload,0) as free_payload_3, ifnull(flp4.flp_free_payload,0) as free_payload_4
	FROM ams_ac.ams_al_flight_plan_transfer tr
		join ams_ac.ams_airport_cpd_for_c p on p.dest_id = tr.dest_id
		join ams_ac.ams_al_flight_plan_payload_sum flp1 on flp1.flp_id = tr.flp_id_1 and flp1.pax_class_id = p.pax_class_id
		join ams_ac.ams_al_flight_plan_payload_sum flp2 on flp2.flp_id = tr.flp_id_2 and flp2.pax_class_id = flp1.pax_class_id
		left join ams_ac.ams_al_flight_plan_payload_sum flp3 on flp3.flp_id = tr.flp_id_3 and flp3.pax_class_id = flp1.pax_class_id
		left join ams_ac.ams_al_flight_plan_payload_sum flp4 on flp4.flp_id = tr.flp_id_4 and flp3.pax_class_id = flp1.pax_class_id
	where p.pax_class_id = 4
	having (payload_diff_1>=0 and payload_diff_2 >= 0 and payload_diff_3 >=0 and payload_diff_4 >= 0) and ppd_price >= flpns_price
    limit 10000;
	SET countRows =  ROW_COUNT();
	commit;
    SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' rows in ams_ac.ams_al_flight_plan_transfer_cpd.' );
	call ams_wad.log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_c_payload_sum` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_c_payload_sum`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_c_payload_sum', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_c_payload_sum';
set countRows = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);
-- SET SQL_SAFE_UPDATES = 0;
-- Update ams_al_flight_plan_payload_sum
	truncate table ams_ac.ams_al_flight_plan_payload_sum;
    SET countRows =  ROW_COUNT();
	commit;
    SET logNote=CONCAT('Truncated ', FORMAT(countRows, 0), ' rows from ams_ac.ams_al_flight_plan_payload_sum.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	INSERT INTO ams_ac.ams_al_flight_plan_payload_sum
		(flp_id, flpns_id, flp_status_id, ac_id,
		pax_class_id, flp_payload, cabin_payload, flp_free_payload, flpns_price)
	SELECT flp.flp_id, flp.flpns_id, flp.flp_status_id, acc.ac_id,
		acc.pax_class_id, 
		sum(if(acc.pax_class_id = 4,ifnull(pl.payload_kg,0), ifnull(pl.pax,0)))as flp_payload,
		acc.cabin_payload,
		round((acc.cabin_payload - 
			sum(if(acc.pax_class_id = 4,ifnull(pl.payload_kg,0), ifnull(pl.pax,0)))
		),2) as flp_free_payload, 
		CASE
			WHEN acc.pax_class_id = 1 THEN ifnull(flpns.price_e,0)
			WHEN acc.pax_class_id = 2 THEN ifnull(flpns.price_b,0)
			WHEN acc.pax_class_id = 3 THEN ifnull(flpns.price_f,0)
			WHEN acc.pax_class_id = 4 THEN round(ifnull(flpns.price_c_p_kg,0),4)
			ELSE 0
		END as flpns_price 
	FROM ams_ac.ams_al_flight_plan flp
		join ams_ac.v_ams_aircraft_cabin acc on acc.ac_id = flp.ac_id
		left join ams_ac.ams_al_flight_plan_payload pl on pl.flp_id = flp.flp_id and pl.pax_class_id = acc.pax_class_id
		JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
	where flp.flp_status_id < 2 
	group by flp.flp_id, acc.pax_class_id
	having cabin_payload > (0.95* flp_free_payload) and flp_free_payload >0;
	SET countRows =  ROW_COUNT();
	commit;
    SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' rows in ams_ac.ams_al_flight_plan_payload_sum.' );
	call ams_wad.log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_c_ppd` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_c_ppd`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_c_ppd', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_c_ppd';
set countRows = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);
-- SET SQL_SAFE_UPDATES = 0;
-- Update ams_al_flight_plan_payload_sum
	truncate table ams_ac.ams_al_flight_plan_transfer_ppd;
    SET countRows =  ROW_COUNT();
	commit;
    SET logNote=CONCAT('Truncated ', FORMAT(countRows, 0), ' rows from ams_ac.ams_al_flight_plan_transfer_ppd.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    truncate table ams_ac.ams_airport_ppd_for_c;
	commit;
	insert into ams_ac.ams_airport_ppd_for_c
	select * from ams_wad.ams_airport_ppd;
     SET countRows =  ROW_COUNT();
	commit;
    SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' rows from ams_ac.ams_airport_ppd_for_c.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	INSERT INTO ams_ac.ams_al_flight_plan_transfer_ppd
		(transfer_id, pax_class_id, ppd_id, d_ap_id, a_ap_id, dest_id, distance_km,
			flp_id_1, flp_id_2, flp_id_3, flp_id_4,
			fl_name_1, fl_name_2, fl_name_3, fl_name_4,
			ppd_pax, ppd_payload_kg, ppd_price, ppd_note,
			pax_diff_1,	pax_diff_2,	pax_diff_3,	pax_diff_4,
			flpns_price_sum, flpns_price, 
            free_payload_1, free_payload_2, free_payload_3, free_payload_4)
	SELECT tr.transfer_id,  p.pax_class_id, p.ppd_id,
		tr.d_ap_id, tr.a_ap_id, tr.dest_id, tr.distance_km, 
		tr.flp_id_1, tr.flp_id_2, tr.flp_id_3, tr.flp_id_4,
		tr.fl_name_1, tr.fl_name_2,tr.fl_name_3,tr.fl_name_4,
		p.pax AS ppd_pax,
		p.payload_kg AS ppd_payload_kg,
		ROUND(p.max_ticket_price, 2) AS ppd_price,
		p.note AS ppd_note,
		 (if(tr.flp_id_1 is null, p.pax, ifnull(flp1.flp_free_payload,-1))-p.pax) as pax_diff_1,
		 (if(tr.flp_id_2 is null, p.pax, ifnull(flp2.flp_free_payload,-1))-p.pax) as pax_diff_2,
		 (if(tr.flp_id_3 is null, p.pax, ifnull(flp3.flp_free_payload,-1))-p.pax) as pax_diff_3,
		 (if(tr.flp_id_4 is null, p.pax, ifnull(flp4.flp_free_payload,-1))-p.pax) as pax_diff_4,
		(ifnull(flp1.flpns_price,0) + ifnull(flp2.flpns_price,0) + ifnull(flp3.flpns_price,0) + ifnull(flp4.flpns_price,0)) as flpns_price_sum,
		round((p.pax * (ifnull(flp1.flpns_price,0) + ifnull(flp2.flpns_price,0) + ifnull(flp3.flpns_price,0) + ifnull(flp4.flpns_price,0))),2) as flpns_price,
		ifnull(flp1.flp_free_payload,0) as free_payload_1, ifnull(flp2.flp_free_payload,0) as free_payload_2, 
		ifnull(flp3.flp_free_payload,0) as free_payload_3, ifnull(flp4.flp_free_payload,0) as free_payload_4
	FROM ams_ac.ams_al_flight_plan_transfer tr
		join ams_ac.ams_airport_ppd_for_c p on p.dest_id = tr.dest_id
		join ams_ac.ams_al_flight_plan_payload_sum flp1 on flp1.flp_id = tr.flp_id_1 and flp1.pax_class_id = p.pax_class_id
		join ams_ac.ams_al_flight_plan_payload_sum flp2 on flp2.flp_id = tr.flp_id_2 and flp2.pax_class_id = flp1.pax_class_id
		left join ams_ac.ams_al_flight_plan_payload_sum flp3 on flp3.flp_id = tr.flp_id_3 and flp3.pax_class_id = flp1.pax_class_id
		left join ams_ac.ams_al_flight_plan_payload_sum flp4 on flp4.flp_id = tr.flp_id_4 and flp3.pax_class_id = flp1.pax_class_id
	where p.pax_class_id < 4
	having (pax_diff_1>=0 and pax_diff_2 >= 0 and pax_diff_3 >=0 and pax_diff_4 >= 0) and ppd_price >= flpns_price
    limit 10000;
	SET countRows =  ROW_COUNT();
	commit;
    SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' rows in ams_ac.ams_al_flight_plan_transfer_ppd.' );
	call ams_wad.log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_d_cpd_by_transfer_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_d_cpd_by_transfer_id`(IN in_transfer_id INT, IN in_cpd_id INT)
BEGIN

DECLARE i_cpd_id, i_tr_id, i_tr_count INT;
DECLARE i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4 INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_d_cpd_by_transfer_id', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_d_cpd_by_transfer_id';
set countRows = 0;

SET logNote=CONCAT('Start (',in_transfer_id,') ->' );
call ams_wad.log_sp(spname, stTime, logNote);
-- SET SQL_SAFE_UPDATES = 0;
set i_tr_count = 0;
-- read v_ams_al_flight_plan_payload_add_n_transfers_ppd_add
set i_cpd_id = 0;set i_flp_id_1 = 0;set i_flp_id_2 = 0;set i_flp_id_3 = 0;set i_flp_id_4 = 0;
SELECT tr.transfer_id, p.cpd_id, tr.flp_id_1, tr.flp_id_2, tr.flp_id_3, tr.flp_id_4,
	(if(tr.flp_id_1 is null,0,1)+if(tr.flp_id_2 is null,0,1)+if(tr.flp_id_3 is null,0,1)+if(tr.flp_id_4 is null,0,1)) tr_count
INTO i_tr_id, i_cpd_id, i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4, i_tr_count
FROM ams_ac.ams_al_flight_plan_transfer tr
join ams_ac.ams_airport_cpd p on p.dest_id = tr.dest_id
left join ams_ac.v_ams_flight_plan_payload_sum flp1 on flp1.flp_id = tr.flp_id_1 and flp1.pax_class_id = p.pax_class_id
left join ams_ac.v_ams_flight_plan_payload_sum flp2 on flp2.flp_id = tr.flp_id_2 and flp2.pax_class_id = p.pax_class_id
left join ams_ac.v_ams_flight_plan_payload_sum flp3 on flp3.flp_id = tr.flp_id_3 and flp3.pax_class_id = p.pax_class_id
left join ams_ac.v_ams_flight_plan_payload_sum flp4 on flp4.flp_id = tr.flp_id_4 and flp3.pax_class_id = p.pax_class_id
where tr.transfer_id=in_transfer_id and p.cpd_id=in_cpd_id
limit 1;
SET logNote=CONCAT('i_cpd_id: ', i_cpd_id, ' .' );
call ams_wad.log_sp(spname, stTime, logNote);
    
IF i_cpd_id  > 0 THEN
    
	IF i_tr_count = 4 THEN
		SET logNote=CONCAT(' i_flp_id_4: ', i_flp_id_4, ', i_tr_id: ', i_tr_id);
		call ams_wad.log_sp(spname, stTime, logNote);
		call ams_ac.sp_al_flight_plan_payload_add_cpd_quadruple(i_flp_id_1, i_cpd_id, i_flp_id_2, i_flp_id_3, i_flp_id_4);
		
	ELSEIF i_tr_count = 3 THEN
		if i_flp_id_1 is null THEN
			SET logNote=CONCAT(' i_flp_id_3: ', i_flp_id_4, ', i_tr_id: ', i_tr_id);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_cpd_triple(i_flp_id_2, i_cpd_id, i_flp_id_3, i_flp_id_4);
        ELSE
			SET logNote=CONCAT(' i_flp_id_3: ', i_flp_id_3, ', i_tr_id: ', i_tr_id);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_cpd_triple(i_flp_id_1, i_cpd_id, i_flp_id_2, i_flp_id_3);
        END IF;
		
	ELSEIF i_tr_count = 2 THEN
		SET logNote=CONCAT(' i_flp_id_2: ', i_flp_id_2, ', i_tr_id: ', i_tr_id);
		call ams_wad.log_sp(spname, stTime, logNote);
		call ams_ac.sp_al_flight_plan_payload_add_cpd_double(i_flp_id_1, i_cpd_id, i_flp_id_2);    
	END IF;
	commit;
		
	delete from ams_ac.ams_airport_cpd where cpd_id = i_cpd_id;
    commit;
	delete from ams_wad.ams_airport_cpd where cpd_id = i_cpd_id;
	commit;
	
END IF;
    
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_d_flp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_d_flp`()
BEGIN

DECLARE i_flp_id, i_ppd_id, i_cpd_id, i_flp_status_id INT;
DECLARE i_payload_jobs, i_flp_count INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote, logStart VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_flp CURSOR FOR SELECT flp_id, ifnull(payload_jobs,0)  
					FROM ams_ac.ams_al_flight_plan flp
					where flp.flp_status_id = 1
					order by flp.payload_jobs, dtime, atime  limit 3 FOR SHARE;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_d_flp', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_d_flp';
set i_flp_status_id = 2; -- Flight is sold out
set countRows = 0;
set i_flp_id = 0;
set i_payload_jobs = 0;
set i_flp_count = 0;
set i_cpd_id = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);
-- update AC PPD table
INSERT INTO ams_ac.ams_airport_ppd
(select p.* from ams_wad.ams_airport_ppd p 
left join ams_ac.ams_airport_ppd pd ON pd.ppd_id = p.ppd_id
where pd.ppd_id is null FOR SHARE);
SET countRows =  ROW_COUNT();
commit;
SET logNote=CONCAT('Added: ', FORMAT(countRows, 0, 'de_DE'), ' records in ams_ac.ams_airport_ppd.' );
call ams_wad.log_sp(spname, stTime, logNote);
-- update AC CPD table
INSERT INTO ams_ac.ams_airport_cpd
(select p.* from ams_wad.ams_airport_cpd p 
left join ams_ac.ams_airport_cpd pd ON pd.cpd_id = p.cpd_id
where pd.cpd_id is null FOR SHARE);
SET countRows =  ROW_COUNT();
commit;
SET logNote=CONCAT('Added: ', FORMAT(countRows, 0, 'de_DE'), ' records in ams_ac.ams_airport_cpd.' );
call ams_wad.log_sp(spname, stTime, logNote);
OPEN cursor_flp;

start transaction;
	delete tr.* FROM ams_ac.ams_al_flight_plan_transfer tr
	where (tr.flp_id_1 is not null and tr.flp_id_1 not in (select flp_id from ams_ac.ams_al_flight_plan FOR SHARE))
	or (tr.flp_id_2  is not null and tr.flp_id_2 not in (select flp_id from ams_ac.ams_al_flight_plan FOR SHARE))
	or (tr.flp_id_3  is not null and tr.flp_id_3 not in (select flp_id from ams_ac.ams_al_flight_plan FOR SHARE))
	or (tr.flp_id_4  is not null and tr.flp_id_4 not in (select flp_id from ams_ac.ams_al_flight_plan FOR SHARE));
	SET countRows =  ROW_COUNT();
commit;
if countRows >0 THEN
	SET logNote=CONCAT('Deleted: from ams_al_flight_plan_transfer ', FORMAT(countRows, 0, 'de_DE'), ' rows.' );
	call ams_wad.log_sp(spname, stTime, logNote);
END IF;

read_loop: LOOP
	
    FETCH cursor_flp INTO i_flp_id, i_payload_jobs;
	set i_flp_count = i_flp_count+1;
    
	IF done THEN
		SET logNote=CONCAT('No flp_id in queue found ', 'done' );
		 call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
    set logStart = CONCAT('flp #',i_flp_count,' - jobs: ',i_payload_jobs, ': ');
    SET logNote=CONCAT(logStart, ' i_flp_id: ',i_flp_id);
	call ams_wad.log_sp(spname, stTime, logNote);
	call ams_ac.sp_al_flight_plan_payload_add_d_flp_ppd_v01(i_flp_id);
	call ams_ac.sp_al_flight_plan_payload_add_d_flp_cpd_v01(i_flp_id);
     /*
    set countRows = 0;
    call ams_ac.sp_al_flight_plan_payload_add_d_flp_payload_sum(i_flp_id, countRows);
	SET logNote=CONCAT(logStart, ' payload_sum rows: ',FORMAT(countRows, 0, 'de_DE'));
	call ams_wad.log_sp(spname, stTime, logNote);
    
    IF countRows > 0 THEN
		set countRows = 0;
		call ams_ac.sp_al_flight_plan_payload_add_d_flp_ppd(i_flp_id, countRows);
        SET logNote=CONCAT(logStart, ' ppd rows: ', FORMAT(countRows, 0, 'de_DE'), '.' );
		call ams_wad.log_sp(spname, stTime, logNote);
        IF countRows > 0 THEN
			call sp_al_flight_plan_payload_add_d_flp_ppd_transfers();
        END IF;
        
        call ams_ac.sp_al_flight_plan_payload_add_d_flp_cpd(i_flp_id, countRows);
        SET logNote=CONCAT(logStart, ' cpd rows: ', FORMAT(countRows, 0, 'de_DE'), '.' );
         IF countRows > 0 THEN
			call sp_al_flight_plan_payload_add_d_flp_cpd_transfers();
        END IF;
	ELSE
		SET logNote=CONCAT(logStart, ' No ppd records for i_flp_id ', i_flp_id, '.' );
		call ams_wad.log_sp(spname, stTime, logNote);
    END IF;
    */
	set countRows = 0;
	update ams_ac.ams_al_flight_plan flp 
	set flp.payload_jobs = ifnull(flp.payload_jobs,0)+1
	where flp.flp_id = i_flp_id;
	commit;
	SET logNote=CONCAT(logStart, ' payload_jobs incremented.' );
	call ams_wad.log_sp(spname, stTime, logNote);
END LOOP;
 
CLOSE cursor_flp;

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_d_flp_cpd` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_d_flp_cpd`(IN in_flp_id INT, out out_count_rows INT)
BEGIN

DECLARE i_cpd_id, i_tr_id INT;
DECLARE i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4 INT;

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_d_flp_cpd', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_d_flp_cpd';
set countRows = 0;
 set out_count_rows = 0;
 
SET logNote=CONCAT('Start () ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);
-- SET SQL_SAFE_UPDATES = 0;
-- Update ams_al_flight_plan_payload_sum
	truncate table ams_ac.ams_al_flight_plan_transfer_flp_cpd;
    SET countRows =  ROW_COUNT();
	commit;
    SET logNote=CONCAT('Truncated ', FORMAT(countRows, 0, 'de_DE'), ' rows from ams_ac.ams_al_flight_plan_transfer_flp_cpd.' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    set countRows = 0;
	INSERT INTO ams_ac.ams_al_flight_plan_transfer_flp_cpd
		(transfer_id, pax_class_id, cpd_id, d_ap_id, a_ap_id, dest_id, distance_km,
			flp_id_1, flp_id_2, flp_id_3, flp_id_4,
			fl_name_1, fl_name_2, fl_name_3, fl_name_4,
			cpd_payload_kg, cpd_price, cpd_note,
			payload_diff_1,	payload_diff_2,	payload_diff_3,	payload_diff_4,
			flpns_price_sum, flpns_price, 
            free_payload_1, free_payload_2, free_payload_3, free_payload_4)
	(SELECT tr.transfer_id, p.pax_class_id, p.cpd_id,
		tr.d_ap_id, tr.a_ap_id, tr.dest_id, tr.distance_km, 
		tr.flp_id_1, tr.flp_id_2, tr.flp_id_3, tr.flp_id_4,
		tr.fl_name_1, tr.fl_name_2,tr.fl_name_3,tr.fl_name_4,
		p.payload_kg AS cpd_payload_kg,
		ROUND(p.max_ticket_price, 2) AS cpd_price,
		p.note AS cpd_note,
		(if(tr.flp_id_1 is null, p.payload_kg, ifnull(flp1.flp_free_payload,-1))-p.payload_kg) as payload_diff_1,
		 (if(tr.flp_id_2 is null, p.payload_kg, ifnull(flp2.flp_free_payload,-1))-p.payload_kg) as payload_diff_2,
		 (if(tr.flp_id_3 is null, p.payload_kg, ifnull(flp3.flp_free_payload,-1))-p.payload_kg) as payload_diff_3,
		 (if(tr.flp_id_4 is null, p.payload_kg, ifnull(flp4.flp_free_payload,-1))-p.payload_kg) as payload_diff_4,
		(ifnull(flp1.flpns_price,0) + ifnull(flp2.flpns_price,0) + ifnull(flp3.flpns_price,0) + ifnull(flp4.flpns_price,0)) as flpns_price_sum,
		round((p.payload_kg * (ifnull(flp1.flpns_price,0) + ifnull(flp2.flpns_price,0) + ifnull(flp3.flpns_price,0) + ifnull(flp4.flpns_price,0))),2) as flpns_price,
		ifnull(flp1.flp_free_payload,0) as free_payload_1, ifnull(flp2.flp_free_payload,0) as free_payload_2, 
		ifnull(flp3.flp_free_payload,0) as free_payload_3, ifnull(flp4.flp_free_payload,0) as free_payload_4
	FROM ams_ac.ams_al_flight_plan_transfer tr
    join ams_ac.ams_airport_cpd p on p.dest_id = tr.dest_id
    left join ams_ac.ams_al_flight_plan_payload_sum flp1 on flp1.flp_id = tr.flp_id_1 and flp1.pax_class_id = p.pax_class_id
    left join ams_ac.ams_al_flight_plan_payload_sum flp2 on flp2.flp_id = tr.flp_id_2 and flp2.pax_class_id = p.pax_class_id
	left join ams_ac.ams_al_flight_plan_payload_sum flp3 on flp3.flp_id = tr.flp_id_3 and flp3.pax_class_id = p.pax_class_id
	left join ams_ac.ams_al_flight_plan_payload_sum flp4 on flp4.flp_id = tr.flp_id_4 and flp3.pax_class_id = p.pax_class_id
	where (tr.flp_id_1 = in_flp_id or tr.flp_id_2 = in_flp_id or tr.flp_id_3 = in_flp_id or tr.flp_id_4 = in_flp_id)
    having (payload_diff_1>=0 and payload_diff_2 >= 0 and payload_diff_3 >=0 and payload_diff_4 >= 0) and cpd_price >= flpns_price FOR SHARE);
    
	SET countRows =  ROW_COUNT();
	commit;
    SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0, 'de_DE'), ' rows in ams_ac.ams_al_flight_plan_transfer_flp_cpd.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    set out_count_rows = countRows;

SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_d_flp_cpd_transfers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_d_flp_cpd_transfers`()
BEGIN

DECLARE i_cpd_id, i_tr_id, i_tr_count INT;
DECLARE i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4 INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_d_flp_cpd_transfers', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_d_flp_cpd_transfers';
set countRows = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);
-- SET SQL_SAFE_UPDATES = 0;
set i_tr_count = 0;
-- read ams_al_flight_plan_transfer_flp_ppd
set i_cpd_id = 0;set i_flp_id_1 = 0;set i_flp_id_2 = 0;set i_flp_id_3 = 0;set i_flp_id_4 = 0;
SELECT tr.transfer_id, tr.cpd_id, tr.flp_id_1, tr.flp_id_2, tr.flp_id_3, tr.flp_id_4,
(if(tr.flp_id_1 is null,0,1)+if(tr.flp_id_2 is null,0,1)+if(tr.flp_id_3 is null,0,1)+if(tr.flp_id_4 is null,0,1)) tr_count
INTO i_tr_id, i_cpd_id, i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4, i_tr_count
FROM ams_ac.ams_al_flight_plan_transfer_flp_cpd tr
order by  cpd_payload_kg desc, distance_km, flpns_price
limit 1;
SET logNote=CONCAT('i_cpd_id: ', i_cpd_id, ' .' );
call ams_wad.log_sp(spname, stTime, logNote);
    
IF i_cpd_id  > 0 THEN
    
	IF i_tr_count = 4 THEN
		SET logNote=CONCAT(' i_flp_id_4: ', i_flp_id_4, ', i_tr_id: ', i_tr_id);
		call ams_wad.log_sp(spname, stTime, logNote);
		call ams_ac.sp_al_flight_plan_payload_add_cpd_quadruple(i_flp_id_1, i_cpd_id, i_flp_id_2, i_flp_id_3, i_flp_id_4);
		
	ELSEIF i_tr_count = 3 THEN
		IF i_flp_id_1 is null THEN
			SET logNote=CONCAT(' i_flp_id_2: ', i_flp_id_2, ', i_cpd_id: ', i_cpd_id, ' i_flp_id_3:', i_flp_id_3, ' i_flp_id_4:', i_flp_id_4);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_cpd_triple(i_flp_id_2, i_cpd_id, i_flp_id_3, i_flp_id_4);
		ELSE
			SET logNote=CONCAT(' i_flp_id_1: ', i_flp_id_1, ', i_cpd_id: ', i_cpd_id, ' i_flp_id_2:', i_flp_id_2, ' i_flp_id_3:', i_flp_id_3);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_cpd_triple(i_flp_id_1, i_cpd_id, i_flp_id_2, i_flp_id_3);
		END IF;
	ELSEIF i_tr_count = 2 THEN
		SET logNote=CONCAT(' i_flp_id_1: ', i_flp_id_1, ', i_cpd_id: ', i_cpd_id, ' i_flp_id_2:', i_flp_id_2);
		call ams_wad.log_sp(spname, stTime, logNote);
		call ams_ac.sp_al_flight_plan_payload_add_cpd_double(i_flp_id_1, i_cpd_id, i_flp_id_2);    
	END IF;
	commit;
		
	delete from ams_ac.ams_airport_cpd where cpd_id = i_cpd_id;
    commit;
	delete from ams_wad.ams_airport_cpd where cpd_id = i_cpd_id;
	commit;
	
END IF;
    
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_d_flp_cpd_v01` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_d_flp_cpd_v01`(IN in_flp_id INT)
BEGIN

DECLARE i_cpd_id, i_tr_id, i_tr_count, i_pax_rows INT;
DECLARE i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4 INT;

DECLARE countRows, i_pax_rows_cicles INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_d_flp_cpd_v01', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_d_flp_cpd_v01';
set countRows = 0;
 
SET logNote=CONCAT('Start () -> in_flp_id: ', in_flp_id );
-- call ams_wad.log_sp(spname, stTime, logNote);
set i_pax_rows_cicles = 0;	
set i_pax_rows = 1;
insert_payload: LOOP
    set i_pax_rows_cicles = i_pax_rows_cicles + 1;	
    set i_pax_rows = 0;
	drop TEMPORARY table IF EXISTS tmp_flight_plan_transfer_flp_cpd;	
	CREATE TEMPORARY TABLE tmp_flight_plan_transfer_flp_cpd
    (SELECT tr.transfer_id, p.pax_class_id, p.cpd_id,
		tr.d_ap_id, tr.a_ap_id, tr.dest_id, tr.distance_km, 
		tr.flp_id_1, tr.flp_id_2, tr.flp_id_3, tr.flp_id_4,
		tr.fl_name_1, tr.fl_name_2,tr.fl_name_3,tr.fl_name_4,
		p.payload_kg AS cpd_payload_kg,
		ROUND(p.max_ticket_price, 2) AS cpd_price,
		p.note AS cpd_note,
		(if(tr.flp_id_1 is null, p.payload_kg, ifnull(flp1.flp_free_payload,-1))-p.payload_kg) as payload_diff_1,
		 (if(tr.flp_id_2 is null, p.payload_kg, ifnull(flp2.flp_free_payload,-1))-p.payload_kg) as payload_diff_2,
		 (if(tr.flp_id_3 is null, p.payload_kg, ifnull(flp3.flp_free_payload,-1))-p.payload_kg) as payload_diff_3,
		 (if(tr.flp_id_4 is null, p.payload_kg, ifnull(flp4.flp_free_payload,-1))-p.payload_kg) as payload_diff_4,
		(ifnull(flp1.flpns_price,0) + ifnull(flp2.flpns_price,0) + ifnull(flp3.flpns_price,0) + ifnull(flp4.flpns_price,0)) as flpns_price_sum,
		round((p.payload_kg * (ifnull(flp1.flpns_price,0) + ifnull(flp2.flpns_price,0) + ifnull(flp3.flpns_price,0) + ifnull(flp4.flpns_price,0))),2) as flpns_price,
		ifnull(flp1.flp_free_payload,0) as free_payload_1, ifnull(flp2.flp_free_payload,0) as free_payload_2, 
		ifnull(flp3.flp_free_payload,0) as free_payload_3, ifnull(flp4.flp_free_payload,0) as free_payload_4
	FROM ams_ac.ams_al_flight_plan_transfer tr
    join ams_ac.ams_airport_cpd p on p.dest_id = tr.dest_id
    left join ams_ac.v_ams_flight_plan_payload_sum flp1 on flp1.flp_id = tr.flp_id_1 and flp1.pax_class_id = p.pax_class_id
    left join ams_ac.v_ams_flight_plan_payload_sum flp2 on flp2.flp_id = tr.flp_id_2 and flp2.pax_class_id = p.pax_class_id
	left join ams_ac.v_ams_flight_plan_payload_sum flp3 on flp3.flp_id = tr.flp_id_3 and flp3.pax_class_id = p.pax_class_id
	left join ams_ac.v_ams_flight_plan_payload_sum flp4 on flp4.flp_id = tr.flp_id_4 and flp4.pax_class_id = p.pax_class_id
	where (tr.flp_id_1 = in_flp_id or tr.flp_id_2 = in_flp_id or tr.flp_id_3 = in_flp_id or tr.flp_id_4 = in_flp_id)
    having (payload_diff_1>=0 and payload_diff_2 >= 0 and payload_diff_3 >=0 and payload_diff_4 >= 0) and cpd_price >= flpns_price FOR SHARE);
    
	SET i_pax_rows =  ROW_COUNT();
	commit;
    SET logNote=CONCAT(i_pax_rows_cicles, ' - Inserted ', FORMAT(i_pax_rows, 0, 'de_DE'), ' rows in tmp_flight_plan_transfer_flp_cpd.' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    IF i_pax_rows = 0 THEN
		SET logNote=CONCAT(i_pax_rows_cicles, ' - LEAVE no transfer flights.');
		call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE insert_payload;
    ELSE
		set i_tr_count = 0;
		set i_cpd_id = 0;set i_flp_id_1 = 0;set i_flp_id_2 = 0;set i_flp_id_3 = 0;set i_flp_id_4 = 0;
		SELECT tr.transfer_id, tr.cpd_id, tr.flp_id_1, tr.flp_id_2, tr.flp_id_3, tr.flp_id_4,
		(if(tr.flp_id_1 is null,0,1)+if(tr.flp_id_2 is null,0,1)+if(tr.flp_id_3 is null,0,1)+if(tr.flp_id_4 is null,0,1)) tr_count
		INTO i_tr_id, i_cpd_id, i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4, i_tr_count
		FROM tmp_flight_plan_transfer_flp_cpd tr
		order by  cpd_payload_kg desc, distance_km, flpns_price
		limit 1;
        
        SET logNote=CONCAT(i_pax_rows_cicles, ' - i_cpd_id: ', i_cpd_id, ' number flights:', i_tr_count );
		call ams_wad.log_sp(spname, stTime, logNote);
        
        IF i_cpd_id  > 0 THEN
			
			IF i_tr_count = 4 THEN
				SET logNote=CONCAT(' i_flp_id_4: ', i_flp_id_4, ', i_tr_id: ', i_tr_id);
				-- call ams_wad.log_sp(spname, stTime, logNote);
				call ams_ac.sp_al_flight_plan_payload_add_cpd_quadruple(i_flp_id_1, i_cpd_id, i_flp_id_2, i_flp_id_3, i_flp_id_4);
				
			ELSEIF i_tr_count = 3 THEN
				IF i_flp_id_1 is null THEN
					SET logNote=CONCAT(' i_flp_id_2: ', i_flp_id_2, ', i_cpd_id: ', i_cpd_id, ' i_flp_id_3:', i_flp_id_3, ' i_flp_id_4:', i_flp_id_4);
					-- call ams_wad.log_sp(spname, stTime, logNote);
					call ams_ac.sp_al_flight_plan_payload_add_cpd_triple(i_flp_id_2, i_cpd_id, i_flp_id_3, i_flp_id_4);
				ELSE
					SET logNote=CONCAT(' i_flp_id_1: ', i_flp_id_1, ', i_cpd_id: ', i_cpd_id, ' i_flp_id_2:', i_flp_id_2, ' i_flp_id_3:', i_flp_id_3);
					-- call ams_wad.log_sp(spname, stTime, logNote);
					call ams_ac.sp_al_flight_plan_payload_add_cpd_triple(i_flp_id_1, i_cpd_id, i_flp_id_2, i_flp_id_3);
				END IF;
			ELSEIF i_tr_count = 2 THEN
				SET logNote=CONCAT(' i_flp_id_1: ', i_flp_id_1, ', i_cpd_id: ', i_cpd_id, ' i_flp_id_2:', i_flp_id_2);
				-- call ams_wad.log_sp(spname, stTime, logNote);
				call ams_ac.sp_al_flight_plan_payload_add_cpd_double(i_flp_id_1, i_cpd_id, i_flp_id_2);    
			END IF;
			commit;
				
			delete from ams_ac.ams_airport_cpd where cpd_id = i_cpd_id;
			commit;
			delete from ams_wad.ams_airport_cpd where cpd_id = i_cpd_id;
			commit;
		else
			SET logNote=CONCAT(i_pax_rows_cicles, ' - LEAVE no i_cpd_id.');
			call ams_wad.log_sp(spname, stTime, logNote);
			LEAVE insert_payload;
		END IF;
        
    END IF; -- IF i_pax_rows > 0 THEN
    IF i_pax_rows_cicles > 10 then
		SET logNote=CONCAT(i_pax_rows_cicles, ' - LEAVE too many cicles.');
		call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE insert_payload;
    end if;
END LOOP;
	drop TEMPORARY table IF EXISTS tmp_flight_plan_transfer_flp_cpd;
    
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_d_flp_payload_sum` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_d_flp_payload_sum`(IN in_flp_id INT, OUT out_count_rows INT)
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_d_flp_payload_sum', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_d_flp_payload_sum';
set countRows = 0;

SET logNote=CONCAT('Start () ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);
-- SET SQL_SAFE_UPDATES = 0;
-- Update ams_al_flight_plan_payload_sum_flp
	truncate table ams_ac.ams_al_flight_plan_payload_sum_flp;
    SET countRows =  ROW_COUNT();
	commit;
    SET logNote=CONCAT('Truncated ', FORMAT(countRows, 0, 'de_DE'), ' rows from ams_ac.ams_al_flight_plan_payload_sum_flp.' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
	INSERT INTO ams_ac.ams_al_flight_plan_payload_sum_flp
		(flp_id, flpns_id, flp_status_id, ac_id,
		pax_class_id, flp_payload, cabin_payload, flp_free_payload, flpns_price)
	(SELECT flp.flp_id, flp.flpns_id, flp.flp_status_id, acc.ac_id,
		acc.pax_class_id, 
		sum(if(acc.pax_class_id = 4,ifnull(pl.payload_kg,0), ifnull(pl.pax,0)))as flp_payload,
		acc.cabin_payload,
		round((acc.cabin_payload - 
			sum(if(acc.pax_class_id = 4,ifnull(pl.payload_kg,0), ifnull(pl.pax,0)))
		),2) as flp_free_payload, 
		CASE
			WHEN acc.pax_class_id = 1 THEN ifnull(flpns.price_e,0)
			WHEN acc.pax_class_id = 2 THEN ifnull(flpns.price_b,0)
			WHEN acc.pax_class_id = 3 THEN ifnull(flpns.price_f,0)
			WHEN acc.pax_class_id = 4 THEN round(ifnull(flpns.price_c_p_kg,0),4)
			ELSE 0
		END as flpns_price 
	FROM ams_ac.ams_al_flight_plan flp
		join ams_ac.v_ams_aircraft_cabin acc on acc.ac_id = flp.ac_id
		left join ams_ac.ams_al_flight_plan_payload pl on pl.flp_id = flp.flp_id and pl.pax_class_id = acc.pax_class_id
		JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
	where flp.flp_status_id < 2 and flp.flp_id = in_flp_id
	group by flp.flp_id, acc.pax_class_id
	having cabin_payload > (0.95* flp_free_payload) and flp_free_payload >0 FOR SHARE);
	SET countRows =  ROW_COUNT();
	commit;
    SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0, 'de_DE'), ' rows in ams_ac.ams_al_flight_plan_payload_sum_flp.' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
	set out_count_rows = countRows;
SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
 select out_count_rows from dual;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_d_flp_ppd` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_d_flp_ppd`(IN in_flp_id INT, out out_count_rows INT)
BEGIN

DECLARE i_ppd_id, i_tr_id INT;
DECLARE i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4 INT;

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_d_flp_ppd', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_d_flp_ppd';
set countRows = 0;
 set out_count_rows = 0;
 
SET logNote=CONCAT('Start () ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);
-- SET SQL_SAFE_UPDATES = 0;
-- Update ams_al_flight_plan_payload_sum
	truncate table ams_ac.ams_al_flight_plan_transfer_flp_ppd;
    SET countRows =  ROW_COUNT();
	commit;
    SET logNote=CONCAT('Truncated ', FORMAT(countRows, 0, 'de_DE'), ' rows from ams_ac.ams_al_flight_plan_transfer_flp_ppd.' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    set countRows = 0;
	INSERT INTO ams_ac.ams_al_flight_plan_transfer_flp_ppd
		(transfer_id, pax_class_id, ppd_id, d_ap_id, a_ap_id, dest_id, distance_km,
			flp_id_1, flp_id_2, flp_id_3, flp_id_4,
			fl_name_1, fl_name_2, fl_name_3, fl_name_4,
			ppd_pax, ppd_payload_kg, ppd_price, ppd_note,
			pax_diff_1,	pax_diff_2,	pax_diff_3,	pax_diff_4,
			flpns_price_sum, flpns_price, 
            free_payload_1, free_payload_2, free_payload_3, free_payload_4)
	(SELECT tr.transfer_id, p.pax_class_id, p.ppd_id,
		tr.d_ap_id, tr.a_ap_id, tr.dest_id, tr.distance_km, 
		tr.flp_id_1, tr.flp_id_2, tr.flp_id_3, tr.flp_id_4,
		tr.fl_name_1, tr.fl_name_2,tr.fl_name_3,tr.fl_name_4,
        p.pax AS ppd_pax,
		p.payload_kg AS ppd_payload_kg,
		ROUND(p.max_ticket_price, 2) AS ppd_price,
		p.note AS ppd_note,
		(if(tr.flp_id_1 is null, p.pax, ifnull(flp1.flp_free_payload,-1))-p.pax) as pax_diff_1,
        (if(tr.flp_id_2 is null, p.pax, ifnull(flp2.flp_free_payload,-1))-p.pax) as pax_diff_2,
		 (if(tr.flp_id_3 is null, p.pax, ifnull(flp3.flp_free_payload,-1))-p.pax) as pax_diff_3,
		 (if(tr.flp_id_4 is null, p.pax, ifnull(flp4.flp_free_payload,-1))-p.pax) as pax_diff_4,
         (ifnull(flp1.flpns_price,0) + ifnull(flp2.flpns_price,0) + ifnull(flp3.flpns_price,0) + ifnull(flp4.flpns_price,0)) as flpns_price_sum,
		round((p.pax * (ifnull(flp1.flpns_price,0) + ifnull(flp2.flpns_price,0) + ifnull(flp3.flpns_price,0) + ifnull(flp4.flpns_price,0))),2) as flpns_price,
		ifnull(flp1.flp_free_payload,0) as free_payload_1, ifnull(flp2.flp_free_payload,0) as free_payload_2, 
		ifnull(flp3.flp_free_payload,0) as free_payload_3, ifnull(flp4.flp_free_payload,0) as free_payload_4
	FROM ams_ac.ams_al_flight_plan_transfer tr
    join ams_ac.ams_airport_ppd p on p.dest_id = tr.dest_id
    left join ams_ac.ams_al_flight_plan_payload_sum flp1 on flp1.flp_id = tr.flp_id_1 and flp1.pax_class_id = p.pax_class_id
    left join ams_ac.ams_al_flight_plan_payload_sum flp2 on flp2.flp_id = tr.flp_id_2 and flp2.pax_class_id = p.pax_class_id
	left join ams_ac.ams_al_flight_plan_payload_sum flp3 on flp3.flp_id = tr.flp_id_3 and flp3.pax_class_id = p.pax_class_id
	left join ams_ac.ams_al_flight_plan_payload_sum flp4 on flp4.flp_id = tr.flp_id_4 and flp4.pax_class_id = p.pax_class_id
	where (tr.flp_id_1 = in_flp_id or tr.flp_id_2 = in_flp_id or tr.flp_id_3 = in_flp_id or tr.flp_id_4 = in_flp_id)
    having (pax_diff_1>=0 and pax_diff_2 >= 0 and pax_diff_3 >=0 and pax_diff_4 >= 0) and ppd_price >= flpns_price FOR SHARE);
    
	SET countRows =  ROW_COUNT();
	commit;
    SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0, 'de_DE'), ' rows in ams_ac.ams_al_flight_plan_transfer_flp_ppd.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    set out_count_rows = countRows;

SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_d_flp_ppd_transfers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_d_flp_ppd_transfers`()
BEGIN

DECLARE i_ppd_id, i_tr_id, i_tr_count INT;
DECLARE i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4 INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_d_flp_ppd_transfers', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_d_flp_ppd_transfers';
set countRows = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);
-- SET SQL_SAFE_UPDATES = 0;
set i_tr_count = 0;
-- read v_ams_al_flight_plan_payload_add_n_transfers_ppd_add
set i_ppd_id = 0;set i_flp_id_1 = 0;set i_flp_id_2 = 0;set i_flp_id_3 = 0;set i_flp_id_4 = 0;
SELECT tr.transfer_id, tr.ppd_id, tr.flp_id_1, tr.flp_id_2, tr.flp_id_3, tr.flp_id_4,
(if(tr.flp_id_1 is null,0,1)+if(tr.flp_id_2 is null,0,1)+if(tr.flp_id_3 is null,0,1)+if(tr.flp_id_4 is null,0,1)) tr_count
INTO i_tr_id, i_ppd_id, i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4, i_tr_count
FROM ams_ac.ams_al_flight_plan_transfer_flp_ppd tr
order by  ppd_pax desc, distance_km, flpns_price
limit 1;
SET logNote=CONCAT('i_ppd_id: ', i_ppd_id, ' .' );
call ams_wad.log_sp(spname, stTime, logNote);
    
IF i_ppd_id  > 0 THEN
    
	IF i_tr_count = 4 THEN
		SET logNote=CONCAT(' i_flp_id_4: ', i_flp_id_4, ', i_tr_id: ', i_tr_id);
		call ams_wad.log_sp(spname, stTime, logNote);
		call ams_ac.sp_al_flight_plan_payload_add_ppd_quadruple(i_flp_id_1, i_ppd_id, i_flp_id_2, i_flp_id_3, i_flp_id_4);
		
	ELSEIF i_tr_count = 3 THEN
		if i_flp_id_1 is null THEN
			SET logNote=CONCAT(' i_flp_id_3: ', i_flp_id_4, ', i_tr_id: ', i_tr_id);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_ppd_triple(i_flp_id_2, i_ppd_id, i_flp_id_3, i_flp_id_4);
        ELSE
			SET logNote=CONCAT(' i_flp_id_3: ', i_flp_id_3, ', i_tr_id: ', i_tr_id);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_ppd_triple(i_flp_id_1, i_ppd_id, i_flp_id_2, i_flp_id_3);
        END IF;
		
	ELSEIF i_tr_count = 2 THEN
		SET logNote=CONCAT(' i_flp_id_2: ', i_flp_id_2, ', i_tr_id: ', i_tr_id);
		call ams_wad.log_sp(spname, stTime, logNote);
		call ams_ac.sp_al_flight_plan_payload_add_ppd_double(i_flp_id_1, i_ppd_id, i_flp_id_2);    
	END IF;
	commit;
		
	delete from ams_ac.ams_airport_ppd where ppd_id = i_ppd_id;
    commit;
	delete from ams_wad.ams_airport_ppd where ppd_id = i_ppd_id;
	commit;
	
END IF;
    
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_d_flp_ppd_v01` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_d_flp_ppd_v01`(IN in_flp_id INT)
BEGIN

DECLARE i_ppd_id, i_tr_id, i_tr_count, i_pax_rows INT;
DECLARE i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4 INT;

DECLARE countRows, i_pax_rows_cicles INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_d_flp_ppd_v01', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_d_flp_ppd_v01';
set countRows = 0;
 
SET logNote=CONCAT('Start () -> in_flp_id: ', in_flp_id );
-- call ams_wad.log_sp(spname, stTime, logNote);
set i_pax_rows_cicles = 0;	
set i_pax_rows = 1;
insert_payload: LOOP
    set i_pax_rows_cicles = i_pax_rows_cicles + 1;	
    set i_pax_rows = 0;
	drop TEMPORARY table IF EXISTS tmp_flight_plan_transfer_flp_ppd;	
	CREATE TEMPORARY TABLE tmp_flight_plan_transfer_flp_ppd
	(SELECT 
        tr.transfer_id AS transfer_id,
        p.pax_class_id AS pax_class_id,
        p.ppd_id AS ppd_id,
        d.dep_ap_id AS d_ap_id,
        d.arr_ap_id AS a_ap_id,
        tr.dest_id AS dest_id,
        tr.distance_km AS distance_km,
        tr.flp_id_1 AS flp_id_1,
        tr.flp_id_2 AS flp_id_2,
        tr.flp_id_3 AS flp_id_3,
        tr.flp_id_4 AS flp_id_4,
        tr.fl_name_1 AS fl_name_1,
        tr.fl_name_2 AS fl_name_2,
        tr.fl_name_3 AS fl_name_3,
        tr.fl_name_4 AS fl_name_4,
        p.pax AS ppd_pax,
        p.payload_kg AS ppd_payload_kg,
        ROUND(p.max_ticket_price, 2) AS ppd_price,
        p.note AS ppd_note,
        p.adate AS adate,
        (IF((tr.flp_id_1 IS NULL),
            p.pax,
            (IFNULL(flp1.cabin_payload, 0) - IFNULL(flp1.flp_payload_sum, 0))) - p.pax) AS pax_diff_1,
        (IF((tr.flp_id_2 IS NULL),
            p.pax,
            (IFNULL(flp2.cabin_payload, 0) - IFNULL(flp2.flp_payload_sum, 0))) - p.pax) AS pax_diff_2,
        (IF((tr.flp_id_3 IS NULL),
            p.pax,
            (IFNULL(flp3.cabin_payload, 0) - IFNULL(flp3.flp_payload_sum, 0))) - p.pax) AS pax_diff_3,
        (IF((tr.flp_id_4 IS NULL),
            p.pax,
            (IFNULL(flp4.cabin_payload, 0) - IFNULL(flp4.flp_payload_sum, 0))) - p.pax) AS pax_diff_4,
        (((IFNULL(flp1.flpns_price, 0) + IFNULL(flp2.flpns_price, 0)) + IFNULL(flp3.flpns_price, 0)) + IFNULL(flp4.flpns_price, 0)) AS flpns_price_sum,
        ROUND((p.pax * (((IFNULL(flp1.flpns_price, 0) + IFNULL(flp2.flpns_price, 0)) + IFNULL(flp3.flpns_price, 0)) + IFNULL(flp4.flpns_price, 0))),
                2) AS flpns_price,
        (IFNULL(flp1.cabin_payload, 0) - IFNULL(flp1.flp_payload_sum, 0)) AS free_payload_1,
        (IFNULL(flp2.cabin_payload, 0) - IFNULL(flp2.flp_payload_sum, 0)) AS free_payload_2,
        (IFNULL(flp3.cabin_payload, 0) - IFNULL(flp3.flp_payload_sum, 0)) AS free_payload_3,
        (IFNULL(flp4.cabin_payload, 0) - IFNULL(flp4.flp_payload_sum, 0)) AS free_payload_4
    FROM
        ((((((ams_al_flight_plan_transfer tr
        JOIN ams_wad.ams_airport_ppd p ON ((p.dest_id = tr.dest_id)))
        JOIN ams_wad.cfg_airport_destination d ON ((d.dest_id = tr.dest_id)))
        LEFT JOIN v_ams_flight_plan_payload_sum_class flp1 ON (((flp1.flp_id = tr.flp_id_1)
            AND (flp1.pax_class_id = p.pax_class_id))))
        LEFT JOIN v_ams_flight_plan_payload_sum_class flp2 ON (((flp2.flp_id = tr.flp_id_2)
            AND (flp2.pax_class_id = p.pax_class_id))))
        LEFT JOIN v_ams_flight_plan_payload_sum_class flp3 ON (((flp3.flp_id = tr.flp_id_3)
            AND (flp3.pax_class_id = p.pax_class_id))))
        LEFT JOIN v_ams_flight_plan_payload_sum_class flp4 ON (((flp4.flp_id = tr.flp_id_4)
            AND (flp4.pax_class_id = p.pax_class_id))))
    WHERE
        ((IF((tr.flp_id_1 IS NULL), p.pax, (IFNULL(flp1.cabin_payload, 0) - IFNULL(flp1.flp_payload_sum, 0))) - p.pax) >= 0)
		AND ((IF((tr.flp_id_2 IS NULL), p.pax, (IFNULL(flp2.cabin_payload, 0) - IFNULL(flp2.flp_payload_sum, 0))) - p.pax) >= 0)
		AND ((IF((tr.flp_id_3 IS NULL), p.pax, (IFNULL(flp3.cabin_payload, 0) - IFNULL(flp3.flp_payload_sum, 0))) - p.pax) >= 0)
		AND ((IF((tr.flp_id_4 IS NULL), p.pax, (IFNULL(flp4.cabin_payload, 0) - IFNULL(flp4.flp_payload_sum, 0))) - p.pax) >= 0)
		AND (tr.flp_id_1 = in_flp_id or tr.flp_id_2 = in_flp_id or tr.flp_id_3 = in_flp_id or tr.flp_id_4 = in_flp_id) FOR SHARE);
    
    -- OLD QUERY
    /*
    SELECT tr.transfer_id, p.pax_class_id, p.ppd_id,
		tr.d_ap_id, tr.a_ap_id, tr.dest_id, tr.distance_km, 
		tr.flp_id_1, tr.flp_id_2, tr.flp_id_3, tr.flp_id_4,
		tr.fl_name_1, tr.fl_name_2,tr.fl_name_3,tr.fl_name_4,
        p.pax AS ppd_pax,
		p.payload_kg AS ppd_payload_kg,
		ROUND(p.max_ticket_price, 2) AS ppd_price,
		p.note AS ppd_note,
		(if(tr.flp_id_1 is null, p.pax, ifnull(flp1.flp_free_payload,-1))-p.pax) as pax_diff_1,
        (if(tr.flp_id_2 is null, p.pax, ifnull(flp2.flp_free_payload,-1))-p.pax) as pax_diff_2,
		 (if(tr.flp_id_3 is null, p.pax, ifnull(flp3.flp_free_payload,-1))-p.pax) as pax_diff_3,
		 (if(tr.flp_id_4 is null, p.pax, ifnull(flp4.flp_free_payload,-1))-p.pax) as pax_diff_4,
         (ifnull(flp1.flpns_price,0) + ifnull(flp2.flpns_price,0) + ifnull(flp3.flpns_price,0) + ifnull(flp4.flpns_price,0)) as flpns_price_sum,
		round((p.pax * (ifnull(flp1.flpns_price,0) + ifnull(flp2.flpns_price,0) + ifnull(flp3.flpns_price,0) + ifnull(flp4.flpns_price,0))),2) as flpns_price,
		ifnull(flp1.flp_free_payload,0) as free_payload_1, ifnull(flp2.flp_free_payload,0) as free_payload_2, 
		ifnull(flp3.flp_free_payload,0) as free_payload_3, ifnull(flp4.flp_free_payload,0) as free_payload_4
	FROM ams_ac.ams_al_flight_plan_transfer tr
    join ams_ac.ams_airport_ppd p on p.dest_id = tr.dest_id
    left join ams_ac.v_ams_flight_plan_payload_sum flp1 on flp1.flp_id = tr.flp_id_1 and flp1.pax_class_id = p.pax_class_id
    left join ams_ac.v_ams_flight_plan_payload_sum flp2 on flp2.flp_id = tr.flp_id_2 and flp2.pax_class_id = p.pax_class_id
	left join ams_ac.v_ams_flight_plan_payload_sum flp3 on flp3.flp_id = tr.flp_id_3 and flp3.pax_class_id = p.pax_class_id
	left join ams_ac.v_ams_flight_plan_payload_sum flp4 on flp4.flp_id = tr.flp_id_4 and flp4.pax_class_id = p.pax_class_id
	where (tr.flp_id_1 = in_flp_id or tr.flp_id_2 = in_flp_id or tr.flp_id_3 = in_flp_id or tr.flp_id_4 = in_flp_id)
    having (pax_diff_1>=0 and pax_diff_2 >= 0 and pax_diff_3 >=0 and pax_diff_4 >= 0) and ppd_price >= flpns_price FOR SHARE
    */
	SET i_pax_rows =  ROW_COUNT();
	commit;
    SET logNote=CONCAT(i_pax_rows_cicles, ' - Inserted ', FORMAT(i_pax_rows, 0, 'de_DE'), ' rows in tmp_flight_plan_transfer_flp_ppd.' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    IF i_pax_rows = 0 THEN
		SET logNote=CONCAT(i_pax_rows_cicles, ' - LEAVE no transfer flights.');
		call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE insert_payload;
    ELSE
		set i_tr_count = 0;
		set i_ppd_id = 0;set i_flp_id_1 = 0;set i_flp_id_2 = 0;set i_flp_id_3 = 0;set i_flp_id_4 = 0;
		SELECT tr.transfer_id, tr.ppd_id, tr.flp_id_1, tr.flp_id_2, tr.flp_id_3, tr.flp_id_4,
		(if(tr.flp_id_1 is null,0,1)+if(tr.flp_id_2 is null,0,1)+if(tr.flp_id_3 is null,0,1)+if(tr.flp_id_4 is null,0,1)) tr_count
		INTO i_tr_id, i_ppd_id, i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4, i_tr_count
		FROM tmp_flight_plan_transfer_flp_ppd tr
		order by  ppd_pax desc, distance_km, flpns_price
		limit 1;
        
        SET logNote=CONCAT(i_pax_rows_cicles, ' - i_ppd_id: ', i_ppd_id, ' number flights:', i_tr_count );
		call ams_wad.log_sp(spname, stTime, logNote);
        
        IF i_ppd_id  > 0 THEN
			
			IF i_tr_count = 4 THEN
				SET logNote=CONCAT(' i_flp_id_4: ', i_flp_id_4, ', i_tr_id: ', i_tr_id);
				-- call ams_wad.log_sp(spname, stTime, logNote);
				call ams_ac.sp_al_flight_plan_payload_add_ppd_quadruple(i_flp_id_1, i_ppd_id, i_flp_id_2, i_flp_id_3, i_flp_id_4);
			ELSEIF i_tr_count = 3 THEN
				if i_flp_id_1 is null THEN
					SET logNote=CONCAT(' i_flp_id_3: ', i_flp_id_4, ', i_tr_id: ', i_tr_id);
					-- call ams_wad.log_sp(spname, stTime, logNote);
					call ams_ac.sp_al_flight_plan_payload_add_ppd_triple(i_flp_id_2, i_ppd_id, i_flp_id_3, i_flp_id_4);
				ELSE
					SET logNote=CONCAT(' i_flp_id_3: ', i_flp_id_3, ', i_tr_id: ', i_tr_id);
					-- call ams_wad.log_sp(spname, stTime, logNote);
					call ams_ac.sp_al_flight_plan_payload_add_ppd_triple(i_flp_id_1, i_ppd_id, i_flp_id_2, i_flp_id_3);
				END IF;
				
			ELSEIF i_tr_count = 2 THEN
				SET logNote=CONCAT(' i_flp_id_2: ', i_flp_id_2, ', i_tr_id: ', i_tr_id);
				-- call ams_wad.log_sp(spname, stTime, logNote);
				call ams_ac.sp_al_flight_plan_payload_add_ppd_double(i_flp_id_1, i_ppd_id, i_flp_id_2);    
			END IF;
			commit;
				
			delete from ams_ac.ams_airport_ppd where ppd_id = i_ppd_id;
			commit;
			delete from ams_wad.ams_airport_ppd where ppd_id = i_ppd_id;
			commit;
		else
			SET logNote=CONCAT(i_pax_rows_cicles, ' - LEAVE no i_ppd_id.');
			call ams_wad.log_sp(spname, stTime, logNote);
			LEAVE insert_payload;
		END IF;
        
    END IF; -- IF i_pax_rows > 0 THEN
    IF i_pax_rows_cicles > 10 then
		SET logNote=CONCAT(i_pax_rows_cicles, ' - LEAVE too many cicles.');
		call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE insert_payload;
    end if;
END LOOP;
	drop TEMPORARY table IF EXISTS tmp_flight_plan_transfer_flp_ppd;
    
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_d_ppd_by_transfer_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_d_ppd_by_transfer_id`(IN in_transfer_id INT, IN in_ppd_id INT)
BEGIN

DECLARE i_ppd_id, i_tr_id, i_tr_count INT;
DECLARE i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4 INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_d_ppd_by_transfer_id', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_d_ppd_by_transfer_id';
set countRows = 0;

SET logNote=CONCAT('Start (',in_transfer_id,') ->' );
call ams_wad.log_sp(spname, stTime, logNote);
-- SET SQL_SAFE_UPDATES = 0;
set i_tr_count = 0;
-- read v_ams_al_flight_plan_payload_add_n_transfers_ppd_add
set i_ppd_id = 0;set i_flp_id_1 = 0;set i_flp_id_2 = 0;set i_flp_id_3 = 0;set i_flp_id_4 = 0;
SELECT tr.transfer_id, p.ppd_id, tr.flp_id_1, tr.flp_id_2, tr.flp_id_3, tr.flp_id_4,
	(if(tr.flp_id_1 is null,0,1)+if(tr.flp_id_2 is null,0,1)+if(tr.flp_id_3 is null,0,1)+if(tr.flp_id_4 is null,0,1)) tr_count
INTO i_tr_id, i_ppd_id, i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4, i_tr_count
FROM ams_ac.ams_al_flight_plan_transfer tr
join ams_ac.ams_airport_ppd p on p.dest_id = tr.dest_id
left join ams_ac.v_ams_flight_plan_payload_sum flp1 on flp1.flp_id = tr.flp_id_1 and flp1.pax_class_id = p.pax_class_id
left join ams_ac.v_ams_flight_plan_payload_sum flp2 on flp2.flp_id = tr.flp_id_2 and flp2.pax_class_id = p.pax_class_id
left join ams_ac.v_ams_flight_plan_payload_sum flp3 on flp3.flp_id = tr.flp_id_3 and flp3.pax_class_id = p.pax_class_id
left join ams_ac.v_ams_flight_plan_payload_sum flp4 on flp4.flp_id = tr.flp_id_4 and flp3.pax_class_id = p.pax_class_id
where tr.transfer_id=in_transfer_id and p.ppd_id=in_ppd_id
limit 1;
SET logNote=CONCAT('i_ppd_id: ', i_ppd_id, ' .' );
call ams_wad.log_sp(spname, stTime, logNote);
    
IF i_ppd_id  > 0 THEN
    
	IF i_tr_count = 4 THEN
		SET logNote=CONCAT(' i_flp_id_4: ', i_flp_id_4, ', i_tr_id: ', i_tr_id);
		call ams_wad.log_sp(spname, stTime, logNote);
		call ams_ac.sp_al_flight_plan_payload_add_ppd_quadruple(i_flp_id_1, i_ppd_id, i_flp_id_2, i_flp_id_3, i_flp_id_4);
		
	ELSEIF i_tr_count = 3 THEN
		if i_flp_id_1 is null THEN
			SET logNote=CONCAT(' i_flp_id_3: ', i_flp_id_4, ', i_tr_id: ', i_tr_id);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_ppd_triple(i_flp_id_2, i_ppd_id, i_flp_id_3, i_flp_id_4);
        ELSE
			SET logNote=CONCAT(' i_flp_id_3: ', i_flp_id_3, ', i_tr_id: ', i_tr_id);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_ppd_triple(i_flp_id_1, i_ppd_id, i_flp_id_2, i_flp_id_3);
        END IF;
		
	ELSEIF i_tr_count = 2 THEN
		SET logNote=CONCAT(' i_flp_id_2: ', i_flp_id_2, ', i_tr_id: ', i_tr_id);
		call ams_wad.log_sp(spname, stTime, logNote);
		call ams_ac.sp_al_flight_plan_payload_add_ppd_double(i_flp_id_1, i_ppd_id, i_flp_id_2);    
	END IF;
	commit;
		
	delete from ams_ac.ams_airport_ppd where ppd_id = i_ppd_id;
    commit;
	delete from ams_wad.ams_airport_ppd where ppd_id = i_ppd_id;
	commit;
	
END IF;
    
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_n` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_n`()
BEGIN

DECLARE i_ppd_id, i_cpd_id, i_tr_id INT;
DECLARE i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4, i_flp_status_id INT;
DECLARE countRows, i_transfers_id, ppdRows, cpdRows INT;
DECLARE i_cicles, i_atime INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_n', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_n';
set i_flp_status_id = 2; -- Flight is sold out
set countRows = 0;
set ppdRows = 0;
set cpdRows = 0;
set i_ppd_id = 0;
set i_cpd_id = 0;
set i_cicles = 50;
set i_atime = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET SQL_SAFE_UPDATES = 0;
-- Update flp remains counters
call ams_ac.sp_ams_al_flight_plan_update();

-- Get current i_transfers_id
SELECT param_value INTO i_transfers_id 
FROM ams_al.cfg_callc_param where param_id=43;

-- Get time sinse last ppd transfer table population
select (UNIX_TIMESTAMP(current_timestamp()) - 
UNIX_TIMESTAMP(ifnull(max(tr.adate), DATE_ADD(now(),INTERVAL -1 DAY))) ) as atime
INTO i_atime 
FROM ams_ac.v_ams_al_flight_plan_payload_add_n_transfers_ppd tr;

call ams_ac.sp_al_flight_plan_payload_add_n_transfers_ppd_add();

select count(*) INTO countRows 
FROM ams_ac.v_ams_al_flight_plan_payload_add_n_transfers_ppd_add tr
where tr.price_diff > 0;
-- Wait for an hour before inserting bew records becouse of ccp table
IF countRows = 0 AND i_atime > 3600 THEN
	-- Clear previouse records
    -- Populate available transfer flights
	call ams_ac.sp_al_flight_plan_payload_add_n_transfers_ppd();
    call ams_ac.sp_al_flight_plan_payload_add_n_transfers_ppd_add();
    
    select count(*) INTO countRows 
	FROM ams_ac.v_ams_al_flight_plan_payload_add_n_transfers_ppd_add tr
	where tr.price_diff > 0;
END IF;

SET logNote=CONCAT('Found ',countRows, ' ppd transfer flights for transfers_id:', i_transfers_id,  ', i_atime:', i_atime);
call ams_wad.log_sp(spname, stTime, logNote);
WHILE countRows > 0  DO

	set i_ppd_id = 0;set i_flp_id_1 = 0;set i_flp_id_2 = 0;set i_flp_id_3 = 0;set i_flp_id_4 = 0;
	select tr.id, tr.ppdId as ppd_id, tr.flpId as flp_id_1, tr.flpId2 as flp_id_2, tr.flpId3 as flp_id_3, tr.flpId4 as flp_id_4 
    INTO i_tr_id, i_ppd_id, i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4
	FROM ams_ac.v_ams_al_flight_plan_payload_add_n_transfers_ppd_add tr
	where tr.price_diff > 0 
    ORDER BY (ifnull(remain_pax, 0) + ifnull(remain_pax2, 0) + ifnull(remain_pax3, 0)+ ifnull(remain_pax4, 0)) 
    limit 1;
    
    IF i_ppd_id  > 0 THEN
    
		IF i_flp_id_4 > 0 THEN
			SET logNote=CONCAT(' i_ppd_id: ',i_ppd_id, ', i_flp_id_4: ', i_flp_id_4, ', i_tr_id: ', i_tr_id);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_ppd_quadruple(i_flp_id_1, i_ppd_id, i_flp_id_2, i_flp_id_3, i_flp_id_4);
            
		ELSEIF i_flp_id_3 > 0 THEN
			SET logNote=CONCAT(' i_ppd_id: ',i_ppd_id, ', i_flp_id_3: ', i_flp_id_3, ', i_tr_id: ', i_tr_id);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_ppd_triple(i_flp_id_1, i_ppd_id, i_flp_id_2, i_flp_id_3);
		ELSEIF i_flp_id_2 > 0 THEN
			SET logNote=CONCAT(' i_ppd_id: ',i_ppd_id, ', i_flp_id_2: ', i_flp_id_2, ', i_tr_id: ', i_tr_id);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_ppd_double(i_flp_id_1, i_ppd_id, i_flp_id_2);    
		END IF;
        commit;
		
        delete from ams_ac.ams_al_flight_plan_transfers_ppd where ppd_id = i_ppd_id;
        delete from ams_ac.ams_al_flight_plan_transfers_ppd_add where ppd_id = i_ppd_id;
        commit;
        SET logNote=CONCAT('Deleted ppd_id: ', i_ppd_id);
		-- call ams_wad.log_sp(spname, stTime, logNote);
        -- Update flp remains counters
		call ams_ac.sp_ams_al_flight_plan_update();
        
        CREATE TEMPORARY TABLE plan_transfers_ppd_add select tr.id
		from ams_ac.ams_al_flight_plan_transfers_ppd_add tr
		JOIN ams_ac.ams_al_flight_plan flp ON flp.flp_id = tr.flp_id
		JOIN ams_al.ams_al_flp_number_schedule flpns ON flpns.flpns_id = flp.flpns_id
		LEFT JOIN ams_al_flight_plan flp2 ON flp2.flp_id = tr.flp_id2
		LEFT JOIN ams_al.ams_al_flp_number_schedule flpns2 ON flpns2.flpns_id = flp2.flpns_id
		LEFT JOIN ams_al_flight_plan flp3 ON flp3.flp_id = tr.flp_id3
		LEFT JOIN ams_al.ams_al_flp_number_schedule flpns3 ON flpns3.flpns_id = flp3.flpns_id
		LEFT JOIN ams_al_flight_plan flp4 ON flp4.flp_id = tr.flp_id4
		LEFT JOIN ams_al.ams_al_flp_number_schedule flpns4 ON flpns4.flpns_id = flp4.flpns_id
		WHERE (IFNULL(flp.flp_status_id, 1) = 2
			or IFNULL(flp2.flp_status_id, 1) = 2
			or IFNULL(flp3.flp_status_id, 1) = 2
			OR IFNULL(flp4.flp_status_id, 1) = 2)
			OR (CASE
            WHEN
                (tr.ppd_pax_class_id = 1)
            THEN
                ROUND((tr.ppd_price - ((((tr.ppd_pax * IFNULL(flpns.price_e, 0)) + (tr.ppd_pax * IFNULL(flpns2.price_e, 0))) + (tr.ppd_pax * IFNULL(flpns3.price_e, 0))) + (tr.ppd_pax * IFNULL(flpns4.price_e, 0)))),
                        2)
            WHEN
                (tr.ppd_pax_class_id = 2)
            THEN
                ROUND((tr.ppd_price - ((((tr.ppd_pax * IFNULL(flpns.price_b, 0)) + (tr.ppd_pax * IFNULL(flpns2.price_b, 0))) + (tr.ppd_pax * IFNULL(flpns3.price_b, 0))) + (tr.ppd_pax * IFNULL(flpns4.price_b, 0)))),
                        2)
            WHEN
                (tr.ppd_pax_class_id = 3)
            THEN
                ROUND((tr.ppd_price - ((((tr.ppd_pax * IFNULL(flpns.price_f, 0)) + (tr.ppd_pax * IFNULL(flpns2.price_f, 0))) + (tr.ppd_pax * IFNULL(flpns3.price_f, 0))) + (tr.ppd_pax * IFNULL(flpns4.price_f, 0)))),
                        2)
            ELSE NULL
        END) < 0
        
        OR ((IFNULL(CASE
            WHEN (tr.ppd_pax_class_id = 1) THEN flp.remain_pax_e
            WHEN (tr.ppd_pax_class_id = 2) THEN flp.remain_pax_b
            WHEN (tr.ppd_pax_class_id = 3) THEN flp.remain_pax_f
            ELSE NULL
        END,
            tr.ppd_pax) < tr.ppd_pax)
        OR (IFNULL(CASE
            WHEN (tr.ppd_pax_class_id = 1) THEN flp2.remain_pax_e
            WHEN (tr.ppd_pax_class_id = 2) THEN flp2.remain_pax_b
            WHEN (tr.ppd_pax_class_id = 3) THEN flp2.remain_pax_f
            ELSE NULL
        END,
            tr.ppd_pax) < tr.ppd_pax)
        OR (IFNULL(CASE
            WHEN (tr.ppd_pax_class_id = 1) THEN flp3.remain_pax_e
            WHEN (tr.ppd_pax_class_id = 2) THEN flp3.remain_pax_b
            WHEN (tr.ppd_pax_class_id = 3) THEN flp3.remain_pax_f
            ELSE NULL
        END,
            tr.ppd_pax) < tr.ppd_pax)
        OR (IFNULL(CASE
            WHEN (tr.ppd_pax_class_id = 1) THEN flp4.remain_pax_e
            WHEN (tr.ppd_pax_class_id = 2) THEN flp4.remain_pax_b
            WHEN (tr.ppd_pax_class_id = 3) THEN flp4.remain_pax_f
            ELSE NULL
        END,
            tr.ppd_pax) < tr.ppd_pax));
            
		commit;
        
        select count(*) INTO countRows from plan_transfers_ppd_add;
        IF countRows > 0 THEN
			delete from ams_ac.ams_al_flight_plan_transfers_ppd_add tr
			where tr.id in (select id from plan_transfers_ppd_add);
			SET countRows =  ROW_COUNT();
			commit;
			
			SET logNote=CONCAT('Deleted ', countRows, ' rows from ams_ac.ams_al_flight_plan_transfers_ppd_add ');
			call ams_wad.log_sp(spname, stTime, logNote);
        END IF;
        drop TEMPORARY table IF EXISTS plan_transfers_ppd_add;
		commit;
	END IF;
    set ppdRows = ppdRows+1;
    
	select count(*) INTO countRows 
	FROM ams_ac.v_ams_al_flight_plan_payload_add_n_transfers_ppd_add tr
	where tr.price_diff > 0;
    -- Exit cicle
    IF ppdRows > i_cicles THEN
		set countRows = 0;
        SET logNote=CONCAT('Processed ppd rows: ', cpdRows);
		-- call ams_wad.log_sp(spname, stTime, logNote);
    END IF;
   
END WHILE;
SET logNote=CONCAT('Processed ppdRows: ', ppdRows);
call ams_wad.log_sp(spname, stTime, logNote);

SELECT param_value INTO i_transfers_id 
FROM ams_al.cfg_callc_param where param_id=44;

call ams_ac.sp_ams_al_flight_plan_update();

set countRows = 0;
select count(*) INTO countRows 
FROM ams_ac.v_ams_al_flight_plan_payload_add_n_transfers_cpd_add tr
where tr.price_diff > 0;
IF countRows = 0 THEN
	SET logNote=CONCAT(countRows, ' records found in v_ams_al_flight_plan_payload_add_n_transfers_cpd_add');
	call ams_wad.log_sp(spname, stTime, logNote);
	-- Clear previouse records
    -- Populate available transfer flights
	call ams_ac.sp_al_flight_plan_payload_add_n_transfers_cpd();
    call ams_ac.sp_al_flight_plan_payload_add_n_transfers_cpd_add();
    select count(*) INTO countRows 
	FROM ams_ac.v_ams_al_flight_plan_payload_add_n_transfers_cpd_add tr
	where tr.price_diff > 0;
END IF;
SET logNote=CONCAT('Found ',countRows, ' cpd transfer flights for transfers_id:', i_transfers_id,  '.');
call ams_wad.log_sp(spname, stTime, logNote);
WHILE countRows > 0  DO

	set i_cpd_id = 0;set i_flp_id_1 = 0;set i_flp_id_2 = 0;set i_flp_id_3 = 0;set i_flp_id_4 = 0;
    select tr.id, tr.cpdId as ppd_id, tr.flpId as flp_id_1, tr.flpId2 as flp_id_2, tr.flpId3 as flp_id_3, tr.flpId4 as flp_id_4 
	INTO i_tr_id, i_cpd_id, i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4
	FROM ams_ac.v_ams_al_flight_plan_payload_add_n_transfers_cpd_add tr
	where tr.price_diff > 0 
    ORDER BY dtime 
    limit 1;
    
    IF i_cpd_id  > 0 THEN
    
		IF i_flp_id_4 > 0 THEN
			SET logNote=CONCAT(cpdRows, ' i_cpd_id: ',i_cpd_id, ', i_flp_id_4: ', i_flp_id_4, ', i_tr_id: ', i_tr_id);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_cpd_quadruple(i_flp_id_1, i_cpd_id, i_flp_id_2, i_flp_id_3, i_flp_id_4);
		ELSEIF i_flp_id_3 > 0 THEN
			SET logNote=CONCAT(cpdRows, ' i_cpd_id: ',i_cpd_id, ', i_flp_id_3: ', i_flp_id_3, ', i_tr_id: ', i_tr_id);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_cpd_triple(i_flp_id_1, i_cpd_id, i_flp_id_2, i_flp_id_3);
		ELSEIF i_flp_id_2 > 0 THEN
			SET logNote=CONCAT(cpdRows, ' i_cpd_id: ',i_cpd_id, ', i_flp_id_2: ', i_flp_id_2, ', i_tr_id: ', i_tr_id);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_cpd_double(i_flp_id_1, i_cpd_id, i_flp_id_2);
		END IF;
		commit;
		
        delete from ams_ac.ams_al_flight_plan_transfers_cpd where cpd_id = i_cpd_id;
        commit;
        delete from ams_ac.ams_al_flight_plan_transfers_cpd_add where cpd_id = i_cpd_id;
        SET countRows =  ROW_COUNT();
        commit;
        
        SET logNote=CONCAT('Deleted ', countRows,' row for cpd_id: ', i_cpd_id);
		call ams_wad.log_sp(spname, stTime, logNote);
        
        call ams_ac.sp_ams_al_flight_plan_update();
        
        CREATE TEMPORARY TABLE plan_transfers_cpd_add select tr.id
		from ams_ac.ams_al_flight_plan_transfers_cpd_add tr
		JOIN ams_ac.ams_al_flight_plan flp ON flp.flp_id = tr.flp_id
		JOIN ams_al.ams_al_flp_number_schedule flpns ON flpns.flpns_id = flp.flpns_id
		LEFT JOIN ams_ac.ams_al_flight_plan flp2 ON flp2.flp_id = tr.flp_id2
		LEFT JOIN ams_al.ams_al_flp_number_schedule flpns2 ON flpns2.flpns_id = flp2.flpns_id
		LEFT JOIN ams_ac.ams_al_flight_plan flp3 ON flp3.flp_id = tr.flp_id3
		LEFT JOIN ams_al.ams_al_flp_number_schedule flpns3 ON flpns3.flpns_id = flp3.flpns_id
		LEFT JOIN ams_ac.ams_al_flight_plan flp4 ON flp4.flp_id = tr.flp_id4
		LEFT JOIN ams_al.ams_al_flp_number_schedule flpns4 ON flpns4.flpns_id = flp4.flpns_id
		WHERE (IFNULL(flp.flp_status_id, 1) = 2
			or IFNULL(flp2.flp_status_id, 1) = 2
			or IFNULL(flp3.flp_status_id, 1) = 2
			OR IFNULL(flp4.flp_status_id, 1) = 2)
			 OR ROUND((tr.cpd_price - (tr.cpd_payload_kg * (((IFNULL(flpns.price_c_p_kg, 0) + IFNULL(flpns2.price_c_p_kg, 0)) + IFNULL(flpns3.price_c_p_kg, 0)) + IFNULL(flpns4.price_c_p_kg, 0)))),2)<0
			OR ((IFNULL(flp.remain_payload_kg,
				tr.cpd_payload_kg) < tr.cpd_payload_kg)
			OR (IFNULL(flp2.remain_payload_kg,
				tr.cpd_payload_kg) < tr.cpd_payload_kg)
			OR (IFNULL(flp3.remain_payload_kg,
				tr.cpd_payload_kg) < tr.cpd_payload_kg)
			OR (IFNULL(flp4.remain_payload_kg,
				tr.cpd_payload_kg) < tr.cpd_payload_kg));
		commit;
        
        select count(*) INTO countRows from plan_transfers_cpd_add;
        IF countRows > 0 THEN
			delete from ams_ac.ams_al_flight_plan_transfers_cpd_add tr
			where tr.id in (select id from plan_transfers_cpd_add);
			SET countRows =  ROW_COUNT();
			commit;
			
			SET logNote=CONCAT('Deleted ', countRows, ' rows from ams_ac.ams_al_flight_plan_transfers_cpd_add ');
			call ams_wad.log_sp(spname, stTime, logNote);
        END IF;
        drop TEMPORARY table IF EXISTS plan_transfers_cpd_add;
		commit;
		
	END IF;
    
    set cpdRows = cpdRows+1;
    
	select count(*) INTO countRows 
	FROM ams_ac.v_ams_al_flight_plan_payload_add_n_transfers_cpd_add tr
	where tr.price_diff > 0;
    -- Exit cicle
    IF cpdRows > (2*i_cicles) THEN
		set countRows = 0;
        SET logNote=CONCAT('Processed cpd rows: ', cpdRows);
		-- call ams_wad.log_sp(spname, stTime, logNote);
    END IF;
    
END WHILE;

SET logNote=CONCAT('Processed cpdRows: ', cpdRows, ', ppdRows: ', ppdRows);
call ams_wad.log_sp(spname, stTime, logNote);
call ams_ac.sp_ams_al_flight_plan_update();

-- SET SQL_SAFE_UPDATES = 1;
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_n_flp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_n_flp`(IN in_flp_id INT)
BEGIN

DECLARE i_ppd_id, i_cpd_id, i_flp_status_id INT;
DECLARE i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4 INT;
DECLARE countRows, i_transfers_id INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_n_flp', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_n_flp';
set i_flp_status_id = 2; -- Flight is sold out
set countRows = 0;
set i_ppd_id = 0;
set i_cpd_id = 0;

SET logNote=CONCAT('Start (in_flp_id:', in_flp_id, ') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

-- Get current i_transfers_id
SELECT param_value INTO i_transfers_id 
FROM ams_al.cfg_callc_param where param_id=43;
    
IF in_flp_id > 0 THEN
	
select count(*)  INTO countRows
from ams_ac.ams_al_flight_plan_transfers tr 
join ams_al_flight_plan flp on flp.flp_id = tr.flpId
JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
left join ams_al_flight_plan flp2 on flp2.flp_id = tr.flpId2
left JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
left join ams_al_flight_plan flp3 on flp3.flp_id = tr.flpId3
left JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
left join ams_al_flight_plan flp4 on flp4.flp_id = tr.flpId3
left JOIN ams_al.ams_al_flp_number_schedule flpns4 ON ((flpns4.flpns_id = flp4.flpns_id))
where transfers_id = i_transfers_id and tr.ppdId is not null
and (flpId = in_flp_id OR flpId2 = in_flp_id OR flpId3 = in_flp_id OR flpId4 = in_flp_id)
and tr.diffPrice > 0
and remainPax >= tr.pax and ifnull(remainPax2, tr.pax) >= tr.pax and ifnull(remainPax3, tr.pax) >= tr.pax and ifnull(remainPax4, tr.pax) >= tr.pax 
and (
	(tr.paxClassId = 1 
		and tr.pax <= flp.remain_pax_e and tr.pax <= ifnull(flp2.remain_pax_e, tr.pax) and tr.pax <= ifnull(flp3.remain_pax_e, tr.pax) and tr.pax <= ifnull(flp4.remain_pax_e, tr.pax) 
        and tr.pPrice >= ((tr.pax*flpns.price_e)+(tr.pax*ifnull(flpns2.price_e,0)) + (tr.pax*ifnull(flpns3.price_e, 0))+ (tr.pax*ifnull(flpns4.price_e,0))))
	or (tr.paxClassId = 2 
		and tr.pax <= flp.remain_pax_b and tr.pax <= ifnull(flp2.remain_pax_b, tr.pax) and tr.pax <= ifnull(flp3.remain_pax_b, tr.pax) and tr.pax <= ifnull(flp4.remain_pax_b, tr.pax) 
        and tr.pPrice >= ((tr.pax*flpns.price_b)+(tr.pax*ifnull(flpns2.price_b,0)) + (tr.pax*ifnull(flpns3.price_b, 0))+ (tr.pax*ifnull(flpns4.price_b,0))))
	or (tr.paxClassId = 3 
		and tr.pax <= flp.remain_pax_f and tr.pax <= ifnull(flp2.remain_pax_f, tr.pax) and tr.pax <= ifnull(flp3.remain_pax_f, tr.pax) and tr.pax <= ifnull(flp4.remain_pax_f, tr.pax) 
        and tr.pPrice >= ((tr.pax*flpns.price_f)+(tr.pax*ifnull(flpns2.price_f,0)) + (tr.pax*ifnull(flpns3.price_f, 0))+ (tr.pax*ifnull(flpns4.price_f,0))))
);
SET logNote=CONCAT('PPD Transfer flights count: ',countRows);
call ams_wad.log_sp(spname, stTime, logNote);
	
if countRows > 0 THEN
		
	select tr.ppdId as ppd_id, flp.flp_id as flp_id_1, flp2.flp_id as flp_id_2, flp3.flp_id as flp_id_3, flp4.flp_id as flp_id_4
	INTO i_ppd_id, i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4
	from ams_ac.ams_al_flight_plan_transfers tr 
	join ams_al_flight_plan flp on flp.flp_id = tr.flpId
	JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
	left join ams_al_flight_plan flp2 on flp2.flp_id = tr.flpId2
	left JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
	left join ams_al_flight_plan flp3 on flp3.flp_id = tr.flpId3
	left JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
	left join ams_al_flight_plan flp4 on flp4.flp_id = tr.flpId3
	left JOIN ams_al.ams_al_flp_number_schedule flpns4 ON ((flpns4.flpns_id = flp4.flpns_id))
	where transfers_id = i_transfers_id and tr.ppdId is not null
	and (flpId = in_flp_id OR flpId2 = in_flp_id OR flpId3 = in_flp_id OR flpId4 = in_flp_id)
	and tr.diffPrice > 0
	and remainPax >= tr.pax and ifnull(remainPax2, tr.pax) >= tr.pax and ifnull(remainPax3, tr.pax) >= tr.pax and ifnull(remainPax4, tr.pax) >= tr.pax 
	and (
		(tr.paxClassId = 1 
			and tr.pax <= flp.remain_pax_e and tr.pax <= ifnull(flp2.remain_pax_e, tr.pax) and tr.pax <= ifnull(flp3.remain_pax_e, tr.pax) and tr.pax <= ifnull(flp4.remain_pax_e, tr.pax) 
			and tr.pPrice >= ((tr.pax*flpns.price_e)+(tr.pax*ifnull(flpns2.price_e,0)) + (tr.pax*ifnull(flpns3.price_e, 0))+ (tr.pax*ifnull(flpns4.price_e,0))))
		or (tr.paxClassId = 2 
			and tr.pax <= flp.remain_pax_b and tr.pax <= ifnull(flp2.remain_pax_b, tr.pax) and tr.pax <= ifnull(flp3.remain_pax_b, tr.pax) and tr.pax <= ifnull(flp4.remain_pax_b, tr.pax) 
			and tr.pPrice >= ((tr.pax*flpns.price_b)+(tr.pax*ifnull(flpns2.price_b,0)) + (tr.pax*ifnull(flpns3.price_b, 0))+ (tr.pax*ifnull(flpns4.price_b,0))))
		or (tr.paxClassId = 3 
			and tr.pax <= flp.remain_pax_f and tr.pax <= ifnull(flp2.remain_pax_f, tr.pax) and tr.pax <= ifnull(flp3.remain_pax_f, tr.pax) and tr.pax <= ifnull(flp4.remain_pax_f, tr.pax) 
			and tr.pPrice >= ((tr.pax*flpns.price_f)+(tr.pax*ifnull(flpns2.price_f,0)) + (tr.pax*ifnull(flpns3.price_f, 0))+ (tr.pax*ifnull(flpns4.price_f,0))))
	)
    ORDER BY tr.sumPrice limit 1;
		
		-- Add double Pax record to payload
		IF i_ppd_id > 0 THEN
			SET logNote=CONCAT(' i_ppd_id: ',i_ppd_id, ', i_flp_id_2: ', i_flp_id_2, ', i_flp_id_4: ', i_flp_id_4);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_ppd_quadruple(in_flp_id, i_ppd_id, i_flp_id_2, i_flp_id_3, i_flp_id_4);
		else
            SET logNote=CONCAT(' No ppd record found for in_flp_id: ',in_flp_id);
			call ams_wad.log_sp(spname, stTime, logNote);
		END IF;
	ELSE
        call ams_ac.sp_al_flight_plan_payload_ppd_downgrade_quadruple(in_flp_id);
	END IF;
	
    call ams_ac.sp_ams_al_flp_transfer_cpd_quadruple(in_flp_id);
	set countRows = 0;
	select count(p.cpd_id) INTO countRows
	from ams_ac.ams_al_flp_transfer_cpd pcpd
	join ams_wad.ams_airport_cpd p on p.cpd_id = pcpd.cpdId 
	join ams_al_flight_plan_payload_pct_full flp on flp.flp_id = pcpd.flpId
	JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
	join ams_al_flight_plan_payload_pct_full flp2 on flp2.flp_id = pcpd.flpId2
	JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
	join ams_al_flight_plan_payload_pct_full flp3 on flp3.flp_id = pcpd.flpId3
	JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
	join ams_al_flight_plan_payload_pct_full flp4 on flp4.flp_id = pcpd.flpId3
	JOIN ams_al.ams_al_flp_number_schedule flpns4 ON ((flpns4.flpns_id = flp4.flpns_id))
    where (
		p.pax_class_id = 4 and 
        p.payload_kg <= flp.remain_payload_kg  and 
        p.payload_kg <= flp2.remain_payload_kg and 
        p.payload_kg <= flp3.remain_payload_kg and 
        p.payload_kg <= flp4.remain_payload_kg and 
        p.max_ticket_price >= ((p.payload_kg*flpns.price_c_p_kg)+(p.payload_kg*flpns2.price_c_p_kg)+(p.payload_kg*flpns3.price_c_p_kg)+(p.payload_kg*flpns4.price_c_p_kg))
	);
    
	SET logNote=CONCAT('CPD Transfer flights count: ',countRows);
	call ams_wad.log_sp(spname, stTime, logNote);
	IF countRows >0 THEN
		
		select p.cpd_id, flp2.flp_id as flp_id_2, flp3.flp_id as flp_id_3, flp4.flp_id as flp_id_4 
        INTO i_cpd_id, i_flp_id_2, i_flp_id_3, i_flp_id_4
		from ams_ac.ams_al_flp_transfer_cpd pcpd
		join ams_wad.ams_airport_cpd p on p.cpd_id = pcpd.cpdId 
		join ams_al_flight_plan_payload_pct_full flp on flp.flp_id = pcpd.flpId
		JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
		join ams_al_flight_plan_payload_pct_full flp2 on flp2.flp_id = pcpd.flpId2
		JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
		join ams_al_flight_plan_payload_pct_full flp3 on flp3.flp_id = pcpd.flpId3
		JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
		join ams_al_flight_plan_payload_pct_full flp4 on flp4.flp_id = pcpd.flpId3
		JOIN ams_al.ams_al_flp_number_schedule flpns4 ON ((flpns4.flpns_id = flp4.flpns_id))
		where (
			p.pax_class_id = 4 and 
			p.payload_kg <= flp.remain_payload_kg  and 
			p.payload_kg <= flp2.remain_payload_kg and 
			p.payload_kg <= flp3.remain_payload_kg and 
			p.payload_kg <= flp4.remain_payload_kg and 
			p.max_ticket_price >= ((p.payload_kg*flpns.price_c_p_kg)+(p.payload_kg*flpns2.price_c_p_kg)+(p.payload_kg*flpns3.price_c_p_kg)+(p.payload_kg*flpns4.price_c_p_kg))
		)
		order by p.adate limit 1;

		
		-- Add double Cargo record to payload
		IF i_cpd_id > 0 THEN
			SET logNote=CONCAT(' i_cpd_id: ',i_cpd_id, ', i_flp_id_2: ', i_flp_id_2, ', i_flp_id_4: ', i_flp_id_4);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_cpd_quadruple(in_flp_id, i_cpd_id, i_flp_id_2, i_flp_id_3, i_flp_id_4);
		else
            SET logNote=CONCAT(' No cpd record found for in_flp_id: ',in_flp_id);
			call ams_wad.log_sp(spname, stTime, logNote);
		END IF;
	END IF;
	
	SET logNote=CONCAT(' Finish in_flp_id: ',in_flp_id, '.');
	call ams_wad.log_sp(spname, stTime, logNote);
	COMMIT;
END IF;
    
  

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_n_ppd` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_n_ppd`(In in_tr_id INT)
BEGIN

DECLARE i_ppd_id, i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4 INT;
DECLARE i_flp_status_id INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_n_ppd', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_n_ppd';
set i_flp_status_id = 2; -- Flight is sold out
set countRows = 0;

SET logNote=CONCAT('Start ->' );
call ams_wad.log_sp(spname, stTime, logNote);
-- SET SQL_SAFE_UPDATES = 0;

select count(*) INTO countRows 
FROM ams_ac.v_ams_al_flight_plan_payload_add_n_transfers_ppd_add tr
where tr.id = in_tr_id and tr.price_diff > 0;

SET logNote=CONCAT('Found ',countRows, ' ppd transfer flights for tr.id:', in_tr_id,  '.');
call ams_wad.log_sp(spname, stTime, logNote);
IF countRows > 0  THEN

	set i_ppd_id = 0;set i_flp_id_1 = 0;set i_flp_id_2 = 0;set i_flp_id_3 = 0;set i_flp_id_4 = 0;
	select tr.ppdId as ppd_id, tr.flpId as flp_id_1, tr.flpId2 as flp_id_2, tr.flpId3 as flp_id_3, tr.flpId4 as flp_id_4 
    INTO i_ppd_id, i_flp_id_1, i_flp_id_2, i_flp_id_3, i_flp_id_4
	FROM ams_ac.v_ams_al_flight_plan_payload_add_n_transfers_ppd_add tr
	where tr.id = in_tr_id;
    
    IF i_ppd_id  > 0 THEN
    
		IF i_flp_id_4 > 0 THEN
			SET logNote=CONCAT(' i_ppd_id: ',i_ppd_id, ', i_flp_id_4: ', i_flp_id_4, ', in_tr_id: ', in_tr_id);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_ppd_quadruple(i_flp_id_1, i_ppd_id, i_flp_id_2, i_flp_id_3, i_flp_id_4);
            
		ELSEIF i_flp_id_3 > 0 THEN
			SET logNote=CONCAT(' i_ppd_id: ',i_ppd_id, ', i_flp_id_3: ', i_flp_id_3, ', in_tr_id: ', in_tr_id);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_ppd_triple(i_flp_id_1, i_ppd_id, i_flp_id_2, i_flp_id_3);
		ELSEIF i_flp_id_2 > 0 THEN
			SET logNote=CONCAT(' i_ppd_id: ',i_ppd_id, ', i_flp_id_2: ', i_flp_id_2, ', in_tr_id: ', in_tr_id);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_ppd_double(i_flp_id_1, i_ppd_id, i_flp_id_2);    
		END IF;
        commit;
		
        delete from ams_ac.ams_al_flight_plan_transfers_ppd where ppd_id = i_ppd_id;
        delete from ams_ac.ams_al_flight_plan_transfers_ppd_add where ppd_id = i_ppd_id;
        SET logNote=CONCAT('Deleted ppd_id: ', i_ppd_id);
		call ams_wad.log_sp(spname, stTime, logNote);
        commit;
        
	END IF;
	call ams_ac.sp_ams_al_flight_plan_update();
END IF;    
-- SET SQL_SAFE_UPDATES = 1;
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_n_single` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_n_single`()
BEGIN

DECLARE i_flp_id, i_processed, i_flp_status_id INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_flp CURSOR FOR SELECT flp.flp_id
			from ams_ac.ams_al_flight_plan flp
			where flp.flp_status_id = 1 
			and (flp.pax_pct_full < 95 or flp.payload_kg_pct_full < 95)
			order by flp.payload_jobs, flp.dtime limit 40;
					
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_n_single', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_n_single';
set countRows = 0;
set i_flp_status_id = 2; -- Flight is sold out
set i_flp_id = 0;
set i_processed = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);

-- Update flp remains counters
-- call ams_ac.sp_ams_al_flight_plan_update();

OPEN cursor_flp;

read_loop: LOOP
	
    FETCH cursor_flp INTO i_flp_id;
	
	IF done THEN
		SET logNote=CONCAT('No i_flp_id in queue found ', 'done' );
		call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
    
		set i_processed = i_processed+1;
		SET logNote=CONCAT(i_processed, ' Flifgt i_flp_id: ',i_flp_id,  ' ...');
		call ams_wad.log_sp(spname, stTime, logNote);
		call ams_ac.sp_al_flight_plan_payload_add_pcpd_single(i_flp_id);
        update ams_ac.ams_al_flight_plan flp 
		set flp.payload_jobs = ifnull(flp.payload_jobs,0)+1
		where flp.flp_id = i_flp_id;
		commit;
END LOOP;
 
CLOSE cursor_flp;

SET logNote=CONCAT('Procesed: ', FORMAT(i_processed, 0) ,  ' flp records.');
call ams_wad.log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_n_transfers` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_n_transfers`()
BEGIN

DECLARE i_transfers_id, countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_n_transfers', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_n_transfers';
set countRows = 0;

SET logNote=CONCAT('Start ', ' ->' );
call ams_wad.log_sp(spname, stTime, logNote);

-- Increment i_transfers_id
UPDATE ams_al.cfg_callc_param set param_value=param_value+1 where param_id=43;
SELECT param_value INTO i_transfers_id FROM ams_al.cfg_callc_param where param_id=43;

SET logNote=CONCAT('i_transfers_id: ', i_transfers_id, '.' );
call ams_wad.log_sp(spname, stTime, logNote);

 delete FROM ams_ac.ams_al_flight_plan_transfers where transfers_id = i_transfers_id;
 commit;
 
	 insert into ams_ac.ams_al_flight_plan_transfers
		(transfers_id, ppdId, flpId, flpId2, 
		depApId, arrApId, distanceKm, flName, flName2, 
		remainPax, remainPax2,
		diffPrice, sumPrice, pPrice, paxClassId, pax, payloadKg, note) 
	SELECT 
		i_transfers_id as transfers_id,
        p.ppd_id AS ppdId,
        flp.flp_id AS flpId,
        flp2.flp_id AS flpId2,
        pd.dep_ap_id AS depApId,
        pd.arr_ap_id AS arrApId,
        pd.distance_km AS distanceKm,
        flp.fl_name AS flName,
        flp2.fl_name AS flName2,
        (((ifnull(flp.available_pax_e,0) + ifnull(flp.available_pax_b,0) + ifnull(flp.available_pax_f,0)) - ifnull(flp.pax,0)) ) AS remainPax,
        (((ifnull(flp2.available_pax_e,0) + ifnull(flp2.available_pax_b,0) + ifnull(flp2.available_pax_f,0)) - ifnull(flp2.pax,0)) ) AS remainPax2,
        ROUND((p.max_ticket_price - (
			(p.pax * flpns.price_e) 
            + (p.pax * flpns2.price_e) 
            )), 2) AS diffPrice,
        ROUND((
			(p.pax * flpns.price_e) 
            + (p.pax * flpns2.price_e) 
            ), 2) AS sumPrice,
        ROUND(p.max_ticket_price, 2) AS pPrice,
        p.pax_class_id AS paxClassId,
        p.pax AS pax,
        p.payload_kg AS payloadKg,
        p.note AS note
    FROM ams_ac.ams_al_flight_plan flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_aircraft_curr ac ON ac.ac_id = flp.ac_id
        JOIN ams_wad.ams_airport_ppd p ON p.dep_ap_id = flp.d_ap_id
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND (pd.arr_ap_id = flp2.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.op_time_min)
		having remainPax>0 and remainPax2>0;
	commit;
SET countRows =  ROW_COUNT();
 
SET logNote=CONCAT('Inserted  ',countRows,  ' rows double transfer ppd <-' );
call ams_wad.log_sp(spname, stTime, logNote);

	insert into ams_ac.ams_al_flight_plan_transfers
		(transfers_id, ppdId, flpId, flpId2, flpId3,
		depApId, arrApId, distanceKm, flName, flName2, flName3,
		remainPax, remainPax2,remainPax3, 
		diffPrice, sumPrice, pPrice, paxClassId, pax, payloadKg, note) 
	SELECT 
		i_transfers_id as transfers_id,
        p.ppd_id AS ppdId,
        flp.flp_id AS flpId,
        flp2.flp_id AS flpId2,
        flp3.flp_id AS flpId3,
        pd.dep_ap_id AS depApId,
        pd.arr_ap_id AS arrApId,
        pd.distance_km AS distanceKm,
        flp.fl_name AS flName,
        flp2.fl_name AS flName2,
        flp3.fl_name AS flName3,
        (((ifnull(flp.available_pax_e,0) + ifnull(flp.available_pax_b,0) + ifnull(flp.available_pax_f,0)) - ifnull(flp.pax,0)) ) AS remainPax,
        (((ifnull(flp2.available_pax_e,0) + ifnull(flp2.available_pax_b,0) + ifnull(flp2.available_pax_f,0)) - ifnull(flp2.pax,0)) ) AS remainPax2,
        (((ifnull(flp3.available_pax_e,0) + ifnull(flp3.available_pax_b,0) + ifnull(flp3.available_pax_f,0)) - ifnull(flp3.pax,0)) ) AS remainPax3,
        ROUND((p.max_ticket_price - (
			(p.pax * flpns.price_e) 
            + (p.pax * flpns2.price_e) 
            + (p.pax * flpns3.price_e) 
            )), 2) AS diffPrice,
        ROUND((
			(p.pax * flpns.price_e) 
            + (p.pax * flpns2.price_e) 
            + (p.pax * flpns3.price_e) 
            ), 2) AS sumPrice,
        ROUND(p.max_ticket_price, 2) AS pPrice,
        p.pax_class_id AS paxClassId,
        p.pax AS pax,
        p.payload_kg AS payloadKg,
        p.note AS note
    FROM ams_ac.ams_al_flight_plan flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp3 ON ((flp3.d_ap_id = flp2.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
        JOIN ams_aircraft_curr ac ON ac.ac_id = flp.ac_id
        JOIN ams_wad.ams_airport_ppd p ON p.dep_ap_id = flp.d_ap_id
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp3.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND flp3.a_ap_id <> flp2.d_ap_id
		AND flp3.a_ap_id <> flp.d_ap_id
		AND (pd.arr_ap_id = flp3.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.op_time_min)
		AND (TIMESTAMPDIFF(MINUTE, flp2.atime, flp3.dtime) > ac.op_time_min)
		having remainPax>0 and remainPax2>0 and remainPax3>0;
            
SET countRows =  ROW_COUNT();
commit; 
SET logNote=CONCAT('Inserted  ',countRows,  ' rows tripple transfer ppd <-' );
call ams_wad.log_sp(spname, stTime, logNote);

	insert into ams_ac.ams_al_flight_plan_transfers
		(transfers_id, ppdId, flpId, flpId2, flpId3, flpId4,
		depApId, arrApId, distanceKm, flName, flName2, flName3,flName4,
		remainPax, remainPax2,remainPax3, remainPax4, 
		diffPrice, sumPrice, pPrice, paxClassId, pax, payloadKg, note) 
	SELECT 
		i_transfers_id as transfers_id,
        p.ppd_id AS ppdId,
        flp.flp_id AS flpId,
        flp2.flp_id AS flpId2,
        flp3.flp_id AS flpId3,
        flp4.flp_id AS flpId4,
        pd.dep_ap_id AS depApId,
        pd.arr_ap_id AS arrApId,
        pd.distance_km AS distanceKm,
        flp.fl_name AS flName,
        flp2.fl_name AS flName2,
        flp3.fl_name AS flName3,
        flp4.fl_name AS flName4,
        (((ifnull(flp.available_pax_e,0) + ifnull(flp.available_pax_b,0) + ifnull(flp.available_pax_f,0)) - ifnull(flp.pax,0)) ) AS remainPax,
        (((ifnull(flp2.available_pax_e,0) + ifnull(flp2.available_pax_b,0) + ifnull(flp2.available_pax_f,0)) - ifnull(flp2.pax,0)) ) AS remainPax2,
        (((ifnull(flp3.available_pax_e,0) + ifnull(flp3.available_pax_b,0) + ifnull(flp3.available_pax_f,0)) - ifnull(flp3.pax,0)) ) AS remainPax3,
        (((ifnull(flp4.available_pax_e,0) + ifnull(flp4.available_pax_b,0) + ifnull(flp4.available_pax_f,0)) - ifnull(flp4.pax,0)) ) AS remainPax4,
        ROUND((p.max_ticket_price - (
			(p.pax * flpns.price_e) 
            + (p.pax * flpns2.price_e) 
            + (p.pax * flpns3.price_e) 
            + (p.pax * flpns4.price_e) 
            )), 2) AS diffPrice,
        ROUND((
			(p.pax * flpns.price_e) 
            + (p.pax * flpns2.price_e) 
            + (p.pax * flpns3.price_e) 
            + (p.pax * flpns4.price_e) 
            ), 2) AS sumPrice,
        ROUND(p.max_ticket_price, 2) AS pPrice,
        p.pax_class_id AS paxClassId,
        p.pax AS pax,
        p.payload_kg AS payloadKg,
        p.note AS note
    FROM ams_ac.ams_al_flight_plan flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp3 ON ((flp3.d_ap_id = flp2.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp4 ON ((flp4.d_ap_id = flp3.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns4 ON ((flpns4.flpns_id = flp4.flpns_id))
        JOIN ams_aircraft_curr ac ON ac.ac_id = flp.ac_id
        JOIN ams_wad.ams_airport_ppd p ON p.dep_ap_id = flp.d_ap_id
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp3.flp_status_id = 1
		AND flp4.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND flp3.a_ap_id <> flp2.d_ap_id
		AND flp3.a_ap_id <> flp.d_ap_id
		AND flp4.a_ap_id <> flp3.d_ap_id
		AND flp4.a_ap_id <> flp2.d_ap_id
		AND flp4.a_ap_id <> flp.d_ap_id
		AND (pd.arr_ap_id = flp4.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.op_time_min)
		AND (TIMESTAMPDIFF(MINUTE, flp2.atime, flp3.dtime) > ac.op_time_min)
		AND (TIMESTAMPDIFF(MINUTE, flp3.atime, flp4.dtime) > ac.op_time_min)
		having remainPax>0 and remainPax2>0 and remainPax3>0 and remainPax4>0;
        
SET countRows =  ROW_COUNT();
commit; 
SET logNote=CONCAT('Inserted  ',countRows,  ' rows tripple quadruple ppd <-' );
call ams_wad.log_sp(spname, stTime, logNote);

insert into ams_ac.ams_al_flight_plan_transfers
		(transfers_id, cpdId, flpId, flpId2, 
		depApId, arrApId, distanceKm, flName, flName2, 
		remainPayloadKg, remainPayloadKg2,
		diffPrice, sumPrice, pPrice, paxClassId, pax, payloadKg, note) 
SELECT 
		i_transfers_id as transfers_id,
        p.cpd_id AS cpdId,
        flp.flp_id AS flpId,
        flp2.flp_id AS flpId2,
        pd.dep_ap_id AS depApId,
        pd.arr_ap_id AS arrApId,
        pd.distance_km AS distanceKm,
        flp.fl_name AS flName,
        flp2.fl_name AS flName2,
        ((ifnull(flp.available_cargo_kg,0) - flp.payload_kg)) AS remainPayloadKg,
        ((ifnull(flp2.available_cargo_kg,0) - flp2.payload_kg)) AS remainPayloadKg2,
        ROUND((p.max_ticket_price - (
			(p.payload_kg * flpns.price_c_p_kg) 
            + (p.payload_kg * flpns2.price_c_p_kg) 
            )), 2) AS diffPrice,
        ROUND((
			(p.payload_kg * flpns.price_c_p_kg) 
            + (p.payload_kg * flpns2.price_c_p_kg) 
            ), 2) AS sumPrice,
        ROUND(p.max_ticket_price, 2) AS pPrice,
        p.pax_class_id AS paxClassId,
        0 AS pax,
        p.payload_kg AS payloadKg,
        p.note AS note
    FROM ams_ac.ams_al_flight_plan flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_aircraft_curr ac ON ac.ac_id = flp.ac_id
        JOIN ams_wad.ams_airport_cpd p ON p.dep_ap_id = flp.d_ap_id
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND (pd.arr_ap_id = flp2.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.op_time_min);
SET countRows =  ROW_COUNT();
commit; 
SET logNote=CONCAT('Inserted  ',countRows,  ' rows double cpd <-' );
call ams_wad.log_sp(spname, stTime, logNote);

	insert into ams_ac.ams_al_flight_plan_transfers
		(transfers_id, cpdId, flpId, flpId2, 
		depApId, arrApId, distanceKm, flName, flName2, 
		remainPayloadKg, remainPayloadKg2, remainPayloadKg3,
		diffPrice, sumPrice, pPrice, paxClassId, pax, payloadKg, note) 
	SELECT 
		i_transfers_id as transfers_id,
        p.cpd_id AS cpdId,
        flp.flp_id AS flpId,
        flp2.flp_id AS flpId2,
        pd.dep_ap_id AS depApId,
        pd.arr_ap_id AS arrApId,
        pd.distance_km AS distanceKm,
        flp.fl_name AS flName,
        flp2.fl_name AS flName2,
        ((ifnull(flp.available_cargo_kg,0) - flp.payload_kg)) AS remainPayloadKg,
        ((ifnull(flp2.available_cargo_kg,0) - flp2.payload_kg)) AS remainPayloadKg2,
        ((ifnull(flp3.available_cargo_kg,0) - flp3.payload_kg)) AS remainPayloadKg3,
        ROUND((p.max_ticket_price - (
			(p.payload_kg * flpns.price_c_p_kg) 
            + (p.payload_kg * flpns2.price_c_p_kg) 
            + (p.payload_kg * flpns3.price_c_p_kg) 
            )), 2) AS diffPrice,
        ROUND((
			(p.payload_kg * flpns.price_c_p_kg) 
            + (p.payload_kg * flpns2.price_c_p_kg) 
            + (p.payload_kg * flpns3.price_c_p_kg) 
            ), 2) AS sumPrice,
        ROUND(p.max_ticket_price, 2) AS pPrice,
        p.pax_class_id AS paxClassId,
        0 AS pax,
        p.payload_kg AS payloadKg,
        p.note AS note
    FROM ams_ac.ams_al_flight_plan flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp3 ON ((flp3.d_ap_id = flp2.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
        JOIN ams_aircraft_curr ac ON ac.ac_id = flp.ac_id
        JOIN ams_wad.ams_airport_cpd p ON p.dep_ap_id = flp.d_ap_id
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
        AND flp3.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
        AND flp3.a_ap_id <> flp2.d_ap_id
		AND (pd.arr_ap_id = flp3.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.op_time_min)
        AND (TIMESTAMPDIFF(MINUTE, flp2.atime, flp3.dtime) > ac.op_time_min);
SET countRows =  ROW_COUNT();
commit; 
SET logNote=CONCAT('Inserted  ',countRows,  ' rows tripple cpd <-' );
call ams_wad.log_sp(spname, stTime, logNote);

	insert into ams_ac.ams_al_flight_plan_transfers
		(transfers_id, cpdId, flpId, flpId2, 
		depApId, arrApId, distanceKm, flName, flName2, 
		remainPayloadKg, remainPayloadKg2, remainPayloadKg3, remainPayloadKg4,
		diffPrice, sumPrice, pPrice, paxClassId, pax, payloadKg, note) 
	SELECT 
		i_transfers_id as transfers_id,
        p.cpd_id AS cpdId,
        flp.flp_id AS flpId,
        flp2.flp_id AS flpId2,
        pd.dep_ap_id AS depApId,
        pd.arr_ap_id AS arrApId,
        pd.distance_km AS distanceKm,
        flp.fl_name AS flName,
        flp2.fl_name AS flName2,
        ((ifnull(flp.available_cargo_kg,0) - flp.payload_kg)) AS remainPayloadKg,
        ((ifnull(flp2.available_cargo_kg,0) - flp2.payload_kg)) AS remainPayloadKg2,
        ((ifnull(flp3.available_cargo_kg,0) - flp3.payload_kg)) AS remainPayloadKg3,
        ((ifnull(flp4.available_cargo_kg,0) - flp4.payload_kg)) AS remainPayloadKg4,
        ROUND((p.max_ticket_price - (
			(p.payload_kg * flpns.price_c_p_kg) 
            + (p.payload_kg * flpns2.price_c_p_kg) 
            + (p.payload_kg * flpns3.price_c_p_kg) 
			+ (p.payload_kg * flpns4.price_c_p_kg) 
            )), 2) AS diffPrice,
        ROUND((
			(p.payload_kg * flpns.price_c_p_kg) 
            + (p.payload_kg * flpns2.price_c_p_kg) 
            + (p.payload_kg * flpns3.price_c_p_kg) 
            + (p.payload_kg * flpns4.price_c_p_kg) 
            ), 2) AS sumPrice,
        ROUND(p.max_ticket_price, 2) AS pPrice,
        p.pax_class_id AS paxClassId,
        0 AS pax,
        p.payload_kg AS payloadKg,
        p.note AS note
    FROM ams_ac.ams_al_flight_plan flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp3 ON ((flp3.d_ap_id = flp2.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
         JOIN ams_ac.ams_al_flight_plan flp4 ON ((flp4.d_ap_id = flp3.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns4 ON ((flpns4.flpns_id = flp4.flpns_id))
        JOIN ams_aircraft_curr ac ON ac.ac_id = flp.ac_id
        JOIN ams_wad.ams_airport_cpd p ON p.dep_ap_id = flp.d_ap_id
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
        AND flp3.flp_status_id = 1
        AND flp4.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
        AND flp3.a_ap_id <> flp2.d_ap_id
		AND flp4.a_ap_id <> flp3.d_ap_id
		AND (pd.arr_ap_id = flp4.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.op_time_min)
        AND (TIMESTAMPDIFF(MINUTE, flp2.atime, flp3.dtime) > ac.op_time_min)
        AND (TIMESTAMPDIFF(MINUTE, flp3.atime, flp4.dtime) > ac.op_time_min)
        having remainPayloadKg>0 and remainPayloadKg2>0 and remainPayloadKg3>0 and remainPayloadKg4>0;
SET logNote=CONCAT('Finish ', ' <- ' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows =  ROW_COUNT();
commit; 
SET logNote=CONCAT('Inserted  ',countRows,  ' rows quadropol cpd <-' );
call ams_wad.log_sp(spname, stTime, logNote);

SELECT count(*) as totalRows from ams_ac.ams_al_flp_transfer_pcpd ;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_n_transfers_cpd` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_n_transfers_cpd`()
BEGIN

DECLARE i_transfers_id, countRows, i_op_time_min INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_n_transfers_cpd', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_n_transfers_cpd';
set countRows = 0;
set i_op_time_min = 30;

SET logNote=CONCAT('Start ', ' ->' );
call ams_wad.log_sp(spname, stTime, logNote);

-- Get i_transfers_id
SELECT param_value INTO i_transfers_id FROM ams_al.cfg_callc_param where param_id=44;

SET logNote=CONCAT('i_transfers_id: ', i_transfers_id, '.' );
call ams_wad.log_sp(spname, stTime, logNote);

 TRUNCATE TABLE ams_ac.ams_al_flight_plan_transfers_cpd;
 commit;
 
 INSERT INTO ams_ac.ams_al_flight_plan_transfers_cpd
	(transfers_id, cpd_id, flp_id, flp_id2, flp_id3, flp_id4,
	dep_ap_id, arr_ap_id, distance_km, fl_name, fl_name2, fl_name3, fl_name4,
	available_cargo_kg, available_cargo_kg2, available_cargo_kg3, available_cargo_kg4,
	cpd_price, cpd_pax_class_id, cpd_payload_kg, cpd_note, cpd_adate)
SELECT 
		i_transfers_id as transfers_id,
        p.cpd_id AS cpd_id,
        flp.flp_id AS flp_id,
        flp2.flp_id AS flp_id2,
        flp3.flp_id AS flp_id3,
        flp4.flp_id AS flp_id4,
        p.dep_ap_id AS dep_ap_id,
        p.arr_ap_id AS arr_ap_id,
        p.distance_km AS distance_km,
        flp.fl_name AS fl_name,
        flp2.fl_name AS fl_name2,
        flp3.fl_name AS fl_name3,
        flp4.fl_name AS fl_name4,
        ifnull(flp.available_cargo_kg,0)  AS available_cargo_kg,
        ifnull(flp2.available_cargo_kg,0) AS available_cargo_kg2,
        ifnull(flp3.available_cargo_kg,0)  AS available_cargo_kg3,
		ifnull(flp4.available_cargo_kg,0) AS available_cargo_kg4,
        ROUND(p.max_ticket_price, 2) AS cpd_price,
        p.pax_class_id AS cpd_pax_class_id,
        p.payload_kg AS cpd_payload_kg,
        p.note AS cpd_note,
        p.adate as cpd_adate
    FROM ams_ac.ams_al_flight_plan_tmp flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON flpns.flpns_id = flp.flpns_id
        JOIN ams_ac.ams_al_flight_plan_tmp flp2 ON flp2.d_ap_id = flp.a_ap_id
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON flpns2.flpns_id = flp2.flpns_id
        JOIN ams_ac.ams_al_flight_plan_tmp flp3 ON flp3.d_ap_id = flp2.a_ap_id
        JOIN ams_al.ams_al_flp_number_schedule flpns3 ON flpns3.flpns_id = flp3.flpns_id
        JOIN ams_ac.ams_al_flight_plan_tmp flp4 ON flp4.d_ap_id = flp3.a_ap_id
        JOIN ams_al.ams_al_flp_number_schedule flpns4 ON flpns4.flpns_id = flp4.flpns_id
        JOIN ams_wad.ams_airport_cpd_for_flp p ON p.dep_ap_id = flp.d_ap_id
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp3.flp_status_id = 1
		AND flp4.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND flp3.a_ap_id <> flp2.d_ap_id
		AND flp3.a_ap_id <> flp.d_ap_id
		AND flp4.a_ap_id <> flp3.d_ap_id
		AND flp4.a_ap_id <> flp2.d_ap_id
		AND flp4.a_ap_id <> flp.d_ap_id
		AND p.arr_ap_id = flp4.a_ap_id
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > i_op_time_min)
		AND (TIMESTAMPDIFF(MINUTE, flp2.atime, flp3.dtime) > i_op_time_min)
		AND (TIMESTAMPDIFF(MINUTE, flp3.atime, flp4.dtime) > i_op_time_min)
		having available_cargo_kg>=cpd_payload_kg and available_cargo_kg2>=cpd_payload_kg 
        and available_cargo_kg3>=cpd_payload_kg and available_cargo_kg4>cpd_payload_kg;
	SET countRows =  ROW_COUNT();
    commit;

SET logNote=CONCAT('Inserted  ',countRows,  ' rows quadruple transfer cpd <-' );
call ams_wad.log_sp(spname, stTime, logNote);
	SET countRows = 0;
	INSERT INTO ams_ac.ams_al_flight_plan_transfers_cpd
	(transfers_id, cpd_id, flp_id, flp_id2, flp_id3, 
	dep_ap_id, arr_ap_id, distance_km, fl_name, fl_name2, fl_name3, 
	available_cargo_kg, available_cargo_kg2, available_cargo_kg3, 
	cpd_price, cpd_pax_class_id, cpd_payload_kg, cpd_note, cpd_adate)
SELECT 
		i_transfers_id as transfers_id,
        p.cpd_id AS cpd_id,
        flp.flp_id AS flp_id,
        flp2.flp_id AS flp_id2,
        flp3.flp_id AS flp_id3,
        p.dep_ap_id AS dep_ap_id,
        p.arr_ap_id AS arr_ap_id,
        p.distance_km AS distance_km,
        flp.fl_name AS fl_name,
        flp2.fl_name AS fl_name2,
        flp3.fl_name AS fl_name3,
        ifnull(flp.available_cargo_kg,0)  AS available_cargo_kg,
        ifnull(flp2.available_cargo_kg,0) AS available_cargo_kg2,
        ifnull(flp3.available_cargo_kg,0)  AS available_cargo_kg3,
        ROUND(p.max_ticket_price, 2) AS cpd_price,
        p.pax_class_id AS cpd_pax_class_id,
        p.payload_kg AS cpd_payload_kg,
        p.note AS cpd_note,
        p.adate as cpd_adate
    FROM ams_ac.ams_al_flight_plan_tmp flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON flpns.flpns_id = flp.flpns_id
        JOIN ams_ac.ams_al_flight_plan_tmp flp2 ON flp2.d_ap_id = flp.a_ap_id
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON flpns2.flpns_id = flp2.flpns_id
        JOIN ams_ac.ams_al_flight_plan_tmp flp3 ON flp3.d_ap_id = flp2.a_ap_id
        JOIN ams_al.ams_al_flp_number_schedule flpns3 ON flpns3.flpns_id = flp3.flpns_id
        JOIN ams_wad.ams_airport_cpd_for_flp p ON p.dep_ap_id = flp.d_ap_id
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp3.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND flp3.a_ap_id <> flp2.d_ap_id
		AND flp3.a_ap_id <> flp.d_ap_id
		AND p.arr_ap_id = flp3.a_ap_id
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > i_op_time_min)
		AND (TIMESTAMPDIFF(MINUTE, flp2.atime, flp3.dtime) > i_op_time_min)
		having available_cargo_kg>=cpd_payload_kg and available_cargo_kg2>=cpd_payload_kg 
        and available_cargo_kg3>=cpd_payload_kg ;
            
SET countRows =  ROW_COUNT();
commit; 
SET logNote=CONCAT('Inserted  ',countRows,  ' rows tripple transfer cpd <-' );
call ams_wad.log_sp(spname, stTime, logNote);
	SET countRows = 0;
	INSERT INTO ams_ac.ams_al_flight_plan_transfers_cpd
	(transfers_id, cpd_id, flp_id, flp_id2,
	dep_ap_id, arr_ap_id, distance_km, fl_name, fl_name2,  
	available_cargo_kg, available_cargo_kg2, 
	cpd_price, cpd_pax_class_id, cpd_payload_kg, cpd_note, cpd_adate)
SELECT 
		i_transfers_id as transfers_id,
        p.cpd_id AS cpd_id,
        flp.flp_id AS flp_id,
        flp2.flp_id AS flp_id2,
        p.dep_ap_id AS dep_ap_id,
        p.arr_ap_id AS arr_ap_id,
        p.distance_km AS distance_km,
        flp.fl_name AS fl_name,
        flp2.fl_name AS fl_name2,
        ifnull(flp.available_cargo_kg,0)  AS available_cargo_kg,
        ifnull(flp2.available_cargo_kg,0) AS available_cargo_kg2,
        ROUND(p.max_ticket_price, 2) AS cpd_price,
        p.pax_class_id AS cpd_pax_class_id,
        p.payload_kg AS cpd_payload_kg,
        p.note AS cpd_note,
        p.adate as cpd_adate
    FROM ams_ac.ams_al_flight_plan_tmp flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON flpns.flpns_id = flp.flpns_id
        JOIN ams_ac.ams_al_flight_plan_tmp flp2 ON flp2.d_ap_id = flp.a_ap_id
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON flpns2.flpns_id = flp2.flpns_id
        JOIN ams_wad.ams_airport_cpd_for_flp p ON p.dep_ap_id = flp.d_ap_id
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND p.arr_ap_id = flp2.a_ap_id
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > i_op_time_min)
		having available_cargo_kg>=cpd_payload_kg and available_cargo_kg2>=cpd_payload_kg ;
        
SET countRows =  ROW_COUNT();
commit; 
SET logNote=CONCAT('Inserted  ',countRows,  ' rows double transfer cpd <-' );
call ams_wad.log_sp(spname, stTime, logNote);

SELECT count(*) INTO countRows from ams_ac.ams_al_flight_plan_transfers_cpd ;

SET logNote=CONCAT('Total  ',countRows,  ' rows in ams_al_flight_plan_transfers_cpd <-' );
call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_n_transfers_cpd_add` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_n_transfers_cpd_add`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_n_transfers_cpd_add', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_n_transfers_cpd_add';
set countRows = 0;

SET logNote=CONCAT('Start ', ' ->' );
call ams_wad.log_sp(spname, stTime, logNote);


 TRUNCATE TABLE ams_ac.ams_al_flight_plan_transfers_cpd_add;
 commit;
 
 INSERT INTO ams_ac.ams_al_flight_plan_transfers_cpd_add
 select tr.*,
 least(ifnull(flp.dtime, now()), ifnull(flp2.dtime, now()), ifnull(flp3.dtime, now()), ifnull(flp4.dtime, now())) as dtime
 from ams_ac.ams_al_flight_plan_transfers_cpd tr
	JOIN ams_al_flight_plan_tmp flp ON flp.flp_id = tr.flp_id
	JOIN ams_al.ams_al_flp_number_schedule flpns ON flpns.flpns_id = flp.flpns_id
	LEFT JOIN ams_al_flight_plan_tmp flp2 ON flp2.flp_id = tr.flp_id2
	LEFT JOIN ams_al.ams_al_flp_number_schedule flpns2 ON flpns2.flpns_id = flp2.flpns_id
	LEFT JOIN ams_al_flight_plan_tmp flp3 ON flp3.flp_id = tr.flp_id3
	LEFT JOIN ams_al.ams_al_flp_number_schedule flpns3 ON flpns3.flpns_id = flp3.flpns_id
	LEFT JOIN ams_al_flight_plan_tmp flp4 ON flp4.flp_id = tr.flp_id4
	LEFT JOIN ams_al.ams_al_flp_number_schedule flpns4 ON flpns4.flpns_id = flp4.flpns_id
	WHERE IFNULL(flp.flp_status_id, 1) = 1
	AND IFNULL(flp2.flp_status_id, 1) = 1
	AND IFNULL(flp3.flp_status_id, 1) = 1
	AND IFNULL(flp4.flp_status_id, 1) = 1
    and ROUND((tr.cpd_price - (tr.cpd_payload_kg * (((IFNULL(flpns.price_c_p_kg, 0) + IFNULL(flpns2.price_c_p_kg, 0)) + IFNULL(flpns3.price_c_p_kg, 0)) + IFNULL(flpns4.price_c_p_kg, 0)))),2)>0
    AND ((IFNULL(flp.remain_payload_kg,
            tr.cpd_payload_kg) >= tr.cpd_payload_kg)
        AND (IFNULL(flp2.remain_payload_kg,
            tr.cpd_payload_kg) >= tr.cpd_payload_kg)
        AND (IFNULL(flp3.remain_payload_kg,
            tr.cpd_payload_kg) >= tr.cpd_payload_kg)
        AND (IFNULL(flp4.remain_payload_kg,
            tr.cpd_payload_kg) >= tr.cpd_payload_kg));
	SET countRows =  ROW_COUNT();
    commit;

SET logNote=CONCAT('Inserted  ',countRows,  ' rows ams_al_flight_plan_transfers_cpd_add <-' );
call ams_wad.log_sp(spname, stTime, logNote);
	
SET logNote=CONCAT('Finish <-' );
call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_n_transfers_ppd` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_n_transfers_ppd`()
BEGIN

DECLARE i_transfers_id, countRows, i_op_time_min INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_n_transfers_ppd', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_n_transfers_ppd';
set countRows = 0;
set i_op_time_min = 30;

SET logNote=CONCAT('Start ', ' ->' );
call ams_wad.log_sp(spname, stTime, logNote);

-- Get i_transfers_id
SELECT param_value INTO i_transfers_id FROM ams_al.cfg_callc_param where param_id=43;

SET logNote=CONCAT('i_transfers_id: ', i_transfers_id, '.' );
call ams_wad.log_sp(spname, stTime, logNote);
-- add records in ams_airport_ppd_for_flp
INSERT INTO ams_wad.ams_airport_ppd_for_flp
select p.ppd_id AS ppd_id,
		ROUND(p.max_ticket_price, 2) AS max_ticket_price,
        p.pax_class_id,
        p.pax,
        p.payload_kg,
        p.note,
        p.adate,
        pd.dep_ap_id AS dep_ap_id,
        pd.arr_ap_id AS arr_ap_id,
        pd.distance_km AS distance_km
from ams_wad.ams_airport_ppd p 
JOIN ams_wad.cfg_airport_destination pd ON pd.dest_id = p.dest_id
left join ams_wad.ams_airport_ppd_for_flp flp on flp.ppd_id = p.ppd_id
where flp.ppd_id is null;
SET countRows =  ROW_COUNT();
commit;
SET logNote=CONCAT('Added: ', countRows, ' records in ams_airport_ppd_for_flp.' );
call ams_wad.log_sp(spname, stTime, logNote);

TRUNCATE TABLE ams_ac.ams_al_flight_plan_transfers_ppd;
commit;

SET logNote=CONCAT('TRUNCATE: ', ' transfers ppd.' );
call ams_wad.log_sp(spname, stTime, logNote);

 INSERT INTO ams_ac.ams_al_flight_plan_transfers_ppd
	(transfers_id, ppd_id, flp_id, flp_id2, flp_id3, flp_id4,
	dep_ap_id, arr_ap_id, distance_km, fl_name, fl_name2, fl_name3, fl_name4,
	available_pax, available_pax2, available_pax3, available_pax4,
	available_class_pax, available_class_pax2, available_class_pax3, available_class_pax4,
	ppd_price, ppd_pax_class_id, ppd_pax, ppd_payload_kg, ppd_note, ppd_adate)
	SELECT 
		i_transfers_id as transfers_id,
        p.ppd_id AS ppd_id,
        flp.flp_id AS flp_id,
        flp2.flp_id AS flp_id2,
        flp3.flp_id AS flp_id3,
        flp4.flp_id AS flp_id4,
        p.dep_ap_id AS dep_ap_id,
        p.arr_ap_id AS arr_ap_id,
        p.distance_km AS distance_km,
        flp.fl_name AS fl_name,
        flp2.fl_name AS fl_name2,
        flp3.fl_name AS fl_name3,
        flp4.fl_name AS fl_name4,
        (ifnull(flp.available_pax_e,0) + ifnull(flp.available_pax_b,0) + ifnull(flp.available_pax_f,0)) AS available_pax,
        (ifnull(flp2.available_pax_e,0) + ifnull(flp2.available_pax_b,0) + ifnull(flp2.available_pax_f,0)) AS available_pax2,
        (ifnull(flp3.available_pax_e,0) + ifnull(flp3.available_pax_b,0) + ifnull(flp3.available_pax_f,0)) AS available_pax3,
        (ifnull(flp4.available_pax_e,0) + ifnull(flp4.available_pax_b,0) + ifnull(flp4.available_pax_f,0)) AS available_pax4,
        CASE
			WHEN p.pax_class_id = 1 THEN flp.available_pax_e
			WHEN p.pax_class_id = 2 THEN flp.available_pax_b
            WHEN p.pax_class_id = 3 THEN flp.available_pax_f
			ELSE null
		END as available_class_pax,
        CASE
			WHEN p.pax_class_id = 1 THEN flp2.available_pax_e
			WHEN p.pax_class_id = 2 THEN flp2.available_pax_b
            WHEN p.pax_class_id = 3 THEN flp2.available_pax_f
			ELSE null
		END as available_class_pax2,
        CASE
			WHEN p.pax_class_id = 1 THEN flp3.available_pax_e
			WHEN p.pax_class_id = 2 THEN flp3.available_pax_b
            WHEN p.pax_class_id = 3 THEN flp3.available_pax_f
			ELSE null
		END as available_class_pax3,
        CASE
			WHEN p.pax_class_id = 1 THEN flp4.available_pax_e
			WHEN p.pax_class_id = 2 THEN flp4.available_pax_b
            WHEN p.pax_class_id = 3 THEN flp4.available_pax_f
			ELSE null
		END as available_class_pax4,
        ROUND(p.max_ticket_price, 2) AS ppd_price,
        p.pax_class_id AS pax_class_id,
        p.pax AS ppd_pax,
        p.payload_kg AS ppd_payload_kg,
        p.note AS ppd_note,
        p.adate as ppd_adate
    FROM ams_ac.ams_al_flight_plan_tmp flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan_tmp flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_ac.ams_al_flight_plan_tmp flp3 ON ((flp3.d_ap_id = flp2.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
        JOIN ams_ac.ams_al_flight_plan_tmp flp4 ON ((flp4.d_ap_id = flp3.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns4 ON ((flpns4.flpns_id = flp4.flpns_id))
        JOIN ams_wad.ams_airport_ppd_for_flp p ON p.dep_ap_id = flp.d_ap_id
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp3.flp_status_id = 1
		AND flp4.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND flp3.a_ap_id <> flp2.d_ap_id
		AND flp3.a_ap_id <> flp.d_ap_id
		AND flp4.a_ap_id <> flp3.d_ap_id
		AND flp4.a_ap_id <> flp2.d_ap_id
		AND flp4.a_ap_id <> flp.d_ap_id
		AND (p.arr_ap_id = flp4.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > i_op_time_min)
		AND (TIMESTAMPDIFF(MINUTE, flp2.atime, flp3.dtime) > i_op_time_min)
		AND (TIMESTAMPDIFF(MINUTE, flp3.atime, flp4.dtime) > i_op_time_min)
		having available_pax>=ppd_pax and available_pax2>=ppd_pax and available_pax3>=ppd_pax and available_pax4>=ppd_pax;
	SET countRows =  ROW_COUNT();
    commit;

SET logNote=CONCAT('Inserted  ',countRows,  ' rows quadruple transfer ppd <-' );
call ams_wad.log_sp(spname, stTime, logNote);
	SET countRows = 0;
	INSERT INTO ams_ac.ams_al_flight_plan_transfers_ppd
		(transfers_id, ppd_id, flp_id, flp_id2, flp_id3,
		dep_ap_id, arr_ap_id, distance_km, fl_name, fl_name2, fl_name3,
		available_pax, available_pax2, available_pax3,
		available_class_pax, available_class_pax2, available_class_pax3, 
		ppd_price, ppd_pax_class_id, ppd_pax, ppd_payload_kg, ppd_note, ppd_adate)
	SELECT 
		i_transfers_id as transfers_id,
        p.ppd_id AS ppd_id,
        flp.flp_id AS flp_id,
        flp2.flp_id AS flp_id2,
        flp3.flp_id AS flp_id3,
        p.dep_ap_id AS dep_ap_id,
        p.arr_ap_id AS arr_ap_id,
        p.distance_km AS distance_km,
        flp.fl_name AS fl_name,
        flp2.fl_name AS fl_name2,
        flp3.fl_name AS fl_name3,
        (ifnull(flp.available_pax_e,0) + ifnull(flp.available_pax_b,0) + ifnull(flp.available_pax_f,0)) AS available_pax,
        (ifnull(flp2.available_pax_e,0) + ifnull(flp2.available_pax_b,0) + ifnull(flp2.available_pax_f,0)) AS available_pax2,
        (ifnull(flp3.available_pax_e,0) + ifnull(flp3.available_pax_b,0) + ifnull(flp3.available_pax_f,0)) AS available_pax3,
        CASE
			WHEN p.pax_class_id = 1 THEN flp.available_pax_e
			WHEN p.pax_class_id = 2 THEN flp.available_pax_b
            WHEN p.pax_class_id = 3 THEN flp.available_pax_f
			ELSE null
		END as available_class_pax,
        CASE
			WHEN p.pax_class_id = 1 THEN flp2.available_pax_e
			WHEN p.pax_class_id = 2 THEN flp2.available_pax_b
            WHEN p.pax_class_id = 3 THEN flp2.available_pax_f
			ELSE null
		END as available_class_pax2,
        CASE
			WHEN p.pax_class_id = 1 THEN flp3.available_pax_e
			WHEN p.pax_class_id = 2 THEN flp3.available_pax_b
            WHEN p.pax_class_id = 3 THEN flp3.available_pax_f
			ELSE null
		END as available_class_pax3,
       
        ROUND(p.max_ticket_price, 2) AS ppd_price,
        p.pax_class_id AS pax_class_id,
        p.pax AS ppd_pax,
        p.payload_kg AS ppd_payload_kg,
        p.note AS ppd_note,
        p.adate as ppd_adate
    FROM ams_ac.ams_al_flight_plan_tmp flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan_tmp flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_ac.ams_al_flight_plan_tmp flp3 ON ((flp3.d_ap_id = flp2.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
        JOIN ams_wad.ams_airport_ppd_for_flp p ON p.dep_ap_id = flp.d_ap_id
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp3.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND flp3.a_ap_id <> flp2.d_ap_id
		AND flp3.a_ap_id <> flp.d_ap_id
		AND (p.arr_ap_id = flp3.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > i_op_time_min)
		AND (TIMESTAMPDIFF(MINUTE, flp2.atime, flp3.dtime) > i_op_time_min)
		having available_pax>=ppd_pax and available_pax2>=ppd_pax and available_pax3>=ppd_pax;
            
SET countRows =  ROW_COUNT();
commit; 
SET logNote=CONCAT('Inserted  ',countRows,  ' rows tripple transfer ppd <-' );
call ams_wad.log_sp(spname, stTime, logNote);
	SET countRows = 0;
	INSERT INTO ams_ac.ams_al_flight_plan_transfers_ppd
		(transfers_id, ppd_id, flp_id, flp_id2,
		dep_ap_id, arr_ap_id, distance_km, fl_name, fl_name2,
		available_pax, available_pax2,
		available_class_pax, available_class_pax2, 
		ppd_price, ppd_pax_class_id, ppd_pax, ppd_payload_kg, ppd_note, ppd_adate)
	SELECT 
		i_transfers_id as transfers_id,
        p.ppd_id AS ppd_id,
        flp.flp_id AS flp_id,
        flp2.flp_id AS flp_id2,
        p.dep_ap_id AS dep_ap_id,
        p.arr_ap_id AS arr_ap_id,
        p.distance_km AS distance_km,
        flp.fl_name AS fl_name,
        flp2.fl_name AS fl_name2,
        (ifnull(flp.available_pax_e,0) + ifnull(flp.available_pax_b,0) + ifnull(flp.available_pax_f,0)) AS available_pax,
        (ifnull(flp2.available_pax_e,0) + ifnull(flp2.available_pax_b,0) + ifnull(flp2.available_pax_f,0)) AS available_pax2,
        CASE
			WHEN p.pax_class_id = 1 THEN flp.available_pax_e
			WHEN p.pax_class_id = 2 THEN flp.available_pax_b
            WHEN p.pax_class_id = 3 THEN flp.available_pax_f
			ELSE null
		END as available_class_pax,
        CASE
			WHEN p.pax_class_id = 1 THEN flp2.available_pax_e
			WHEN p.pax_class_id = 2 THEN flp2.available_pax_b
            WHEN p.pax_class_id = 3 THEN flp2.available_pax_f
			ELSE null
		END as available_class_pax2,
        ROUND(p.max_ticket_price, 2) AS ppd_price,
        p.pax_class_id AS pax_class_id,
        p.pax AS ppd_pax,
        p.payload_kg AS ppd_payload_kg,
        p.note AS ppd_note,
        p.adate as ppd_adate
    FROM ams_ac.ams_al_flight_plan_tmp flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan_tmp flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_wad.ams_airport_ppd_for_flp p ON p.dep_ap_id = flp.d_ap_id
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND (p.arr_ap_id = flp2.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > i_op_time_min)
		having available_pax>=ppd_pax and available_pax2>=ppd_pax;
        
SET countRows =  ROW_COUNT();
commit; 
SET logNote=CONCAT('Inserted  ',countRows,  ' rows double transfer ppd <-' );
call ams_wad.log_sp(spname, stTime, logNote);

SELECT count(*) INTO countRows from ams_ac.ams_al_flight_plan_transfers_ppd ;

SET logNote=CONCAT('Total  ',countRows,  ' rows in ams_al_flight_plan_transfers_ppd <-' );
call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_n_transfers_ppd_add` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_n_transfers_ppd_add`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_n_transfers_ppd_add', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_n_transfers_ppd_add';
set countRows = 0;

SET logNote=CONCAT('Start ', ' ->' );
call ams_wad.log_sp(spname, stTime, logNote);

select count(*) INTO countRows from ams_ac.ams_al_flight_plan_transfers_ppd;
SET logNote=CONCAT('Found  ',countRows,  ' rows ams_al_flight_plan_transfers_ppd.' );
call ams_wad.log_sp(spname, stTime, logNote);

 TRUNCATE TABLE ams_ac.ams_al_flight_plan_transfers_ppd_add;
 commit;
 
 INSERT INTO ams_ac.ams_al_flight_plan_transfers_ppd_add
 select tr.*,
 least(ifnull(flp.dtime, now()), ifnull(flp2.dtime, now()), ifnull(flp3.dtime, now()), ifnull(flp4.dtime, now())) as dtime
 from ams_ac.ams_al_flight_plan_transfers_ppd tr
	JOIN ams_al_flight_plan_tmp flp ON flp.flp_id = tr.flp_id
	JOIN ams_al.ams_al_flp_number_schedule flpns ON flpns.flpns_id = flp.flpns_id
	LEFT JOIN ams_al_flight_plan_tmp flp2 ON flp2.flp_id = tr.flp_id2
	LEFT JOIN ams_al.ams_al_flp_number_schedule flpns2 ON flpns2.flpns_id = flp2.flpns_id
	LEFT JOIN ams_al_flight_plan_tmp flp3 ON flp3.flp_id = tr.flp_id3
	LEFT JOIN ams_al.ams_al_flp_number_schedule flpns3 ON flpns3.flpns_id = flp3.flpns_id
	LEFT JOIN ams_al_flight_plan_tmp flp4 ON flp4.flp_id = tr.flp_id4
	LEFT JOIN ams_al.ams_al_flp_number_schedule flpns4 ON flpns4.flpns_id = flp4.flpns_id
	WHERE IFNULL(flp.flp_status_id, 1) = 1
	AND IFNULL(flp2.flp_status_id, 1) = 1
	AND IFNULL(flp3.flp_status_id, 1) = 1
	AND IFNULL(flp4.flp_status_id, 1) = 1
    and (CASE
            WHEN
                (tr.ppd_pax_class_id = 1)
            THEN
                ROUND((tr.ppd_price - ((((tr.ppd_pax * IFNULL(flpns.price_e, 0)) + (tr.ppd_pax * IFNULL(flpns2.price_e, 0))) + (tr.ppd_pax * IFNULL(flpns3.price_e, 0))) + (tr.ppd_pax * IFNULL(flpns4.price_e, 0)))),
                        2)
            WHEN
                (tr.ppd_pax_class_id = 2)
            THEN
                ROUND((tr.ppd_price - ((((tr.ppd_pax * IFNULL(flpns.price_b, 0)) + (tr.ppd_pax * IFNULL(flpns2.price_b, 0))) + (tr.ppd_pax * IFNULL(flpns3.price_b, 0))) + (tr.ppd_pax * IFNULL(flpns4.price_b, 0)))),
                        2)
            WHEN
                (tr.ppd_pax_class_id = 3)
            THEN
                ROUND((tr.ppd_price - ((((tr.ppd_pax * IFNULL(flpns.price_f, 0)) + (tr.ppd_pax * IFNULL(flpns2.price_f, 0))) + (tr.ppd_pax * IFNULL(flpns3.price_f, 0))) + (tr.ppd_pax * IFNULL(flpns4.price_f, 0)))),
                        2)
            ELSE NULL
        END)  > 0
    AND ((IFNULL(CASE
            WHEN (tr.ppd_pax_class_id = 1) THEN flp.remain_pax_e
            WHEN (tr.ppd_pax_class_id = 2) THEN flp.remain_pax_b
            WHEN (tr.ppd_pax_class_id = 3) THEN flp.remain_pax_f
            ELSE NULL
        END,
            tr.ppd_pax) >= tr.ppd_pax)
        AND (IFNULL(CASE
            WHEN (tr.ppd_pax_class_id = 1) THEN flp2.remain_pax_e
            WHEN (tr.ppd_pax_class_id = 2) THEN flp2.remain_pax_b
            WHEN (tr.ppd_pax_class_id = 3) THEN flp2.remain_pax_f
            ELSE NULL
        END,
            tr.ppd_pax) >= tr.ppd_pax)
        AND (IFNULL(CASE
            WHEN (tr.ppd_pax_class_id = 1) THEN flp3.remain_pax_e
            WHEN (tr.ppd_pax_class_id = 2) THEN flp3.remain_pax_b
            WHEN (tr.ppd_pax_class_id = 3) THEN flp3.remain_pax_f
            ELSE NULL
        END,
            tr.ppd_pax) >= tr.ppd_pax)
        AND (IFNULL(CASE
            WHEN (tr.ppd_pax_class_id = 1) THEN flp4.remain_pax_e
            WHEN (tr.ppd_pax_class_id = 2) THEN flp4.remain_pax_b
            WHEN (tr.ppd_pax_class_id = 3) THEN flp4.remain_pax_f
            ELSE NULL
        END,
            tr.ppd_pax) >= tr.ppd_pax));
            
	SET countRows =  ROW_COUNT();
    commit;

SET logNote=CONCAT('Inserted  ',countRows,  ' rows ams_al_flight_plan_transfers_ppd_add <-' );
call ams_wad.log_sp(spname, stTime, logNote);
	
SET logNote=CONCAT('Finish <-' );
call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_pcpd_double` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_pcpd_double`(IN in_flp_id INT)
BEGIN

DECLARE i_ppd_id, i_cpd_id, i_flp_status_id INT;
DECLARE i_flp_id_2 INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_pcpd_double', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_pcpd_double';
set i_flp_status_id = 2; -- Flight is sold out
set countRows = 0;
set i_ppd_id = 0;
set i_cpd_id = 0;

SET logNote=CONCAT('Start (in_flp_id:', in_flp_id, ') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

    
IF in_flp_id > 0 THEN
	
	set countRows = 0;
	select count(p.ppd_id) INTO countRows
	FROM ams_ac.ams_al_flight_plan flp
        JOIN ams_ac.v_ams_al_flight_plan_payload_sum flpp ON ((flpp.flp_id = flp.flp_id))
		JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
		JOIN ams_ac.v_ams_al_flight_plan_payload_sum flpp2 ON ((flpp2.flp_id = flp2.flp_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_aircraft_curr ac ON ac.ac_id = flp.ac_id
        JOIN ams_wad.ams_airport_ppd p ON p.dep_ap_id = flp.d_ap_id
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE  flp.flp_id = in_flp_id and
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND (pd.arr_ap_id = flp2.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.op_time_min)
        and (
			(p.pax_class_id = 1 and p.pax <= flpp.remain_pax_e and p.pax <= flpp2.remain_pax_e and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e)))
			or (p.pax_class_id = 2 and p.pax <= flpp.remain_pax_b and p.pax <= flpp2.remain_pax_b and p.max_ticket_price >= ((p.pax*flpns.price_b)+(p.pax*flpns2.price_b)))
			or (p.pax_class_id = 3 and p.pax <= flpp.remain_pax_f and p.pax <= flpp2.remain_pax_f and p.max_ticket_price >= ((p.pax*flpns.price_f)+(p.pax*flpns2.price_f)))
		);
    SET logNote=CONCAT('PPD Transfer flights count: ',countRows);
	call ams_wad.log_sp(spname, stTime, logNote);
	
	if countRows > 0 THEN
		select p.ppd_id, flp2.flp_id as flp_id_2 INTO i_ppd_id, i_flp_id_2
		FROM ams_ac.ams_al_flight_plan flp
        JOIN ams_ac.v_ams_al_flight_plan_payload_sum flpp ON ((flpp.flp_id = flp.flp_id))
		JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
		JOIN ams_ac.v_ams_al_flight_plan_payload_sum flpp2 ON ((flpp2.flp_id = flp2.flp_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_aircraft_curr ac ON ac.ac_id = flp.ac_id
        JOIN ams_wad.ams_airport_ppd p ON p.dep_ap_id = flp.d_ap_id
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE  flp.flp_id = in_flp_id and
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND (pd.arr_ap_id = flp2.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.op_time_min)
        and (
			(p.pax_class_id = 1 and p.pax <= flpp.remain_pax_e and p.pax <= flpp2.remain_pax_e and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e)))
			or (p.pax_class_id = 2 and p.pax <= flpp.remain_pax_b and p.pax <= flpp2.remain_pax_b and p.max_ticket_price >= ((p.pax*flpns.price_b)+(p.pax*flpns2.price_b)))
			or (p.pax_class_id = 3 and p.pax <= flpp.remain_pax_f and p.pax <= flpp2.remain_pax_f and p.max_ticket_price >= ((p.pax*flpns.price_f)+(p.pax*flpns2.price_f)))
		)
		order by p.adate limit 1;
		
		
		-- Add double Pax record to payload
		IF i_ppd_id > 0 THEN
			SET logNote=CONCAT(' i_ppd_id: ',i_ppd_id, ', i_flp_id_2: ', i_flp_id_2);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_ppd_double(in_flp_id, i_ppd_id, i_flp_id_2);
		else
            SET logNote=CONCAT(' No ppd record found for in_flp_id: ',in_flp_id);
			call ams_wad.log_sp(spname, stTime, logNote);
		END IF;
	ELSE
        call ams_ac.sp_al_flight_plan_payload_ppd_downgrade_double(in_flp_id);
	END IF;
	
	set countRows = 0;
	select count(p.cpd_id) INTO countRows
	FROM ams_ac.ams_al_flight_plan flp
        JOIN ams_ac.v_ams_al_flight_plan_payload_sum flpp ON ((flpp.flp_id = flp.flp_id))
		JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
		JOIN ams_ac.v_ams_al_flight_plan_payload_sum flpp2 ON ((flpp2.flp_id = flp2.flp_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_aircraft_curr ac ON ac.ac_id = flp.ac_id
        JOIN ams_wad.ams_airport_cpd p ON p.dep_ap_id = flp.d_ap_id
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE  flp.flp_id = in_flp_id and
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND (pd.arr_ap_id = flp2.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.op_time_min)
        and (
			p.pax_class_id = 4 and p.payload_kg <= flpp.remain_payload_kg  and p.payload_kg <= flpp2.remain_payload_kg and p.max_ticket_price >= ((p.payload_kg*flpns.price_c_p_kg)+(p.payload_kg*flpns.price_c_p_kg))
		);
    
	SET logNote=CONCAT('CPD Transfer flights count: ',countRows);
	call ams_wad.log_sp(spname, stTime, logNote);
	IF countRows >0 THEN
		
		select p.cpd_id, flp2.flp_id as flp_id_2 INTO i_cpd_id, i_flp_id_2
		FROM ams_ac.ams_al_flight_plan flp
        JOIN ams_ac.v_ams_al_flight_plan_payload_sum flpp ON ((flpp.flp_id = flp.flp_id))
		JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
		JOIN ams_ac.v_ams_al_flight_plan_payload_sum flpp2 ON ((flpp2.flp_id = flp2.flp_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_aircraft_curr ac ON ac.ac_id = flp.ac_id
        JOIN ams_wad.ams_airport_cpd p ON p.dep_ap_id = flp.d_ap_id
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE  flp.flp_id = in_flp_id and
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND (pd.arr_ap_id = flp2.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.op_time_min)
        and (
			p.pax_class_id = 4 and p.payload_kg <= flpp.remain_payload_kg  and p.payload_kg <= flpp2.remain_payload_kg and p.max_ticket_price >= ((p.payload_kg*flpns.price_c_p_kg)+(p.payload_kg*flpns.price_c_p_kg))
		)
		order by p.adate limit 1;

		
		-- Add double Cargo record to payload
		IF i_cpd_id > 0 THEN
			SET logNote=CONCAT(' i_cpd_id: ',i_cpd_id, ', i_flp_id_2: ', i_flp_id_2);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_cpd_double(in_flp_id, i_cpd_id, i_flp_id_2);
		else
            SET logNote=CONCAT(' No cpd record found for in_flp_id: ',in_flp_id);
			call ams_wad.log_sp(spname, stTime, logNote);
		END IF;
	END IF;
	
END IF;

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_pcpd_quadruple` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_pcpd_quadruple`(IN in_flp_id INT)
BEGIN

DECLARE i_ppd_id, i_cpd_id, i_flp_status_id INT;
DECLARE i_flp_id_2, i_flp_id_3, i_flp_id_4 INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_pcpd_quadruple', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_pcpd_quadruple';
set i_flp_status_id = 2; -- Flight is sold out
set countRows = 0;
set i_ppd_id = 0;
set i_cpd_id = 0;

SET logNote=CONCAT('Start (in_flp_id:', in_flp_id, ') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

    
IF in_flp_id > 0 THEN
	
	call ams_ac.sp_ams_al_flp_transfer_ppd_quadruple(in_flp_id);
	set countRows = 0;
	select count(*) into countRows 
	from ams_ac.ams_al_flp_transfer_ppd pcpd
	join ams_wad.ams_airport_ppd p on p.ppd_id = pcpd.ppdId 
	join ams_al_flight_plan_payload_pct_full flp on flp.flp_id = pcpd.flpId
	JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
	join ams_al_flight_plan_payload_pct_full flp2 on flp2.flp_id = pcpd.flpId2
	JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
	join ams_al_flight_plan_payload_pct_full flp3 on flp3.flp_id = pcpd.flpId3
	JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
	join ams_al_flight_plan_payload_pct_full flp4 on flp4.flp_id = pcpd.flpId3
	JOIN ams_al.ams_al_flp_number_schedule flpns4 ON ((flpns4.flpns_id = flp4.flpns_id))
	where (
			(p.pax_class_id = 1 and p.pax <= flp.remain_pax_e and p.pax <= flp2.remain_pax_e and p.pax <= flp3.remain_pax_e and p.pax <= flp4.remain_pax_e and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e) + (p.pax*flpns3.price_e)+ (p.pax*flpns4.price_e)))
			or (p.pax_class_id = 2 and p.pax <= flp.remain_pax_b and p.pax <= flp2.remain_pax_b and p.pax <= flp3.remain_pax_b  and p.pax <= flp4.remain_pax_b and p.max_ticket_price >= ((p.pax*flpns.price_b)+(p.pax*flpns2.price_b)+(p.pax*flpns3.price_b)+(p.pax*flpns4.price_b)))
			or (p.pax_class_id = 3 and p.pax <= flp.remain_pax_f and p.pax <= flp2.remain_pax_f and p.pax <= flp3.remain_pax_f and p.pax <= flp4.remain_pax_f  and p.max_ticket_price >= ((p.pax*flpns.price_f)+(p.pax*flpns2.price_f)+(p.pax*flpns3.price_f)+(p.pax*flpns4.price_f)))
		);
    SET logNote=CONCAT('PPD Transfer flights count: ',countRows);
	call ams_wad.log_sp(spname, stTime, logNote);
	
	if countRows > 0 THEN
		select p.ppd_id, flp2.flp_id as flp_id_2, flp3.flp_id as flp_id_3, flp4.flp_id as flp_id_4 
		 INTO i_ppd_id, i_flp_id_2, i_flp_id_3, i_flp_id_4
		 from ams_ac.ams_al_flp_transfer_ppd pcpd
		 join ams_wad.ams_airport_ppd p on p.ppd_id = pcpd.ppdId 
		 join ams_al_flight_plan_payload_pct_full flp on flp.flp_id = pcpd.flpId
		 JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
		 join ams_al_flight_plan_payload_pct_full flp2 on flp2.flp_id = pcpd.flpId2
		 JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
		 join ams_al_flight_plan_payload_pct_full flp3 on flp3.flp_id = pcpd.flpId3
		 JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
		 join ams_al_flight_plan_payload_pct_full flp4 on flp4.flp_id = pcpd.flpId3
		 JOIN ams_al.ams_al_flp_number_schedule flpns4 ON ((flpns4.flpns_id = flp4.flpns_id))
		 where (
					(p.pax_class_id = 1 and p.pax <= flp.remain_pax_e and p.pax <= flp2.remain_pax_e and p.pax <= flp3.remain_pax_e and p.pax <= flp4.remain_pax_e and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e) + (p.pax*flpns3.price_e)+ (p.pax*flpns4.price_e)))
					or (p.pax_class_id = 2 and p.pax <= flp.remain_pax_b and p.pax <= flp2.remain_pax_b and p.pax <= flp3.remain_pax_b  and p.pax <= flp4.remain_pax_b and p.max_ticket_price >= ((p.pax*flpns.price_b)+(p.pax*flpns2.price_b)+(p.pax*flpns3.price_b)+(p.pax*flpns4.price_b)))
					or (p.pax_class_id = 3 and p.pax <= flp.remain_pax_f and p.pax <= flp2.remain_pax_f and p.pax <= flp3.remain_pax_f and p.pax <= flp4.remain_pax_f  and p.max_ticket_price >= ((p.pax*flpns.price_f)+(p.pax*flpns2.price_f)+(p.pax*flpns3.price_f)+(p.pax*flpns4.price_f)))
				)
		order by p.adate limit 1;
		
		
		-- Add double Pax record to payload
		IF i_ppd_id > 0 THEN
			SET logNote=CONCAT(' i_ppd_id: ',i_ppd_id, ', i_flp_id_2: ', i_flp_id_2, ', i_flp_id_4: ', i_flp_id_4);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_ppd_quadruple(in_flp_id, i_ppd_id, i_flp_id_2, i_flp_id_3, i_flp_id_4);
		else
            SET logNote=CONCAT(' No ppd record found for in_flp_id: ',in_flp_id);
			call ams_wad.log_sp(spname, stTime, logNote);
		END IF;
	ELSE
        call ams_ac.sp_al_flight_plan_payload_ppd_downgrade_quadruple(in_flp_id);
	END IF;
	
    call ams_ac.sp_ams_al_flp_transfer_cpd_quadruple(in_flp_id);
	set countRows = 0;
	select count(p.cpd_id) INTO countRows
	from ams_ac.ams_al_flp_transfer_cpd pcpd
	join ams_wad.ams_airport_cpd p on p.cpd_id = pcpd.cpdId 
	join ams_al_flight_plan_payload_pct_full flp on flp.flp_id = pcpd.flpId
	JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
	join ams_al_flight_plan_payload_pct_full flp2 on flp2.flp_id = pcpd.flpId2
	JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
	join ams_al_flight_plan_payload_pct_full flp3 on flp3.flp_id = pcpd.flpId3
	JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
	join ams_al_flight_plan_payload_pct_full flp4 on flp4.flp_id = pcpd.flpId3
	JOIN ams_al.ams_al_flp_number_schedule flpns4 ON ((flpns4.flpns_id = flp4.flpns_id))
    where (
		p.pax_class_id = 4 and 
        p.payload_kg <= flp.remain_payload_kg  and 
        p.payload_kg <= flp2.remain_payload_kg and 
        p.payload_kg <= flp3.remain_payload_kg and 
        p.payload_kg <= flp4.remain_payload_kg and 
        p.max_ticket_price >= ((p.payload_kg*flpns.price_c_p_kg)+(p.payload_kg*flpns2.price_c_p_kg)+(p.payload_kg*flpns3.price_c_p_kg)+(p.payload_kg*flpns4.price_c_p_kg))
	);
    
	SET logNote=CONCAT('CPD Transfer flights count: ',countRows);
	call ams_wad.log_sp(spname, stTime, logNote);
	IF countRows >0 THEN
		
		select p.cpd_id, flp2.flp_id as flp_id_2, flp3.flp_id as flp_id_3, flp4.flp_id as flp_id_4 
        INTO i_cpd_id, i_flp_id_2, i_flp_id_3, i_flp_id_4
		from ams_ac.ams_al_flp_transfer_cpd pcpd
		join ams_wad.ams_airport_cpd p on p.cpd_id = pcpd.cpdId 
		join ams_al_flight_plan_payload_pct_full flp on flp.flp_id = pcpd.flpId
		JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
		join ams_al_flight_plan_payload_pct_full flp2 on flp2.flp_id = pcpd.flpId2
		JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
		join ams_al_flight_plan_payload_pct_full flp3 on flp3.flp_id = pcpd.flpId3
		JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
		join ams_al_flight_plan_payload_pct_full flp4 on flp4.flp_id = pcpd.flpId3
		JOIN ams_al.ams_al_flp_number_schedule flpns4 ON ((flpns4.flpns_id = flp4.flpns_id))
		where (
			p.pax_class_id = 4 and 
			p.payload_kg <= flp.remain_payload_kg  and 
			p.payload_kg <= flp2.remain_payload_kg and 
			p.payload_kg <= flp3.remain_payload_kg and 
			p.payload_kg <= flp4.remain_payload_kg and 
			p.max_ticket_price >= ((p.payload_kg*flpns.price_c_p_kg)+(p.payload_kg*flpns2.price_c_p_kg)+(p.payload_kg*flpns3.price_c_p_kg)+(p.payload_kg*flpns4.price_c_p_kg))
		)
		order by p.adate limit 1;

		
		-- Add double Cargo record to payload
		IF i_cpd_id > 0 THEN
			SET logNote=CONCAT(' i_cpd_id: ',i_cpd_id, ', i_flp_id_2: ', i_flp_id_2, ', i_flp_id_4: ', i_flp_id_4);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_cpd_quadruple(in_flp_id, i_cpd_id, i_flp_id_2, i_flp_id_3, i_flp_id_4);
		else
            SET logNote=CONCAT(' No cpd record found for in_flp_id: ',in_flp_id);
			call ams_wad.log_sp(spname, stTime, logNote);
		END IF;
	END IF;
	
	SET logNote=CONCAT(' Finish in_flp_id: ',in_flp_id, '.');
	call ams_wad.log_sp(spname, stTime, logNote);
	COMMIT;
END IF;
    
  

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_pcpd_single` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_pcpd_single`(IN in_flp_id INT)
BEGIN

DECLARE i_ppd_id, i_cpd_id, i_flp_status_id INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_pcpd_single', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_pcpd_single';
set i_flp_status_id = 2; -- Flight is sold out
set countRows = 0;
set i_ppd_id = 0;
set i_cpd_id = 0;

SET logNote=CONCAT('Start ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

IF in_flp_id > 0 THEN
	SET logNote=CONCAT(' in_flp_id: ',in_flp_id, ' payload add...');
	-- call ams_wad.log_sp(spname, stTime, logNote);
	call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id);
    
	set countRows = 0;
	select count(p.ppd_id) INTO countRows
	from ams_wad.ams_airport_ppd p
	join ams_ac.ams_al_flight_plan flp on flp.dest_id = p.dest_id 
	join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where flp.flp_id = in_flp_id 
	and (
		(p.pax_class_id = 1 and p.pax <= flp.remain_pax_e and p.max_ticket_price >= (p.pax*flpns.price_e))
		or (p.pax_class_id = 2 and p.pax <= flp.remain_pax_b and p.max_ticket_price >= (p.pax*flpns.price_b))
		or (p.pax_class_id = 3 and p.pax <= flp.remain_pax_f and p.max_ticket_price >= (p.pax*flpns.price_f))
	) 
    and flp.flp_status_id = 1 and (flp.pax_pct_full < 95);
    
	if countRows > 0 THEN
		select p.ppd_id INTO i_ppd_id
		from ams_wad.ams_airport_ppd p
		join ams_ac.ams_al_flight_plan flp on flp.dest_id = p.dest_id 
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
		where flp.flp_id = in_flp_id 
		and (
			(p.pax_class_id = 1 and p.pax <= flp.remain_pax_e and p.max_ticket_price >= (p.pax*flpns.price_e))
			or (p.pax_class_id = 2 and p.pax <= flp.remain_pax_b and p.max_ticket_price >= (p.pax*flpns.price_b))
			or (p.pax_class_id = 3 and p.pax <= flp.remain_pax_f and p.max_ticket_price >= (p.pax*flpns.price_f))
		)
		order by p.adate limit 1;
		
		SET logNote=CONCAT(' in_flp_id: ',in_flp_id,', i_ppd_id: ',i_ppd_id, ' Single');
		-- call ams_wad.log_sp(spname, stTime, logNote);
		-- Add single Pax record to payload
		IF i_ppd_id > 0 THEN
			call ams_ac.sp_al_flight_plan_payload_add_ppd_single(in_flp_id, i_ppd_id);
		END IF;
	ELSE
		SET logNote=CONCAT('ppd downgrade for in_flp_id: ',in_flp_id, '.');
		-- call ams_wad.log_sp(spname, stTime, logNote);
        call ams_ac.sp_al_flight_plan_payload_add_pcpd_single_downgrade(in_flp_id);
	END IF;
	
	set countRows = 0;
	select count(p.cpd_id) INTO countRows
	from ams_wad.ams_airport_cpd p
	join ams_ac.ams_al_flight_plan flp on flp.dest_id = p.dest_id 
	join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where flp.flp_id = in_flp_id
	and (
		p.pax_class_id = 4 and p.payload_kg <= flp.remain_payload_kg and p.max_ticket_price >= (p.payload_kg*flpns.price_c_p_kg)
	)
    and flp.flp_status_id = 1 and flp.payload_kg_pct_full < 95;
	
	IF countRows >0 THEN
		select p.cpd_id INTO i_cpd_id
		from ams_wad.ams_airport_cpd p
		join ams_ac.ams_al_flight_plan flp on flp.dest_id = p.dest_id 
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
		where flp.flp_id = in_flp_id
		and (
			p.pax_class_id = 4 and p.payload_kg <= flp.remain_payload_kg and p.max_ticket_price >= (p.payload_kg*flpns.price_c_p_kg)
		)order by p.adate limit 1;

		SET logNote=CONCAT(' in_flp_id: ',in_flp_id,' i_cpd_id: ',i_cpd_id, ' Single');
		-- call ams_wad.log_sp(spname, stTime, logNote);
		-- Add single Pax record to payload
		IF i_cpd_id > 0 THEN
			call ams_ac.sp_al_flight_plan_payload_add_cpd_single(in_flp_id, i_cpd_id);
		END IF;
	END IF;
	
	SET logNote=CONCAT(' Finish in_flp_id: ',in_flp_id, '.');
	-- call ams_wad.log_sp(spname, stTime, logNote);
END IF;
    
  

SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_pcpd_single_downgrade` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_pcpd_single_downgrade`(IN in_flp_id INT)
BEGIN

DECLARE i_ppd_id INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote, ppd_note VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_pcpd_single_downgrade', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_pcpd_single_downgrade';
set countRows = 0;
set i_ppd_id = 0;

SET logNote=CONCAT('Start (in_flp_id:', in_flp_id, ') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

IF in_flp_id > 0 THEN
	
	set countRows = 0;
	select count(p.ppd_id) INTO countRows
	from ams_wad.ams_airport_ppd p
	join ams_ac.ams_al_flight_plan flp on flp.dest_id = p.dest_id 
	join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where flp.flp_id = in_flp_id 
	and (
		(p.pax_class_id = 2 and p.pax <= flp.remain_pax_e and p.max_ticket_price >= (p.pax*flpns.price_e))
		or (p.pax_class_id = 3 and p.pax <= flp.remain_pax_e and p.max_ticket_price >= (p.pax*flpns.price_e))
	);
   
	if countRows > 0 THEN
    
		SET logNote=CONCAT('Found ', FORMAT(countRows, 0), ' ppd rows to downgrade.' );
		call ams_wad.log_sp(spname, stTime, logNote);
        
		select p.ppd_id INTO i_ppd_id
		from ams_wad.ams_airport_ppd p
		join ams_ac.ams_al_flight_plan_payload_pct_full flp on flp.dest_id = p.dest_id 
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
		where flp.flp_id = in_flp_id 
		and (
			(p.pax_class_id = 2 and p.pax <= flp.remain_pax_e and p.max_ticket_price >= (p.pax*flpns.price_e))
			or (p.pax_class_id = 3 and p.pax <= flp.remain_pax_e and p.max_ticket_price >= (p.pax*flpns.price_e))
		)
		order by p.adate limit 1;
		
		-- Add single Pax record to payload
		IF i_ppd_id > 0 THEN
			SET logNote=CONCAT('Downgrade i_ppd_id: ',i_ppd_id, ' Single');
			-- call ams_wad.log_sp(spname, stTime, logNote);
			UPDATE ams_wad.ams_airport_ppd p
            set p.pax_class_id = 1, p.note = concat(p.note, ' Downgraded')
            where p.ppd_id = i_ppd_id;
            commit;
            
            set countRows = 0;
            select count(*) INTO countRows 
            from ams_wad.ams_airport_ppd_for_flp
            where ppd_id = i_ppd_id;
            SET logNote=CONCAT('Found ',countRows, ' rows in ams_wad.ams_airport_ppd_for_flp');
			-- call ams_wad.log_sp(spname, stTime, logNote);
            IF countRows > 0 THEN
				UPDATE ams_wad.ams_airport_ppd_for_flp p
				set p.pax_class_id = 1, p.note = concat(p.note, ' Downgraded')
				where p.ppd_id = i_ppd_id;
				commit;
            END IF;
		END IF;
	END IF;
END IF;
    
SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_pcpd_triple` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_pcpd_triple`(IN in_flp_id INT)
BEGIN

DECLARE i_ppd_id, i_cpd_id, i_flp_status_id INT;
DECLARE i_flp_id_2, i_flp_id_3 INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_pcpd_triple', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_pcpd_triple';
set i_flp_status_id = 2; -- Flight is sold out
set countRows = 0;
set i_ppd_id = 0;
set i_cpd_id = 0;

SET logNote=CONCAT('Start (in_flp_id:', in_flp_id, ') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

    
IF in_flp_id > 0 THEN
	
    call ams_ac.sp_ams_al_flp_transfer_ppd_tripple(in_flp_id);
	set countRows = 0;
	select count(p.ppd_id) INTO countRows
    from ams_ac.ams_al_flp_transfer_ppd pcpd
	join ams_wad.ams_airport_ppd p on p.ppd_id = pcpd.ppdId 
	join ams_al_flight_plan_payload_pct_full flp on flp.flp_id = pcpd.flpId
	JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
	join ams_al_flight_plan_payload_pct_full flp2 on flp2.flp_id = pcpd.flpId2
	JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
	join ams_al_flight_plan_payload_pct_full flp3 on flp3.flp_id = pcpd.flpId3
	JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
    Where (
		(p.pax_class_id = 1 and p.pax <= flp.remain_pax_e and p.pax <= flp2.remain_pax_e and p.pax <= flp3.remain_pax_e and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e) + (p.pax*flpns3.price_e)))
		or (p.pax_class_id = 2 and p.pax <= flp.remain_pax_b and p.pax <= flp2.remain_pax_b and p.pax <= flp3.remain_pax_b and p.max_ticket_price >= ((p.pax*flpns.price_b)+(p.pax*flpns2.price_b)+(p.pax*flpns3.price_b)))
		or (p.pax_class_id = 3 and p.pax <= flp.remain_pax_f and p.pax <= flp2.remain_pax_f and p.pax <= flp3.remain_pax_f and p.max_ticket_price >= ((p.pax*flpns.price_f)+(p.pax*flpns2.price_f)+(p.pax*flpns3.price_f)))
	);
    SET logNote=CONCAT('PPD Transfer flights count: ',countRows);
	call ams_wad.log_sp(spname, stTime, logNote);
	
	if countRows > 0 THEN
		select p.ppd_id, flp2.flp_id as flp_id_2, flp3.flp_id as flp_id_3 
		INTO i_ppd_id, i_flp_id_2, i_flp_id_3
		from ams_ac.ams_al_flp_transfer_ppd pcpd
		join ams_wad.ams_airport_ppd p on p.ppd_id = pcpd.ppdId 
		join ams_al_flight_plan_payload_pct_full flp on flp.flp_id = pcpd.flpId
		JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
		join ams_al_flight_plan_payload_pct_full flp2 on flp2.flp_id = pcpd.flpId2
		JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
		join ams_al_flight_plan_payload_pct_full flp3 on flp3.flp_id = pcpd.flpId3
		JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
		Where (
			(p.pax_class_id = 1 and p.pax <= flp.remain_pax_e and p.pax <= flp2.remain_pax_e and p.pax <= flp3.remain_pax_e and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e) + (p.pax*flpns3.price_e)))
			or (p.pax_class_id = 2 and p.pax <= flp.remain_pax_b and p.pax <= flp2.remain_pax_b and p.pax <= flp3.remain_pax_b and p.max_ticket_price >= ((p.pax*flpns.price_b)+(p.pax*flpns2.price_b)+(p.pax*flpns3.price_b)))
			or (p.pax_class_id = 3 and p.pax <= flp.remain_pax_f and p.pax <= flp2.remain_pax_f and p.pax <= flp3.remain_pax_f and p.max_ticket_price >= ((p.pax*flpns.price_f)+(p.pax*flpns2.price_f)+(p.pax*flpns3.price_f)))
		)
		order by p.adate limit 1;
		
		
		-- Add double Pax record to payload
		IF i_ppd_id > 0 THEN
			SET logNote=CONCAT(' i_ppd_id: ',i_ppd_id, ', i_flp_id_2: ', i_flp_id_2, ', i_flp_id_3: ', i_flp_id_3);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_ppd_triple(in_flp_id, i_ppd_id, i_flp_id_2, i_flp_id_3);
		else
            SET logNote=CONCAT(' No ppd record found for in_flp_id: ',in_flp_id);
			call ams_wad.log_sp(spname, stTime, logNote);
		END IF;
	ELSE
        call ams_ac.sp_al_flight_plan_payload_ppd_downgrade_triple(in_flp_id);
	END IF;
	
    call ams_ac.sp_ams_al_flp_transfer_cpd_tripple(in_flp_id);
	set countRows = 0;
	select count(p.cpd_id) INTO countRows
	from ams_ac.ams_al_flp_transfer_cpd pcpd
	join ams_wad.ams_airport_cpd p on p.cpd_id = pcpd.cpdId 
	join ams_al_flight_plan_payload_pct_full flp on flp.flp_id = pcpd.flpId
	JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
	join ams_al_flight_plan_payload_pct_full flp2 on flp2.flp_id = pcpd.flpId2
	JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
	join ams_al_flight_plan_payload_pct_full flp3 on flp3.flp_id = pcpd.flpId3
	JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
    Where (
		p.pax_class_id = 4 and p.payload_kg <= flp.remain_payload_kg  and p.payload_kg <= flp2.remain_payload_kg and p.payload_kg <= flp3.remain_payload_kg and p.max_ticket_price >= ((p.payload_kg*flpns.price_c_p_kg)+(p.payload_kg*flpns2.price_c_p_kg)+(p.payload_kg*flpns3.price_c_p_kg))
	);
    
	SET logNote=CONCAT('CPD Transfer flights count: ',countRows);
	call ams_wad.log_sp(spname, stTime, logNote);
	IF countRows >0 THEN
		
		select p.cpd_id, flp2.flp_id as flp_id_2, flp3.flp_id as flp_id_3 
        INTO i_cpd_id, i_flp_id_2, i_flp_id_3
		from ams_ac.ams_al_flp_transfer_cpd pcpd
		join ams_wad.ams_airport_cpd p on p.cpd_id = pcpd.cpdId 
		join ams_al_flight_plan_payload_pct_full flp on flp.flp_id = pcpd.flpId
		JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
		join ams_al_flight_plan_payload_pct_full flp2 on flp2.flp_id = pcpd.flpId2
		JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
		join ams_al_flight_plan_payload_pct_full flp3 on flp3.flp_id = pcpd.flpId3
		JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
		Where (
			p.pax_class_id = 4 and p.payload_kg <= flp.remain_payload_kg  and p.payload_kg <= flp2.remain_payload_kg and p.payload_kg <= flp3.remain_payload_kg and p.max_ticket_price >= ((p.payload_kg*flpns.price_c_p_kg)+(p.payload_kg*flpns2.price_c_p_kg)+(p.payload_kg*flpns3.price_c_p_kg))
		)
		order by p.adate limit 1;

		
		-- Add double Cargo record to payload
		IF i_cpd_id > 0 THEN
			SET logNote=CONCAT(' i_cpd_id: ',i_cpd_id, ', i_flp_id_2: ', i_flp_id_2, ', i_flp_id_3: ', i_flp_id_3);
			call ams_wad.log_sp(spname, stTime, logNote);
			call ams_ac.sp_al_flight_plan_payload_add_cpd_triple(in_flp_id, i_cpd_id, i_flp_id_2, i_flp_id_3);
		else
            SET logNote=CONCAT(' No cpd record found for in_flp_id: ',in_flp_id);
			call ams_wad.log_sp(spname, stTime, logNote);
		END IF;
	END IF;
	
END IF;

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_ppd_double` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_ppd_double`(IN in_flp_id INT, IN in_ppd_id INT, IN in_flp_id_2 INT)
BEGIN


DECLARE i_pcpd_id, i_flppl_id INT;
DECLARE i_pcpd_id_2, i_flppl_id_2 INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_ppd_double', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_ppd_double';
set countRows = 0;
set i_pcpd_id = 0;

SET logNote=CONCAT('Start (', in_flp_id, ', ', in_ppd_id, ', ', in_flp_id_2, ') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

select count(*) into countRows from ams_wad.ams_airport_ppd p where p.ppd_id = in_ppd_id;

IF countRows > 0 THEN

	INSERT INTO ams_ac.ams_al_flight_pcpd
		(ppd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	select 
		p.ppd_id, flp.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 1 as sequence, 
		p.pax, p.pax_class_id, p.ppd_type_id, p.payload_kg, p.max_ticket_price,
        concat(p.note, ', #1 of 2 transfer flights')
		from ams_wad.ams_airport_ppd p 
		join ams_ac.ams_al_flight_plan flp on 1=1 
	where flp.flp_id = in_flp_id and p.ppd_id = in_ppd_id;
    
    SET i_pcpd_id = LAST_INSERT_ID();
    
	INSERT INTO ams_ac.ams_al_flight_pcpd
		(ppd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	select 
		p.ppd_id, flp.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 2 as sequence, 
		p.pax, p.pax_class_id, p.ppd_type_id, p.payload_kg, p.max_ticket_price,
        concat(p.note, ', #2 of 2 transfer flights')
		from ams_wad.ams_airport_ppd p 
		join ams_ac.ams_al_flight_plan flp on 1=1 
	where flp.flp_id = in_flp_id_2 and p.ppd_id = in_ppd_id;
    
    SET i_pcpd_id_2 = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_pcpd_id: ', i_pcpd_id, ', i_pcpd_id_2: ', i_pcpd_id_2 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
    select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.pax*
		 (CASE
		 when pc.pax_class_id=1 then flpns.price_e
		 when pc.pax_class_id=2 then flpns.price_b
		 else flpns.price_f
		 end)) as price
	from ams_al_flight_pcpd pc
		join ams_ac.ams_al_flight_plan flp on 1=1 and flp.flp_id = in_flp_id 
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where pc.pcpd_id = i_pcpd_id;
    
    SET i_flppl_id_2 = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_flppl_id: ', i_flppl_id );
    -- call ams_wad.log_sp(spname, stTime, logNote);
    
     INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
    select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.pax*
		 (CASE
		 when pc.pax_class_id=1 then flpns.price_e
		 when pc.pax_class_id=2 then flpns.price_b
		 else flpns.price_f
		 end)) as price
	from ams_al_flight_pcpd pc
		join ams_ac.ams_al_flight_plan flp on 1=1 and flp.flp_id = in_flp_id_2 
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where pc.pcpd_id = i_pcpd_id_2;
    
    SET i_flppl_id_2 = LAST_INSERT_ID();
    SET logNote=CONCAT('Inserted i_flppl_id_2: ', i_flppl_id_2 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    COMMIT;
    
    DELETE FROM ams_wad.ams_airport_ppd_for_flp where ppd_id = in_ppd_id;
    DELETE FROM ams_wad.ams_airport_ppd where ppd_id = in_ppd_id;
    COMMIT;
	SET logNote=CONCAT(' deleted from ams_airport_ppd ppd_id: ', in_ppd_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id);
	call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id_2);
    
END IF;


SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_ppd_quadruple` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_ppd_quadruple`(IN in_flp_id INT, IN in_ppd_id INT, IN in_flp_id_2 INT, IN in_flp_id_3 INT, IN in_flp_id_4 INT)
BEGIN


DECLARE i_pcpd_id, i_flppl_id INT;
DECLARE i_pcpd_id_2, i_flppl_id_2 INT;
DECLARE i_pcpd_id_3, i_flppl_id_3 INT;
DECLARE i_pcpd_id_4, i_flppl_id_4 INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_ppd_quadruple', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_ppd_quadruple';
set countRows = 0;
set i_pcpd_id = 0;

SET logNote=CONCAT('Start->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

select count(*) into countRows from ams_wad.ams_airport_ppd p where p.ppd_id = in_ppd_id FOR SHARE;

IF countRows > 0 THEN

	INSERT INTO ams_ac.ams_al_flight_pcpd
		(ppd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	(select 
		p.ppd_id, flp.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 1 as sequence, 
		p.pax, p.pax_class_id, p.ppd_type_id, p.payload_kg, p.max_ticket_price,
        concat(p.note, ', #1 of 4 transfer flights')
		from ams_wad.ams_airport_ppd p 
		join ams_ac.ams_al_flight_plan flp on 1=1 
	where flp.flp_id = in_flp_id and p.ppd_id = in_ppd_id FOR SHARE);
    
    SET i_pcpd_id = LAST_INSERT_ID();
    
	INSERT INTO ams_ac.ams_al_flight_pcpd
		(ppd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	(select 
		p.ppd_id, flp.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 2 as sequence, 
		p.pax, p.pax_class_id, p.ppd_type_id, p.payload_kg, p.max_ticket_price,
        concat(p.note, ', #2 of 4 transfer flights')
		from ams_wad.ams_airport_ppd p 
		join ams_ac.ams_al_flight_plan flp on 1=1 
	where flp.flp_id = in_flp_id_2 and p.ppd_id = in_ppd_id FOR SHARE);
    
    SET i_pcpd_id_2 = LAST_INSERT_ID();
    
    INSERT INTO ams_ac.ams_al_flight_pcpd
		(ppd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	(select 
		p.ppd_id, flp.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 3 as sequence, 
		p.pax, p.pax_class_id, p.ppd_type_id, p.payload_kg, p.max_ticket_price,
        concat(p.note, ', #3 of 4 transfer flights')
		from ams_wad.ams_airport_ppd p 
		join ams_ac.ams_al_flight_plan flp on 1=1 
	where flp.flp_id = in_flp_id_3 and p.ppd_id = in_ppd_id FOR SHARE);
    
    SET i_pcpd_id_3 = LAST_INSERT_ID();
    
    INSERT INTO ams_ac.ams_al_flight_pcpd
		(ppd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	(select 
		p.ppd_id, flp.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 4 as sequence, 
		p.pax, p.pax_class_id, p.ppd_type_id, p.payload_kg, p.max_ticket_price,
        concat(p.note, ', #4 of 4 transfer flights')
		from ams_wad.ams_airport_ppd p 
		join ams_ac.ams_al_flight_plan flp on 1=1 
	where flp.flp_id = in_flp_id_4 and p.ppd_id = in_ppd_id FOR SHARE);
    
    SET i_pcpd_id_4 = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_pcpd_id: ', i_pcpd_id, ', i_pcpd_id_2: ', i_pcpd_id_2, ', i_pcpd_id_4: ', i_pcpd_id_4 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
    (select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.pax*
		 (CASE
		 when pc.pax_class_id=1 then flpns.price_e
		 when pc.pax_class_id=2 then flpns.price_b
		 else flpns.price_f
		 end)) as price
	from ams_al_flight_pcpd pc
		join ams_ac.ams_al_flight_plan flp on 1=1 and flp.flp_id = in_flp_id 
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where pc.pcpd_id = i_pcpd_id FOR SHARE);
    
    SET i_flppl_id = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_flppl_id: ', i_flppl_id );
    -- call ams_wad.log_sp(spname, stTime, logNote);
    
     INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
    (select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.pax*
		 (CASE
		 when pc.pax_class_id=1 then flpns.price_e
		 when pc.pax_class_id=2 then flpns.price_b
		 else flpns.price_f
		 end)) as price
	from ams_al_flight_pcpd pc
		join ams_ac.ams_al_flight_plan flp on 1=1 and flp.flp_id = in_flp_id_2 
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where pc.pcpd_id = i_pcpd_id_2 FOR SHARE);
    
    SET i_flppl_id_2 = LAST_INSERT_ID();
    SET logNote=CONCAT('Inserted i_flppl_id_2: ', i_flppl_id_2 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
    (select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.pax*
		 (CASE
		 when pc.pax_class_id=1 then flpns.price_e
		 when pc.pax_class_id=2 then flpns.price_b
		 else flpns.price_f
		 end)) as price
	from ams_al_flight_pcpd pc
		join ams_ac.ams_al_flight_plan flp on 1=1 and flp.flp_id = in_flp_id_2 
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where pc.pcpd_id = i_pcpd_id_3 FOR SHARE);
    
    SET i_flppl_id_3 = LAST_INSERT_ID();
    SET logNote=CONCAT('Inserted i_flppl_id_3: ', i_flppl_id_3 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
     INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
    (select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.pax*
		 (CASE
		 when pc.pax_class_id=1 then flpns.price_e
		 when pc.pax_class_id=2 then flpns.price_b
		 else flpns.price_f
		 end)) as price
	from ams_al_flight_pcpd pc
		join ams_ac.ams_al_flight_plan flp on 1=1 and flp.flp_id = in_flp_id_2 
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where pc.pcpd_id = i_pcpd_id_4 FOR SHARE);
    
    SET i_flppl_id_4 = LAST_INSERT_ID();
    SET logNote=CONCAT('Inserted i_flppl_id_4: ', i_flppl_id_4 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    COMMIT;
    
    DELETE FROM ams_wad.ams_airport_ppd_for_flp where ppd_id = in_ppd_id;
    DELETE FROM ams_wad.ams_airport_ppd where ppd_id = in_ppd_id;
	SET logNote=CONCAT(' deleted from ams_airport_ppd ppd_id: ', in_ppd_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
	COMMIT;

	call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id);
	call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id_2);
	call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id_3);
	call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id_4);
	
END IF;

SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_ppd_single` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_ppd_single`(IN in_flp_id INT, IN in_ppd_id INT)
BEGIN


DECLARE i_pcpd_id, i_flppl_id INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_ppd_single', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_ppd_single';
set countRows = 0;
set i_pcpd_id = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);

select count(*) into countRows from ams_wad.ams_airport_ppd p where p.ppd_id = in_ppd_id;

IF countRows>0 THEN

	INSERT INTO ams_ac.ams_al_flight_pcpd
		(ppd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	select 
		p.ppd_id, p.dest_id, p.dep_ap_id, flp.a_ap_id, flp.al_id, p.sequence, 
		p.pax, p.pax_class_id, p.ppd_type_id, p.payload_kg, p.max_ticket_price,
        p.note
		from ams_wad.ams_airport_ppd p 
		left join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
		join ams_ac.ams_al_flight_plan flp on flp.dest_id = p.dest_id and flp.flp_id = in_flp_id 
	where p.ppd_id = in_ppd_id;
    
    SET i_pcpd_id = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_pcpd_id: ', i_pcpd_id );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
    select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.pax*
		 (CASE
		 when pc.pax_class_id=1 then flpns.price_e
		 when pc.pax_class_id=2 then flpns.price_b
		 else flpns.price_f
		 end)) as price
	from ams_al_flight_pcpd pc
		join ams_ac.ams_al_flight_plan flp on flp.dest_id = pc.dest_id and flp.flp_id = in_flp_id 
		left join ams_wad.cfg_airport_destination d on d.dest_id = pc.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where pc.pcpd_id = i_pcpd_id;
    
    SET i_flppl_id = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_flppl_id: ', i_flppl_id );
	call ams_wad.log_sp(spname, stTime, logNote);
    COMMIT;
    
    DELETE FROM ams_wad.ams_airport_ppd_for_flp where ppd_id = in_ppd_id;
    DELETE FROM ams_wad.ams_airport_ppd where ppd_id = in_ppd_id;
    COMMIT;
    
	SET logNote=CONCAT(' deleted from ams_airport_ppd ppd_id: ', in_ppd_id );
	call ams_wad.log_sp(spname, stTime, logNote);
	call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id);
END IF;


SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_ppd_triple` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_ppd_triple`(IN in_flp_id INT, IN in_ppd_id INT, IN in_flp_id_2 INT, IN in_flp_id_3 INT)
BEGIN


DECLARE i_pcpd_id, i_flppl_id INT;
DECLARE i_pcpd_id_2, i_flppl_id_2 INT;
DECLARE i_pcpd_id_3, i_flppl_id_3 INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add_ppd_triple', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add_ppd_triple';
set countRows = 0;
set i_pcpd_id = 0;

SET logNote=CONCAT('Start (', in_flp_id, ', ', in_ppd_id, ', ', in_flp_id_2, ', ', in_flp_id_3, ') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

select count(*) into countRows from ams_wad.ams_airport_ppd p where p.ppd_id = in_ppd_id;

IF countRows > 0 THEN

	INSERT INTO ams_ac.ams_al_flight_pcpd
		(ppd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	select 
		p.ppd_id, flp.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 1 as sequence, 
		p.pax, p.pax_class_id, p.ppd_type_id, p.payload_kg, p.max_ticket_price,
        concat(p.note, ', #1 of 3 transfer flights')
		from ams_wad.ams_airport_ppd p 
		join ams_ac.ams_al_flight_plan flp on 1=1 
	where flp.flp_id = in_flp_id and p.ppd_id = in_ppd_id;
    
    SET i_pcpd_id = LAST_INSERT_ID();
    
	INSERT INTO ams_ac.ams_al_flight_pcpd
		(ppd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	select 
		p.ppd_id, flp.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 2 as sequence, 
		p.pax, p.pax_class_id, p.ppd_type_id, p.payload_kg, p.max_ticket_price,
        concat(p.note, ', #2 of 3 transfer flights')
		from ams_wad.ams_airport_ppd p 
		join ams_ac.ams_al_flight_plan flp on 1=1 
	where flp.flp_id = in_flp_id_2 and p.ppd_id = in_ppd_id;
    
    SET i_pcpd_id_2 = LAST_INSERT_ID();
    
    INSERT INTO ams_ac.ams_al_flight_pcpd
		(ppd_id, dest_id, d_ap_id, a_ap_id, al_id, sequence, 
		pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
	select 
		p.ppd_id, flp.dest_id, flp.d_ap_id, flp.a_ap_id, flp.al_id, 3 as sequence, 
		p.pax, p.pax_class_id, p.ppd_type_id, p.payload_kg, p.max_ticket_price,
        concat(p.note, ', #3 of 3 transfer flights')
		from ams_wad.ams_airport_ppd p 
		join ams_ac.ams_al_flight_plan flp on 1=1 
	where flp.flp_id = in_flp_id_3 and p.ppd_id = in_ppd_id;
    
    SET i_pcpd_id_3 = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_pcpd_id: ', i_pcpd_id, ', i_pcpd_id_2: ', i_pcpd_id_2, ', i_pcpd_id_3: ', i_pcpd_id_3 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
    select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.pax*
		 (CASE
		 when pc.pax_class_id=1 then flpns.price_e
		 when pc.pax_class_id=2 then flpns.price_b
		 else flpns.price_f
		 end)) as price
	from ams_al_flight_pcpd pc
		join ams_ac.ams_al_flight_plan flp on 1=1 and flp.flp_id = in_flp_id 
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where pc.pcpd_id = i_pcpd_id;
    
    SET i_flppl_id = LAST_INSERT_ID();
    
    SET logNote=CONCAT('Inserted i_flppl_id: ', i_flppl_id );
    -- call ams_wad.log_sp(spname, stTime, logNote);
    
     INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
    select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.pax*
		 (CASE
		 when pc.pax_class_id=1 then flpns.price_e
		 when pc.pax_class_id=2 then flpns.price_b
		 else flpns.price_f
		 end)) as price
	from ams_al_flight_pcpd pc
		join ams_ac.ams_al_flight_plan flp on 1=1 and flp.flp_id = in_flp_id_2 
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where pc.pcpd_id = i_pcpd_id_2;
    
    SET i_flppl_id_2 = LAST_INSERT_ID();
    SET logNote=CONCAT('Inserted i_flppl_id_2: ', i_flppl_id_2 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_al_flight_plan_payload
		(flp_id, pcpd_id, pax, payload_kg, pax_class_id, ppd_type_id, note, price)
    select 
		flp.flp_id, pc.pcpd_id, pc.pax, pc.payload_kg, pc.pax_class_id, pc.ppd_type_id, pc.note,
		(pc.pax*
		 (CASE
		 when pc.pax_class_id=1 then flpns.price_e
		 when pc.pax_class_id=2 then flpns.price_b
		 else flpns.price_f
		 end)) as price
	from ams_al_flight_pcpd pc
		join ams_ac.ams_al_flight_plan flp on 1=1 and flp.flp_id = in_flp_id_2 
		left join ams_wad.cfg_airport_destination d on d.dest_id = flp.dest_id
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where pc.pcpd_id = i_pcpd_id_3;
    
    SET i_flppl_id_3 = LAST_INSERT_ID();
    SET logNote=CONCAT('Inserted i_flppl_id_3: ', i_flppl_id_3 );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    COMMIT;
    
    DELETE FROM ams_wad.ams_airport_ppd_for_flp where ppd_id = in_ppd_id;
    DELETE FROM ams_wad.ams_airport_ppd where ppd_id = in_ppd_id;
	SET logNote=CONCAT(' deleted from ams_airport_ppd ppd_id: ', in_ppd_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    COMMIT;
    call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id);
	call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id_2);
	call ams_ac.sp_ams_al_flight_plan_update_id(in_flp_id_3);
		
END IF;


SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_add_v1` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_add_v1`()
BEGIN

DECLARE i_flp_id, i_ppd_id, i_cpd_id, i_flp_status_id INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_flp CURSOR FOR SELECT flp_id   
					FROM ams_ac.ams_al_flight_plan flp
					where flp.flp_status_id = 1
					order by flp.payload_jobs, dtime, atime limit 10;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_add', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_add';
set i_flp_status_id = 2; -- Flight is sold out
set countRows = 0;
set i_flp_id = 0;
set i_ppd_id = 0;
set i_cpd_id = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);

call ams_ac.sp_ams_al_flight_plan_update();

OPEN cursor_flp;

read_loop: LOOP
	
    FETCH cursor_flp INTO i_flp_id;
	
    SET logNote=CONCAT('FETCH i_flp_id: ',i_flp_id, ', done:', done,  '...');
	call ams_wad.log_sp(spname, stTime, logNote);
    
	IF done THEN
		SET logNote=CONCAT('No i_flp_id in queue found ', 'done' );
		 call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
    
    call  ams_ac.sp_al_flight_plan_payload_add_pcpd_quadruple(i_flp_id);
    call ams_ac.sp_ams_al_flight_plan_update();
    
	set countRows = 0;
	select count(flp.flp_id) into countRows
	from ams_ac.ams_al_flight_plan_payload_pct_full flp 
	where flp.flp_status_id = 1 and 
		flp.flp_id = i_flp_id and 
        (flp.pax_pct_full < 95 or flp.payload_kg_pct_full < 95);
        
	IF countRows > 0 THEN
		call ams_ac.sp_al_flight_plan_payload_add_pcpd_triple(i_flp_id);
        call ams_ac.sp_ams_al_flight_plan_update();
	END IF;
    
    set countRows = 0;
	select count(flp.flp_id) into countRows
	from ams_ac.ams_al_flight_plan_payload_pct_full flp 
	where flp.flp_status_id = 1 and 
		flp.flp_id = i_flp_id and 
        (flp.pax_pct_full < 95 or flp.payload_kg_pct_full < 95);
        
	IF countRows > 0 THEN
		call ams_ac.sp_al_flight_plan_payload_add_pcpd_double(i_flp_id);
        call ams_ac.sp_ams_al_flight_plan_update();
	END IF;
    
    set countRows = 0;
	select count(flp.flp_id) into countRows
	from ams_ac.ams_al_flight_plan_payload_pct_full flp 
	where flp.flp_status_id = 1 and 
		flp.flp_id = i_flp_id and 
        (flp.pax_pct_full < 95 or flp.payload_kg_pct_full < 95);
        
	IF countRows > 0 THEN
		call ams_ac.sp_al_flight_plan_payload_add_pcpd_single(i_flp_id);
	else
		update ams_ac.ams_al_flight_plan flp 
		set flp.flp_status_id = 2
		where flp.flp_id = i_flp_id;
		commit;
	END IF;
    
	
END LOOP;
 
CLOSE cursor_flp;

call ams_ac.sp_al_flight_plan_payload_add_n();

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_ppd_downgrade_double` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_ppd_downgrade_double`(IN in_flp_id INT)
BEGIN

DECLARE i_ppd_id INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_ppd_downgrade_double', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_ppd_downgrade_double';
set countRows = 0;
set i_ppd_id = 0;

SET logNote=CONCAT('Start (in_flp_id:', in_flp_id, ') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

    
IF in_flp_id > 0 THEN
	
	set countRows = 0;
	select count(p.ppd_id) INTO countRows
    from ams_ac.ams_al_flight_plan_payload_pct_full flp 
	join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
    join ams_ac.v_ams_ac ac on ac.acId = flp.ac_id
    join ams_ac.ams_al_flight_plan_payload_pct_full flp2 on flp2.flp_id <> flp.flp_id and flp2.d_ap_id = flp.a_ap_id
    join ams_al.ams_al_flp_number_schedule flpns2 on flpns2.flpns_id = flp2.flpns_id
    join ams_wad.ams_airport_ppd p on p.dep_ap_id = flp.d_ap_id  
    join ams_wad.cfg_airport_destination pd on pd.dest_id = p.dest_id and pd.arr_ap_id = flp2.a_ap_id
	where flp.flp_id = in_flp_id and TIMESTAMPDIFF(MINUTE,  flp.atime, flp2.dtime) > ac.opTimeMin
    and (
		(p.pax_class_id = 2 and p.pax <= flp.remain_pax_e and p.pax <= flp2.remain_pax_e and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e)))
		or (p.pax_class_id = 3 and p.pax <= flp.remain_pax_b and p.pax <= flp2.remain_pax_e and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e)))
	);
    

	if countRows > 0 THEN
		SET logNote=CONCAT('Found: ',countRows, ' ppd to downgrade...');
		call ams_wad.log_sp(spname, stTime, logNote);
        
		select p.ppd_id INTO i_ppd_id
		from ams_ac.ams_al_flight_plan_payload_pct_full flp 
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
		join ams_ac.v_ams_ac ac on ac.acId = flp.ac_id
		join ams_ac.ams_al_flight_plan_payload_pct_full flp2 on flp2.flp_id <> flp.flp_id and flp2.d_ap_id = flp.a_ap_id
		join ams_al.ams_al_flp_number_schedule flpns2 on flpns2.flpns_id = flp2.flpns_id
		join ams_wad.ams_airport_ppd p on p.dep_ap_id = flp.d_ap_id  
		join ams_wad.cfg_airport_destination pd on pd.dest_id = p.dest_id and pd.arr_ap_id = flp2.a_ap_id
		where flp.flp_id = in_flp_id and TIMESTAMPDIFF(MINUTE,  flp.atime, flp2.dtime) > ac.opTimeMin
		and (
			(p.pax_class_id = 2 and p.pax <= flp.remain_pax_e and p.pax <= flp2.remain_pax_e and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e)))
			or (p.pax_class_id = 3 and p.pax <= flp.remain_pax_b and p.pax <= flp2.remain_pax_e and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e)))
		)
		order by p.adate limit 1;
		
		-- Add single Pax record to payload
		IF i_ppd_id > 0 THEN
			SET logNote=CONCAT('Downgrade i_ppd_id: ',i_ppd_id, ' ');
			call ams_wad.log_sp(spname, stTime, logNote);
			UPDATE ams_wad.ams_airport_ppd p
            set p.pax_class_id = 1, p.note = concat(p.note, ' Downgrade to E.')
            where p.ppd_id = i_ppd_id;
            commit;
		END IF;
	END IF;
END IF;
    
  

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_ppd_downgrade_quadruple` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_ppd_downgrade_quadruple`(IN in_flp_id INT)
BEGIN

DECLARE i_ppd_id INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_ppd_downgrade_quadruple', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_ppd_downgrade_quadruple';
set countRows = 0;
set i_ppd_id = 0;

SET logNote=CONCAT('Start (in_flp_id:', in_flp_id, ') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

    
IF in_flp_id > 0 THEN
	
	set countRows = 0;
	select count(p.ppd_id) INTO countRows
     from ams_ac.ams_al_flp_transfer_ppd pcpd
	 join ams_wad.ams_airport_ppd p on p.ppd_id = pcpd.ppdId 
	 join ams_al_flight_plan_payload_pct_full flp on flp.flp_id = pcpd.flpId
	 JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
	 join ams_al_flight_plan_payload_pct_full flp2 on flp2.flp_id = pcpd.flpId2
	 JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
	 join ams_al_flight_plan_payload_pct_full flp3 on flp3.flp_id = pcpd.flpId3
	 JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
	 join ams_al_flight_plan_payload_pct_full flp4 on flp4.flp_id = pcpd.flpId3
	 JOIN ams_al.ams_al_flp_number_schedule flpns4 ON ((flpns4.flpns_id = flp4.flpns_id))
	 where  (
		(p.pax_class_id = 2 and p.pax <= flp.remain_pax_e and p.pax <= flp2.remain_pax_e and p.pax <= flp3.remain_pax_e and p.pax <= flp4.remain_pax_e and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e)+(p.pax*flpns3.price_e)+(p.pax*flpns4.price_e)))
		or (p.pax_class_id = 3 and p.pax <= flp.remain_pax_b and p.pax <= flp2.remain_pax_e and p.pax <= flp3.remain_pax_e and p.pax <= flp4.remain_pax_e and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e)+(p.pax*flpns3.price_e)+(p.pax*flpns4.price_e)))
		);
    

	if countRows > 0 THEN
		SET logNote=CONCAT('Found: ',countRows, ' ppd to downgrade...');
		call ams_wad.log_sp(spname, stTime, logNote);
        
		select p.ppd_id INTO i_ppd_id
		from ams_ac.ams_al_flp_transfer_ppd pcpd
	 join ams_wad.ams_airport_ppd p on p.ppd_id = pcpd.ppdId 
	 join ams_al_flight_plan_payload_pct_full flp on flp.flp_id = pcpd.flpId
	 JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
	 join ams_al_flight_plan_payload_pct_full flp2 on flp2.flp_id = pcpd.flpId2
	 JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
	 join ams_al_flight_plan_payload_pct_full flp3 on flp3.flp_id = pcpd.flpId3
	 JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
	 join ams_al_flight_plan_payload_pct_full flp4 on flp4.flp_id = pcpd.flpId3
	 JOIN ams_al.ams_al_flp_number_schedule flpns4 ON ((flpns4.flpns_id = flp4.flpns_id))
	 where (
			(p.pax_class_id = 2 and p.pax <= flp.remain_pax_e and p.pax <= flp2.remain_pax_e and p.pax <= flp3.remain_pax_e and p.pax <= flp4.remain_pax_e and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e)+(p.pax*flpns3.price_e)+(p.pax*flpns4.price_e)))
			or (p.pax_class_id = 3 and p.pax <= flp.remain_pax_b and p.pax <= flp2.remain_pax_e and p.pax <= flp3.remain_pax_e and p.pax <= flp4.remain_pax_e and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e)+(p.pax*flpns3.price_e)+(p.pax*flpns4.price_e)))
		)
		order by p.adate limit 1;
		
		-- Add single Pax record to payload
		IF i_ppd_id > 0 THEN
			SET logNote=CONCAT('Downgrade i_ppd_id: ',i_ppd_id, ' ');
			call ams_wad.log_sp(spname, stTime, logNote);
			UPDATE ams_wad.ams_airport_ppd p
            set p.pax_class_id = 1, p.note = concat(p.note, ' Downgraded')
            where p.ppd_id = i_ppd_id;
            commit;
		END IF;
	END IF;
END IF;
    
  

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_ppd_downgrade_single` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_ppd_downgrade_single`(IN in_flp_id INT)
BEGIN

DECLARE i_ppd_id INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_ppd_downgrade_single', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_ppd_downgrade_single';
set countRows = 0;
set i_ppd_id = 0;

SET logNote=CONCAT('Start (in_flp_id:', in_flp_id, ') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

    
IF in_flp_id > 0 THEN
	
	set countRows = 0;
	select count(p.ppd_id) INTO countRows
	from ams_wad.ams_airport_ppd p
	join ams_ac.ams_al_flight_plan flp on flp.dest_id = p.dest_id 
	join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
	where flp.flp_id = in_flp_id 
	and (
		(p.pax_class_id = 2 and p.pax <= flp.remain_pax_e and p.max_ticket_price >= (p.pax*flpns.price_e))
		or (p.pax_class_id = 3 and p.pax <= flp.remain_pax_e and p.max_ticket_price >= (p.pax*flpns.price_e))
	);
    

	if countRows > 0 THEN
		SET logNote=CONCAT('Found: ',countRows, ' ppd to downgrade...');
		-- call ams_wad.log_sp(spname, stTime, logNote);
        
		select p.ppd_id INTO i_ppd_id
		from ams_wad.ams_airport_ppd p
		join ams_ac.ams_al_flight_plan_payload_pct_full flp on flp.dest_id = p.dest_id 
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
		where flp.flp_id = in_flp_id 
		and (
			(p.pax_class_id = 2 and p.pax <= flp.remain_pax_e and p.max_ticket_price >= (p.pax*flpns.price_e))
			or (p.pax_class_id = 3 and p.pax <= flp.remain_pax_e and p.max_ticket_price >= (p.pax*flpns.price_e))
		)
		order by p.adate limit 1;
		
		-- Add single Pax record to payload
		IF i_ppd_id > 0 THEN
			SET logNote=CONCAT('Downgrade i_ppd_id: ',i_ppd_id, ' Single');
			call ams_wad.log_sp(spname, stTime, logNote);
			UPDATE ams_wad.ams_airport_ppd p
            set p.pax_class_id = 1, p.note = concat(p.note, ' Downgraded')
            where p.ppd_id = i_ppd_id;
            commit;
		END IF;
	END IF;
END IF;
    
  

SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_payload_ppd_downgrade_triple` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_payload_ppd_downgrade_triple`(IN in_flp_id INT)
BEGIN

DECLARE i_ppd_id INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_payload_ppd_downgrade_triple', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_payload_ppd_downgrade_triple';
set countRows = 0;
set i_ppd_id = 0;

SET logNote=CONCAT('Start (in_flp_id:', in_flp_id, ') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

    
IF in_flp_id > 0 THEN
	
	set countRows = 0;
	select count(p.ppd_id) INTO countRows
    from ams_ac.ams_al_flight_plan_payload_pct_full flp 
	join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
    join ams_ac.v_ams_ac ac on ac.acId = flp.ac_id
    join ams_ac.ams_al_flight_plan_payload_pct_full flp2 on flp2.flp_id <> flp.flp_id and flp2.d_ap_id = flp.a_ap_id and flp2.a_ap_id <> flp.d_ap_id
    join ams_al.ams_al_flp_number_schedule flpns2 on flpns2.flpns_id = flp2.flpns_id
    join ams_ac.ams_al_flight_plan_payload_pct_full flp3 on flp3.flp_id <> flp.flp_id and flp3.flp_id <> flp2.flp_id and flp3.d_ap_id = flp2.a_ap_id and flp3.a_ap_id <> flp2.d_ap_id
    join ams_al.ams_al_flp_number_schedule flpns3 on flpns3.flpns_id = flp3.flpns_id
    join ams_wad.ams_airport_ppd p on p.dep_ap_id = flp.d_ap_id  
    join ams_wad.cfg_airport_destination pd on pd.dest_id = p.dest_id and pd.arr_ap_id = flp3.a_ap_id
	where flp.flp_id = in_flp_id and TIMESTAMPDIFF(MINUTE,  flp.atime, flp2.dtime) > ac.opTimeMin
    and TIMESTAMPDIFF(MINUTE,  flp2.atime, flp3.dtime) > ac.opTimeMin
    and (
		(p.pax_class_id = 2 and p.pax <= flp.remain_pax_e and p.pax <= flp2.remain_pax_e and p.pax <= flp3.remain_pax_e  and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e)+(p.pax*flpns3.price_e)))
		or (p.pax_class_id = 3 and p.pax <= flp.remain_pax_b and p.pax <= flp2.remain_pax_e and p.pax <= flp3.remain_pax_e and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e)+(p.pax*flpns3.price_e)))
	);
    

	if countRows > 0 THEN
		SET logNote=CONCAT('Found: ',countRows, ' ppd to downgrade...');
		call ams_wad.log_sp(spname, stTime, logNote);
        
		select p.ppd_id INTO i_ppd_id
		from ams_ac.ams_al_flight_plan_payload_pct_full flp 
		join ams_al.ams_al_flp_number_schedule flpns on flpns.flpns_id = flp.flpns_id
		join ams_ac.v_ams_ac ac on ac.acId = flp.ac_id
		join ams_ac.ams_al_flight_plan_payload_pct_full flp2 on flp2.flp_id <> flp.flp_id and flp2.d_ap_id = flp.a_ap_id and flp2.a_ap_id <> flp.d_ap_id
		join ams_al.ams_al_flp_number_schedule flpns2 on flpns2.flpns_id = flp2.flpns_id
		join ams_ac.ams_al_flight_plan_payload_pct_full flp3 on flp3.flp_id <> flp.flp_id and flp3.flp_id <> flp2.flp_id and flp3.d_ap_id = flp2.a_ap_id and flp3.a_ap_id <> flp2.d_ap_id
		join ams_al.ams_al_flp_number_schedule flpns3 on flpns3.flpns_id = flp3.flpns_id
		join ams_wad.ams_airport_ppd p on p.dep_ap_id = flp.d_ap_id  
		join ams_wad.cfg_airport_destination pd on pd.dest_id = p.dest_id and pd.arr_ap_id = flp3.a_ap_id
		where flp.flp_id = in_flp_id and TIMESTAMPDIFF(MINUTE,  flp.atime, flp2.dtime) > ac.opTimeMin
		and TIMESTAMPDIFF(MINUTE,  flp2.atime, flp3.dtime) > ac.opTimeMin
		and (
			(p.pax_class_id = 2 and p.pax <= flp.remain_pax_e and p.pax <= flp2.remain_pax_e and p.pax <= flp3.remain_pax_e  and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e)+(p.pax*flpns3.price_e)))
			or (p.pax_class_id = 3 and p.pax <= flp.remain_pax_b and p.pax <= flp2.remain_pax_e and p.pax <= flp3.remain_pax_e and p.max_ticket_price >= ((p.pax*flpns.price_e)+(p.pax*flpns2.price_e)+(p.pax*flpns3.price_e)))
		)
		order by p.adate limit 1;
		
		-- Add single Pax record to payload
		IF i_ppd_id > 0 THEN
			SET logNote=CONCAT('Downgrade i_ppd_id: ',i_ppd_id, ' ');
			call ams_wad.log_sp(spname, stTime, logNote);
			UPDATE ams_wad.ams_airport_ppd p
            set p.pax_class_id = 1, p.note = concat(p.note, ' Downgraded')
            where p.ppd_id = i_ppd_id;
            commit;
		END IF;
	END IF;
END IF;
    
  

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_process` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_process`()
BEGIN

DECLARE i_flp_id, i_ac_id, i_row, i_status_id INT;
DECLARE i_stat_cansel, i_stat_add_to_flq INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_flp CURSOR FOR SELECT flp.flp_id, flp.ac_id
						FROM ams_ac.ams_al_flight_plan flp
						join (SELECT param_value FROM ams_al.cfg_callc_param where param_name='flight_plan_add_days_in_advance' ) days on 1=1
                        where flp.flp_status_id <> 10 and UNIX_TIMESTAMP(DATE_ADD(flp.dtime, INTERVAL -1 DAY)) < UNIX_TIMESTAMP(current_timestamp())
                        order by dtime
                        limit 4;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_process', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_process';
SET i_stat_add_to_flq = 4;
SET i_stat_cansel = 10;
set countRows = 0;
set i_flp_id = 0;
set i_ac_id = 0;
set i_row = 0;

SET logNote=CONCAT('Start () ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_flp;

read_loop: LOOP
	
    FETCH cursor_flp INTO i_flp_id, i_ac_id;
	
	IF done THEN
		SET logNote=CONCAT('No i_flp_id in queue found ', 'done' );
		-- call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
    
    SET logNote=CONCAT(i_row, ' i_flp_id: ', i_flp_id, ' process...');
	-- call ams_wad.log_sp(spname, stTime, logNote);
      
    set countRows = 0;
     set i_status_id = 0;
     
    SELECT count(*), flp.flp_status_id INTO countRows, i_status_id
	FROM ams_ac.ams_al_flight_plan flp
    where flp.flp_id = i_flp_id and flp.flp_status_id <> i_stat_cansel FOR SHARE;
    
    if countRows > 0 then
		IF i_status_id = 1 then
			call ams_ac.sp_al_flight_plan_payload_add_d_flp_ppd_v01(i_flp_id);
            call ams_ac.sp_al_flight_plan_payload_add_d_flp_cpd_v01(i_flp_id);
        end if;
        
        set i_row = i_row+1;
        SET logNote=CONCAT( i_row, ' Start i_flp_id: ', i_flp_id, ', i_ac_id:', i_ac_id, ' ');
		-- call ams_wad.log_sp(spname, stTime, logNote);
		call ams_ac.sp_al_flight_plan_to_flight_queue(i_flp_id);
    else
		SET logNote=CONCAT(i_row, ' canseled i_flp_id: ', i_flp_id);
		call ams_wad.log_sp(spname, stTime, logNote);
	END IF;
    SET logNote=CONCAT(i_row, ' started i_flp_id: ', i_flp_id, ' ');
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
END LOOP;
 
CLOSE cursor_flp;
SET logNote=CONCAT('Added ', i_row, ' flights in FLQ. ');
call ams_wad.log_sp(spname, stTime, logNote);
SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_process_ac_check` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_process_ac_check`(IN in_flp_id INT)
BEGIN

DECLARE i_ac_status_operational, i_acat_planeStand INT;
DECLARE i_ac_id, i_stat_add_to_flq, i_stat_cansel INT;
DECLARE i_d_ap_id, i_st_time, is_home_ap, is_on_d_ap, is_in_mnt INT;
DECLARE d_remain_h_to_mnt DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_process_ac_check', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_process_ac_check';
set countRows = 0;
SET logNote=CONCAT('Start in_flp_id:', in_flp_id , ' ->' );
call ams_wad.log_sp(spname, stTime, logNote);
set d_remain_h_to_mnt = 0;
SET i_ac_status_operational = 2;
SET i_acat_planeStand = 6;
SET i_stat_add_to_flq = 4;
SET i_stat_cansel = 10;
-- Action Type maintenance Chack = 31

	SELECT flp.ac_id, 
		round(ac.flight_h_to_maintenance - (ac.flight_h_in_queue + flp.flight_h),2) as remain_h_to_mnt,
		if(ac.homeApId = flp.d_ap_id, 1, 0) is_home_ap,
		if(ac.currApId = flp.d_ap_id, 1, 0) is_on_d_ap, flp.d_ap_id,
        if((ac.acActionTypeId = 31 and ac.last_flq_atime > flp.dtime), 1, 0) is_in_mnt 
	INTO i_ac_id, d_remain_h_to_mnt, is_home_ap, is_on_d_ap, i_d_ap_id, is_in_mnt
    FROM ams_ac.v_ams_ac ac
	join ams_ac.ams_al_flight_plan flp on flp.ac_id = ac.acId
	where flp.flp_id = in_flp_id;
    
    SET logNote=CONCAT('i_ac_id: ', i_ac_id, ', d_remain_h_to_mnt:', d_remain_h_to_mnt, ', is_home_ap:', is_home_ap  );
	call ams_wad.log_sp(spname, stTime, logNote);

	IF d_remain_h_to_mnt < 1 AND is_home_ap > 0 THEN
		-- start Maintenance and set i_ac_id to -1
        SET logNote=CONCAT('Starting Maintenance for ac_id: ', i_ac_id, ', d_remain_h_to_mnt:', d_remain_h_to_mnt, ', is_home_ap:', is_home_ap  );
        call ams_wad.log_sp(spname, stTime, logNote);
        call ams_ac.sp_al_flight_plan_process_ac_mnt(i_ac_id);
        SET i_ac_id = -1;
	END IF;
    
    IF is_in_mnt > 0 THEN
		SET logNote=CONCAT('Aircraft is in Maintenance Check ac_id: ', i_ac_id, ', is_home_ap:', is_home_ap  );
        call ams_wad.log_sp(spname, stTime, logNote);
		SET i_ac_id = -1;
    END IF;
    
    IF i_ac_id = -1 THEN
     -- Get new AC from available aircrafts
		SELECT a.acId, if(flp.d_ap_id = ifnull(a.last_flq_a_ap_id, a.currApId),1, 0) as is_on_d_ap, flp.d_ap_id
        INTO i_ac_id, is_on_d_ap, i_d_ap_id
		FROM ams_ac.ams_al_flight_plan flp
		join ams_ac.v_ams_ac ac on ac.acId = flp.ac_id
		join ams_ac.v_ams_ac a on a.ownerAlId = ac.ownerAlId
		where flp.flp_id = 174  and a.acId <> ac.acId 
			and ac.paxE <= a.paxE and ac.paxE <= a.paxE and ac.paxb <= a.paxb and ac.paxf <= a.paxf and ac.cargoKg <= a.cargoKg 
			and round(a.flight_h_to_maintenance - (a.flight_h_in_queue + flp.flight_h),2) > 1 
			and a.last_flq_atime <= flp.dtime and a.forSheduleFlights=0 and a.last_flq_atime <= flp.dtime 
		order by if(flp.d_ap_id = ifnull(a.last_flq_a_ap_id, a.currApId),1, 0) desc, a.last_flq_atime, a.flight_h_to_maintenance limit 1;
        
        SET logNote=CONCAT('i_ac_id: ', i_ac_id, ', is_on_d_ap: ', is_on_d_ap );
		call ams_wad.log_sp(spname, stTime, logNote);
        
        IF i_ac_id > 0 THEN
			update ams_ac.ams_al_flight_plan set ac_id = i_ac_id where flp_id =  in_flp_id;
			commit;
		END IF;
        
    END IF;
	/*
	IF i_ac_id > 0 and is_on_d_ap = 0 THEN
		SET logNote=CONCAT('Start transfer flight for ac: ', i_ac_id, ' to ap: ',  i_d_ap_id);
		call ams_wad.log_sp(spname, stTime, logNote);
        call ams_ac.sp_al_flight_plan_process_ac_transfer(i_ac_id, i_d_ap_id);
	END IF;	
	*/	
			
	/*
	-- Cancel Flight
		update ams_ac.ams_al_flight_plan set flp_status_id = i_stat_cansel
		where flp_id =  in_flp_id;
		commit;
		*/
		

	SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_process_ac_mnt` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_process_ac_mnt`(IN in_ac_id INT)
BEGIN

DECLARE i_amt_id, i_st_time INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_process_ac_mnt', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_process_ac_mnt';
set countRows = 0;
SET logNote=CONCAT('Start in_ac_id:', in_ac_id , ' ->' );
call ams_wad.log_sp(spname, stTime, logNote);

	select ac.next_amt_id, (ac.last_flq_atime + ac.opTimeMin) 
	INTO i_amt_id, i_st_time
	FROM ams_ac.v_ams_ac ac where acId = in_ac_id;
    
    SET logNote=CONCAT('next_amt_id: ', next_amt_id );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	call ams_ac.sp_ac_mnt_plan_create(i_ac_id,i_amt_id, i_st_time);

    SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_process_ac_transfer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_process_ac_transfer`(IN in_ac_id INT, IN in_a_ap_id INT, out i_fl_id int)
BEGIN

DECLARE i_ac_status_operational, i_acat_planeStand, i_fl_type_transfer, i_distance_km INT;
-- DECLARE i_fl_id INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_process_ac_transfer', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_process_ac_transfer';
set countRows = 0;
SET logNote=CONCAT('Start (', in_ac_id ,', ', in_a_ap_id, ') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

SET i_distance_km = 0;
SET i_ac_status_operational = 2;
SET i_acat_planeStand = 6;
SET i_fl_type_transfer = 3;
    
    IF in_ac_id > 0 THEN 
    
		INSERT INTO ams_ac.ams_al_flight_queue ( 
				fl_type_id, fl_name, fl_description, al_id, ac_id, 
				d_ap_id, a_ap_id, distance_km, oil_l, flight_h, pax, payload_kg, dtime, atime)
		select i_fl_type_transfer as fl_type_id, 
			concat(al.iata, LPAD((ifnull(alc.fl_transfer, 0) +1),4,'0'),'TR') as fl_name, 
			CONCAT( IFNULL(dep.ap_iata, dep.ap_icao), ' to ',IFNULL(arr.ap_iata, arr.ap_icao), ' Transfer Flight ') as fl_description,
			ac.ownerAlId as al_id, 
			ac.acId as ac_id, 
			dep.ap_id as d_ap_id, 
			arr.ap_id as a_ap_id, 
			d.distance_km,
			ROUND((d.distance_km /100)* macr.fuel_consumption_lp100km,2) as oil_l,
            Round( (((d.distance_km/macr.cruise_speed_kmph)*60)+(1*ac.opTimeMin))/60,2) as flight_h,
			0 as pax, 0 as payload_kg,
			DATE_ADD( now(),INTERVAL p.st_to_ht_min MINUTE) as dtime,
			DATE_ADD(
				DATE_ADD( now(),INTERVAL p.st_to_ht_min MINUTE) , 
				INTERVAL Round(((d.distance_km/macr.cruise_speed_kmph)*60)+(1*ac.opTimeMin),0) MINUTE)  as atime
		from ams_ac.v_ams_ac ac 
			join ams_wad.cfg_airport_destination d on d.dep_ap_id = ac.currApId and d.arr_ap_id = in_a_ap_id
			left join ams_al.ams_airline_counters alc on alc.al_id = ac.ownerAlId
			join ams_al.ams_airline al on al.al_id = ac.ownerAlId
			join ams_wad.cfg_airport dep on dep.ap_id = ac.currApId
			join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
			join ams_ac.cfg_mac macr on macr.mac_id = ac.macId
			join ams_al.cfg_oil_price op on 1=1 and op.oil_price_id = 1 
			join ams_al.v_al_callc_param_h p on p.al_id = ac.ownerAlId
			join ams_al.ams_bank b on b.al_id = ac.ownerAlId
		where ac.acId = in_ac_id;
    
        
        SET i_fl_id = LAST_INSERT_ID();
        commit;
        
		SET logNote=CONCAT('Transfer flight i_fl_id: ', i_fl_id);
		call ams_wad.log_sp(spname, stTime, logNote);
        
         set countRows = 0;
        -- Update FL Transfer counter
        SELECT count(*) INTO countRows
		FROM ams_al.ams_airline_counters alc
		join ams_ac.ams_al_flight_queue flq on flq.al_id = alc.al_id
        where flq.fl_id = i_fl_id;
			
        IF countRows = 0 THEN
			SET logNote=CONCAT('inserted ams_airline_counters record ');
			INSERT INTO ams_al.ams_airline_counters (al_id, fl_transfer)
			SELECT al_id, 1 as fl_num from ams_ac.ams_al_flight_queue where fl_id = i_fl_id;
        ELSE
			SET logNote=CONCAT('Updated ams_airline_counters record ');
			update ams_al.ams_airline_counters alc
			join ams_ac.ams_al_flight_queue flq on flq.al_id = alc.al_id
			set alc.fl_transfer = (ifnull(alc.fl_transfer, 0) +1)
			where flq.fl_id = i_fl_id;
        END IF;
        COMMIT;
        CALL ams_ac.sp_ams_aircraft_curr_update_ac(in_ac_id);
        
	END IF;
    
    SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    select i_fl_id as fl_id from dual;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_to_flight_queue` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_to_flight_queue`(IN in_flp_id INT)
BEGIN

DECLARE i_ac_status_operational, i_acat_planeStand INT;
DECLARE i_fl_id, i_ac_id, i_al_id, i_stat_add_to_flq INT;
declare i_idx int;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg, '. in_flp_id:', in_flp_id);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_to_flight_queue', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_to_flight_queue';
set countRows = 0;
SET logNote=CONCAT('Start in_flp_id:', in_flp_id , ' ->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET i_ac_status_operational = 2;
SET i_acat_planeStand = 6;
SET i_stat_add_to_flq = 4;

	select count(*) INTO countRows from ams_ac.ams_al_flight_plan 
    where flp_id =  in_flp_id and flp_status_id <> i_stat_add_to_flq FOR SHARE;
	IF countRows > 0 THEN
			SET SQL_SAFE_UPDATES = 0;
			-- Update status to Adding to flight queu (4)
            set countRows = 0;
			SET i_idx = 0;
            /*
			SELECT count(*) INTO countRows
			FROM performance_schema.table_handles 
			WHERE OBJECT_SCHEMA='ams_ac' and OBJECT_NAME='ams_al_flight_plan'
			and (EXTERNAL_LOCK IS NOT NULL or INTERNAL_LOCK IS NOT NULL);
	
			WHILE countRows > 0 DO
				SELECT SLEEP(0.2);
				SET i_idx = i_idx + 1;
				SELECT count(*) INTO countRows
				FROM performance_schema.table_handles 
				WHERE OBJECT_SCHEMA='ams_ac' and OBJECT_NAME='ams_al_flight_plan'
				and (EXTERNAL_LOCK IS NOT NULL or INTERNAL_LOCK IS NOT NULL);
			END WHILE;
			*/
			IF i_idx > 0 THEN
				SET logNote=CONCAT(i_idx, ' cicles wait for unlock table ams_al_flight_plan');
				call ams_wad.log_sp(spname, stTime, logNote);
			END IF;
    
			update ams_ac.ams_al_flight_plan set flp_status_id = i_stat_add_to_flq
			where flp_id =  in_flp_id;
            commit;
			set countRows = 0;
			
			INSERT INTO ams_ac.ams_al_flight_queue (
				fl_type_id, fl_name, fl_description, flp_id, route_id, al_id, ac_id, 
				d_ap_id, a_ap_id, distance_km, oil_l, flight_h, pax, payload_kg, dtime, atime)
			(SELECT flp.fl_type_id, flp.fl_name, flp.fl_name as fl_description, flp.flp_id,
				flp.route_id, flp.al_id, flp.ac_id, 
				flp.d_ap_id, flp.a_ap_id, flp.distance_km, round((m.fuel_consumption_lp100km/100)*flp.distance_km,2) as oil_l, flp.flight_h, 
                sum(ifnull(p.pax,0)) as pax, 
                round(sum( if(p.pax_class_id = 4, ifnull(p.payload_kg,0),0)),2) as payload_kg, flp.dtime, flp.atime
			FROM ams_ac.ams_al_flight_plan flp 
			left join ams_ac.ams_al_flight_plan_payload p on p.flp_id = flp.flp_id
            join ams_ac.ams_aircraft a on a.ac_id = flp.ac_id
            join ams_ac.cfg_mac m on m.mac_id = a.mac_id
			where flp.flp_id = in_flp_id
            group by flp.flp_id FOR SHARE);
			
			SET i_fl_id = LAST_INSERT_ID();
            commit;
	
			select flp.ac_id, flp.al_id INTO i_ac_id, i_al_id 
            from ams_ac.ams_al_flight_plan flp where flp.flp_id = in_flp_id FOR SHARE;
			SET logNote=CONCAT('inserted  ams_al_flight_queue i_fl_id ', i_fl_id, ', i_ac_id:', i_ac_id, ', al_id:', i_al_id);
			call ams_wad.log_sp(spname, stTime, logNote);
			
			INSERT INTO ams_ac.ams_al_flight_queue_payload
				( fl_id, flp_id, payload_type, pax, cargo_kg, payload_kg, 
				pax_class_id, ppd_type_id, description, price)
			(SELECT i_fl_id as fl_id, ifnull(p.flp_id, in_flp_id), ifnull(c.pax_class_abrev, 'E') as payload_type, 
			ifnull(p.pax, 0), ifnull(p.payload_kg, 0) as cargo_kg, ifnull(p.payload_kg, 0), 
			ifnull(p.pax_class_id, 1), ifnull(p.ppd_type_id, 1), ifnull(p.note, 'empty') as description, ifnull(p.price, 0)
			FROM ams_ac.ams_al_flight_plan_payload p
			join ams_wad.cfg_pax_class c on c.pax_class_id = p.pax_class_id
			where p.flp_id = in_flp_id FOR SHARE);
			SET countRows =  ROW_COUNT();
			commit;
			SET logNote=CONCAT('inserted in ams_al_flight_queue_payload ', countRows, ' rows for in_flp_id: ', in_flp_id);
			call ams_wad.log_sp(spname, stTime, logNote);
		
			insert into ams_ac.ams_al_flight_plan_h
			(select * from ams_ac.ams_al_flight_plan 
			where flp_id = in_flp_id FOR SHARE);
			commit;
			insert into ams_ac.ams_al_flight_plan_payload_h
			(select * from ams_ac.ams_al_flight_plan_payload 
			where flp_id = in_flp_id FOR SHARE);
			commit;
else
	 SELECT flq.fl_id, flq.ac_id, flq.al_id  INTO i_fl_id, i_ac_id, i_al_id 
	 FROM ams_ac.ams_al_flight_queue flq where flp_id = in_flp_id for share;
	 SET logNote=CONCAT('fl_id:', i_fl_id, ', ac_id: ', i_ac_id, ', al_id: ', i_al_id);
	 call ams_wad.log_sp(spname, stTime, logNote);
 end if;

IF i_fl_id > 0 THEN
			delete from ams_al_flight_plan where flp_id = in_flp_id;
            commit;
			delete from ams_al_flight_plan_payload where flp_id = in_flp_id;
			SET countRows =  ROW_COUNT();
            commit;
			SET logNote=CONCAT('deleted from in ams_al_flight_plan_payload ',  FORMAT(countRows, 0), ' rows. in_flp_id:', in_flp_id);
			call ams_wad.log_sp(spname, stTime, logNote);
            
            delete pcpd.* 
			from ams_ac.ams_al_flight_pcpd pcpd
			left join ams_ac.ams_al_flight_pcpd_h pcpdh on pcpdh.pcpd_id = pcpd.pcpd_id
			where pcpdh.pcpd_id is not null;
			commit;
            
            insert into ams_ac.ams_al_flight_pcpd_h
			select pcpd.* 
			from ams_ac.ams_al_flight_pcpd pcpd
			left join ams_ac.ams_al_flight_plan_payload pl on pl.pcpd_id = pcpd.pcpd_id
			where pl.pcpd_id is null;
            SET countRows =  ROW_COUNT();
            commit;
            SET logNote=CONCAT('Moved to ams_ac.ams_al_flight_pcpd_h ',  FORMAT(countRows, 0), ' rows.');
			call ams_wad.log_sp(spname, stTime, logNote);
            
			delete pcpd.* 
			from ams_ac.ams_al_flight_pcpd pcpd
			left join ams_ac.ams_al_flight_pcpd_h pcpdh on pcpdh.pcpd_id = pcpd.pcpd_id
			where pcpdh.pcpd_id is not null;
            SET countRows =  ROW_COUNT();
            commit;
            
            SET logNote=CONCAT('deleted from ams_ac.ams_al_flight_pcpd ',  FORMAT(countRows, 0), ' rows.');
			call ams_wad.log_sp(spname, stTime, logNote);
            
			
		-- Update FL Shedule counter
		SELECT count(*) INTO countRows
		FROM ams_al.ams_airline_counters alc
		where alc.al_id = i_al_id;
		
		IF countRows = 0 THEN
			INSERT INTO ams_al.ams_airline_counters (al_id, lf_shedule)
			values(i_al_id, 1);
            commit;
            SET logNote=CONCAT('inserted ams_airline_counters record',' al_id: ', i_al_id);
			call ams_wad.log_sp(spname, stTime, logNote);
		ELSE
			update ams_al.ams_airline_counters alc
			set alc.lf_shedule = (alc.lf_shedule+1)
			where alc.al_id = i_al_id;
            commit;
            SET logNote=CONCAT('Updated ams_airline_counters record ',' al_id: ', i_al_id);
			call ams_wad.log_sp(spname, stTime, logNote);
		END IF;
		
END IF;

	SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);
		
	select i_fl_id as fl_id from dual;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_flight_plan_transfer_clean` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_flight_plan_transfer_clean`()
BEGIN

declare i_idx int;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_flight_plan_transfer_clean', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_flight_plan_transfer_clean';
set countRows = 0;
SET logNote=CONCAT('Start ',  ' ->' );
call ams_wad.log_sp(spname, stTime, logNote);
            
		drop TEMPORARY table IF EXISTS transfers_to_del;
		
		create TEMPORARY table transfers_to_del as (select tr.transfer_id 
		from ams_ac.ams_al_flight_plan_transfer tr
		left join (select flp_id from ams_ac.ams_al_flight_plan FOR SHARE) flp1 on flp1.flp_id = tr.flp_id_1
		left join (select flp_id from ams_ac.ams_al_flight_plan FOR SHARE) flp2 on flp2.flp_id = tr.flp_id_2
		left join (select flp_id from ams_ac.ams_al_flight_plan FOR SHARE) flp3 on flp3.flp_id = tr.flp_id_3
		left join (select flp_id from ams_ac.ams_al_flight_plan FOR SHARE) flp4 on flp4.flp_id = tr.flp_id_4
		where flp1.flp_id is null and  flp2.flp_id is null and flp3.flp_id is null and flp4.flp_id is null  for share);
		commit;
        
		SET countRows =  0;
		select count(*) INTO countRows from transfers_to_del for share;
		SET logNote=CONCAT('Found ', FORMAT(countRows, 0), ' rows from flight_plan_transfer.' );
		call ams_wad.log_sp(spname, stTime, logNote);
        
		IF countRows > 0 THEN
			set countRows = 0;
			SET i_idx = 0;
			SELECT count(*) INTO countRows
			FROM performance_schema.table_handles 
			WHERE OBJECT_SCHEMA='ams_ac' and OBJECT_NAME='ams_al_flight_plan_transfer'
			and (EXTERNAL_LOCK IS NOT NULL or INTERNAL_LOCK IS NOT NULL);

			WHILE countRows > 0 DO
				SELECT SLEEP(0.2);
				SET i_idx = i_idx + 1;
				SELECT count(*) INTO countRows
				FROM performance_schema.table_handles 
				WHERE OBJECT_SCHEMA='ams_ac' and OBJECT_NAME='ams_al_flight_plan_transfer'
				and (EXTERNAL_LOCK IS NOT NULL or INTERNAL_LOCK IS NOT NULL);
				if i_idx > 10000 THEN
					set countRows = 0;
				end if;
			END WHILE;
		
			IF i_idx > 0 THEN
				SET logNote=CONCAT('Sleep ', FORMAT((i_idx*0.2)/60,0), ' MIN for unlock table ams_ac.ams_al_flight_plan_transfer');
				call ams_wad.log_sp(spname, stTime, logNote);
			END IF;
            
			delete tr.* from  ams_ac.ams_al_flight_plan_transfer tr
			join transfers_to_del trd on trd.transfer_id = tr.transfer_id
			where trd.transfer_id is not null; 
			SET countRows =  ROW_COUNT();
				
			SET logNote=CONCAT('Deleted ', FORMAT(countRows, 0), ' rows from flight_plan_transfer.' );
			call ams_wad.log_sp(spname, stTime, logNote);
		END IF;
		
        drop TEMPORARY table IF EXISTS transfers_to_del;
			

	SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_fl_charter_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_fl_charter_create`(IN in_fle_id INT)
BEGIN

DECLARE i_ac_status_operational, i_acat_planeStand, i_fl_type, i_distance_km INT;
DECLARE i_fl_id, i_pax, i_ac_id INT;
DECLARE i_payload_kg DOUBLE;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_fl_charter_create', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_fl_charter_create';
set countRows = 0;
SET logNote=CONCAT('Start (', in_fle_id , ') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

SET i_distance_km = 0;
SET i_ac_status_operational = 2;
SET i_acat_planeStand = 6;
SET i_fl_type = 2; -- Charter

	SELECT e.distance_km
	INTO i_distance_km
	FROM ams_ac.ams_al_flight_estimate e
	join ams_ac.ams_aircraft_curr ac on ac.ac_id = e.ac_id
	join ams_ac.ams_aircraft a on a.ac_id = e.ac_id
	join ams_ac.ams_aircraft_log l on l.ac_id = ac.ac_id
	where e.fle_id = in_fle_id and e.fl_type_id = i_fl_type
	and a.ac_status_id = i_ac_status_operational;
    
    SET logNote=CONCAT('distance: ', i_distance_km , ' ' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    IF i_distance_km > 1 THEN 
        
		INSERT INTO ams_ac.ams_al_flight_queue (
			fl_type_id, fl_name, fl_description, fle_id, ch_id, flp_id, route_id, al_id, ac_id, 
			d_ap_id, a_ap_id, distance_km, oil_l, flight_h, payload_kg, dtime, atime)
		select fl_type_id, fl_name, fl_description, fle_id, ch_id, flp_id, route_id, al_id, ac_id, 
			d_ap_id, a_ap_id, distance_km, oil_l, round(duration_min/60, 2), payload_kg, dtime, atime
		FROM ams_ac.ams_al_flight_estimate e
		where e.fle_id = in_fle_id;
        
        SET i_fl_id = LAST_INSERT_ID();
        
		SET logNote=CONCAT('inserted i_fl_id ', i_fl_id);
		call ams_wad.log_sp(spname, stTime, logNote);
        commit;
        
        INSERT INTO ams_ac.ams_al_flight_queue_payload
        ( fl_id, ch_id, payload_type, pax, cargo_kg, payload_kg, description, price, adate)
        select i_fl_id as fl_id, ch_id, payload_type, pax, cargo_kg, payload_kg, description, price, adate
        from ams_ac.ams_al_flight_estimate_payload where fle_id = in_fle_id;
        commit;
        
		update ams_ac.ams_al_flight_queue flq
        join (
        select flqp.fl_id, sum(ifnull(flqp.pax,0)) as pax, 
			sum(ifnull(flqp.cargo_kg,0)) as cargo_kg, 
			sum(ifnull(flqp.payload_kg,0)) as payload_kg,
            sum(ifnull(flqp.price,0)) as price
			from ams_ac.ams_al_flight_queue_payload flqp where flqp.fl_id = i_fl_id
			group by flqp.fl_id
			)
			flqp on flqp.fl_id = flq.fl_id
        set flq.pax = ifnull(flqp.pax,0),
        flq.payload_kg = ifnull(flqp.payload_kg,0)
        where flqp.fl_id = i_fl_id;
		commit;
        
         update ams_ac.ams_charter ch
        join (
        select flqp.ch_id, flqp.fl_id, sum(ifnull(flqp.pax,0)) as pax, 
			sum(ifnull(flqp.cargo_kg,0)) as cargo_kg, 
			sum(ifnull(flqp.payload_kg,0)) as payload_kg,
            sum(ifnull(flqp.price,0)) as price
			from ams_ac.ams_al_flight_queue_payload flqp where flqp.fl_id = i_fl_id
			group by flqp.ch_id, flqp.fl_id
			)
			flqp on flqp.ch_id = ch.ch_id
        set ch.pax_completed = ifnull(ch.pax_completed,0) + ifnull(flqp.pax,0),
        ch.cargo_kg_compleated = ifnull(ch.cargo_kg_compleated,0) + ifnull(flqp.cargo_kg,0),
        ch.payload_kg_completed = ifnull(ch.payload_kg_completed,0) + ifnull(flqp.payload_kg,0),
        ch.price_completed = ifnull(ch.price_completed,0) + ifnull(flqp.price,0)
        where flqp.fl_id = i_fl_id;
        
        commit;
        update ams_ac.ams_charter ch
        join ams_ac.ams_al_flight_queue_payload flqp on flqp.ch_id = ch.ch_id
        set ch.completed = if(ch.payload_kg_completed>=ch.payload_kg, 1, 0)
        where flqp.fl_id = i_fl_id;
        commit;
        
        -- Update FL Charter counter
        SELECT count(*) INTO countRows
		FROM ams_al.ams_airline_counters alc
		join ams_al_flight_estimate fle on fle.al_id = alc.al_id
        where fle.fle_id = in_fle_id;
			
        IF countRows = 0 THEN
			SET logNote=CONCAT('inserted ams_airline_counters record ');
			INSERT INTO ams_al.ams_airline_counters (al_id, fl_charter)
			SELECT al_id, fl_num from ams_al_flight_estimate where fle_id = in_fle_id;
        ELSE
			SET logNote=CONCAT('Updated ams_airline_counters record ');
			update ams_al.ams_airline_counters alc
			join ams_al_flight_estimate fle on fle.al_id = alc.al_id
			set alc.fl_charter = fle.fl_num
			where fle.fle_id = in_fle_id;
        END IF;
        
        select ac_id into i_ac_id from ams_ac.ams_al_flight_queue where fl_id = i_fl_id;
		CALL ams_ac.sp_ams_aircraft_curr_update_ac(i_ac_id);
        
	END IF;
    
    SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    select i_fl_id as fl_id from dual;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_fl_charter_estimate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_fl_charter_estimate`(IN in_ac_id INT, IN in_d_ap_id INT)
BEGIN

DECLARE countRows, o_fl_num_nex, i_d_ap_id, i_duration_min, i_distance_km, i_fle_id INT;
DECLARE s_al_iata VARCHAR(5);
DECLARE stTime, i_dtime TIMESTAMP;
DECLARE vflight_h, vamount, vprice double;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_fl_charter_estimate', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_fl_charter_estimate';
set countRows = 0;
SET logNote=CONCAT('Start (', in_ac_id ,', ', in_d_ap_id, ') ->' );
call ams_wad.log_sp(spname, stTime, logNote);
set i_dtime = current_timestamp();

	call ams_ac.sp_al_fl_charter_estimate_time(in_ac_id, in_d_ap_id, i_dtime, o_fl_num_nex);

	
    SET logNote=CONCAT('Finish ', o_fl_num_nex, '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    -- select * from ams_ac.ams_al_flight_estimate where fle.ac_id = in_ac_id and fle.fl_type_id=2 and fle.fl_num=i_fl_num_nex;
    
    select o_fl_num_nex as fl_num from dual;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_fl_charter_estimate_time` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_fl_charter_estimate_time`(IN in_ac_id INT, IN in_d_ap_id INT,  IN in_dtime TIMESTAMP, out o_fl_num_nex int)
BEGIN

DECLARE countRows, i_d_ap_id, i_duration_min, i_distance_km, i_fle_id INT;
DECLARE s_al_iata VARCHAR(5);
DECLARE stTime TIMESTAMP;
DECLARE d_cost_per_fh, vflight_h, vamount, vprice double;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_fl_charter_estimate_time', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_fl_charter_estimate_time';
set countRows = 0;
SET logNote=CONCAT('Start (', in_ac_id, ', ', in_d_ap_id ,', ', in_dtime,') ->' );
call ams_wad.log_sp(spname, stTime, logNote);


	SELECT count(*) INTO countRows
	FROM ams_ac.ams_charter ch 
    where ch.d_ap_id = in_d_ap_id and ch.completed = 0;
    
    delete from ams_ac.ams_al_flight_estimate where ac_id = in_ac_id;
	delete FROM ams_ac.ams_al_flight_estimate_payload
	where fle_id not in (select fle_id from ams_ac.ams_al_flight_estimate);
	commit;
        
    IF countRows >0 THEN
    
		SELECT ifnull(alc.fl_charter, 0) +1 INTO o_fl_num_nex
        FROM ams_ac.ams_aircraft ac
        left join ams_al.ams_airline_counters alc on alc.al_id = ac.owner_al_id
        where ac.ac_id = in_ac_id;
        
        SET logNote=CONCAT('Next flight number ', o_fl_num_nex, ' ' );
		-- call ams_wad.log_sp(spname, stTime, logNote);
         
		
        -- Insert new fle_id records
        -- CONCAT( IFNULL(dep.ap_iata, dep.ap_icao), ' to ',IFNULL(arr.ap_iata, arr.ap_icao), ' ',ch.fl_description, ' charter flight') as fl_description,
        INSERT INTO ams_ac.ams_al_flight_estimate
		(ch_id, route_id, fl_type_id,
			fl_num, fl_name, fl_description,
			al_id, ac_id, d_ap_id, a_ap_id,
			distance_km, duration_min,
			oil_l, oli_price, 
			fl_price, bank_id_from,
			dtime, atime)
			SELECT ch.ch_id as ch_id, null as route_id, 2 as fl_type_id,
			ifnull(alc.fl_charter, 0) +1 as fl_num, 
			concat(al.iata, LPAD((ifnull(alc.fl_charter, 0) +1),5,'0'),ch.fl_name) as fl_name,
			CONCAT(ch.fl_name, ' ',ch.fl_description, ' ') as fl_description,
			al.al_id, ac.ac_id, dep.ap_id as d_ap_id, arr.ap_id as a_ap_id, ch.distance_km,
			Round(((ch.distance_km/macr.cruise_speed_kmph)*60)+(2*acc.op_time_min),0) as duration_min,
			ROUND((ch.distance_km /100)* macr.fuel_consumption_lp100km,2) as oil_l,
			ROUND((ch.distance_km /100)* macr.fuel_consumption_lp100km*p.price_per_l*-1,2) as oli_price,
			Round(((ch.distance_km/macr.cruise_speed_kmph))*(macr.cost_per_fh*p.cost_per_fh_factor)*-1,0) as fl_price,
			b.bank_id as bank_id_from,
			DATE_ADD( in_dtime,INTERVAL 1 MINUTE) as dtime,
            -- DATE_ADD( in_dtime,INTERVAL p.st_to_ht_min MINUTE) as dtime,
				DATE_ADD(
					DATE_ADD( in_dtime,INTERVAL 1 MINUTE) , 
					INTERVAL Round(((ch.distance_km/macr.cruise_speed_kmph)*60)+(1*acc.op_time_min),0) MINUTE)  as atime
			
			FROM ams_ac.ams_aircraft ac
            left join ams_al.ams_airline_counters alc on alc.al_id = ac.owner_al_id
			join ams_al.ams_airline al on al.al_id = ac.owner_al_id
			join ams_al.ams_bank b on b.al_id = al.al_id
			join ams_ac.ams_aircraft_curr acc on acc.ac_id = ac.ac_id
			join ams_al.v_al_callc_param_h p on p.al_id = al.al_id
			join ams_ac.cfg_mac macr on ac.mac_id = macr.mac_id
			join (select * from ams_ac.ams_charter 
				where d_ap_id = in_d_ap_id and completed = 0
			) ch on 1=1 
			join ams_wad.cfg_airport arr on arr.ap_id = ch.a_ap_id
			join ams_wad.cfg_airport dep on dep.ap_id = ch.d_ap_id
            left join ams_wad.cfg_airport_destination d on d.dep_ap_id = ch.d_ap_id and d.arr_ap_id = ch.a_ap_id
			where ac.ac_id = in_ac_id and macr.max_range_km > ch.distance_km and ifnull(d.min_rw_lenght_m, macr.take_off_m+50) > macr.take_off_m;
            
         commit;   
         -- Check new fle_id records
		select count(*) INTO countRows
        from ams_ac.ams_al_flight_estimate 
        where ac_id = in_ac_id and fl_type_id=2 and fl_num=o_fl_num_nex;
        
		SET logNote=CONCAT('inserted ', countRows, ' rows');
		call ams_wad.log_sp(spname, stTime, logNote);
        
			insert into ams_ac.ams_al_flight_estimate_payload
			(fle_id, ch_id, payload_type, pax, cargo_kg, payload_kg)
			select fle.fle_id, ch.ch_id, ch.payload_type, 
			if(ch.payload_type <> 'C', pl.pax,0) as pax,
            if( ch.payload_type = 'C', pl.cargo_kg, 0) as cargo_kg,
			Round( ifnull(pl.cargo_kg,0) + (pl.pax*p.pax_weight_kg), 2) as payload_kg
			from ams_ac.ams_al_flight_estimate  fle
			join ams_ac.ams_charter ch on ch.ch_id = fle.ch_id
			join ams_ac.ams_aircraft ac on ac.ac_id = fle.ac_id
			join ( select fle.fle_id,  CASE ch.payload_type
					WHEN 'E' THEN if((ifnull(ch.pax,0)-ifnull(ch.pax_completed,0)) >= ac.pax_e, ac.pax_e, (ifnull(ch.pax,0)-ifnull(ch.pax_completed,0)))
					WHEN 'B' THEN if((ifnull(ch.pax,0)-ifnull(ch.pax_completed,0)) >= ac.pax_b, ac.pax_b, (ifnull(ch.pax,0)-ifnull(ch.pax_completed,0)))
					WHEN 'F' THEN if((ifnull(ch.pax,0)-ifnull(ch.pax_completed,0)) >= ac.pax_f, ac.pax_f, (ifnull(ch.pax,0)-ifnull(ch.pax_completed,0)))
					ELSE 0
				END as pax,
				if(ch.payload_type = 'C', if( (ch.cargo_kg > ac.cargo_kg), ac.cargo_kg, ch.cargo_kg), 0) as cargo_kg
				from ams_ac.ams_al_flight_estimate  fle
				join ams_ac.ams_charter ch on ch.ch_id = fle.ch_id
				join ams_ac.ams_aircraft ac on ac.ac_id = fle.ac_id 
				where fle.ac_id = in_ac_id and fle.fl_type_id=2 and fle.fl_num=o_fl_num_nex
            ) pl on pl.fle_id = fle.fle_id
			join ams_al.v_al_callc_param_h p on p.al_id = ac.owner_al_id
			where fle.ac_id = in_ac_id and fle.fl_type_id=2 and fle.fl_num=o_fl_num_nex;
			commit;  
        
        update ams_ac.ams_al_flight_estimate_payload flep
		join ams_ac.ams_charter ch on ch.ch_id = flep.ch_id
		join ams_ac.ams_al_flight_estimate  fle on fle.ch_id = flep.ch_id
		set flep.price = Round((ch.price/ch.payload_kg) * flep.payload_kg,2)
		where fle.ac_id = in_ac_id and fle.fl_type_id=2 and fle.fl_num=o_fl_num_nex;
        commit;  
        
         update ams_ac.ams_al_flight_estimate  fle    
		 join (select fle.fle_id, sum(flep.payload_kg) as payload_kg, sum(flep.pax) as pax       
		 from ams_ac.ams_al_flight_estimate  fle    
		 left join ams_ac.ams_al_flight_estimate_payload flep on flep.fle_id = fle.fle_id
		 where fle.ac_id = in_ac_id and fle.fl_type_id=2 and fle.fl_num=o_fl_num_nex
		 group by fle.fle_id) flep1 on flep1.fle_id = fle.fle_id
		 set fle.pax = flep1.pax, fle.payload_kg = flep1.payload_kg;
         commit;  
         
         update  ams_ac.ams_al_flight_estimate  fle 
		join ams_ac.ams_charter ch on ch.ch_id = fle.ch_id
		join ams_ac.ams_al_flight_estimate_payload flep on flep.fle_id = fle.fle_id
		join ams_al.v_al_callc_param_h p on p.al_id = fle.al_id
		join ams_ac.ams_aircraft ac on ac.ac_id = fle.ac_id
		join ams_ac.cfg_mac macr on ac.mac_id = macr.mac_id
		join ams_wad.cfg_airport arr on arr.ap_id = fle.a_ap_id
		join ams_wad.cfg_airport dep on dep.ap_id = fle.d_ap_id
		set fle.landin_fee = ROUND( 2 * p.landing_fee_p_kg_mtow*((dep.ap_type_id+arr.ap_type_id)/5)*
					round((ac.pilots_count + ac.crew_count )*p.pax_weight_kg + fle.payload_kg + macr.empty_w_kg ,0)*-1
				,2) ,
			fle.ground_crew_fee = ROUND( 2 * p.ground_crew_fee_p_kg_mtow*((dep.ap_type_id+arr.ap_type_id)/5)*
					round((ac.pilots_count + ac.crew_count )*p.pax_weight_kg + fle.payload_kg + macr.empty_w_kg ,0)
				*-1,2),
			fle.boarding_fee = Round(-1 * 2 * p.boarding_fee_p_pax*((dep.ap_type_id+arr.ap_type_id)/5)* (fle.pax + ac.pilots_count + ac.crew_count ) ,2) ,
			fle.fl_price = Round(((fle.distance_km/macr.cruise_speed_kmph))*(macr.cost_per_fh * p.cost_per_fh_factor)*-1,0) ,
			fle.mtow = ROUND((fle.payload_kg + (ac.pilots_count + ac.crew_count)*p.pax_weight_kg  + 
				((fle.distance_km /100)* macr.fuel_consumption_lp100km) + macr.empty_w_kg),2)
		where fle.ac_id = in_ac_id and fle.fl_type_id=2 and fle.fl_num=o_fl_num_nex;
        
        commit; 
        
        update ams_ac.ams_al_flight_estimate fle
		join (
		SELECT fle.fle_id, (oli_price +  landin_fee + ground_crew_fee + boarding_fee + fl_price) as expences,
		sum(flep.price) as income
		FROM ams_ac.ams_al_flight_estimate fle
		left join ams_ac.ams_al_flight_estimate_payload flep on flep.fle_id = fle.fle_id
		where fle.ac_id = in_ac_id and fle.fl_type_id=2 and fle.fl_num=o_fl_num_nex
		group by fle.fle_id) fle1 on fle1.fle_id = fle.fle_id
		set fle.expences = fle1.expences,
		fle.income = fle1.income,fle.revenue = Round(fle1.income + fle1.expences)
		where fle.ac_id = in_ac_id and fle.fl_type_id=2 and fle.fl_num=o_fl_num_nex;
        commit;
			
    ELSE
		SET logNote=CONCAT('No Chanred flights for ap ', in_d_ap_id, ' ' );
		call ams_wad.log_sp(spname, stTime, logNote);
    END IF;
    
    SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    -- select * from ams_ac.ams_al_flight_estimate where fle.ac_id = in_ac_id and fle.fl_type_id=2 and fle.fl_num=o_fl_num_nex;
    
    select o_fl_num_nex as fl_num from dual;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_fl_transfer_create` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_fl_transfer_create`(IN in_fle_id INT)
BEGIN

DECLARE i_ac_status_operational, i_acat_planeStand, i_fl_type_transfer, i_distance_km INT;
DECLARE i_fl_id, i_ac_id INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_fl_transfer_create', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_fl_transfer_create';
set countRows = 0;
SET logNote=CONCAT('Start (', in_fle_id , ') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

SET i_distance_km = 0;
SET i_ac_status_operational = 2;
SET i_acat_planeStand = 6;
SET i_fl_type_transfer = 3;

	SELECT e.distance_km
	INTO i_distance_km
	FROM ams_ac.ams_al_flight_estimate e
	join ams_ac.ams_aircraft_curr ac on ac.ac_id = e.ac_id
	join ams_ac.ams_aircraft a on a.ac_id = e.ac_id
	join ams_ac.ams_aircraft_log l on l.ac_id = ac.ac_id
	where e.fle_id = in_fle_id and e.fl_type_id = i_fl_type_transfer
	and a.ac_status_id = i_ac_status_operational and l.acat_id = i_acat_planeStand;
    
    SET logNote=CONCAT('distance: ', i_distance_km , ' ' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    
    IF i_distance_km > 1 THEN 
    
		INSERT INTO ams_ac.ams_al_flight_queue ( 
			fl_type_id, fl_name, fl_description, fle_id, ch_id, flp_id, route_id, al_id, ac_id, 
			d_ap_id, a_ap_id, distance_km, oil_l, payload_kg, dtime, atime)
		select fl_type_id, fl_name, fl_description, fle_id, ch_id, flp_id, route_id, al_id, ac_id, 
			d_ap_id, a_ap_id, distance_km, oil_l, payload_kg, dtime, atime
		FROM ams_ac.ams_al_flight_estimate e
		where e.fle_id = in_fle_id;
        
        SET i_fl_id = LAST_INSERT_ID();
        
		SET logNote=CONCAT('inserted i_fl_id ', i_fl_id);
		call ams_wad.log_sp(spname, stTime, logNote);
        commit;
        
        -- Update FL Transfer counter
        SELECT count(*) INTO countRows
		FROM ams_al.ams_airline_counters alc
		join ams_al_flight_estimate fle on fle.al_id = alc.al_id
        where fle.fle_id = in_fle_id;
			
        IF countRows = 0 THEN
			SET logNote=CONCAT('inserted ams_airline_counters record ');
			INSERT INTO ams_al.ams_airline_counters (al_id, fl_transfer)
			SELECT al_id, fl_num from ams_al_flight_estimate where fle_id = in_fle_id;
        ELSE
			SET logNote=CONCAT('Updated ams_airline_counters record ');
			update ams_al.ams_airline_counters alc
			join ams_al_flight_estimate fle on fle.al_id = alc.al_id
			set alc.fl_transfer = fle.fl_num
			where fle.fle_id = in_fle_id;
        END IF;
       
        select ac_id into i_ac_id from ams_ac.ams_al_flight_queue where fl_id = i_fl_id;
        CALL ams_ac.sp_ams_aircraft_curr_update_ac(i_ac_id);
        
        set countRows = 0;
        call ams_wad.log_sp(spname, stTime, logNote);
        
	END IF;
    
    SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    select i_fl_id as fl_id from dual;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_al_fl_transfer_estimate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_al_fl_transfer_estimate`(IN in_ac_id INT, IN in_a_ap_id INT)
BEGIN

DECLARE countRows, ifl_num_nex, i_d_ap_id, i_duration_min, i_distance_km, i_fle_id, i_dest_id INT;
DECLARE s_al_iata VARCHAR(5);
DECLARE stTime TIMESTAMP;
DECLARE d_cost_per_fh, vflight_h, vamount, vprice double;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_al_fl_transfer_estimate', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_al_fl_transfer_estimate';
SET logNote=CONCAT('Start (', in_ac_id ,', ', in_a_ap_id, ') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

	-- Get next charter flight number for this airline , ac.ac_status_id
    SELECT ifnull(alc.fl_transfer, 0) +1 as ifl_num_nex, al.iata, acc.curr_ap_id, ac.cost_per_fh
    INTO ifl_num_nex, s_al_iata, i_d_ap_id, d_cost_per_fh
	FROM ams_ac.ams_aircraft ac
	join ams_al.ams_airline al on al.al_id = ac.owner_al_id
	join ams_ac.ams_aircraft_curr acc on acc.ac_id = ac.ac_id
	left join ams_al.ams_airline_counters alc on alc.al_id = ac.owner_al_id
	where ac.ac_id = in_ac_id;
    
    SET logNote=CONCAT('Next Flight ', ifl_num_nex , ', iata: ', s_al_iata, ', dep ap_id: ', i_d_ap_id );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    call ams_wad.get_cfg_airport_destination(i_d_ap_id, in_a_ap_id, i_dest_id);
    
    SELECT Round(p.distance_unit
		 * DEGREES(ACOS(COS(RADIANS(orig.ap_lat))
		 * COS(RADIANS(dest.ap_lat))
		 * COS(RADIANS(orig.ap_lon - dest.ap_lon))
		 + SIN(RADIANS(orig.ap_lat))
		 * SIN(RADIANS(dest.ap_lat)))),2) AS distance_km, orig.ap_id 
	INTO i_distance_km, i_d_ap_id	
	FROM ams_ac.ams_aircraft_curr acr
	join ams_wad.cfg_airport orig on orig.ap_id=acr.curr_ap_id 
	join ams_wad.cfg_airport dest on 1=1 and dest.ap_id = in_a_ap_id
	JOIN (SELECT  111.045 AS distance_unit) AS p ON 1=1
	where acr.ac_id=in_ac_id;
    
    INSERT INTO ams_ac.ams_al_flight_estimate
		(flp_id, route_id, fl_type_id,
		fl_num, fl_name, fl_description,
		al_id, ac_id, d_ap_id, a_ap_id,
		distance_km, duration_min,
		oil_l, oli_price, landin_fee, ground_crew_fee, boarding_fee,
		fl_price, bank_id_from, payload_kg, mtow,
		dtime, atime, expences, income, revenue)
    select null, null, 3 as fl_type_id, 
		(op.flch_next_nr) as fl_num,
        concat(al.iata, LPAD((op.flch_next_nr),6,'0'),'TR') as fl_name,
		CONCAT( IFNULL(dep.ap_iata, dep.ap_icao), ' to ',IFNULL(arr.ap_iata, arr.ap_icao), ' Transfer Flight ') as fl_description,
		ac1.owner_al_id as al_id, 
		ac.ac_id, 
        dep.ap_id as d_ap_id, 
        arr.ap_id as a_ap_id, 
		op.distance_km, 
		Round(((op.distance_km/macr.cruise_speed_kmph)*60)+(2*ac.op_time_min),0) as duration_min,
        ROUND((op.distance_km /100)* macr.fuel_consumption_lp100km,2) as oil_l,
        ROUND((op.distance_km /100)* macr.fuel_consumption_lp100km*op.price_per_l*-1,2) as oli_price,
        ROUND( 2 * p.landing_fee_p_kg_mtow*((dep.ap_type_id+arr.ap_type_id)/2)*
			round((ac1.pilots_count + ac1.crew_count )*p.pax_weight_kg  + macr.empty_w_kg ,0)*-1
		,2) as landin_fee,
        ROUND( 2 * p.ground_crew_fee_p_kg_mtow*((dep.ap_type_id+arr.ap_type_id)/2)*
			round((ac1.pilots_count + ac1.crew_count )*p.pax_weight_kg  + macr.empty_w_kg ,0)
		*-1,2) as ground_crew_fee,
        0 as boarding_fee,
        Round(((op.distance_km/macr.cruise_speed_kmph))*(macr.cost_per_fh * p.cost_per_fh_factor)*-1,0) as fl_price,
        b.bank_id as bank_id_from,
        0 as payload_kg,
        ROUND(0*p.pax_weight_kg + ((ac1.pilots_count + ac1.crew_count)*p.pax_weight_kg  + 
        ((op.distance_km /100)* macr.fuel_consumption_lp100km) + macr.empty_w_kg),2) as mtow,
        DATE_ADD( now(),INTERVAL p.st_to_ht_min + (ac.op_time_min*4) MINUTE) as dtime,
        DATE_ADD(
			DATE_ADD( now(),INTERVAL p.st_to_ht_min + (ac.op_time_min*4) MINUTE) , 
			INTERVAL Round(((op.distance_km/macr.cruise_speed_kmph)*60)+(2*ac.op_time_min),0) MINUTE)  as atime,
		ROUND(((op.distance_km /100)* macr.fuel_consumption_lp100km*op.price_per_l) +
        Round(((op.distance_km/macr.cruise_speed_kmph))*(macr.cost_per_fh * p.cost_per_fh_factor),0) +
        ( 2 * p.landing_fee_p_kg_mtow*((dep.ap_type_id+arr.ap_type_id)/2)*
			round((ac1.pilots_count + ac1.crew_count )*p.pax_weight_kg  + macr.empty_w_kg ,0)) + 
        ( 2 * p.ground_crew_fee_p_kg_mtow*((dep.ap_type_id+arr.ap_type_id)/2)*
			round((ac1.pilots_count + ac1.crew_count )*p.pax_weight_kg  + macr.empty_w_kg ,0) ) +
            (0*p.boarding_fee_p_pax*2) ,2)*-1 as expenses,
            0 as income,
        null as revenue
	FROM ams_ac.ams_aircraft_curr ac
    join ams_ac.ams_aircraft ac1 on ac1.ac_id = ac.ac_id
	join ams_al.v_al_callc_param_h p on p.al_id = ac1.owner_al_id
	join (select * from ams_wad.cfg_airport where ap_id = in_a_ap_id) arr on 1=1
    join (select * from ams_wad.cfg_airport where ap_id = i_d_ap_id) dep on 1=1
	join ( SELECT price_per_l, i_distance_km as distance_km, 
			s_al_iata as airline_iata_code,
            ifl_num_nex as flch_next_nr  FROM ams_al.cfg_oil_price WHERE oil_price_id = 1 ) op on 1=1
	join ams_ac.cfg_mac macr on ac1.mac_id = macr.mac_id
    join ams_al.ams_airline al on al.al_id = ac1.owner_al_id
    join ams_al.ams_bank b on b.al_id = ac1.owner_al_id
	where ac.ac_id=in_ac_id;
    
     SET i_fle_id = LAST_INSERT_ID();
        
	SET logNote=CONCAT('inserted i_fle_id ', i_fle_id);
    call ams_wad.log_sp(spname, stTime, logNote);
    
    SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    -- select * from ams_ac.ams_al_flight_estimate where fle_id = i_fle_id;
    
    select i_fle_id as fle_id from dual;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ams_aircraft_curr_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ams_aircraft_curr_update`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_ams_aircraft_curr_update', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_ams_aircraft_curr_update';
set countRows = 0;

SET logNote=CONCAT('Start ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);
	SET SQL_SAFE_UPDATES = 0;
    
	update ams_ac.ams_aircraft_curr ac
	join ams_ac.ams_aircraft a on a.ac_id = ac.ac_id 
	join (
		SELECT ac.ac_id,
        (count(q.fl_id) + count(mp.acmp_id)) as flight_in_queue, 
        round(sum(ifnull(q.flight_h,0)),2) as flight_h_in_queue,
        max(ifnull(q.atime, current_timestamp())) last_flq_arrtime, 
        max(ifnull(q.dtime, current_timestamp())) last_flq_deptime, 
        max(ifnull(mp.end_time, current_timestamp())) as last_mp_atime,
        max(mp.acmp_id) as next_mnt_id,
        max(fl_id) as last_flq_id,
        min(fl_id) as next_flq_id
		FROM ams_ac.ams_aircraft_curr ac
        join ams_ac.ams_aircraft a on a.ac_id = ac.ac_id
		left join ams_ac.ams_al_flight_queue q on q.ac_id = ac.ac_id and q.processed=0
		left join ams_ac.ams_aircraft_maintenance_plan mp on mp.ac_id = ac.ac_id and mp.processed=0
        where a.owner_al_id is not null
		group by  ac.ac_id	
        ) q1 on q1.ac_id = ac.ac_id
		set ac.flight_in_queue = q1.flight_in_queue,
        ac.flight_h_in_queue= q1.flight_h_in_queue, 
        ac.last_flq_atime = if(q1.last_flq_arrtime>q1.last_mp_atime, q1.last_flq_arrtime, q1.last_mp_atime),
        ac.next_mnt_id = q1.next_mnt_id,
        ac.next_flq_id = q1.next_flq_id,
        ac.last_flq_id = q1.last_flq_id
        where a.owner_al_id is not null;
        
        SET countRows =  ROW_COUNT();
		COMMIT;
    
	SET logNote=CONCAT('Updated ',countRows,  ' in ams_ac.ams_aircraft_curr <-' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
   
    
    update ams_ac.ams_aircraft_curr ac
	join ams_ac.ams_aircraft a on a.ac_id = ac.ac_id
	join ams_ac.cfg_mac mc on mc.mac_id = a.mac_id
	join ams_ac.cfg_mac_cabin c on c.cabin_id = a.cabin_id
	join ams_ac.ams_aircraft_ife aife on aife.ac_id = a.ac_id
	left join ams_ac.cfg_aircraft_ife_type f on f.ife_id = aife.f_ife_id
	left join ams_ac.cfg_aircraft_ife_type b on b.ife_id = aife.b_ife_id
	left join ams_ac.cfg_aircraft_ife_type e on e.ife_id = aife.e_ife_id
	set ac.op_time_min = mc.operation_time_min,
	ac.stars = ROUND((if(aife.f_ife_id is null, 0, f.stars) + if(aife.b_ife_id is null, 0, b.stars) + if(aife.e_ife_id is null, 0, e.stars))/
				(if(aife.f_ife_id is null, 0, 1) + if(aife.b_ife_id is null, 0, 1) + if(aife.e_ife_id is null, 0, 1))) ,
	ac.points = (mc.popularity +
		ROUND((if(aife.f_ife_id is null, 0, f.points) + if(aife.b_ife_id is null, 0, b.points) + if(aife.e_ife_id is null, 0, e.points))/
				(if(aife.f_ife_id is null, 0, 1) + if(aife.b_ife_id is null, 0, 1) + if(aife.e_ife_id is null, 0, 1)))) ;          
	
    SET countRows =  ROW_COUNT();
	COMMIT;
	SET logNote=CONCAT('Updated ',countRows,  ' in op_time_min ' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    UPDATE ams_ac.ams_aircraft a
	join ams_ac.cfg_mac_cabin c on c.cabin_id = a.cabin_id
	set a.crew_count = c.seat_crew_count,
	a.pax_f = c.seat_f_count,
	a.pax_b = c.seat_b_count,
	a.pax_e = c.seat_e_count,
	a.cargo_kg = c.cargo_kg;
	
    SET countRows =  ROW_COUNT();
	COMMIT;
	SET logNote=CONCAT('Updated ',countRows,  ' in ams_aircraft ' );
	call ams_wad.log_sp(spname, stTime, logNote);
	
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ams_aircraft_curr_update_ac` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ams_aircraft_curr_update_ac`(IN in_ac_id INT)
BEGIN
DECLARE i_flight_in_queue, i_next_mnt_id, i_next_flq_id, i_last_flq_id, i_next_amt_id INT;
DECLARE d_flight_h_in_queue, d_flight_h_to_next_amt_id DOUBLE;
DECLARE countRows INT;
DECLARE stTime, t_last_flq_atime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_ams_aircraft_curr_update_ac', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_ams_aircraft_curr_update_ac';
set countRows = 0;

SET logNote=CONCAT('Start ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);
	SET SQL_SAFE_UPDATES = 0;
    SELECT (count(q.fl_id) + count(mp.acmp_id)) as flight_in_queue, 
	round(sum(ifnull(q.flight_h,0)),2) as flight_h_in_queue,
    if(max(ifnull(q.atime, current_timestamp()))>max(ifnull(mp.end_time, current_timestamp())), 
    max(ifnull(q.atime, current_timestamp())), max(ifnull(mp.end_time, current_timestamp()))) as last_flq_atime,
	max(mp.acmp_id) as next_mnt_id,
	max(fl_id) as last_flq_id,
	min(fl_id) as next_flq_id,
    (case 
		when (ifnull(ac.next_amt_id,1) = 1) then round((ifnull(a_check_fh,0) - ifnull(flight_hours,0)),2) 
        when (ifnull(ac.next_amt_id,1) = 2) then round((ifnull(b_check_fh,0) - ifnull(flight_hours,0)),2) 
        when (ifnull(ac.next_amt_id,1) = 3) then round((ifnull(c_check_fh,0) - ifnull(flight_hours,0)),2) 
        when (ifnull(ac.next_amt_id,1) = 4) then round((ifnull(d_check_fh,0) - ifnull(flight_hours,0)),2) 
	end) as flight_h_to_next_amt_id
    INTO i_flight_in_queue, d_flight_h_in_queue, t_last_flq_atime, i_next_mnt_id,
    i_last_flq_id, i_next_flq_id, d_flight_h_to_next_amt_id 
	FROM ams_ac.ams_aircraft_curr ac
	left join ams_ac.ams_al_flight_queue q on q.ac_id = ac.ac_id and q.processed=0
	left join ams_ac.ams_aircraft_maintenance_plan mp on mp.ac_id = ac.ac_id and mp.processed=0
	where ac.ac_id = in_ac_id
	group by  ac.ac_id;	
    
	update ams_ac.ams_aircraft_curr ac
	set ac.flight_in_queue = i_flight_in_queue,
	ac.flight_h_in_queue= d_flight_h_in_queue, 
	ac.last_flq_atime = t_last_flq_atime,
	ac.next_mnt_id = i_next_mnt_id,
	ac.next_flq_id = i_next_flq_id,
	ac.last_flq_id = i_last_flq_id,
    ac.flight_h_to_next_amt_id = d_flight_h_to_next_amt_id
	where ac.ac_id = in_ac_id;
        
	SET countRows =  ROW_COUNT();
	COMMIT;
    
	SET logNote=CONCAT('Updated ',countRows,  ' in ams_ac.ams_aircraft_curr <-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
     UPDATE ams_ac.ams_aircraft a
	join ams_ac.cfg_mac_cabin c on c.cabin_id = a.cabin_id
	set a.crew_count = c.seat_crew_count,
	a.pax_f = c.seat_f_count,
	a.pax_b = c.seat_b_count,
	a.pax_e = c.seat_e_count,
	a.cargo_kg = c.cargo_kg
    where a.ac_id = in_ac_id;
	
    SET countRows =  ROW_COUNT();
	COMMIT;
	SET logNote=CONCAT('Updated ',countRows,  ' in ams_aircraft ' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ams_al_flight_plan_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ams_al_flight_plan_update`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_ams_al_flight_plan_update', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_ams_al_flight_plan_update';
set countRows = 0;

SET logNote=CONCAT('Start ->' );
call ams_wad.log_sp(spname, stTime, logNote);
	SET SQL_SAFE_UPDATES = 0;
    
    CREATE TEMPORARY TABLE flight_plan_payload_sum 
    select flp_id, 
		SUM(if(pax is null, 0, if(pax_class_id=1, pax, 0))) as load_pax_e,
		SUM(if(pax is null, 0, if(pax_class_id=2, pax, 0))) as load_pax_b,
		SUM(if(pax is null, 0, if(pax_class_id=3, pax, 0))) as load_pax_f,
		SUM(if(payload_kg is null, 0, if(pax_class_id=4, payload_kg, 0))) as load_cargo_kg,
		Round(SUM(if(price is null, 0, price)),2) as price
	FROM ams_ac.ams_al_flight_plan_payload group by flp_id;
        
	update ams_ac.ams_al_flight_plan flp
	left JOIN flight_plan_payload_sum p ON p.flp_id = flp.flp_id
	set flp.remain_pax_e = (flp.available_pax_e - ifnull(p.load_pax_e, 0)),
	flp.remain_pax_b = (flp.available_pax_b - ifnull(p.load_pax_b, 0)),
	flp.remain_pax_f = (flp.available_pax_f - ifnull(p.load_pax_f,0)),
    flp.payload_kg = p.load_cargo_kg,
    flp.price = p.price,
	flp.remain_payload_kg = Round((flp.available_cargo_kg - ifnull(p.load_cargo_kg,0)),2);
        
	SET countRows =  ROW_COUNT();
	COMMIT;
	
    drop TEMPORARY table IF EXISTS flight_plan_payload_sum;
	SET logNote=CONCAT('updated ',countRows,  ' in ams_al_flight_plan' );
	call ams_wad.log_sp(spname, stTime, logNote);

SET countRows = 0;
select count(flp.flp_id) into countRows
from ams_ac.ams_al_flight_plan flp 
where flp.flp_status_id=1 
and (flp.pax_pct_full > 95 and flp.payload_kg_pct_full > 95);
IF countRows > 0 THEN
    update ams_ac.ams_al_flight_plan flp 
	set flp.flp_status_id = 2
	where flp.flp_status_id=1 
	and (flp.pax_pct_full > 95 and flp.payload_kg_pct_full > 95);
    SET countRows =  ROW_COUNT();
	COMMIT;
    SET logNote=CONCAT(' Sold out ',countRows, ' flights.');
	call ams_wad.log_sp(spname, stTime, logNote);
END IF;

SET countRows = 0;
truncate table ams_ac.ams_al_flight_plan_tmp;
insert into ams_ac.ams_al_flight_plan_tmp select * from ams_ac.ams_al_flight_plan;
SET countRows =  ROW_COUNT();
commit;
SET logNote=CONCAT('Inserted ',countRows,  ' in ams_al_flight_plan_tmp' );
call ams_wad.log_sp(spname, stTime, logNote);

-- SET SQL_SAFE_UPDATES = 1;
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ams_al_flight_plan_update_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ams_al_flight_plan_update_id`(IN in_flp_id INT)
BEGIN

DECLARE countRows, i_idx INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_ams_al_flight_plan_update_id', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_ams_al_flight_plan_update_id';
set countRows = 0;

SET logNote=CONCAT('Start -> in_flp_id:', in_flp_id );
-- call ams_wad.log_sp(spname, stTime, logNote);
	SET SQL_SAFE_UPDATES = 0;
    
    CREATE TEMPORARY TABLE flight_plan_payload_suma as 
    (select flp_id, 
	SUM(if(pax is null, 0, if(pax_class_id=1, pax, 0))) as load_pax_e,
	SUM(if(pax is null, 0, if(pax_class_id=2, pax, 0))) as load_pax_b,
	SUM(if(pax is null, 0, if(pax_class_id=3, pax, 0))) as load_pax_f,
	SUM(if(payload_kg is null, 0, if(pax_class_id=4, payload_kg, 0))) as load_cargo_kg,
	Round(SUM(if(price is null, 0, price)),2) as price
	FROM ams_ac.ams_al_flight_plan_payload where flp_id = in_flp_id group by flp_id for share);
    
    select count(*) INTO countRows from flight_plan_payload_suma;
    -- SET countRows =  ROW_COUNT();
    SET logNote=CONCAT('Selected ',countRows,  ' row in temp table.' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    commit;
    
    set countRows = 0;
    SET i_idx = 0;
    SELECT count(*) INTO countRows
	FROM performance_schema.table_handles 
	WHERE OBJECT_SCHEMA='ams_ac' and OBJECT_NAME='ams_al_flight_plan_payload'
    and (EXTERNAL_LOCK IS NOT NULL or INTERNAL_LOCK IS NOT NULL);
	
    WHILE countRows > 0 DO
		SELECT SLEEP(0.2);
        SET i_idx = i_idx + 1;
        SELECT count(*) INTO countRows
		FROM performance_schema.table_handles 
		WHERE OBJECT_SCHEMA='ams_ac' and OBJECT_NAME='ams_al_flight_plan_payload'
		and (EXTERNAL_LOCK IS NOT NULL or INTERNAL_LOCK IS NOT NULL);
        IF i_idx > 500 THEN
			set countRows = 0;
        END IF;
    END WHILE;
    
    IF i_idx > 0 THEN
		SET logNote=CONCAT(i_idx,' - ', FORMAT((i_idx*0.2)/60,2), ' min wait for unlock ams_ac.ams_al_flight_plan_payload table.');
		call ams_wad.log_sp(spname, stTime, logNote);
    END IF;
    
	update ams_ac.ams_al_flight_plan flp
	JOIN flight_plan_payload_suma p ON p.flp_id = flp.flp_id
	set flp.remain_pax_e = (flp.available_pax_e - ifnull(p.load_pax_e, 0)),
	flp.remain_pax_b = (flp.available_pax_b - ifnull(p.load_pax_b, 0)),
	flp.remain_pax_f = (flp.available_pax_f - ifnull(p.load_pax_f,0)),
	flp.remain_payload_kg = Round((flp.available_cargo_kg - ifnull(p.load_cargo_kg,0)),2),
    flp.price = ifnull(p.price,0)
    where flp.flp_id = in_flp_id;
        
	SET countRows =  ROW_COUNT();
	COMMIT;
    SET logNote=CONCAT('Updated ',countRows,  ' in ams_ac.sp_ams_al_flight_plan <-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    SET countRows = 0;
    select count(*) INTO countRows from ams_ac.ams_al_flight_plan_tmp flp where flp.flp_id = in_flp_id;
	IF countRows > 0 THEN
		update ams_ac.ams_al_flight_plan_tmp flp
		JOIN flight_plan_payload_suma p ON p.flp_id = flp.flp_id
		set flp.remain_pax_e = (flp.available_pax_e - ifnull(p.load_pax_e, 0)),
		flp.remain_pax_b = (flp.available_pax_b - ifnull(p.load_pax_b, 0)),
		flp.remain_pax_f = (flp.available_pax_f - ifnull(p.load_pax_f,0)),
		flp.remain_payload_kg = Round((flp.available_cargo_kg - ifnull(p.load_cargo_kg,0)),2),
		flp.price = ifnull(p.price,0)
		where flp.flp_id = in_flp_id;
        SET countRows =  ROW_COUNT();
		COMMIT;
		SET logNote=CONCAT('Updated ',countRows,  ' in ams_ac.ams_al_flight_plan_tmp <-' );
		-- call ams_wad.log_sp(spname, stTime, logNote);
	END IF;
    
    drop TEMPORARY table IF EXISTS flight_plan_payload_suma;
    commit;
	

SET countRows = 0;
select count(flp.flp_id) into countRows from ams_ac.ams_al_flight_plan flp 
where flp.flp_id = in_flp_id and flp.flp_status_id=1 
and (flp.pax_pct_full > 95 and flp.payload_kg_pct_full > 95) FOR SHARE;

IF countRows > 0 THEN
	SET logNote=CONCAT(' Sold out ',countRows, ' flp_id:', in_flp_id);
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    update ams_ac.ams_al_flight_plan flp set flp.flp_status_id = 2
	where flp.flp_id = in_flp_id and flp.flp_status_id=1 
	and (flp.pax_pct_full > 95 and flp.payload_kg_pct_full > 95);
    SET countRows =  ROW_COUNT();
	COMMIT;
    SET logNote=CONCAT(' Sold out flp_id:', in_flp_id);
	-- call ams_wad.log_sp(spname, stTime, logNote);
    SET countRows = 0;
    select count(*) INTO countRows from ams_ac.ams_al_flight_plan_tmp flp where flp.flp_id = in_flp_id;
	IF countRows > 0 THEN
		update ams_ac.ams_al_flight_plan_tmp flp set flp.flp_status_id = 2
		where flp.flp_id = in_flp_id and flp.flp_status_id=1 
		and (flp.pax_pct_full > 95 and flp.payload_kg_pct_full > 95);
        SET countRows =  ROW_COUNT();
		COMMIT;
	END IF;
END IF;
SET countRows = 0;

SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ams_al_flp_transfer_cpd_quadruple` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ams_al_flp_transfer_cpd_quadruple`(IN in_flp_id INT)
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_ams_al_flp_transfer_cpd_quadruple', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_ams_al_flp_transfer_cpd_quadruple';
set countRows = 0;

SET logNote=CONCAT('Start (in_flp_id:', in_flp_id, ') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

 TRUNCATE TABLE ams_ac.ams_al_flp_transfer_cpd;
 
 INSERT INTO ams_ac.ams_al_flp_transfer_cpd
 SELECT 
        p.cpd_id AS cpdId,
        flp.flp_id AS flpId,
        flp2.flp_id AS flpId2,
        flp3.flp_id AS flpId3,
        flp4.flp_id AS flpId4,
        pd.dep_ap_id AS depApId,
        pd.arr_ap_id AS arrApId,
        pd.distance_km AS distanceKm,
        flp.fl_name AS flName,
        flp2.fl_name AS flName2,
        flp3.fl_name AS flName3,
        flp4.fl_name AS flName4,
        flp.remain_payload_kg AS remainPayloadKg,
        flp2.remain_payload_kg AS remainPayloadKg2,
        flp3.remain_payload_kg AS remainPayloadKg3,
        flp4.remain_payload_kg AS remainPayloadKg4,
        ROUND((p.max_ticket_price - ((((p.payload_kg * flpns.price_c_p_kg) + (p.payload_kg * flpns2.price_c_p_kg)) + (p.payload_kg * flpns3.price_c_p_kg)) + (p.payload_kg * flpns4.price_c_p_kg))),
                2) AS diffPrice,
        ROUND(((((p.payload_kg * flpns.price_c_p_kg) + (p.payload_kg * flpns2.price_c_p_kg)) + (p.payload_kg * flpns3.price_c_p_kg)) + (p.payload_kg * flpns4.price_c_p_kg)),
                2) AS sumPrice,
        ROUND(p.max_ticket_price, 2) AS pPrice,
        p.pax_class_id AS paxClassId,
        0 AS pax,
        p.payload_kg AS payloadKg,
        p.note AS note
    FROM
        ams_al_flight_plan_payload_pct_full flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN v_ams_ac ac ON ((ac.acId = flp.ac_id))
        JOIN ams_al_flight_plan_payload_pct_full flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_al_flight_plan_payload_pct_full flp3 ON ((flp3.d_ap_id = flp2.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
        JOIN ams_al_flight_plan_payload_pct_full flp4 ON ((flp4.d_ap_id = flp3.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns4 ON ((flpns4.flpns_id = flp4.flpns_id))
        JOIN ams_wad.ams_airport_cpd p ON ((p.dep_ap_id = flp.d_ap_id))
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE flp.flp_id = in_flp_id and
        ((flp.flp_status_id = 1)
			AND (flp.remain_payload_kg > 0)
            AND (flp2.remain_payload_kg > 0)
            AND (flp3.remain_payload_kg > 0)
            AND (flp4.remain_payload_kg > 0)
            AND (flp2.flp_status_id = 1)
            AND (flp2.flp_id <> flp.flp_id)
            AND (flp2.a_ap_id <> flp.d_ap_id)
            AND (flp3.flp_status_id = 1)
            AND (flp3.flp_id <> flp2.flp_id)
            AND (flp3.a_ap_id <> flp2.d_ap_id)
            AND (flp3.flp_id <> flp.flp_id)
            AND (flp3.a_ap_id <> flp.d_ap_id)
            AND (flp4.flp_status_id = 1)
            AND (flp4.flp_id <> flp3.flp_id)
            AND (flp4.a_ap_id <> flp3.d_ap_id)
            AND (flp4.flp_id <> flp2.flp_id)
            AND (flp4.a_ap_id <> flp2.d_ap_id)
            AND (flp4.flp_id <> flp.flp_id)
            AND (flp4.a_ap_id <> flp.d_ap_id)
            AND (pd.arr_ap_id = flp4.a_ap_id)
            AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.opTimeMin)
            AND (TIMESTAMPDIFF(MINUTE, flp2.atime, flp3.dtime) > ac.opTimeMin)
            AND (TIMESTAMPDIFF(MINUTE, flp3.atime, flp4.dtime) > ac.opTimeMin));
	commit;
 set countRows = 0;
 select count(*) into countRows from ams_ac.ams_al_flp_transfer_cpd;
 
SET logNote=CONCAT('Found  ',countRows,  ' rows <-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ams_al_flp_transfer_cpd_tripple` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ams_al_flp_transfer_cpd_tripple`(IN in_flp_id INT)
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_ams_al_flp_transfer_cpd_tripple', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_ams_al_flp_transfer_cpd_tripple';
set countRows = 0;

SET logNote=CONCAT('Start (in_flp_id:', in_flp_id, ') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

 TRUNCATE TABLE ams_ac.ams_al_flp_transfer_cpd;
 
 INSERT INTO ams_ac.ams_al_flp_transfer_cpd
 SELECT 
        p.cpd_id AS cpdId,
        flp.flp_id AS flpId,
        flp2.flp_id AS flpId2,
        flp3.flp_id AS flpId3,
        null AS flpId4,
        pd.dep_ap_id AS depApId,
        pd.arr_ap_id AS arrApId,
        pd.distance_km AS distanceKm,
        flp.fl_name AS flName,
        flp2.fl_name AS flName2,
        flp3.fl_name AS flName3,
        null AS flName4,
        flp.remain_payload_kg AS remainPayloadKg,
        flp2.remain_payload_kg AS remainPayloadKg2,
        flp3.remain_payload_kg AS remainPayloadKg3,
        null AS remainPayloadKg4,
        ROUND((p.max_ticket_price - ((((p.payload_kg * flpns.price_c_p_kg) + (p.payload_kg * flpns2.price_c_p_kg)) + (p.payload_kg * flpns3.price_c_p_kg)))),
                2) AS diffPrice,
        ROUND(((((p.payload_kg * flpns.price_c_p_kg) + (p.payload_kg * flpns2.price_c_p_kg)) + (p.payload_kg * flpns3.price_c_p_kg))),
                2) AS sumPrice,
        ROUND(p.max_ticket_price, 2) AS pPrice,
        p.pax_class_id AS paxClassId,
        0 AS pax,
        p.payload_kg AS payloadKg,
        p.note AS note
    FROM
        ams_al_flight_plan_payload_pct_full flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN v_ams_ac ac ON ((ac.acId = flp.ac_id))
        JOIN ams_al_flight_plan_payload_pct_full flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_al_flight_plan_payload_pct_full flp3 ON ((flp3.d_ap_id = flp2.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
        JOIN ams_wad.ams_airport_cpd p ON ((p.dep_ap_id = flp.d_ap_id))
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE flp.flp_id = in_flp_id and
        ((flp.flp_status_id = 1)
			AND (flp.remain_payload_kg > 0)
            AND (flp2.remain_payload_kg > 0)
            AND (flp3.remain_payload_kg > 0)
            AND (flp2.flp_status_id = 1)
            AND (flp2.flp_id <> flp.flp_id)
            AND (flp2.a_ap_id <> flp.d_ap_id)
            AND (flp3.flp_status_id = 1)
            AND (flp3.flp_id <> flp2.flp_id)
            AND (flp3.a_ap_id <> flp2.d_ap_id)
            AND (flp3.flp_id <> flp.flp_id)
            AND (flp3.a_ap_id <> flp.d_ap_id)
            AND (pd.arr_ap_id = flp3.a_ap_id)
            AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.opTimeMin)
            AND (TIMESTAMPDIFF(MINUTE, flp2.atime, flp3.dtime) > ac.opTimeMin));
	commit;
 set countRows = 0;
 select count(*) into countRows from ams_ac.ams_al_flp_transfer_cpd;
 
SET logNote=CONCAT('Found  ',countRows,  ' rows <-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ams_al_flp_transfer_pcpd` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ams_al_flp_transfer_pcpd`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_ams_al_flp_transfer_pcpd', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_ams_al_flp_transfer_pcpd';
set countRows = 0;

SET logNote=CONCAT('Start ', ' ->' );
call ams_wad.log_sp(spname, stTime, logNote);

 TRUNCATE TABLE ams_ac.ams_al_flp_transfer_pcpd;
 commit;
 
	 insert into ams_ac.ams_al_flp_transfer_pcpd 
		(ppdId, flpId, flpId2, 
		depApId, arrApId, distanceKm, flName, flName2, 
		remainPax, remainPax2,
		diffPrice, sumPrice, pPrice, paxClassId, pax, payloadKg, note) 
	SELECT 
        p.ppd_id AS ppdId,
        flp.flp_id AS flpId,
        flp2.flp_id AS flpId2,
        pd.dep_ap_id AS depApId,
        pd.arr_ap_id AS arrApId,
        pd.distance_km AS distanceKm,
        flp.fl_name AS flName,
        flp2.fl_name AS flName2,
        (((ifnull(flp.available_pax_e,0) + ifnull(flp.available_pax_b,0) + ifnull(flp.available_pax_f,0)) - ifnull(flp.pax,0)) ) AS remainPax,
        (((ifnull(flp2.available_pax_e,0) + ifnull(flp2.available_pax_b,0) + ifnull(flp2.available_pax_f,0)) - ifnull(flp2.pax,0)) ) AS remainPax2,
        ROUND((p.max_ticket_price - (
			(p.pax * flpns.price_e) 
            + (p.pax * flpns2.price_e) 
            )), 2) AS diffPrice,
        ROUND((
			(p.pax * flpns.price_e) 
            + (p.pax * flpns2.price_e) 
            ), 2) AS sumPrice,
        ROUND(p.max_ticket_price, 2) AS pPrice,
        p.pax_class_id AS paxClassId,
        p.pax AS pax,
        p.payload_kg AS payloadKg,
        p.note AS note
    FROM ams_ac.ams_al_flight_plan flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_aircraft_curr ac ON ac.ac_id = flp.ac_id
        JOIN ams_wad.ams_airport_ppd p ON p.dep_ap_id = flp.d_ap_id
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND (pd.arr_ap_id = flp2.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.op_time_min);
	commit;
SET countRows =  ROW_COUNT();
 
SET logNote=CONCAT('Inserted  ',countRows,  ' rows double transfer ppd <-' );
call ams_wad.log_sp(spname, stTime, logNote);

	insert into ams_ac.ams_al_flp_transfer_pcpd 
		(ppdId, flpId, flpId2, flpId3,
		depApId, arrApId, distanceKm, flName, flName2, flName3,
		remainPax, remainPax2,remainPax3, 
		diffPrice, sumPrice, pPrice, paxClassId, pax, payloadKg, note) 
	SELECT 
        p.ppd_id AS ppdId,
        flp.flp_id AS flpId,
        flp2.flp_id AS flpId2,
        flp3.flp_id AS flpId3,
        pd.dep_ap_id AS depApId,
        pd.arr_ap_id AS arrApId,
        pd.distance_km AS distanceKm,
        flp.fl_name AS flName,
        flp2.fl_name AS flName2,
        flp3.fl_name AS flName3,
        (((ifnull(flp.available_pax_e,0) + ifnull(flp.available_pax_b,0) + ifnull(flp.available_pax_f,0)) - ifnull(flp.pax,0)) ) AS remainPax,
        (((ifnull(flp2.available_pax_e,0) + ifnull(flp2.available_pax_b,0) + ifnull(flp2.available_pax_f,0)) - ifnull(flp2.pax,0)) ) AS remainPax2,
        (((ifnull(flp3.available_pax_e,0) + ifnull(flp3.available_pax_b,0) + ifnull(flp3.available_pax_f,0)) - ifnull(flp3.pax,0)) ) AS remainPax3,
        ROUND((p.max_ticket_price - (
			(p.pax * flpns.price_e) 
            + (p.pax * flpns2.price_e) 
            + (p.pax * flpns3.price_e) 
            )), 2) AS diffPrice,
        ROUND((
			(p.pax * flpns.price_e) 
            + (p.pax * flpns2.price_e) 
            + (p.pax * flpns3.price_e) 
            ), 2) AS sumPrice,
        ROUND(p.max_ticket_price, 2) AS pPrice,
        p.pax_class_id AS paxClassId,
        p.pax AS pax,
        p.payload_kg AS payloadKg,
        p.note AS note
    FROM ams_ac.ams_al_flight_plan flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp3 ON ((flp3.d_ap_id = flp2.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
        JOIN ams_aircraft_curr ac ON ac.ac_id = flp.ac_id
        JOIN ams_wad.ams_airport_ppd p ON p.dep_ap_id = flp.d_ap_id
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp3.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND flp3.a_ap_id <> flp2.d_ap_id
		AND flp3.a_ap_id <> flp.d_ap_id
		AND (pd.arr_ap_id = flp3.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.op_time_min)
		AND (TIMESTAMPDIFF(MINUTE, flp2.atime, flp3.dtime) > ac.op_time_min);
            
SET countRows =  ROW_COUNT();
commit; 
SET logNote=CONCAT('Inserted  ',countRows,  ' rows tripple transfer ppd <-' );
call ams_wad.log_sp(spname, stTime, logNote);

	insert into ams_ac.ams_al_flp_transfer_pcpd 
		(ppdId, flpId, flpId2, flpId3, flpId4,
		depApId, arrApId, distanceKm, flName, flName2, flName3,flName4,
		remainPax, remainPax2,remainPax3, remainPax4, 
		diffPrice, sumPrice, pPrice, paxClassId, pax, payloadKg, note) 
	SELECT 
        p.ppd_id AS ppdId,
        flp.flp_id AS flpId,
        flp2.flp_id AS flpId2,
        flp3.flp_id AS flpId3,
        flp4.flp_id AS flpId4,
        pd.dep_ap_id AS depApId,
        pd.arr_ap_id AS arrApId,
        pd.distance_km AS distanceKm,
        flp.fl_name AS flName,
        flp2.fl_name AS flName2,
        flp3.fl_name AS flName3,
        flp4.fl_name AS flName4,
        (((ifnull(flp.available_pax_e,0) + ifnull(flp.available_pax_b,0) + ifnull(flp.available_pax_f,0)) - ifnull(flp.pax,0)) ) AS remainPax,
        (((ifnull(flp2.available_pax_e,0) + ifnull(flp2.available_pax_b,0) + ifnull(flp2.available_pax_f,0)) - ifnull(flp2.pax,0)) ) AS remainPax2,
        (((ifnull(flp3.available_pax_e,0) + ifnull(flp3.available_pax_b,0) + ifnull(flp3.available_pax_f,0)) - ifnull(flp3.pax,0)) ) AS remainPax3,
        (((ifnull(flp4.available_pax_e,0) + ifnull(flp4.available_pax_b,0) + ifnull(flp4.available_pax_f,0)) - ifnull(flp4.pax,0)) ) AS remainPax4,
        ROUND((p.max_ticket_price - (
			(p.pax * flpns.price_e) 
            + (p.pax * flpns2.price_e) 
            + (p.pax * flpns3.price_e) 
            + (p.pax * flpns4.price_e) 
            )), 2) AS diffPrice,
        ROUND((
			(p.pax * flpns.price_e) 
            + (p.pax * flpns2.price_e) 
            + (p.pax * flpns3.price_e) 
            + (p.pax * flpns4.price_e) 
            ), 2) AS sumPrice,
        ROUND(p.max_ticket_price, 2) AS pPrice,
        p.pax_class_id AS paxClassId,
        p.pax AS pax,
        p.payload_kg AS payloadKg,
        p.note AS note
    FROM ams_ac.ams_al_flight_plan flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp3 ON ((flp3.d_ap_id = flp2.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp4 ON ((flp4.d_ap_id = flp3.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns4 ON ((flpns4.flpns_id = flp4.flpns_id))
        JOIN ams_aircraft_curr ac ON ac.ac_id = flp.ac_id
        JOIN ams_wad.ams_airport_ppd p ON p.dep_ap_id = flp.d_ap_id
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp3.flp_status_id = 1
		AND flp4.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND flp3.a_ap_id <> flp2.d_ap_id
		AND flp3.a_ap_id <> flp.d_ap_id
		AND flp4.a_ap_id <> flp3.d_ap_id
		AND flp4.a_ap_id <> flp2.d_ap_id
		AND flp4.a_ap_id <> flp.d_ap_id
		AND (pd.arr_ap_id = flp4.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.op_time_min)
		AND (TIMESTAMPDIFF(MINUTE, flp2.atime, flp3.dtime) > ac.op_time_min)
		AND (TIMESTAMPDIFF(MINUTE, flp3.atime, flp4.dtime) > ac.op_time_min);
        
SET countRows =  ROW_COUNT();
commit; 
SET logNote=CONCAT('Inserted  ',countRows,  ' rows tripple quadruple ppd <-' );
call ams_wad.log_sp(spname, stTime, logNote);

insert into ams_ac.ams_al_flp_transfer_pcpd 
		(cpdId, flpId, flpId2, 
		depApId, arrApId, distanceKm, flName, flName2, 
		remainPayloadKg, remainPayloadKg2,
		diffPrice, sumPrice, pPrice, paxClassId, pax, payloadKg, note) 
SELECT 
        p.cpd_id AS cpdId,
        flp.flp_id AS flpId,
        flp2.flp_id AS flpId2,
        pd.dep_ap_id AS depApId,
        pd.arr_ap_id AS arrApId,
        pd.distance_km AS distanceKm,
        flp.fl_name AS flName,
        flp2.fl_name AS flName2,
        ((ifnull(flp.available_cargo_kg,0) - flp.payload_kg)) AS remainPayloadKg,
        ((ifnull(flp2.available_cargo_kg,0) - flp2.payload_kg)) AS remainPayloadKg2,
        ROUND((p.max_ticket_price - (
			(p.payload_kg * flpns.price_c_p_kg) 
            + (p.payload_kg * flpns2.price_c_p_kg) 
            )), 2) AS diffPrice,
        ROUND((
			(p.payload_kg * flpns.price_c_p_kg) 
            + (p.payload_kg * flpns2.price_c_p_kg) 
            ), 2) AS sumPrice,
        ROUND(p.max_ticket_price, 2) AS pPrice,
        p.pax_class_id AS paxClassId,
        0 AS pax,
        p.payload_kg AS payloadKg,
        p.note AS note
    FROM ams_ac.ams_al_flight_plan flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_aircraft_curr ac ON ac.ac_id = flp.ac_id
        JOIN ams_wad.ams_airport_cpd p ON p.dep_ap_id = flp.d_ap_id
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
		AND (pd.arr_ap_id = flp2.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.op_time_min);
SET countRows =  ROW_COUNT();
commit; 
SET logNote=CONCAT('Inserted  ',countRows,  ' rows double cpd <-' );
call ams_wad.log_sp(spname, stTime, logNote);

	insert into ams_ac.ams_al_flp_transfer_pcpd 
		(cpdId, flpId, flpId2, 
		depApId, arrApId, distanceKm, flName, flName2, 
		remainPayloadKg, remainPayloadKg2, remainPayloadKg3,
		diffPrice, sumPrice, pPrice, paxClassId, pax, payloadKg, note) 
	SELECT 
        p.cpd_id AS cpdId,
        flp.flp_id AS flpId,
        flp2.flp_id AS flpId2,
        pd.dep_ap_id AS depApId,
        pd.arr_ap_id AS arrApId,
        pd.distance_km AS distanceKm,
        flp.fl_name AS flName,
        flp2.fl_name AS flName2,
        ((ifnull(flp.available_cargo_kg,0) - flp.payload_kg)) AS remainPayloadKg,
        ((ifnull(flp2.available_cargo_kg,0) - flp2.payload_kg)) AS remainPayloadKg2,
        ((ifnull(flp3.available_cargo_kg,0) - flp3.payload_kg)) AS remainPayloadKg3,
        ROUND((p.max_ticket_price - (
			(p.payload_kg * flpns.price_c_p_kg) 
            + (p.payload_kg * flpns2.price_c_p_kg) 
            + (p.payload_kg * flpns3.price_c_p_kg) 
            )), 2) AS diffPrice,
        ROUND((
			(p.payload_kg * flpns.price_c_p_kg) 
            + (p.payload_kg * flpns2.price_c_p_kg) 
            + (p.payload_kg * flpns3.price_c_p_kg) 
            ), 2) AS sumPrice,
        ROUND(p.max_ticket_price, 2) AS pPrice,
        p.pax_class_id AS paxClassId,
        0 AS pax,
        p.payload_kg AS payloadKg,
        p.note AS note
    FROM ams_ac.ams_al_flight_plan flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp3 ON ((flp3.d_ap_id = flp2.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
        JOIN ams_aircraft_curr ac ON ac.ac_id = flp.ac_id
        JOIN ams_wad.ams_airport_cpd p ON p.dep_ap_id = flp.d_ap_id
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
        AND flp3.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
        AND flp3.a_ap_id <> flp2.d_ap_id
		AND (pd.arr_ap_id = flp3.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.op_time_min)
        AND (TIMESTAMPDIFF(MINUTE, flp2.atime, flp3.dtime) > ac.op_time_min);
SET countRows =  ROW_COUNT();
commit; 
SET logNote=CONCAT('Inserted  ',countRows,  ' rows tripple cpd <-' );
call ams_wad.log_sp(spname, stTime, logNote);

	insert into ams_ac.ams_al_flp_transfer_pcpd 
		(cpdId, flpId, flpId2, 
		depApId, arrApId, distanceKm, flName, flName2, 
		remainPayloadKg, remainPayloadKg2, remainPayloadKg3, remainPayloadKg4,
		diffPrice, sumPrice, pPrice, paxClassId, pax, payloadKg, note) 
	SELECT 
        p.cpd_id AS cpdId,
        flp.flp_id AS flpId,
        flp2.flp_id AS flpId2,
        pd.dep_ap_id AS depApId,
        pd.arr_ap_id AS arrApId,
        pd.distance_km AS distanceKm,
        flp.fl_name AS flName,
        flp2.fl_name AS flName2,
        ((ifnull(flp.available_cargo_kg,0) - flp.payload_kg)) AS remainPayloadKg,
        ((ifnull(flp2.available_cargo_kg,0) - flp2.payload_kg)) AS remainPayloadKg2,
        ((ifnull(flp3.available_cargo_kg,0) - flp3.payload_kg)) AS remainPayloadKg3,
        ((ifnull(flp4.available_cargo_kg,0) - flp4.payload_kg)) AS remainPayloadKg4,
        ROUND((p.max_ticket_price - (
			(p.payload_kg * flpns.price_c_p_kg) 
            + (p.payload_kg * flpns2.price_c_p_kg) 
            + (p.payload_kg * flpns3.price_c_p_kg) 
			+ (p.payload_kg * flpns4.price_c_p_kg) 
            )), 2) AS diffPrice,
        ROUND((
			(p.payload_kg * flpns.price_c_p_kg) 
            + (p.payload_kg * flpns2.price_c_p_kg) 
            + (p.payload_kg * flpns3.price_c_p_kg) 
            + (p.payload_kg * flpns4.price_c_p_kg) 
            ), 2) AS sumPrice,
        ROUND(p.max_ticket_price, 2) AS pPrice,
        p.pax_class_id AS paxClassId,
        0 AS pax,
        p.payload_kg AS payloadKg,
        p.note AS note
    FROM ams_ac.ams_al_flight_plan flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_ac.ams_al_flight_plan flp3 ON ((flp3.d_ap_id = flp2.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
         JOIN ams_ac.ams_al_flight_plan flp4 ON ((flp4.d_ap_id = flp3.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns4 ON ((flpns4.flpns_id = flp4.flpns_id))
        JOIN ams_aircraft_curr ac ON ac.ac_id = flp.ac_id
        JOIN ams_wad.ams_airport_cpd p ON p.dep_ap_id = flp.d_ap_id
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE  
		 flp.flp_status_id = 1
		AND flp2.flp_status_id = 1
        AND flp3.flp_status_id = 1
        AND flp4.flp_status_id = 1
		AND flp2.a_ap_id <> flp.d_ap_id
        AND flp3.a_ap_id <> flp2.d_ap_id
		AND flp4.a_ap_id <> flp3.d_ap_id
		AND (pd.arr_ap_id = flp4.a_ap_id)
		AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.op_time_min)
        AND (TIMESTAMPDIFF(MINUTE, flp2.atime, flp3.dtime) > ac.op_time_min)
        AND (TIMESTAMPDIFF(MINUTE, flp3.atime, flp4.dtime) > ac.op_time_min);
SET logNote=CONCAT('Finish ', ' <- ' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows =  ROW_COUNT();
commit; 
SET logNote=CONCAT('Inserted  ',countRows,  ' rows quadropol cpd <-' );
call ams_wad.log_sp(spname, stTime, logNote);

SELECT count(*) as totalRows from ams_ac.ams_al_flp_transfer_pcpd ;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ams_al_flp_transfer_ppd_quadruple` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ams_al_flp_transfer_ppd_quadruple`(IN in_flp_id INT)
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_ams_al_flp_transfer_ppd_quadruple', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_ams_al_flp_transfer_ppd_quadruple';
set countRows = 0;

SET logNote=CONCAT('Start (in_flp_id:', in_flp_id, ') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

 TRUNCATE TABLE ams_ac.ams_al_flp_transfer_ppd;
 
 INSERT INTO ams_ac.ams_al_flp_transfer_ppd
 SELECT 
        p.ppd_id AS ppdId,
        flp.flp_id AS flpId,
        flp2.flp_id AS flpId2,
        flp3.flp_id AS flpId3,
        flp4.flp_id AS flpId4,
        pd.dep_ap_id AS depApId,
        pd.arr_ap_id AS arrApId,
        pd.distance_km AS distanceKm,
        flp.fl_name AS flName,
        flp2.fl_name AS flName2,
        flp3.fl_name AS flName3,
        flp4.fl_name AS flName4,
        flp.remain_pax_e AS remainPaxE,
        flp2.remain_pax_e AS remainPaxE2,
        flp3.remain_pax_e AS remainPaxE3,
        flp4.remain_pax_e AS remainPaxE4,
        ROUND((p.max_ticket_price - ((((p.pax * flpns.price_e) + (p.pax * flpns2.price_e)) + (p.pax * flpns3.price_e)) + (p.pax * flpns4.price_e))),
                2) AS diffPrice,
        ROUND(((((p.pax * flpns.price_e) + (p.pax * flpns2.price_e)) + (p.pax * flpns3.price_e)) + (p.pax * flpns4.price_e)),
                2) AS sumPrice,
        ROUND(p.max_ticket_price, 2) AS pPrice,
        p.pax_class_id AS paxClassId,
        p.pax AS pax,
        p.payload_kg AS payloadKg,
        p.note AS note
    FROM
        ams_al_flight_plan_payload_pct_full flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN v_ams_ac ac ON ((ac.acId = flp.ac_id))
        JOIN ams_al_flight_plan_payload_pct_full flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_al_flight_plan_payload_pct_full flp3 ON ((flp3.d_ap_id = flp2.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
        JOIN ams_al_flight_plan_payload_pct_full flp4 ON ((flp4.d_ap_id = flp3.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns4 ON ((flpns4.flpns_id = flp4.flpns_id))
        JOIN ams_wad.ams_airport_ppd p ON ((p.dep_ap_id = flp.d_ap_id))
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE flp.flp_id = in_flp_id and
        ((flp.flp_status_id = 1)
            AND (flp2.remain_pax_e > 0)
            AND (flp3.remain_pax_e > 0)
            AND (flp4.remain_pax_e > 0)
            AND (flp2.flp_status_id = 1)
            AND (flp2.flp_id <> flp.flp_id)
            AND (flp2.a_ap_id <> flp.d_ap_id)
            AND (flp3.flp_status_id = 1)
            AND (flp3.flp_id <> flp2.flp_id)
            AND (flp3.a_ap_id <> flp2.d_ap_id)
            AND (flp3.flp_id <> flp.flp_id)
            AND (flp3.a_ap_id <> flp.d_ap_id)
            AND (flp4.flp_status_id = 1)
            AND (flp4.flp_id <> flp3.flp_id)
            AND (flp4.a_ap_id <> flp3.d_ap_id)
            AND (flp4.flp_id <> flp2.flp_id)
            AND (flp4.a_ap_id <> flp2.d_ap_id)
            AND (flp4.flp_id <> flp.flp_id)
            AND (flp4.a_ap_id <> flp.d_ap_id)
            AND (pd.arr_ap_id = flp4.a_ap_id)
            AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.opTimeMin)
            AND (TIMESTAMPDIFF(MINUTE, flp2.atime, flp3.dtime) > ac.opTimeMin)
            AND (TIMESTAMPDIFF(MINUTE, flp3.atime, flp4.dtime) > ac.opTimeMin));
	commit;
 set countRows = 0;
 select count(*) into countRows from ams_ac.ams_al_flp_transfer_ppd;
 
SET logNote=CONCAT('Found  ',countRows,  ' rows <-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ams_al_flp_transfer_ppd_tripple` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ams_al_flp_transfer_ppd_tripple`(IN in_flp_id INT)
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_ams_al_flp_transfer_ppd_tripple', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_ams_al_flp_transfer_ppd_tripple';
set countRows = 0;

SET logNote=CONCAT('Start (in_flp_id:', in_flp_id, ') ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

 TRUNCATE TABLE ams_ac.ams_al_flp_transfer_ppd;
 
 INSERT INTO ams_ac.ams_al_flp_transfer_ppd
 SELECT 
        p.ppd_id AS ppdId,
        flp.flp_id AS flpId,
        flp2.flp_id AS flpId2,
        flp3.flp_id AS flpId3,
        null AS flpId4,
        pd.dep_ap_id AS depApId,
        pd.arr_ap_id AS arrApId,
        pd.distance_km AS distanceKm,
        flp.fl_name AS flName,
        flp2.fl_name AS flName2,
        flp3.fl_name AS flName3,
        null AS flName4,
        flp.remain_pax_e AS remainPaxE,
        flp2.remain_pax_e AS remainPaxE2,
        flp3.remain_pax_e AS remainPaxE3,
        null AS remainPaxE4,
        ROUND((p.max_ticket_price - ((p.pax * flpns.price_e) + (p.pax * flpns2.price_e)) + (p.pax * flpns3.price_e)),
                2) AS diffPrice,
        ROUND(((((p.pax * flpns.price_e) + (p.pax * flpns2.price_e)) + (p.pax * flpns3.price_e)) ),
                2) AS sumPrice,
        ROUND(p.max_ticket_price, 2) AS pPrice,
        p.pax_class_id AS paxClassId,
        p.pax AS pax,
        p.payload_kg AS payloadKg,
        p.note AS note
    FROM
        ams_al_flight_plan_payload_pct_full flp
        JOIN ams_al.ams_al_flp_number_schedule flpns ON ((flpns.flpns_id = flp.flpns_id))
        JOIN v_ams_ac ac ON ((ac.acId = flp.ac_id))
        JOIN ams_al_flight_plan_payload_pct_full flp2 ON ((flp2.d_ap_id = flp.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns2 ON ((flpns2.flpns_id = flp2.flpns_id))
        JOIN ams_al_flight_plan_payload_pct_full flp3 ON ((flp3.d_ap_id = flp2.a_ap_id))
        JOIN ams_al.ams_al_flp_number_schedule flpns3 ON ((flpns3.flpns_id = flp3.flpns_id))
        JOIN ams_wad.ams_airport_ppd p ON ((p.dep_ap_id = flp.d_ap_id))
        JOIN ams_wad.cfg_airport_destination pd ON ((pd.dest_id = p.dest_id))
    WHERE flp.flp_id = in_flp_id and
			((flp.flp_status_id = 1)
			AND (flp.remain_pax_e > 0)
            AND (flp2.remain_pax_e > 0)
            AND (flp3.remain_pax_e > 0)
            AND (flp2.flp_status_id = 1)
            AND (flp2.flp_id <> flp.flp_id)
            AND (flp2.a_ap_id <> flp.d_ap_id)
            AND (flp3.flp_status_id = 1)
            AND (flp3.flp_id <> flp2.flp_id)
            AND (flp3.a_ap_id <> flp2.d_ap_id)
            AND (flp3.flp_id <> flp.flp_id)
            AND (flp3.a_ap_id <> flp.d_ap_id)
            AND (pd.arr_ap_id = flp3.a_ap_id)
            AND (TIMESTAMPDIFF(MINUTE, flp.atime, flp2.dtime) > ac.opTimeMin)
            AND (TIMESTAMPDIFF(MINUTE, flp2.atime, flp3.dtime) > ac.opTimeMin));
	commit;
 set countRows = 0;
 select count(*) into countRows from ams_ac.ams_al_flp_transfer_ppd;
 
SET logNote=CONCAT('Found  ',countRows,  ' rows <-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_calc_ams_flight_plan_payload_sum_class` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_calc_ams_flight_plan_payload_sum_class`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_calc_ams_flight_plan_payload_sum_class', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_calc_ams_flight_plan_payload_sum_class';
set countRows = 0;

SET logNote=CONCAT('Start ', ' ->' );
call ams_wad.log_sp(spname, stTime, logNote);

	drop table if exists ams_ac.ams_flight_plan_payload_sum_class_new;
    commit;
    
	CREATE TABLE ams_ac.ams_flight_plan_payload_sum_class_new (
	  `transfer_id` int NOT NULL DEFAULT '0',
	  `pax_class_id` int DEFAULT NULL,
	  `d_ap_id` int DEFAULT NULL,
	  `a_ap_id` int DEFAULT NULL,
	  `dest_id` int DEFAULT NULL,
	  `distance_km` double DEFAULT NULL,
	  `flp_id_1` int DEFAULT NULL,
	  `flp_id_2` int DEFAULT NULL,
	  `flp_id_3` int DEFAULT NULL,
	  `flp_id_4` int DEFAULT NULL,
	  `fl_name_1` varchar(512) DEFAULT NULL,
	  `fl_name_2` varchar(512) DEFAULT NULL,
	  `fl_name_3` varchar(512) DEFAULT NULL,
	  `fl_name_4` varchar(512) DEFAULT NULL,
	  `payload` double NOT NULL DEFAULT '0',
	  `ppd_price` double NOT NULL DEFAULT '0',
	  `ppd_note` varchar(512) NOT NULL DEFAULT '',
	  `ppd_id` int DEFAULT NULL,
	  `cpd_id` int DEFAULT NULL,
	  `id` int NOT NULL AUTO_INCREMENT,
	  PRIMARY KEY (`id`),
	  KEY `IDX_DEST_ID` (`dest_id`),
	  KEY `IDX_FLP1` (`flp_id_1`),
	  KEY `IDX_FLP2` (`flp_id_2`),
	  KEY `IDX_FLP3` (`flp_id_3`),
	  KEY `IDX_FLP4` (`flp_id_4`),
	  KEY `IDX_PAX_CLASS` (`pax_class_id`),
	  KEY `IDX_TRANSFERID` (`transfer_id`,`pax_class_id`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
	commit;
    
		insert into ams_ac.ams_flight_plan_payload_sum_class_new 
		(transfer_id, pax_class_id, d_ap_id, a_ap_id, dest_id, distance_km,
		flp_id_1, flp_id_2, flp_id_3, flp_id_4, 
		fl_name_1, fl_name_2, fl_name_3, fl_name_4, 
		payload, ppd_price, ppd_note, ppd_id) 
		( select  tr.transfer_id, flp1.pax_class_id,
				tr.d_ap_id, tr.a_ap_id, tr.dest_id, tr.distance_km, 
				tr.flp_id_1, tr.flp_id_2, tr.flp_id_3, tr.flp_id_4,
				tr.fl_name_1, tr.fl_name_2,tr.fl_name_3,tr.fl_name_4,
				IFNULL(p.pax, 0) AS payload,
				ROUND(IFNULL(p.max_ticket_price, 0),2) AS ppd_price, 
				IFNULL(p.note, 0) AS ppd_note,
				p.ppd_id
		FROM ams_ac.ams_al_flight_plan_transfer tr
		join ams_ac.v_ams_flight_plan_payload_sum_class flp1 on flp1.flp_id = tr.flp_id_1
		join ams_ac.v_ams_flight_plan_payload_sum_class flp2 on flp2.flp_id = tr.flp_id_2 and flp2.pax_class_id = flp1.pax_class_id
		left join ams_ac.v_ams_flight_plan_payload_sum_class flp3 on flp3.flp_id = tr.flp_id_3 and flp3.pax_class_id = flp1.pax_class_id
		left join ams_ac.v_ams_flight_plan_payload_sum_class flp4 on flp4.flp_id = tr.flp_id_4 and flp4.pax_class_id = flp1.pax_class_id
		join ams_ac.ams_airport_ppd p on p.dest_id = tr.dest_id and p.pax_class_id = flp1.pax_class_id for share);
		commit;
        
        insert into ams_ac.ams_flight_plan_payload_sum_class_new 
			(transfer_id, pax_class_id, d_ap_id, a_ap_id, dest_id, distance_km,
			flp_id_1, flp_id_2, flp_id_3, flp_id_4, 
			fl_name_1, fl_name_2, fl_name_3, fl_name_4, 
			payload, ppd_price, ppd_note, cpd_id) 
        (select  tr.transfer_id, flp1.pax_class_id,
			tr.d_ap_id, tr.a_ap_id, tr.dest_id, tr.distance_km, 
			tr.flp_id_1, tr.flp_id_2, tr.flp_id_3, tr.flp_id_4,
			tr.fl_name_1, tr.fl_name_2,tr.fl_name_3,tr.fl_name_4,
			IFNULL(p.payload_kg, 0) AS payload,
			ROUND(IFNULL(p.max_ticket_price, 0),2) AS ppd_price, 
			IFNULL(p.note, 0) AS ppd_note,
			p.cpd_id
	FROM ams_ac.ams_al_flight_plan_transfer tr
	join ams_ac.v_ams_flight_plan_payload_sum_class flp1 on flp1.flp_id = tr.flp_id_1
	join ams_ac.v_ams_flight_plan_payload_sum_class flp2 on flp2.flp_id = tr.flp_id_2 and flp2.pax_class_id = flp1.pax_class_id
	left join ams_ac.v_ams_flight_plan_payload_sum_class flp3 on flp3.flp_id = tr.flp_id_3 and flp3.pax_class_id = flp1.pax_class_id
	left join ams_ac.v_ams_flight_plan_payload_sum_class flp4 on flp4.flp_id = tr.flp_id_4 and flp4.pax_class_id = flp1.pax_class_id
	join ams_ac.ams_airport_cpd p on p.dest_id = tr.dest_id and p.pax_class_id = flp1.pax_class_id for share);
	SET countRows =  ROW_COUNT();
    commit;
 
	SET logNote=CONCAT('Inserted  ',format(countRows,0),  ' PPD rows.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    /*
	 insert into ams_ac.ams_flight_plan_payload_sum_class_new 
		(transfer_id, pax_class_id, d_ap_id, a_ap_id, dest_id, distance_km,
		flp_id_1, flp_id_2, flp_id_3, flp_id_4, 
		fl_name_1, fl_name_2, fl_name_3, fl_name_4, 
		payload, ppd_price, ppd_note, ppd_id, cpd_id) 
	(SELECT tr.transfer_id, flp1.pax_class_id,
		tr.d_ap_id, tr.a_ap_id, tr.dest_id, tr.distance_km, 
		tr.flp_id_1, tr.flp_id_2, tr.flp_id_3, tr.flp_id_4,
		tr.fl_name_1, tr.fl_name_2,tr.fl_name_3,tr.fl_name_4,
         IF((flp1.pax_class_id = 4),
            IFNULL(c.payload_kg, 0),
            IFNULL(p.pax, 0)) AS payload,
        ROUND(IF((flp1.pax_class_id = 4),
            IFNULL(c.max_ticket_price, 0),
            IFNULL(p.max_ticket_price, 0)),2) AS ppd_price, 
            IF((flp1.pax_class_id = 4), IFNULL(c.note, 0), IFNULL(p.note, 0)) AS ppd_note,
            p.ppd_id, c.cpd_id
		FROM ams_ac.ams_al_flight_plan_transfer tr
		left join ams_ac.v_ams_flight_plan_payload_sum_class flp1 on flp1.flp_id = tr.flp_id_1
		left join ams_ac.v_ams_flight_plan_payload_sum_class flp2 on flp2.flp_id = tr.flp_id_2 and flp2.pax_class_id = flp1.pax_class_id
		left join ams_ac.v_ams_flight_plan_payload_sum_class flp3 on flp3.flp_id = tr.flp_id_3 and flp3.pax_class_id = flp1.pax_class_id
		left join ams_ac.v_ams_flight_plan_payload_sum_class flp4 on flp4.flp_id = tr.flp_id_4 and flp4.pax_class_id = flp1.pax_class_id
		left join ams_ac.ams_airport_ppd p on p.dest_id = tr.dest_id and p.pax_class_id = flp1.pax_class_id
		left join ams_ac.ams_airport_cpd c on c.dest_id = tr.dest_id and c.pax_class_id = flp1.pax_class_id FOR SHARE);
	*/
	SET countRows =  ROW_COUNT();
    commit;
 
	SET logNote=CONCAT('Inserted  ',format(countRows,0),  'CPD rows.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    RENAME TABLE ams_flight_plan_payload_sum_class TO ams_flight_plan_payload_sum_class_old, ams_flight_plan_payload_sum_class_new TO ams_flight_plan_payload_sum_class;
	commit;
    SET logNote=CONCAT('Renamed new table',  '.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	SELECT count(*) as totalRows from ams_ac.ams_flight_plan_payload_sum_class;
    
    drop table if exists ams_ac.ams_flight_plan_payload_sum_class_old;
    commit;
	SET logNote=CONCAT('Dropped old table',  '.' );
	call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ev_mac_produce` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ev_mac_produce`()
BEGIN

DECLARE countRows, vmac_id INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote, sqlScr VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;
DECLARE cursor_tr CURSOR FOR SELECT m.mac_id
					FROM ams_ac.cfg_mac m
					where m.ams_status_id = 4 and ap_id is not null 
					  and m.production_start < now() 
  and ((UNIX_TIMESTAMP(current_timestamp()) - if(m.last_produced_on is null, 0, UNIX_TIMESTAMP(m.last_produced_on)))/(60*60)) > m.production_rate_h;
                        
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('sp_ev_mac_produce', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'sp_ev_mac_produce';
set countRows = 0;

SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_tr;

read_loop: LOOP
			FETCH cursor_tr INTO vmac_id;
		
        IF done THEN
			SET logNote=CONCAT('No mac for production found ', 'done' );
			call ams_wad.log_sp(spname, stTime, logNote);
			LEAVE read_loop;
		END IF;
        
        SET logNote=CONCAT('Manufacturer aircraft vmac_id: ',vmac_id,  ' produce ...');
		call ams_wad.log_sp(spname, stTime, logNote);
        
        CALL sp_mac_produce(vmac_id);
        
        UPDATE ams_ac.cfg_mac m set m.last_produced_on = now(), 
			m.number_build = m.number_build+1
            where m.mac_id=vmac_id;
            
		SET countRows = countRows+1;
		
     END LOOP;

	CLOSE cursor_tr;
    COMMIT;
    SET logNote=CONCAT('Produced ', countRows, ' manufacturer aircrafts.');
	call ams_wad.log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_flight` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_flight`()
BEGIN

DECLARE i_fl_id, i_fl_log_id,  i_acfs_id, i_fl_type_id, i_ac_id INT;
DECLARE next_fl_id, next_fl_type_id, is_on_next_d_ap, next_d_ap_id, o_tr_fl_id INT;
DECLARE i_fl_time_left_sec, i_fl_time_min, i_fl_distance_km INT;
DECLARE countRows, i_rows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote, s_descript VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_fl CURSOR FOR SELECT fl.fl_id, l.fl_log_id, l.acfs_id, fl.fl_type_id, fl.ac_id,
				concat(acfs.acfs_name, ' for flight ', fl.fl_name) as description
				FROM ams_ac.ams_al_flight fl
				join ams_ac.ams_al_flight_log l on fl.fl_id = l.fl_id
				left join ams_ac.cfg_aircraft_flight_status acfs on acfs.acfs_id = l.acfs_id
                where IF(l.acfs_id<>6,((UNIX_TIMESTAMP(l.udate) + (l.op_time_sec+60))- UNIX_TIMESTAMP(current_timestamp())),50)<80;
                
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_flight', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_flight';
set countRows = 0;
set i_acfs_id = 1; -- Plane stand

SET logNote=CONCAT('Start() ->' );
call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_fl;
read_loop: LOOP
		FETCH cursor_fl INTO i_fl_id, i_fl_log_id,  i_acfs_id, i_fl_type_id, i_ac_id, s_descript;
	
	IF done THEN
		SET logNote=CONCAT('No flight found ', 'done' );
		call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
	
	SET logNote=CONCAT(' fl_id: ',i_fl_id, ' i_acfs_id: ',i_acfs_id, ', ',s_descript);
	call ams_wad.log_sp(spname, stTime, logNote);
	IF i_acfs_id is null THEN
		SET i_acfs_id = 1;
		update ams_ac.ams_al_flight_log l set l.acfs_id = i_acfs_id 
        where l.fl_log_id = i_fl_log_id;
        COMMIT;
    END IF;
    
    -- Plane stand: start of flight
    IF i_acfs_id = 1 then
		CALL ams_ac.sp_acfs_refuling(i_fl_log_id);
    end if;
    -- Refuling
    if i_acfs_id = 2 then
		CALL ams_ac.sp_acfs_cargo_load(i_fl_log_id);
    end if;
    -- Cargo Load
    if i_acfs_id = 3 then 
		CALL ams_ac.sp_acfs_boarding(i_fl_log_id);
    end if;
    -- Boarding
    if i_acfs_id = 4 then
		CALL ams_ac.sp_acfs_takeoff(i_fl_log_id);
    end if;
    -- Taxy and Takeoff sp_acfs_in_flight
    if i_acfs_id = 5 then
		CALL ams_ac.sp_acfs_in_flight(i_fl_log_id);
    end if;
    -- In Flight
    if i_acfs_id = 6 then
		CALL ams_ac.sp_acfs_trip_to_landing(i_fl_log_id);
    end if;
	-- Landing and Taxy to Gate
     if i_acfs_id = 7 then
		CALL ams_ac.sp_acfs_disembarking(i_fl_log_id);
    end if;
    -- Disembarking
    if i_acfs_id = 8 then
		CALL ams_ac.sp_acfs_cargo_unload(i_fl_log_id);
    end if;
    -- Cargo Unload and Finish Flight
    if i_acfs_id = 9 then
		CALL ams_ac.sp_acfs_finish(i_fl_log_id);
        CALL ams_ac.sp_acfs_history(i_fl_log_id);
	end if;
    if i_acfs_id = 10 then    
		CALL ams_ac.sp_acfs_history(i_fl_log_id);
    end if;
        
	SET logNote=CONCAT('Flight i_fl_id: ', i_fl_id , ', processed.');
	-- call ams_wad.log_sp(spname, stTime, logNote);
		
	SET countRows = countRows+1;
    COMMIT;
    
 END LOOP;
CLOSE cursor_fl;


SET logNote=CONCAT('Processed  ', countRows, ' flights.');
call ams_wad.log_sp(spname, stTime, logNote);
 
 CALL ams_al.sp_tr_post();
 
SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);

select countRows as flights from dual;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_flight_ac_transfer` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_flight_ac_transfer`(IN in_ac_id INT, IN in_a_ap_id INT, IN in_fl_id INT, out i_fl_id int)
BEGIN

DECLARE i_ac_status_operational, i_acat_planeStand, i_fl_type_transfer, i_distance_km INT;
DECLARE i_arr_ap_id, i_dep_ap_is, i_dest_id INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_flight_ac_transfer', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_flight_ac_transfer';
set countRows = 0;
SET logNote=CONCAT('Start (', in_ac_id ,', in_a_ap_id:', in_a_ap_id, ', in_fl_id:', in_fl_id,') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

SET i_distance_km = 0;
SET i_ac_status_operational = 2;
SET i_acat_planeStand = 6;
SET i_fl_type_transfer = 3;
    
    IF in_ac_id > 0 THEN 
    
		SELECT acc.curr_ap_id, flq.d_ap_id INTO i_dep_ap_is, i_arr_ap_id
        FROM ams_ac.ams_al_flight_queue flq 
		join ams_ac.ams_aircraft_curr acc on acc.ac_id = flq.ac_id 
		where flq.fl_id = in_fl_id;
        
        CALL ams_wad.get_cfg_airport_destination(i_dep_ap_is, i_arr_ap_id, i_dest_id);
		 SET i_fl_id = 0;
    
		INSERT INTO ams_ac.ams_al_flight_queue ( 
				fl_type_id, fl_name, fl_description, al_id, ac_id, 
				d_ap_id, a_ap_id, distance_km, oil_l, flight_h, pax, payload_kg, dtime, atime)
		select i_fl_type_transfer as fl_type_id, 
			concat(al.iata, LPAD((ifnull(alc.fl_transfer, 0) +1),4,'0'),'TR') as fl_name, 
			CONCAT( IFNULL(dep.ap_iata, dep.ap_icao), ' to ',IFNULL(arr.ap_iata, arr.ap_icao), ' Transfer Flight ') as fl_description,
			ac.ownerAlId as al_id, 
			ac.acId as ac_id, 
			dep.ap_id as d_ap_id, 
			arr.ap_id as a_ap_id, 
			d.distance_km,
			ROUND((d.distance_km /100)* macr.fuel_consumption_lp100km,2) as oil_l,
            Round( (((d.distance_km/macr.cruise_speed_kmph)*60)+(1*ac.opTimeMin))/60,2) as flight_h,
			0 as pax, 0 as payload_kg,
			DATE_ADD( flq.dtime,INTERVAL -20 MINUTE) as dtime,
			DATE_ADD(
				DATE_ADD( flq.dtime,INTERVAL -20 MINUTE) , 
				INTERVAL Round(((d.distance_km/macr.cruise_speed_kmph)*60)+(1*ac.opTimeMin),0) MINUTE)  as atime
			from ams_ac.ams_al_flight_queue flq  
			join ams_ac.v_ams_ac ac on ac.acId = flq.ac_id 
			left join ams_wad.cfg_airport_destination d on d.dep_ap_id = ac.currApId and d.arr_ap_id = flq.d_ap_id
			left join ams_al.ams_airline_counters alc on alc.al_id = ac.ownerAlId
			join ams_al.ams_airline al on al.al_id = ac.ownerAlId
			join ams_wad.cfg_airport dep on dep.ap_id = ac.currApId
			join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
			join ams_ac.cfg_mac macr on macr.mac_id = ac.macId
			join ams_al.cfg_oil_price op on 1=1 and op.oil_price_id = 1 
			join ams_al.v_al_callc_param_h p on p.al_id = ac.ownerAlId
			join ams_al.ams_bank b on b.al_id = ac.ownerAlId
		where flq.fl_id = in_fl_id;
    
        
        SET i_fl_id = LAST_INSERT_ID();
        commit;
        
		SET logNote=CONCAT('Transfer flight i_fl_id: ', i_fl_id);
		call ams_wad.log_sp(spname, stTime, logNote);
        
         set countRows = 0;
        -- Update FL Transfer counter
        SELECT count(*) INTO countRows
		FROM ams_al.ams_airline_counters alc
		join ams_ac.ams_al_flight_queue flq on flq.al_id = alc.al_id
        where flq.fl_id = i_fl_id;
		COMMIT;	
        
        IF countRows = 0 THEN
			SET logNote=CONCAT('inserted ams_airline_counters record ');
			INSERT INTO ams_al.ams_airline_counters (al_id, fl_transfer)
			SELECT al_id, 1 as fl_num from ams_ac.ams_al_flight_queue where fl_id = i_fl_id;
        ELSE
			SET logNote=CONCAT('Updated ams_airline_counters record ');
			update ams_al.ams_airline_counters alc
			join ams_ac.ams_al_flight_queue flq on flq.al_id = alc.al_id
			set alc.fl_transfer = (ifnull(alc.fl_transfer, 0) +1)
			where flq.fl_id = i_fl_id;
        END IF;
        COMMIT;
        CALL ams_ac.sp_ams_aircraft_curr_update_ac(in_ac_id);
        
	END IF;
    
    SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    select i_fl_id as fl_id from dual;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_flight_clear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_flight_clear`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote, s_fl_name VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_flight_clear', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_flight_clear';
set countRows = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);

	delete from ams_ac.ams_al_flight_estimate 
	where Round((UNIX_TIMESTAMP(current_timestamp()) - UNIX_TIMESTAMP(adate))/60,0)>30;
    SET countRows =  ROW_COUNT();
    COMMIT;
    SET logNote=CONCAT('ams_al_flight_estimate ',countRows, ' deleted...');
	call ams_wad.log_sp(spname, stTime, logNote);
    
    delete FROM ams_ac.ams_al_flight_estimate_payload where fle_id not in
	(select fle_id from ams_al_flight_estimate);
    SET countRows =  ROW_COUNT();
    COMMIT;
    SET logNote=CONCAT('ams_al_flight_estimate_payload ',countRows, ' deleted...');
	call ams_wad.log_sp(spname, stTime, logNote);
	
	delete FROM ams_ac.ams_al_flight_queue where processed = 1;
    SET countRows =  ROW_COUNT();
    COMMIT;
    SET logNote=CONCAT('ams_al_flight_queue ',countRows, ' deleted...');
	call ams_wad.log_sp(spname, stTime, logNote);
	COMMIT;
     
    delete FROM ams_ac.ams_al_flight_queue_payload where fl_id not in
	(select fl_id from ams_al_flight_queue);
	SET countRows =  ROW_COUNT();
    COMMIT;
    SET logNote=CONCAT('delete from ams_al_flight_queue_payload ',countRows, ' deleted...');
	call ams_wad.log_sp(spname, stTime, logNote);
    COMMIT;
    
    CALL ams_ac.sp_ams_al_flight_plan_update();
	CALL ams_ac.sp_ams_aircraft_curr_update();
    
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_fl_init` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_fl_init`()
BEGIN

DECLARE i_fl_type_id, i_fl_id, i_ac_id, i_fl_log_id, i_acfs_id INT;
DECLARE is_on_d_ap, i_d_ap_id INT;
DECLARE countRows, i_records INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote, s_fl_name VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_flq CURSOR FOR SELECT flq.ac_id
						FROM ams_ac.ams_al_flight_queue flq
						join ams_ac.ams_aircraft a on a.ac_id = flq.ac_id 
						join ams_ac.ams_aircraft_curr ac on ac.ac_id = flq.ac_id
						left join ams_ac.ams_aircraft_log acl on acl.ac_id = flq.ac_id
                        left join ams_ac.ams_al_flight fl on fl.ac_id = a.ac_id
						where flq.processed = 0 and a.ac_status_id = 2 and ifnull(acl.acat_id,6)=6 and ac.flh_to_mnt > 0
                        and fl.fl_id is null
						and ((if(flq.dtime is null, 0, UNIX_TIMESTAMP(flq.dtime))-(ac.op_time_min*60)) - UNIX_TIMESTAMP(current_timestamp()))<180
						group by flq.ac_id
						order by MIN(flq.dtime) asc;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_fl_init', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_fl_init';
set countRows = 0;
SET is_on_d_ap = 0;
set i_acfs_id = 1; -- Plane stand

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_flq;
read_loop: LOOP
	FETCH cursor_flq INTO i_ac_id;
	
	IF done THEN
		SET logNote=CONCAT('No aircraft ready for flight found in cursor ', 'done' );
		 -- call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
    CALL ams_ac.sp_ams_aircraft_curr_update_ac(i_ac_id);
    SET logNote=CONCAT(' i_ac_id: ',i_ac_id, ' next flight ...');
	call ams_wad.log_sp(spname, stTime, logNote);
    
    SELECT flq.fl_id, flq.fl_name,  flq.fl_type_id, 
	if(ac.curr_ap_id = flq.d_ap_id, 1, 0) is_on_d_ap, flq.d_ap_id
    INTO i_fl_id, s_fl_name, i_fl_type_id, is_on_d_ap, i_d_ap_id
	FROM ams_ac.ams_al_flight_queue flq
	join ams_ac.ams_aircraft a on a.ac_id = flq.ac_id and a.ac_status_id = 2
	join ams_ac.ams_aircraft_curr ac on ac.ac_id = flq.ac_id
	left join ams_ac.ams_aircraft_log acl on acl.ac_id = flq.ac_id
    left join ams_ac.ams_al_flight fl on fl.ac_id = a.ac_id
	where flq.ac_id = i_ac_id and flq.processed = 0  and ifnull(acl.acat_id,6)=6
    and fl.fl_id is null
	and ((if(flq.dtime is null, 0, UNIX_TIMESTAMP(flq.dtime))-(ac.op_time_min*60)) - UNIX_TIMESTAMP(current_timestamp()))<180
	order by flq.dtime asc limit 1;
	
	SET logNote=CONCAT('flqid: ',i_fl_id, ', name: ',s_fl_name, ', type: ',i_fl_type_id);
	call ams_wad.log_sp(spname, stTime, logNote);
    
	-- transfer flight
    IF is_on_d_ap = 0 THEN
		-- Check For existing transfer flight
        SET logNote=CONCAT('is_on_d_ap: ',is_on_d_ap, ' transfer flight ');
		call ams_wad.log_sp(spname, stTime, logNote);
		set countRows = 0;
		SELECT count(*) INTO countRows
		FROM ams_ac.ams_al_flight_queue 
        where ac_id = i_ac_id and fl_type_id=3 AND processed =0;
        SET logNote=CONCAT('transfer flq count: ',countRows);
		call ams_wad.log_sp(spname, stTime, logNote);
        
        IF countRows = 0 THEN
			-- Check if plane already in flight
			SELECT count(*) INTO countRows
			FROM ams_ac.ams_aircraft_log acl 
			where acl.ac_id = i_ac_id and acl.acat_id = 6 ;
            SET logNote=CONCAT('acat_id=6 count: ',countRows);
            call ams_wad.log_sp(spname, stTime, logNote);
            IF countRows > 0 THEN
				SET logNote=CONCAT('Start transfer flight for ac: ', i_ac_id, ' to ap: ',  i_d_ap_id);
				call ams_wad.log_sp(spname, stTime, logNote);
				call ams_ac.sp_flight_ac_transfer(i_ac_id, i_d_ap_id, i_fl_id, i_fl_id);
			
				SET logNote=CONCAT('Transfer flight i_fl_id: ', i_fl_id, '. ');
				call ams_wad.log_sp(spname, stTime, logNote);
				-- Switch and start processing created transfer flight
				IF i_fl_id > 0 and false THEN
					SELECT flq.fl_id, flq.fl_name,  flq.fl_type_id,
					if(ac.curr_ap_id = flq.d_ap_id, 1, 0) is_on_d_ap, flq.d_ap_id
					INTO i_fl_id, s_fl_name, i_fl_type_id, is_on_d_ap, i_d_ap_id
					FROM ams_ac.ams_al_flight_queue flq
					join ams_ac.ams_aircraft a on a.ac_id = flq.ac_id and a.ac_status_id = 2
					join ams_ac.ams_aircraft_curr ac on ac.ac_id = flq.ac_id
					left join ams_ac.ams_aircraft_log acl on acl.ac_id = flq.ac_id
					where flq.fl_id = i_fl_id;
              END IF;      
			END IF;
		END IF;
 END IF;
    -- Check if plane is in stand not in flight
    set countRows = 0;
    SELECT count(*) INTO countRows
	FROM ams_ac.ams_aircraft_log acl 
	where acl.ac_id = i_ac_id and acl.acat_id = 6;
    SET logNote=CONCAT('acat_id=6 count: ',countRows, ', is_on_d_ap: ',is_on_d_ap);
	call ams_wad.log_sp(spname, stTime, logNote);
    
    IF i_fl_id > 0 and is_on_d_ap > 0 and countRows > 0 THEN
		SET logNote=CONCAT('INSERT flight flqid: ',i_fl_id, ', name: ',s_fl_name, ', type: ',i_fl_type_id);
		call ams_wad.log_sp(spname, stTime, logNote);
		INSERT INTO ams_ac.ams_al_flight (
			fl_id, fl_type_id, fl_name, fl_description, fle_id, flp_id, ch_id, route_id, al_id, ac_id, 
			d_ap_id, a_ap_id, distance_km, oil_l, pax, payload_kg, dtime, atime)
		select fl_id, fl_type_id, fl_name, fl_description, fle_id, flp_id, ch_id, route_id, al_id, ac_id, 
			d_ap_id, a_ap_id, distance_km, oil_l, ifnull(pax, 0) as pax, payload_kg, dtime, atime
		FROM ams_ac.ams_al_flight_queue e
		where e.fl_id = i_fl_id;
		
		INSERT INTO ams_ac.ams_al_flight_payload
		( flpl_id, fl_id, ch_id, payload_type, pax, cargo_kg, payload_kg, description, price, adate)
		select flpl_id, fl_id, ch_id, payload_type, ifnull(pax, 0), ifnull(cargo_kg, 0), payload_kg, description, price, adate
		from ams_ac.ams_al_flight_queue_payload where fl_id = i_fl_id;
		commit;
		
		set i_records = 0;
		select count(*) INTO i_records from ams_ac.ams_al_flight where fl_id = i_fl_id;   
		if i_records > 0 THEN
			SET logNote=CONCAT('inserted ams_al_flight record for ', i_fl_id);
			-- call ams_wad.log_sp(spname, stTime, logNote);
		
			UPDATE ams_ac.ams_al_flight_queue set processed = 1 where fl_id = i_fl_id;
		
			CALL ams_ac.sp_acat_flight(i_ac_id, i_fl_type_id, i_fl_id);
		
			COMMIT;
		
			INSERT INTO ams_ac.ams_al_flight_log (acfs_id, fl_id, fl_log_description, delay_min, op_time_sec, fl_time_min, fl_distance_km)
			SELECT i_acfs_id as acfs_id, fl.fl_id, 
			concat('Plane stand - prepare for flight:', fl.fl_name) as fl_log_description ,
			if(Round(((if(fl.dtime is null, 0, UNIX_TIMESTAMP(fl.dtime))-ac.op_time_min*60) - UNIX_TIMESTAMP(current_timestamp()))/60,0)>0,0, 
			Round(((if(fl.dtime is null, 0, UNIX_TIMESTAMP(fl.dtime))-ac.op_time_min*60) - UNIX_TIMESTAMP(current_timestamp()))/60,0)) as delay_min,
			((ac.op_time_min*60) * ifnull(acfsn.acfs_op_time_part, 0)) as op_time_sec, 
			0 as fl_time_min, 0 as fl_distance_km
			FROM ams_ac.ams_al_flight fl 
			join ams_ac.ams_aircraft_curr ac on ac.ac_id = fl.ac_id
			left join ams_ac.cfg_aircraft_flight_status acfsn on acfsn.acfs_id = i_acfs_id
			where fl.fl_id = i_fl_id;
			
			 SET i_fl_log_id = LAST_INSERT_ID();
				
			SET logNote=CONCAT('INSERTed first ams_al_flight_log fl_log_id: ', i_fl_log_id , ', fl_name: ', s_fl_name );
			-- call ams_wad.log_sp(spname, stTime, logNote);
				
			SET countRows = countRows+1;
		else
			SET logNote=CONCAT('Error no records found for fl_id ', i_fl_id);
			call ams_wad.log_sp(spname, stTime, logNote);
		END IF;
	
    END IF;
	COMMIT;
    
 END LOOP;
CLOSE cursor_flq;
COMMIT;

SET logNote=CONCAT('Processed  ', countRows, ' flights from queue.');
call ams_wad.log_sp(spname, stTime, logNote);
 
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);

select countRows as flights from dual;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_mac_produce` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_mac_produce`(IN vmac_id int)
BEGIN

DECLARE countRows, vac_id, vlog_id, vduration_min, vdistance_km, vflight_h INT;
DECLARE stTime TIMESTAMP;
DECLARE vamount double;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_mac_produce', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_mac_produce';
set countRows = 0;

SET logNote=CONCAT('Start (', vmac_id ,') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

	INSERT INTO ams_ac.ams_aircraft
		(registration, mac_id, home_ap_id,
		cabin_id, pilots_count, crew_count,
		pax_f, pax_b, pax_e, cargo_kg, cost_per_fh, ac_status_id)
	SELECT concat(m.mfr_iata, ' ', LPAD(mc.mac_id, 2, '0'), ' ', LPAD((mc.number_build +1), 4, '0')) ,
		mc.mac_id, mc.ap_id, c.cabin_id, mc.pilots, c.seat_crew_count - mc.pilots,
		c.seat_f_count, c.seat_b_count, c.seat_e_count,
		mc.load_kg, mc.cost_per_fh, 1 
	FROM ams_ac.cfg_mac mc
	join ams_ac.cfg_manufacturer m on m.mfr_id = mc.mfr_id
	join ams_ac.cfg_mac_cabin c on c.mac_id = mc.mac_id and c.default=1
	where mc.mac_id = vmac_id;
    
    SET vac_id = LAST_INSERT_ID();
	SET logNote=CONCAT('Created ac_id ', vac_id, ' aircraft for mac_id: ',vmac_id , '' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_aircraft_ife
		(ac_id, f_ife_id, b_ife_id, e_ife_id)
	select a.ac_id, 
		if(c.seat_f_count=0, null, c.max_ife_id) as f_ife_id,
		if(c.seat_b_count=0, null, c.max_ife_id) as b_ife_id,
		if(c.seat_e_count=0, null, c.max_ife_id) as e_ife_id
	from ams_ac.ams_aircraft a
	join ams_ac.cfg_mac_cabin c on c.cabin_id = a.cabin_id
	join ams_ac.cfg_aircraft_ife_type ife on ife.ife_id = c.max_ife_id
	where a.ac_id = vac_id;
    
    SET logNote=CONCAT('INSERT record in ams_aircraft_ife for ac_id ', vac_id , '' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_ac.ams_aircraft_curr
		(ac_id, curr_ap_id, flight_hours, distance_km, state_value, price_value, op_time_min, 
		a_check_fh, b_check_fh, c_check_fh, d_check_fh, stars, points)
    SELECT a.ac_id, a.home_ap_id as curr_ap_id,
		0 as flight_hours, 0 as distance_km, 1 as state_value, mc.price + c.price as  price_value,
		mc.operation_time_min as op_time_min,
		ach.period_h as a_check_fh,
		bch.period_h as b_check_fh,
		cch.period_h as c_check_fh,
		dch.period_h as d_check_fh,
		ROUND((if(aife.f_ife_id is null, 0, f.stars) + if(aife.b_ife_id is null, 0, b.stars) + if(aife.e_ife_id is null, 0, e.stars))/
				(if(aife.f_ife_id is null, 0, 1) + if(aife.b_ife_id is null, 0, 1) + if(aife.e_ife_id is null, 0, 1))) as stars,
		mc.popularity +
		ROUND((if(aife.f_ife_id is null, 0, f.points) + if(aife.b_ife_id is null, 0, b.points) + if(aife.e_ife_id is null, 0, e.points))/
				(if(aife.f_ife_id is null, 0, 1) + if(aife.b_ife_id is null, 0, 1) + if(aife.e_ife_id is null, 0, 1))) as points
	FROM ams_ac.ams_aircraft a
		join ams_ac.cfg_mac mc on mc.mac_id = a.mac_id
		join ams_ac.cfg_mac_cabin c on c.cabin_id = a.cabin_id
		join (SELECT period_h FROM ams_ac.cfg_aircraft_maintenance_type where amt_id=1) ach on 1=1
		join (SELECT period_h FROM ams_ac.cfg_aircraft_maintenance_type where amt_id=2) bch on 1=1
		join (SELECT period_h FROM ams_ac.cfg_aircraft_maintenance_type where amt_id=3) cch on 1=1
		join (SELECT period_h FROM ams_ac.cfg_aircraft_maintenance_type where amt_id=4) dch on 1=1
		join ams_ac.ams_aircraft_ife aife on aife.ac_id = a.ac_id
		left join ams_ac.cfg_aircraft_ife_type f on f.ife_id = aife.f_ife_id
		left join ams_ac.cfg_aircraft_ife_type b on b.ife_id = aife.b_ife_id
		left join ams_ac.cfg_aircraft_ife_type e on e.ife_id = aife.e_ife_id
		where a.ac_id=vac_id;
        
        SET logNote=CONCAT('INSERT record in ams_aircraft_curr for ac_id ', vac_id , '' );
		call ams_wad.log_sp(spname, stTime, logNote);
        
        call ams_ac.sp_acat_certificate(vac_id);
        call ams_ac.sp_acat_park(vac_id);
        
        COMMIT;

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_st_aircraft_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_st_aircraft_day`()
BEGIN

DECLARE countRows, i_yearweek INT;
DECLARE stTime TIMESTAMP;
DECLARE s_stdate DATE;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_fl_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_ac.sp_st_aircraft_day', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_ac.sp_st_aircraft_day';
set countRows = 0;

SET logNote=CONCAT('Start ->' );
call ams_wad.log_sp(spname, stTime, logNote);

	select ifnull(max(stdate), DATE_SUB(CURDATE(), INTERVAL 1 year)) 
    INTO s_stdate 
    from ams_ac.st_aircraft_day;
    
	SET logNote=CONCAT('Last st date: ', s_stdate );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	-- Check for not compressed records in ams_al.ams_bank_transaction
	SELECT count(distinct(flh.fl_id)) as fl_count
    INTO countRows
	FROM ams_ac.ams_al_flight_h flh
	where date(flh.atime_real) > date(s_stdate) and date(flh.atime_real) < date(now());
    
    SET logNote=CONCAT('Flights to process: ', countRows );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    IF countRows > 0 THEN
    
		set countRows = 0;
        
        INSERT INTO ams_ac.st_aircraft_day
		( ac_id, al_id, stdate, fl_count, distance_km, oil_l,
		pax, payload_kg, expences, income, revenue, charters_count, flight_h, cost_per_flight_h)
        SELECT flh.ac_id, flh.al_id, 
		date(flh.atime_real) as stdate,
		count(distinct(flh.fl_id)) as fl_count,
		sum(flh.distance_km) as distance_km,
		round(sum(flh.oil_l),2) as oil_l,
		sum(flh.pax) as pax,
		sum(flh.payload_kg) as payload_kg,
		round(sum(flh.expences),2) as expences,
		round(sum(flh.income),2) as income,
		round(sum(flh.revenue),2) as revenue,
		sum(if(flh.ch_id is null, 0, 1)) as charters_count,
        b.flight_h, b.cost_p_fl_h
		FROM ams_ac.ams_al_flight_h flh
        join (
			SELECT ac.ac_id, round(ac.flight_hours,2) as flight_h, 
				round( (sum(ifnull(tr.amount,0)) / ac.flight_hours),2) as cost_p_fl_h,
				round(sum(ifnull(tr.amount,0)),2) expences_sum
			from ams_ac.ams_aircraft_curr ac
			join ams_al.ams_bank_transaction tr on tr.aircraft_id = ac.ac_id
			join  ams_al.cfg_transaction_type trt on trt.tr_type_id = tr.tr_type_id
			where tr.posted = 1 
			and trt.tr_group in ('Flight','Aircraft') and tr.tr_type_id not in (2, 14) 
			group by ac.ac_id
        ) b on b.ac_id = flh.ac_id
        where date(flh.atime_real) > date(s_stdate) and date(flh.atime_real) < date(now())
		group by flh.ac_id, flh.al_id, date(flh.atime_real);

        SET countRows =  ROW_COUNT();
        SET logNote=CONCAT('Inserted: ', countRows, ' rows st_aircraft_day.');
        call ams_wad.log_sp(spname, stTime, logNote);
        
        INSERT INTO ams_ac.st_aircraft_tr_day
		( ac_id, al_id, stdate, tr_group,
		tr_count, amount, mac_id)
        SELECT tre.aircraft_id as ac_id, a.owner_al_id as al_id,
		date(tre.payment_time) as stdate, tte.tr_group, count(tre.tr_id) as tr_count,
		Round(  ifnull(tre.amount*if(tre.bank_id_to = b.bank_id,1,-1), 0),2) as amount, a.mac_id
		FROM  ams_al.ams_bank_transaction tre
		join ams_al.cfg_transaction_type tte on tte.tr_type_id = tre.tr_type_id and tte.tr_group ='Aircraft'
		join ams_ac.ams_aircraft a on a.ac_id = tre.aircraft_id
		join ams_al.ams_bank b on b.al_id = a.owner_al_id
		where tre.posted = 1 and date(tre.payment_time) > date(s_stdate) and date(tre.payment_time) < date(now())
		group by tre.aircraft_id, a.owner_al_id, date(tre.payment_time), tte.tr_group;

		SET countRows =  ROW_COUNT();
        SET logNote=CONCAT('Inserted: ', countRows, ' rows st_aircraft_tr_day.');
        call ams_wad.log_sp(spname, stTime, logNote);
  
        COMMIT;
    
    END IF;
 
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 15:09:49
