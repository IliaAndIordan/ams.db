-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: ams_ac
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ams_al_flight_queue_id`
--

DROP TABLE IF EXISTS `ams_al_flight_queue_id`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ams_al_flight_queue_id` (
  `fl_id` int(11) NOT NULL AUTO_INCREMENT,
  `fl_name` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`fl_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1092 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ams_al_flight_queue_id`
--

LOCK TABLES `ams_al_flight_queue_id` WRITE;
/*!40000 ALTER TABLE `ams_al_flight_queue_id` DISABLE KEYS */;
INSERT INTO `ams_al_flight_queue_id` VALUES (1065,'Charter: in_fle_id= 407 '),(1066,'Charter: in_fle_id= 414 '),(1067,'Charter: in_fle_id= 418 '),(1068,'Charter: in_fle_id= 419 '),(1069,'Charter: in_fle_id= 423 '),(1070,'Charter: in_fle_id= 430 '),(1071,'Charter: in_fle_id= 433 '),(1072,'Charter: in_fle_id= 435 '),(1073,'Charter: in_fle_id= 441 '),(1074,'Charter: in_fle_id= 445 '),(1075,'Charter: in_fle_id= 446 '),(1076,'Charter: in_fle_id= 453 '),(1077,'Charter: in_fle_id= 461 '),(1078,'Charter: in_fle_id= 464 '),(1079,'Charter: in_fle_id= 470 '),(1080,'Charter: in_fle_id= 481 '),(1081,'Charter: in_fle_id= 484 '),(1082,'Charter: in_fle_id= 487 '),(1083,'Charter: in_fle_id= 494 '),(1084,'Charter: in_fle_id= 498 '),(1085,'Charter: in_fle_id= 502 '),(1086,'Charter: in_fle_id= 505 '),(1087,'Charter: in_fle_id= 508 '),(1088,'Charter: in_fle_id= 511 '),(1089,'Charter: in_fle_id= 514 '),(1090,'Charter: in_fle_id= 517 '),(1091,'Charter: in_fle_id= 521 ');
/*!40000 ALTER TABLE `ams_al_flight_queue_id` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-03-04 10:13:13
