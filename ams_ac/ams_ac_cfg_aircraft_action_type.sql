CREATE DATABASE  IF NOT EXISTS `ams_ac` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_ac`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_ac
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfg_aircraft_action_type`
--

DROP TABLE IF EXISTS `cfg_aircraft_action_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfg_aircraft_action_type` (
  `acat_id` int NOT NULL,
  `acat_name` varchar(45) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `duration_min` int DEFAULT NULL,
  `distance_km` int DEFAULT NULL,
  `flight_h` int DEFAULT NULL,
  PRIMARY KEY (`acat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfg_aircraft_action_type`
--

LOCK TABLES `cfg_aircraft_action_type` WRITE;
/*!40000 ALTER TABLE `cfg_aircraft_action_type` DISABLE KEYS */;
INSERT INTO `cfg_aircraft_action_type` VALUES (1,'Airworthiness Certificate',NULL,180,60,2),(2,'Aircraft Purchase',NULL,NULL,NULL,NULL),(3,'Cabin Configuration Change',NULL,NULL,NULL,NULL),(4,'Transfer Flight',NULL,NULL,NULL,NULL),(5,'Flight',NULL,NULL,NULL,NULL),(6,'Plane stand',NULL,NULL,NULL,NULL),(7,'Aircraft Sell',NULL,NULL,NULL,NULL),(10,'Tax Hangar Rent Week',NULL,NULL,NULL,NULL),(11,'Tax Airport Week',NULL,NULL,NULL,NULL),(12,'Insurance Week',NULL,NULL,NULL,NULL),(20,'Aircrat Sell',NULL,NULL,NULL,NULL),(30,'Meintenance',NULL,NULL,NULL,NULL),(31,'Maintenance Check',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `cfg_aircraft_action_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 15:09:27
