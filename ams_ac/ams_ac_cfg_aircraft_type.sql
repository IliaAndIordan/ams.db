CREATE DATABASE  IF NOT EXISTS `ams_ac` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_ac`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_ac
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfg_aircraft_type`
--

DROP TABLE IF EXISTS `cfg_aircraft_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfg_aircraft_type` (
  `ac_type_id` int NOT NULL AUTO_INCREMENT,
  `ac_type_name` varchar(256) DEFAULT NULL,
  `hangar_cost_week` double DEFAULT NULL COMMENT '1. Hangar payment, $19,200.00 year\n2. Hangar insurance, $700.00 year\n3. Condo payment (Hangar Association) $1200.00 year (increased to $2000.00 next year!)',
  `airport_cost_week` double DEFAULT NULL COMMENT '4. Airport access fee, $192.00 year\n5. Airplane Insurance, $ 1200.00 year\n9. Special assessment for taxi way repaving (Condo fees one time)$2500.00\n10. Special assessment for easement land to access runway, one time (Condo fees one time) $1200.00',
  `insurance_cost_week` double DEFAULT NULL COMMENT '6. Annual $1500.00 - 3000.00 year',
  `notes` varchar(2000) DEFAULT NULL,
  `amorization_per_flh` double DEFAULT '0.01' COMMENT 'Amortization of aircraft: reduction of the operating condition of the aircraft for a flight hour 1 percent for 100 fl h = 0.01',
  `max_mtow_kg` int DEFAULT NULL,
  `rent_per_h` double DEFAULT NULL,
  PRIMARY KEY (`ac_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfg_aircraft_type`
--

LOCK TABLES `cfg_aircraft_type` WRITE;
/*!40000 ALTER TABLE `cfg_aircraft_type` DISABLE KEYS */;
INSERT INTO `cfg_aircraft_type` VALUES (1,'Piston',100,81.08,25,'Hangar payment per year	$19,000.00',0.01,1700,NULL),(2,'Turbo Prop ',143.75,116.51,35.92,'Twin:	$80.00/night',0.01,3700,NULL),(3,'Large Turbo Prop ',300,243.24,75,'Turbo Prop (King Air/PC12):	$170.00/night    ',0.01,5700,NULL),(4,'Small Jet',375,304.05,93.75,'Small Jet (12,500-20,000 lbs):$215.00/night     ',0.03,9100,NULL),(5,'Medium Jet ',500,405.4,125,'Medium Jet (20,001 - 40,000 lbs):$285.00/night    ',0.02,18150,NULL),(6,'Large Jet',700,567.56,175,'Large Jet (<40,000 lbs): $500.00/night',0.007,0,NULL);
/*!40000 ALTER TABLE `cfg_aircraft_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 15:09:28
