CREATE DATABASE  IF NOT EXISTS `ams_ac` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_ac`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_ac
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfg_manufacturer`
--

DROP TABLE IF EXISTS `cfg_manufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfg_manufacturer` (
  `mfr_id` int NOT NULL AUTO_INCREMENT,
  `mfr_name` varchar(512) DEFAULT NULL,
  `mfr_headquartier` varchar(256) DEFAULT NULL,
  `mfr_notes` varchar(2000) DEFAULT NULL,
  `mfr_wikilink` varchar(1000) DEFAULT NULL,
  `mfr_web` varchar(1000) DEFAULT NULL,
  `mfr_logo` varchar(1000) DEFAULT NULL,
  `ap_id` int DEFAULT NULL,
  `mfr_iata` varchar(2) DEFAULT NULL,
  `ams_status_id` int DEFAULT NULL,
  `udate` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`mfr_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfg_manufacturer`
--

LOCK TABLES `cfg_manufacturer` WRITE;
/*!40000 ALTER TABLE `cfg_manufacturer` DISABLE KEYS */;
INSERT INTO `cfg_manufacturer` VALUES (1,'Cessna Textron Aviation','Wichita, Kansas, United States','The Cessna Aircraft Company is an American general aviation aircraft manufacturing corporation headquartered in Wichita, Kansas. Best known for small, piston-powered aircraft, Cessna also produces business jets. The company is a subsidiary of the U.S. conglomerate Textron.','https://en.wikipedia.org/wiki/Cessna','http://cessna.txtav.com/','cessna-logo-white.png',27,'KS',2,'2024-07-12 20:28:30'),(2,'Embraer S.A.','Sao Jose dos Campos, Sao Paulo, Brazil','The company is the third largest producer of civil aircraft, after Boeing and Airbus.By December 2018, Embraer claimed to lead the sub 150 seat jetliner market with 100 operators of the ERJ and E-Jet families.','https://en.wikipedia.org/wiki/Embraer','https://embraer.com/','Embraer_logo.png',115,'ER',1,'2024-08-03 09:27:16'),(3,'Cirrus Aircraft','Duluth, Minnesota, United States','Founded in 1984 by Alan and Dale Klapmeier to produce the VK-30 kit aircraft, and is headquartered in Duluth, Minnesota, United States. The company is majority-owned by a subsidiary of the Aviation Industry Corporation of China (AVIC), with operational locations in seven states across the US','https://en.wikipedia.org/wiki/Cirrus_Aircraft','https://cirrusaircraft.com/','Cirrus_Aircraft_logo_white.png',114,'C9',2,'2024-09-30 20:29:17'),(4,'Dassault Aviation','Paris, France','Dassault Aviation S.A. is a French manufacturer of military aircraft and business jets. It was founded in 1929 by Marcel Bloch.','https://en.wikipedia.org/wiki/Dassault_Aviation','https://www.dassault-aviation.com/en/','Dassault_Aviation_Logo.png',118,'DF',2,'2022-07-14 12:01:29'),(5,'Britten-Norman',' Bembridge, United Kingdom','Privately owned British aircraft manufacturer and aviation services provider. Founded at June 29, 1954. The company has 165 employees and sold almost 1,300 aircraft to customers in more than 120 countries.','https://en.wikipedia.org/wiki/Britten-Norman','https://britten-norman.com/','britten-norman_logo_white.png',119,'BN',4,'2024-08-28 17:11:46'),(6,'ATR','1 Allée Pierre Nadot 31712 Blagnac, Toulose, France','The company was founded in 1981 as a joint venture between Aérospatiale of France (now Airbus) and Aeritalia (now Leonardo) of Italy.[5] Its main products are the ATR 42 and ATR 72 aircraft. ATR has sold more than 1,600 aircraft and has over 200 operators in more than 100 countries.','https://en.wikipedia.org/wiki/ATR_(aircraft_manufacturer)','https://www.atr-aircraft.com/','ATR_logo.png',235,'AT',2,'2024-07-09 21:59:12'),(7,'De Havilland Canada','	3615 34 St NE, Calgary, AB T1Y 6Z8, Canada','Founded in 1928 as a subsidiary of de Havilland Aircraft (UK), de Havilland Canada was first located at De Lesseps Field in Toronto, before moving to Downsview Airport in 1929.\n\nThe original home of De Havilland Canada was the Canadian Air and Space Museum located in what is now Downsview Park.','https://en.wikipedia.org/wiki/De_Havilland_Canada','https://dehavilland.com/en','DH_logo_nav.png',271,'DH',4,'2024-08-28 13:57:31'),(8,'Antonov','Kyiv , Ukraine','Founded 31 May 1946. Built a total of approximately 22,000 aircraft.\n','https://en.wikipedia.org/wiki/Antonov','https://www.antonov.com/en','an-logo-white_1.png',263,'AN',4,'2024-08-14 21:55:50'),(9,'Bombardier','Dorval, Quebec, Canada','Bombardier Aviation is a division of Bombardier Inc. It is headquartered in Dorval, Quebec, Canada.[2] Its most popular aircraft included the Dash 8 Series 400, CRJ100/200/440, and CRJ700/900/1000 lines of regional airliners, and the newer CSeries (also known as the Airbus A220). ','https://en.wikipedia.org/wiki/Bombardier_Aviation','https://bombardier.com/en/aircraft','bombardier_logo.png',NULL,'DH',1,'2024-09-26 11:02:18');
/*!40000 ALTER TABLE `cfg_manufacturer` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 15:09:26
