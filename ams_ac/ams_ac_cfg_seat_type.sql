CREATE DATABASE  IF NOT EXISTS `ams_ac` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_ac`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_ac
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfg_seat_type`
--

DROP TABLE IF EXISTS `cfg_seat_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfg_seat_type` (
  `seat_type_id` int NOT NULL AUTO_INCREMENT,
  `seat_type_class` varchar(1) NOT NULL,
  `seat_type_name` varchar(45) DEFAULT NULL,
  `seat_type_image` varchar(255) DEFAULT NULL,
  `seat_type_price` double DEFAULT NULL,
  `seat_type_confort` double DEFAULT NULL,
  `seat_type_max_distance_km` int DEFAULT NULL,
  PRIMARY KEY (`seat_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfg_seat_type`
--

LOCK TABLES `cfg_seat_type` WRITE;
/*!40000 ALTER TABLE `cfg_seat_type` DISABLE KEYS */;
INSERT INTO `cfg_seat_type` VALUES (1,'E','Standart','Seat_E_1.jpg',2500,0.5,500),(2,'E','Slimline','Seat_E_2.jpg',3000,0.8,1500),(3,'E','Plus','Seat_E_3.jpg',4000,1,2500),(4,'E','Premium ','Seat_E_4.jpg',4500,1.05,5000),(5,'E','Extra','Seat_E_5.jpg',5000,1.2,20000),(6,'B','Shost Haul','Seat_B_1.jpg',4000,0.5,500),(7,'B','Eurobusiness','Seat_B_2.jpg',6000,0.8,1500),(8,'B','Recliner','Seat_B_3.jpg',8000,1,2500),(9,'B','Cradle sleeper','Seat_B_4.jpg',12000,1.1,5000),(10,'B','Fully flat','Seat_B_5.jpg',18000,1.2,20000),(11,'F','Angled lie-flat','Seat_F_1.jpg',12000,0.8,1500),(12,'F','Offset fully flat','Seat_F_2.jpg',15000,0.9,2500),(13,'F','Herringbone aisle access','Seat_F_3.jpg',18000,1,5000),(14,'F','Sofa-seat aisle access','Seat_F_4.jpg',24000,1.2,10000),(15,'F','Super First Class suites','Seat_F_5.jpg',48000,1.5,20000);
/*!40000 ALTER TABLE `cfg_seat_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 15:09:40
