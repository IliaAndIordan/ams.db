CREATE DATABASE  IF NOT EXISTS `ams_wad` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_wad`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_wad
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfg_task`
--

DROP TABLE IF EXISTS `cfg_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfg_task` (
  `task_id` int NOT NULL AUTO_INCREMENT,
  `task_type` varchar(45) DEFAULT NULL,
  `task_name` varchar(256) DEFAULT NULL,
  `sp_name` varchar(112) DEFAULT NULL,
  `period_min` int DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `finish` datetime DEFAULT NULL,
  `is_running` int DEFAULT '0',
  `exec_time` varchar(45) DEFAULT NULL,
  `next_run` datetime DEFAULT NULL,
  `info` varchar(1000) DEFAULT NULL,
  `skip_count` int DEFAULT '0',
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfg_task`
--

LOCK TABLES `cfg_task` WRITE;
/*!40000 ALTER TABLE `cfg_task` DISABLE KEYS */;
INSERT INTO `cfg_task` VALUES (1,'sp_ev_min_five','Bank Account Balanse for Day','ams_al.sp_ams_bank_h_day',1440,'2025-01-20 23:59:30','2025-01-20 23:59:30',0,'0','2025-01-21 23:59:00',NULL,0),(2,'sp_ev_min_five','Truncate Log Tables.','ams_wad.sp_log_clear',1440,'2025-01-20 23:54:30','2025-01-20 23:54:31',0,'1000.000','2025-01-21 23:50:00','Clear log tables ones per day before midnight callculations start',0),(3,'sp_ev_min_five','Manufacturer aircraft production','ams_ac.sp_ev_mac_produce',1440,'2025-01-20 03:54:30','2025-01-20 03:54:48',0,'18000.000','2025-01-21 03:50:00','Manufacturer aircraft production',0),(4,'sp_ev_min_one','Process Flight Queue','ams_ac.sp_fl_init',1,'2025-01-21 00:18:55','2025-01-21 00:18:55',0,'0','2025-01-21 00:19:00','Process Flight Queue',0),(5,'sp_ev_min_one','Process Flights','ams_ac.sp_flight',1,'2025-01-21 00:18:55','2025-01-21 00:18:56',0,'1000.000','2025-01-21 00:19:00','Process Flights',0),(6,'sp_ev_min_five','AC revenue for day statistics','ams_al.sp_st_ac_revenue_day',1440,'2025-01-20 01:04:30','2025-01-20 01:04:30',0,'0','2025-01-21 01:00:00','Ones per day calculate statistics for AC Expences',0),(7,'sp_ev_min_five','New Charter records ','ams_wad.ams_charter_insert',360,'2025-01-20 19:54:30','2025-01-20 19:54:31',0,'1000.000','2025-01-21 01:50:00','On every 6 hours',0),(9,'sp_ev_min_five','Flight Q ans Estimate Clear','ams_ac.sp_flight_clear',1440,'2025-01-20 23:54:31','2025-01-20 23:54:37',0,'6000.000','2025-01-21 23:50:00',NULL,0),(10,'sp_ev_min_five','callculate airline transactions statistics','ams_al.sp_st_bank_transaction_day',1440,'2025-01-20 23:54:37','2025-01-20 23:54:47',0,'10000.000','2025-01-21 23:50:00',NULL,0),(11,'sp_ev_min_five','Calculate aircraft statistics for day','ams_ac.sp_st_aircraft_day',1440,'2025-01-20 23:54:47','2025-01-20 23:54:52',0,'5000.000','2025-01-21 23:50:00','Aircraft Stat for day',0),(12,'sp_ev_min_five','Weekly Insurance for Aircrafts','ams_wad.ams_ac_insurance_week',10080,'2025-01-20 01:04:30','2025-01-20 01:04:46',0,'16000.000','2025-01-27 01:00:00',NULL,0),(13,'sp_ev_min_five','Start Maintenanse Check','ams_ac.sp_ac_mnt_plan_start',5,'2025-01-21 00:19:30','2025-01-21 00:19:30',0,'0','2025-01-21 00:20:00',NULL,0),(14,'sp_ev_min_five','Finish Maintenance Check','ams_ac.sp_ac_mnt_plan_finish',5,'2025-01-21 00:19:30','2025-01-21 00:19:30',0,'0','2025-01-21 00:20:00',NULL,0),(15,'sp_ev_min_five','Process PPD CPD calculation ','ams_wad.process_calc_airport_payload_demand',240,'2025-01-20 23:24:30','2025-01-20 23:28:24',0,'234000.000','2025-01-21 03:20:00','Calculates PPD and Cpd Load demand for an Airport',0),(16,'sp_ev_min_five','Save Daily stat for PPD and CPD','ams_wad.st_airport_destination_day',1440,'2025-01-20 23:44:31','2025-01-20 23:45:34',0,'63000.000','2025-01-21 23:40:00','Every Day',0),(17,'sp_ev_min_five','Hub Upkeep Transactions weekly','ams_wad.ams_al_hub_upkeep_week',1440,'2025-01-21 00:09:31','2025-01-21 00:09:33',0,'2000.000','2025-01-22 00:05:00','Every Day',0),(18,'sp_ev_min_five','Save Daily AP pax and cargo stst','ams_al.sp_st_al_ap_pc_tranfered_day',1440,'2025-01-21 00:09:33','2025-01-21 00:09:33',0,'0','2025-01-22 00:05:00','Every Day',0),(20,'sp_ev_min_five','Flight Group -> Schedule Flight','ams_ac.sp_al_flight_plan_create',30,'2025-01-21 00:19:30','2025-01-20 23:49:38',1,'7000.000','2025-01-21 00:15:00',NULL,0),(21,'sp_ev_min_five','Schedule Flight -> Payloads','ams_ac.sp_al_flight_plan_payload_add',20,'2025-01-21 00:09:33','2025-01-21 00:14:13',0,'280000.000','2025-01-21 00:25:00',NULL,0),(22,'sp_ev_min_one','Schedule  Flights -> Queue','ams_ac.sp_al_flight_plan_process',5,'2025-01-21 00:17:55','2025-01-21 00:17:55',0,'0','2025-01-21 00:22:00',NULL,0),(23,'sp_ev_min_five','PPD Land Travel','ams_wad.calc_ams_airport_ppd_land_trans',60,'2025-01-20 23:45:34','2025-01-20 23:45:45',0,'11000.000','2025-01-21 00:40:00','Every Hour Lanf Trip for PPT',0),(24,'sp_ev_min_five','CPD Land Travel','ams_wad.calc_ams_airport_cpd_land_trans',60,'2025-01-20 23:45:45','2025-01-20 23:46:03',0,'18000.000','2025-01-21 00:40:00','Every Hour Lanf Trip for PPT',0),(25,'sp_ev_min_five','Schedule Flight -> Payloads Single','ams_ac.sp_al_flight_plan_payload_add_n_single',5,'2024-12-19 00:08:08','2024-12-19 00:08:40',1,'32000.000','2024-12-19 12:09:00',NULL,0);
/*!40000 ALTER TABLE `cfg_task` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`iordanov_ams`@`%`*/ /*!50003 TRIGGER `cfg_task_AFTER_UPDATE` AFTER UPDATE ON `cfg_task` FOR EACH ROW BEGIN
if(NEW.next_run > OLD.next_run) THEN
	INSERT INTO ams_wad.cfg_task_h
	(task_id, task_type, task_name,
	sp_name, period_min, start, finish,
	is_running, exec_time, next_run, info)
	SELECT 
		task_id, task_type,
		task_name, sp_name,
		period_min, start,
		finish, is_running,
		exec_time, next_run,info
	 from ams_wad.cfg_task where task_id=OLD.task_id;
 END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 13:53:51
