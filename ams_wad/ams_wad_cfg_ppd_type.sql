CREATE DATABASE  IF NOT EXISTS `ams_wad` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_wad`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_wad
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfg_ppd_type`
--

DROP TABLE IF EXISTS `cfg_ppd_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfg_ppd_type` (
  `ppd_type_id` int NOT NULL,
  `ppd_type_name` varchar(256) DEFAULT NULL,
  `pax_class_id` int DEFAULT NULL,
  PRIMARY KEY (`ppd_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfg_ppd_type`
--

LOCK TABLES `cfg_ppd_type` WRITE;
/*!40000 ALTER TABLE `cfg_ppd_type` DISABLE KEYS */;
INSERT INTO `cfg_ppd_type` VALUES (1,'Passenger(s)',1),(2,'Tourists(s)',1),(3,'VIP Passenger(s)',1),(4,'Medical Personnel',1),(5,'Aircrew',1),(6,'Corporate Worker(s)',1),(7,'Government Agent(s)',1),(8,'Military Personnels',1),(9,'Journalists',1),(10,'Students',1),(11,'Passenger(s)',1),(12,'Tourists(s)',1),(13,'Passenger(s)',1),(14,'Military Personnels',1),(15,'Passenger(s)',1),(20,'Worker(s)',2),(21,'Corporate Agents',2),(22,'Tourists',2),(23,'Officers',2),(24,'VIP Journalists',2),(30,'Executive(s)',3),(31,'VIP Passengers',3),(100,'Cargo',4),(101,'Medical Supplies',4);
/*!40000 ALTER TABLE `cfg_ppd_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 14:03:59
