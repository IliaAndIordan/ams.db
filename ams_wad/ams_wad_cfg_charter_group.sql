CREATE DATABASE  IF NOT EXISTS `ams_wad` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_wad`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_wad
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfg_charter_group`
--

DROP TABLE IF EXISTS `cfg_charter_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfg_charter_group` (
  `ch_grp_id` int NOT NULL AUTO_INCREMENT,
  `ap_id` int DEFAULT NULL,
  `ch_grp_code` varchar(5) DEFAULT NULL,
  `ch_grp_name` varchar(256) DEFAULT NULL,
  `ch_grp_notes` varchar(512) DEFAULT NULL,
  PRIMARY KEY (`ch_grp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfg_charter_group`
--

LOCK TABLES `cfg_charter_group` WRITE;
/*!40000 ALTER TABLE `cfg_charter_group` DISABLE KEYS */;
INSERT INTO `cfg_charter_group` VALUES (1,3,'SBT','Sunny Beach Bourgas, BG',NULL),(2,214,'LGMG','Attica Beach, Megara, GR',NULL),(3,212,'LGM','Marathon Beach LGKN, Marathon, GR',NULL),(4,12,'GST','Golden Sands, Varna, BG',NULL),(5,98,'DRCR','Danube River Cruises Tourists -  Ruse,BG',NULL),(6,108,'G-BG','BG Government Pax to Silistra',NULL),(7,276,'MNL','MNL Charter',NULL),(8,244,'GEB','Babushara, Sukhumi, GE ',NULL),(9,269,'ADA','Ada Boyana, Ulcinj, ME',NULL),(10,21,'BAL','Albena, Balchik,  BG',NULL),(11,4,'LBPR','Bulgaria South, Primorsko, BG',NULL),(16,39,'DRCV','Danube River Cruises Tourists -  Vidin,BG',NULL),(17,312,'SJJ','Dinaric Alps Winter - Sarajevo, BA ',NULL),(18,184,'SBT2','Sunny Beach -  Pchelnik, BG ',NULL),(19,436,'LYVA','Divčibare Recreational Centre',NULL),(20,353,'UKS','Lyubimovka Beach',NULL),(21,355,'UKFE','Yevpatoriya Bay',NULL),(22,470,'UZC','Zlatibor Tourist',NULL),(23,380,'MPW','Mariupol UA,  Trade and Manufacturing',NULL);
/*!40000 ALTER TABLE `cfg_charter_group` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 13:53:47
