CREATE DATABASE  IF NOT EXISTS `ams_wad` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_wad`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_wad
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfg_airport_type`
--

DROP TABLE IF EXISTS `cfg_airport_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfg_airport_type` (
  `ap_type_id` int NOT NULL,
  `ap_type_name` varchar(45) DEFAULT NULL,
  `ap_type_trafic_min` int DEFAULT NULL,
  `map_symbol_radius` int DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `payload_local` double DEFAULT NULL,
  `payload_regional` double DEFAULT NULL,
  `range_km` int DEFAULT NULL,
  PRIMARY KEY (`ap_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfg_airport_type`
--

LOCK TABLES `cfg_airport_type` WRITE;
/*!40000 ALTER TABLE `cfg_airport_type` DISABLE KEYS */;
INSERT INTO `cfg_airport_type` VALUES (1,'Heliport',10,6,'Heliport',1,0,150),(2,'Airfield',30,8,'Very small airport that serve only local area, no First class passengers will be generated',0.7,0.3,250),(3,'Local',50,9,'Very small airport that serve only local area, no First class passengers will be generated',0.6,0.4,400),(4,'Regional',100,10,'small airport that serve only region of 800 nm, no First class passengers will be generated',0.5,0.5,800),(5,'Small International',1500,11,'',0.2,0.8,1200),(6,'International',50000,12,'',0.1,0.9,2400),(7,'Large International',500000,14,'',0.1,0.9,5000),(8,'Mega International',10000000,16,'',0.1,0.9,18000);
/*!40000 ALTER TABLE `cfg_airport_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 13:51:42
