CREATE DATABASE  IF NOT EXISTS `ams_wad` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_wad`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_wad
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cfg_subregion`
--

DROP TABLE IF EXISTS `cfg_subregion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cfg_subregion` (
  `subregion_id` int NOT NULL AUTO_INCREMENT,
  `subregion_code` varchar(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `subregion_name` varchar(256) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `wiki_link` varchar(500) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `region_id` int DEFAULT NULL,
  `image_url` varchar(1000) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`subregion_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cfg_subregion`
--

LOCK TABLES `cfg_subregion` WRITE;
/*!40000 ALTER TABLE `cfg_subregion` DISABLE KEYS */;
INSERT INTO `cfg_subregion` VALUES (1,'015','Northern Africa','https://en.wikipedia.org/wiki/North_Africa',1,NULL),(2,'014','Eastern Africa','https://en.wikipedia.org/wiki/East_Africa',1,NULL),(3,'017','Middle Africa','https://en.wikipedia.org/wiki/Central_Africa',1,NULL),(4,'018','Southern Africa','https://en.wikipedia.org/wiki/Southern_Africa',1,NULL),(5,'011','Western Africa','https://en.wikipedia.org/wiki/West_Africa',1,NULL),(6,'029','Caribbean','https://en.wikipedia.org/wiki/Caribbean',2,NULL),(7,'013','Central America','https://en.wikipedia.org/wiki/Central_America',2,NULL),(8,'005','South America','https://en.wikipedia.org/wiki/South_America',2,NULL),(9,'021','Northern America','https://en.wikipedia.org/wiki/Northern_America',2,NULL),(10,'143','Central Asia','https://en.wikipedia.org/wiki/Central_Asia',3,NULL),(11,'030','Eastern Asia','https://en.wikipedia.org/wiki/East_Asia',3,NULL),(12,'035','Southeast Asia','https://en.wikipedia.org/wiki/Southeast_Asia',3,NULL),(13,'034','South Asia','https://en.wikipedia.org/wiki/South_Asia',3,NULL),(14,'145','Western Asia','https://en.wikipedia.org/wiki/Western_Asia',3,NULL),(15,'151','Eastern Europe (including Northern Asia)','https://en.wikipedia.org/wiki/Eastern_Europe',4,NULL),(16,'154','Northern Europe','https://en.wikipedia.org/wiki/Northern_Europe',4,NULL),(17,'039','Southern Europe','https://en.wikipedia.org/wiki/Southern_Europe',4,NULL),(18,'155','Western Europe','https://en.wikipedia.org/wiki/Western_Europe',4,NULL),(19,'053','Australia and New Zealand','https://en.wikipedia.org/wiki/Australia',5,NULL),(20,'054','Melanesia','https://en.wikipedia.org/wiki/Melanesia',5,NULL),(21,'057','Micronesia','https://en.wikipedia.org/wiki/Micronesia',5,NULL),(22,'061','Polynesia','https://en.wikipedia.org/wiki/Polynesia',5,NULL);
/*!40000 ALTER TABLE `cfg_subregion` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 13:52:19
