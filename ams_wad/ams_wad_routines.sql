CREATE DATABASE  IF NOT EXISTS `ams_wad` /*!40100 DEFAULT CHARACTER SET utf8mb3 */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `ams_wad`;
-- MySQL dump 10.13  Distrib 8.0.38, for Win64 (x86_64)
--
-- Host: 192.168.1.3    Database: ams_wad
-- ------------------------------------------------------
-- Server version	8.0.32

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `v_country_airports_count`
--

DROP TABLE IF EXISTS `v_country_airports_count`;
/*!50001 DROP VIEW IF EXISTS `v_country_airports_count`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_country_airports_count` AS SELECT 
 1 AS `country_id`,
 1 AS `airports`,
 1 AS `apActive`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `v_solr_airport`
--

DROP TABLE IF EXISTS `v_solr_airport`;
/*!50001 DROP VIEW IF EXISTS `v_solr_airport`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `v_solr_airport` AS SELECT 
 1 AS `apId`,
 1 AS `iata`,
 1 AS `icao`,
 1 AS `apName`,
 1 AS `lat`,
 1 AS `lon`,
 1 AS `elevation`,
 1 AS `typeId`,
 1 AS `regionId`,
 1 AS `subregionId`,
 1 AS `countryId`,
 1 AS `iso2`,
 1 AS `cName`,
 1 AS `stateId`,
 1 AS `stName`,
 1 AS `cityId`,
 1 AS `ctName`,
 1 AS `active`,
 1 AS `wikiUrl`,
 1 AS `amsStatus`,
 1 AS `wadStatus`,
 1 AS `homeUrl`,
 1 AS `notes`,
 1 AS `utc`,
 1 AS `baseTypeId`,
 1 AS `svgline`,
 1 AS `svgcircle`,
 1 AS `adate`,
 1 AS `udate`,
 1 AS `runwaysCount`,
 1 AS `oldApId`,
 1 AS `maxRwLenght`,
 1 AS `maxMtow`,
 1 AS `paxDemandDay`,
 1 AS `paxDemandPeriod`,
 1 AS `cargoDemandPeriodKg`,
 1 AS `paxDemandPeriodDays`,
 1 AS `plddate`,
 1 AS `apLabel`,
 1 AS `lat_lon`,
 1 AS `iso_country_state`,
 1 AS `flpns_count`*/;
SET character_set_client = @saved_cs_client;

--
-- Final view structure for view `v_country_airports_count`
--

/*!50001 DROP VIEW IF EXISTS `v_country_airports_count`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb3 */;
/*!50001 SET character_set_results     = utf8mb3 */;
/*!50001 SET collation_connection      = utf8mb3_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_country_airports_count` AS select `cfg_airport`.`country_id` AS `country_id`,count(`cfg_airport`.`ap_id`) AS `airports`,sum(if((`cfg_airport`.`ap_active` <> 0),1,0)) AS `apActive` from `cfg_airport` group by `cfg_airport`.`country_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `v_solr_airport`
--

/*!50001 DROP VIEW IF EXISTS `v_solr_airport`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb3 */;
/*!50001 SET character_set_results     = utf8mb3 */;
/*!50001 SET collation_connection      = utf8mb3_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`iordanov_ams`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `v_solr_airport` AS select `a`.`ap_id` AS `apId`,`a`.`ap_iata` AS `iata`,`a`.`ap_icao` AS `icao`,`a`.`ap_name` AS `apName`,`a`.`ap_lat` AS `lat`,`a`.`ap_lon` AS `lon`,`a`.`ap_elevation_m` AS `elevation`,`a`.`ap_type_id` AS `typeId`,`a`.`region_id` AS `regionId`,`a`.`subregion_id` AS `subregionId`,`a`.`country_id` AS `countryId`,`c`.`country_iso2` AS `iso2`,`c`.`country_name` AS `cName`,`a`.`state_id` AS `stateId`,`st`.`state_name` AS `stName`,`a`.`city_id` AS `cityId`,`ct`.`city_name` AS `ctName`,`a`.`ap_active` AS `active`,`a`.`ap_wikiurl` AS `wikiUrl`,`a`.`ams_status_id` AS `amsStatus`,`a`.`wad_status_id` AS `wadStatus`,`a`.`ap_homeurl` AS `homeUrl`,`a`.`ap_notes` AS `notes`,`a`.`ap_utc` AS `utc`,`a`.`ap_type_base` AS `baseTypeId`,`a`.`rw_svg_line` AS `svgline`,`a`.`rw_svg_path_circle` AS `svgcircle`,`a`.`adate` AS `adate`,`a`.`udate` AS `udate`,count(`rw`.`rw_id`) AS `runwaysCount`,`a`.`air_airport_id` AS `oldApId`,`a`.`max_rw_lenght_m` AS `maxRwLenght`,`a`.`max_mtow_kg` AS `maxMtow`,`pld`.`pax_demand_for_day` AS `paxDemandDay`,`pld`.`pax_demand_for_period` AS `paxDemandPeriod`,`pld`.`cargo_demand_for_period_kg` AS `cargoDemandPeriodKg`,`pld`.`period_days` AS `paxDemandPeriodDays`,`pld`.`period_nexttime` AS `plddate`,concat(if(((`a`.`ap_iata` is null) or (`a`.`ap_iata` = '')),`a`.`ap_icao`,`a`.`ap_iata`),' ',`a`.`ap_name`,' - ',`ct`.`city_name`,' ',`st`.`state_name`,', ',`c`.`country_iso2`) AS `apLabel`,concat(`a`.`ap_lat`,',',`a`.`ap_lon`) AS `lat_lon`,concat(`c`.`country_iso2`,'_',replace(`st`.`state_name`,' ','_')) AS `iso_country_state`,sum((ifnull(`apc`.`flpns_dep_count`,0) + ifnull(`apc`.`flpns_arr_count`,0))) AS `flpns_count` from ((((((`cfg_airport` `a` left join `cfg_airport_runway` `rw` on((`rw`.`ap_id` = `a`.`ap_id`))) left join `ams_airport_counters` `apc` on((`a`.`ap_id` = `apc`.`ap_id`))) left join `cfg_city` `ct` on((`ct`.`city_id` = `a`.`city_id`))) left join `cfg_country_state` `st` on((`st`.`state_id` = `a`.`state_id`))) left join `cfg_country` `c` on((`c`.`country_id` = `a`.`country_id`))) left join `calc_airport_payload_demand` `pld` on((`pld`.`ap_id` = `a`.`ap_id`))) group by `a`.`ap_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Dumping events for database 'ams_wad'
--
/*!50106 SET @save_time_zone= @@TIME_ZONE */ ;
/*!50106 DROP EVENT IF EXISTS `ev_min_five` */;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8mb3 */ ;;
/*!50003 SET character_set_results = utf8mb3 */ ;;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`iordanov_ams`@`%`*/ /*!50106 EVENT `ev_min_five` ON SCHEDULE EVERY 5 MINUTE STARTS '2021-10-14 00:09:30' ON COMPLETION NOT PRESERVE ENABLE COMMENT 'On 5 min ev_min_five' DO BEGIN
  		DECLARE stTime TIMESTAMP;
        DECLARE logNote VARCHAR(4000);
        
  		set stTime = current_timestamp();
        SET logNote=CONCAT('Start', '->' );
		call ams_wad.log_sp('ev_min_five', stTime, logNote);
    
		call ams_wad.sp_ev_min_five();
            
  		SET logNote=CONCAT('Finish ', '<-' );
		call ams_wad.log_sp('ev_min_five', stTime, logNote);
          
  		END */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
/*!50106 DROP EVENT IF EXISTS `ev_min_one` */;;
DELIMITER ;;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;;
/*!50003 SET character_set_client  = utf8mb3 */ ;;
/*!50003 SET character_set_results = utf8mb3 */ ;;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;;
/*!50003 SET @saved_time_zone      = @@time_zone */ ;;
/*!50003 SET time_zone             = 'SYSTEM' */ ;;
/*!50106 CREATE*/ /*!50117 DEFINER=`iordanov_ams`@`%`*/ /*!50106 EVENT `ev_min_one` ON SCHEDULE EVERY 1 MINUTE STARTS '2022-05-04 12:14:55' ON COMPLETION NOT PRESERVE ENABLE COMMENT 'ams_wad one minute event' DO BEGIN
        DECLARE stTime TIMESTAMP;
		DECLARE logNote VARCHAR(4000);

		set stTime = current_timestamp();
		SET logNote=CONCAT('Start', '->' );
		call ams_wad.log_sp('ev_min_one', stTime, logNote);

		call ams_wad.sp_ev_min_one();

		SET logNote=CONCAT('Finish ', '<-' );
		call ams_wad.log_sp('ev_min_one', stTime, logNote);
      END */ ;;
/*!50003 SET time_zone             = @saved_time_zone */ ;;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;;
/*!50003 SET character_set_client  = @saved_cs_client */ ;;
/*!50003 SET character_set_results = @saved_cs_results */ ;;
/*!50003 SET collation_connection  = @saved_col_connection */ ;;
DELIMITER ;
/*!50106 SET TIME_ZONE= @save_time_zone */ ;

--
-- Dumping routines for database 'ams_wad'
--
/*!50003 DROP PROCEDURE IF EXISTS `add_city_payload_demand` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `add_city_payload_demand`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE done INT DEFAULT FALSE;
DECLARE i_city_id, i_ap_count INT;

DECLARE cursorCityIds CURSOR FOR select d.city_id as city_id,  d.apCount as ap_count from
	(SELECT count(a.ap_id) as apCount, a.city_id as city_id, sum(apd.pax_demand_for_period) as pax_demand_for_period
	FROM ams_wad.cfg_airport a 
	left join ams_wad.calc_airport_payload_demand apd on apd.ap_id = a.ap_id
	group by a.city_id) d
	where d.pax_demand_for_period is null 
	order by ap_count desc;
    
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    SET done = TRUE;
    call ams_wad.log_sp('ams_wad.add_city_payload_demand', stTime, logNote);
END;


DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.add_city_payload_demand', stTime, logNote);
END;



set stTime = current_timestamp();
set spname = 'ams_wad.add_city_payload_demand';

SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;

-- TRUNCATE `ams_wad`.`calc_city_payload_demand`;
-- commit;

OPEN cursorCityIds;
read_loop: LOOP
    FETCH cursorCityIds INTO i_city_id, i_ap_count;
    
    IF done THEN
      LEAVE read_loop;
    END IF;
    
    IF i_ap_count>0 THEN
		delete from ams_wad.calc_city_payload_demand 
        where city_id = i_city_id;
        commit;
        
		CALL ams_wad.calc_city_payload_demand_insert(i_city_id);
		SET countRows = countRows+1;
	END IF;
   
END LOOP;

CLOSE cursorCityIds;
  
COMMIT;

SET logNote=CONCAT('Updated ',countRows,' calc_city_payload_demand rows.');
call log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish ', '<-' );
call log_sp(spname, stTime, logNote);


select countRows as rowsCount from dual;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ams_ac_insurance_week` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `ams_ac_insurance_week`()
BEGIN

DECLARE countRows, i_ac_id, i_al_id, o_tr_id INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE done INT DEFAULT FALSE;
DECLARE cursor_ch CURSOR FOR SELECT ac.ac_id, ac.owner_al_id as al_id
			FROM ams_ac.ams_aircraft ac
			join ams_ac.ams_aircraft_curr acc on acc.ac_id = ac.ac_id
			where ac.owner_al_id is not null and ac.ac_status_id = 2
			and (if(acc.insurance_expiration_date is null, 0, UNIX_TIMESTAMP(acc.insurance_expiration_date)) - UNIX_TIMESTAMP(current_timestamp()))<86400;
                        
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.ams_ac_insurance_week', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.ams_ac_insurance_week';
set countRows = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_ch;

	read_loop: LOOP
			FETCH cursor_ch INTO i_ac_id, i_al_id;
		
        IF done THEN
			SET logNote=CONCAT('No AC for insurance found ', 'done' );
			call ams_wad.log_sp(spname, stTime, logNote);
			LEAVE read_loop;
		END IF;
        
        SET logNote=CONCAT('Start insurance for ac_id: ',i_ac_id);
		-- call ams_wad.log_sp(spname, stTime, logNote);
        
        CALL `ams_al`.`sp_tr_ac_insurance_week`(i_ac_id, o_tr_id);
            
		SET countRows = countRows+1;
		
     END LOOP;

	CLOSE cursor_ch;
    COMMIT;
    
	SET logNote=CONCAT('Created ', countRows, ' insurance transactions;');
	call ams_wad.log_sp(spname, stTime, logNote);

	SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ams_al_hub_upkeep_week` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `ams_al_hub_upkeep_week`()
BEGIN

DECLARE countRows, i_hub_id, i_al_id, o_tr_id INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE done INT DEFAULT FALSE;
DECLARE cursor_ch CURSOR FOR SELECT h. hub_id, h.al_id
			FROM ams_al.ams_al_hub h
			where  (if(h.upkeep_day is null, 0, UNIX_TIMESTAMP(h.upkeep_day)) - UNIX_TIMESTAMP(current_timestamp()))<86400;
                        
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.ams_al_hub_upkeep_week', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.ams_al_hub_upkeep_week';
set countRows = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_ch;

	read_loop: LOOP
			FETCH cursor_ch INTO i_hub_id, i_al_id;
		
        IF done THEN
			SET logNote=CONCAT('No hub for upkeep found ', 'done' );
			call ams_wad.log_sp(spname, stTime, logNote);
			LEAVE read_loop;
		END IF;
        
        SET logNote=CONCAT('Start hub upkeep for hub_id: ',i_hub_id);
		-- call ams_wad.log_sp(spname, stTime, logNote);
        
        CALL `ams_al`.`sp_tr_al_hub_upkeep_week`(i_hub_id, o_tr_id);
            
		SET countRows = countRows+1;
		
     END LOOP;

	CLOSE cursor_ch;
    COMMIT;
    
	SET logNote=CONCAT('Created ', countRows, ' hub upkeep transactions;');
	call ams_wad.log_sp(spname, stTime, logNote);

	SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ams_charter_insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `ams_charter_insert`()
BEGIN

DECLARE countRows, i_charter_id INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE done INT DEFAULT FALSE;
DECLARE cursor_ch CURSOR FOR select ch.charter_id from ams_wad.cfg_charter ch
	join ams_wad.cfg_airport dep on dep.ap_id=ch.d_ap_id and dep.ap_active = 1
	join ams_wad.cfg_airport arr on arr.ap_id = ch.a_ap_id and arr.ap_active = 1
	where now() between ifnull(ch.pstart, DATE_ADD(now(), INTERVAL -1 DAY)) and ifnull(ch.pend,DATE_ADD(now(), INTERVAL 1 DAY))
    and (if(ch.next_run is null, 0, UNIX_TIMESTAMP(ch.next_run)) - UNIX_TIMESTAMP(current_timestamp()))<0;
                        
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.ams_charter_insert', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.ams_charter_insert';
set countRows = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_ch;

	read_loop: LOOP
			FETCH cursor_ch INTO i_charter_id;
		
        IF done THEN
			SET logNote=CONCAT('No charter_id found ', 'done' );
			call ams_wad.log_sp(spname, stTime, logNote);
			LEAVE read_loop;
		END IF;
        
        SET logNote=CONCAT('Start processing charter_id: ',i_charter_id);
		call ams_wad.log_sp(spname, stTime, logNote);
        
        INSERT INTO ams_ac.ams_charter
			(charter_id, d_ap_id, a_ap_id,
			fl_name, fl_description,
			payload_type, pax, cargo_kg, payload_kg, price,
			distance_km, completed,expiration)
		select ch.charter_id, ch.d_ap_id, ch.a_ap_id,
			concat(ch.fl_name, LPAD((ch.charter_id),3,'0')) as fl_name, ch.fl_description,
			ch.payload_type, ch.pax, ch.cargo_kg, ch.payload_kg, ch.price,
			ch.distance_km, 0 as completed, DATE_ADD(now(), INTERVAL ch.period_days DAY) as expiration
		 from ams_wad.cfg_charter ch
		join ams_wad.cfg_airport dep on dep.ap_id=ch.d_ap_id and dep.ap_active = 1
		join ams_wad.cfg_airport arr on arr.ap_id = ch.a_ap_id and arr.ap_active = 1
		where ch.charter_id = i_charter_id;
        
        UPDATE ams_wad.cfg_charter 
			set last_run = now(),
            next_run = DATE_ADD(current_timestamp(), INTERVAL period_days DAY)
		where charter_id = i_charter_id;
        
       COMMIT;
            
		SET countRows = countRows+1;
		
     END LOOP;

	CLOSE cursor_ch;
    COMMIT;
    
	SET logNote=CONCAT('Created ', countRows, ' charters;');
	call ams_wad.log_sp(spname, stTime, logNote);
	
    -- generate ppd for expired charter flights
    set countRows = 0;
	select COUNT(*) INTO countRows
	from ams_ac.ams_charter
	where (if(expiration is null, 0, UNIX_TIMESTAMP(expiration)) - UNIX_TIMESTAMP(current_timestamp()))<0;
    
    IF countRows > 0 THEN
		call ams_wad.ams_charter_to_ppd();
    END IF;
    
    -- ARCHIVE 
    INSERT INTO ams_ac.ams_charter_h
		(ch_id, charter_id, d_ap_id, a_ap_id, fl_name, fl_description,
		payload_type, pax, cargo_kg, payload_kg, price, distance_km, completed,
		pax_completed, cargo_kg_compleated, payload_kg_completed, price_completed,
		expiration,	adate)
    select ch_id, charter_id, d_ap_id, a_ap_id, fl_name, fl_description,
		payload_type, pax, cargo_kg, payload_kg, price, distance_km, completed,
		pax_completed, cargo_kg_compleated, payload_kg_completed, price_completed,
		expiration,	adate from ams_ac.ams_charter
    where (if(expiration is null, 0, UNIX_TIMESTAMP(expiration)) - UNIX_TIMESTAMP(current_timestamp()))<0;
    
    delete from ams_ac.ams_charter
    where (if(expiration is null, 0, UNIX_TIMESTAMP(expiration)) - UNIX_TIMESTAMP(current_timestamp()))<0;
    SET countRows = ROW_COUNT();
    COMMIT;
    
    SET logNote=CONCAT('Archived ', countRows, ' charters;');
	call ams_wad.log_sp(spname, stTime, logNote);

	SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `ams_charter_to_ppd` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `ams_charter_to_ppd`()
BEGIN

DECLARE countRows, i_ch_id, i_ppd_Id INT;
DECLARE i_pax, ippdCount, ipaxCount INT;
DECLARE d_price, d_ap_type_factor, d_distance_km DOUBLE;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE done INT DEFAULT FALSE;
DECLARE cursor_ch CURSOR FOR select ch.ch_id, (ch.pax - ifnull(ch.pax_completed,0)) as pax,
								round((ch.price - ifnull(ch.price_completed ,0)),2) as price
								from ams_ac.ams_charter ch
								where (if(ch.expiration is null, 0, UNIX_TIMESTAMP(ch.expiration)) - UNIX_TIMESTAMP(current_timestamp()))<0;
                        
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.ams_charter_to_ppd', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.ams_charter_to_ppd';
set countRows = 0;

SET logNote=CONCAT('Start () ->' );
call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_ch;

	read_loop: LOOP
			FETCH cursor_ch INTO i_ch_id, i_pax, d_price;
		
        IF done THEN
			SET logNote=CONCAT('No ch_id found ', 'done' );
			call ams_wad.log_sp(spname, stTime, logNote);
			LEAVE read_loop;
		END IF;
        
        SET logNote=CONCAT('Transfer ', i_pax,' to ppd for ch_id: ',i_ch_id, ', price: ', d_price);
		call ams_wad.log_sp(spname, stTime, logNote);
        set ippdCount = i_pax;
		ppdCountLoop: WHILE ippdCount > 0 DO
			SET ipaxCount = 0;
			-- Callculate max-ppd to add -> ipaxCount
			SELECT FLOOR(RAND() * ((FLOOR(d.ap_type_factor)+1)-1) +1 ) as ipaxCount, d.ap_type_factor, d.distance_km
			INTO ipaxCount, d_ap_type_factor, d_distance_km
			FROM ams_ac.ams_charter ch
			join ams_wad.cfg_airport_destination d on d.dep_ap_id = ch.d_ap_id and d.arr_ap_id = ch.a_ap_id
			where ch.ch_id = i_ch_id;
			
            
			IF ipaxCount > ippdCount THEN
				SET ipaxCount = ippdCount;
			END IF;
            SET ippdCount = ippdCount - ipaxCount;
			
            IF ipaxCount < 1 THEN
				SET logNote=CONCAT(' ipaxCount ', ipaxCount, 'done' );
				call ams_wad.log_sp(spname, stTime, logNote);
				LEAVE ppdCountLoop;
			END IF;
        
			INSERT INTO ams_wad.ams_airport_ppd
				(dest_id, dep_ap_id, sequence, pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
			SELECT d.dest_id, ch.d_ap_id as dep_ap_id, 1 as sequence,
				p.pax, pc.pax_class_id, 13 as ppd_type_id,
				(p.pax*pwkg.param_value) as payload_kg,
				round((p.all_price*(p.pax/p.all_pax)),2) as max_ticket_price,
				concat(ch.fl_name, ' ', ch.fl_description, ' From Charter.' ) as note
			FROM ams_ac.ams_charter ch
			join (select ipaxCount as pax, i_pax as all_pax, d_price as all_price) p on 1=1
			join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'pax_weight_kg' ) pwkg on 1=1
			join ams_wad.cfg_airport_destination d on d.dep_ap_id = ch.d_ap_id and d.arr_ap_id = ch.a_ap_id
			join ams_wad.cfg_pax_class pc on pc.pax_class_abrev = ch.payload_type
			where ch.ch_id = i_ch_id;
			
			SET i_ppd_Id = LAST_INSERT_ID();
			COMMIT;
			
			SET logNote=CONCAT(i_ppd_Id, ' ppd row inserted for ipaxCount:', ipaxCount);
			call ams_wad.log_sp(spname, stTime, logNote);
			
			SET countRows = countRows+1;
		END WHILE;
        
	END LOOP;

CLOSE cursor_ch;
    
SET logNote=CONCAT('Created ', countRows, ' ppd rows from expired charter flights.');
call ams_wad.log_sp(spname, stTime, logNote);
	
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calc_ams_airport_cpd` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `calc_ams_airport_cpd`(in_dep_ap_id int, in_cpd_kg int, in in_sequence int)
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE done INT DEFAULT FALSE;
DECLARE i_arr_ap_id, iperiod_days, i_dest_id INT;
DECLARE iId, i_ap_type_factor, i_pax_class_id, i_ppd_type_id INT;
DECLARE d_cpd_kg, cpd_kg DOUBLE;
declare d_ap_type_factor, d_distance_km DOUBLE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    SET done = TRUE;
    call ams_wad.log_sp('ams_wad.calc_ams_airport_cpd', stTime, logNote);
END;


DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.calc_ams_airport_cpd', stTime, logNote);
END;



set stTime = current_timestamp();
set spname = 'ams_wad.calc_ams_airport_cpd';
set d_cpd_kg = in_cpd_kg;
set cpd_kg = in_cpd_kg;

SET logNote=CONCAT('Start', '->' );
-- call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;
SET logNote=CONCAT(' in_dep_ap_id:', in_dep_ap_id, ',  in_cpd_kg:', in_cpd_kg );
-- call ams_wad.log_sp(spname, stTime, logNote);


ppdCountLoop: WHILE d_cpd_kg > 0 DO

	-- Get Arrival Airport ID Randome a.wad_status_id = 4 and
	SELECT a.ap_id INTO i_arr_ap_id
	FROM ams_wad.cfg_airport a
    join (select * from ams_wad.cfg_airport where ap_id = in_dep_ap_id) dep on 1=1
    join ams_wad.cfg_airport_type apt on apt.ap_type_id = dep.ap_type_id
	JOIN (SELECT  111.045 AS distance_unit from dual) AS p ON 1=1
	where  a.ap_id <> in_dep_ap_id and a.ap_active = 1 and
    a.ap_lat
	 BETWEEN dep.ap_lat  - (apt.range_km / p.distance_unit)
		 AND dep.ap_lat  + (apt.range_km / p.distance_unit)
	AND a.ap_lon
	 BETWEEN dep.ap_lon - (apt.range_km / (p.distance_unit * COS(RADIANS(dep.ap_lat))))
		 AND dep.ap_lon + (apt.range_km / (p.distance_unit * COS(RADIANS(dep.ap_lat))))
	ORDER BY RAND()
	LIMIT 1;
    /**
	SELECT ap_id INTO i_arr_ap_id
	FROM ams_wad.cfg_airport
	where wad_status_id = 4 and ap_id <> in_dep_ap_id
	ORDER BY RAND()
	LIMIT 1;
	*/
    -- Config Destination Row
	call ams_wad.get_cfg_airport_destination(in_dep_ap_id, i_arr_ap_id, i_dest_id);
	
    SET logNote=CONCAT('i_dest_id:', i_dest_id, ',  i_arr_ap_id:', i_arr_ap_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);

	if(i_dest_id is null) then
		SET logNote=CONCAT('ERROR: Failed to get dest_id for dep_ap_id', in_dep_ap_id, ',  arr_ap_id:', i_arr_ap_id );
		call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE ppdCountLoop;
	END IF;
    -- Callculate max-cpd to add -> cpd_kg
    SELECT round((RAND() * (FLOOR(d_cpd_kg/d.ap_type_factor))+60) ,2), 
    d.ap_type_factor, d.distance_km
    INTO cpd_kg, d_ap_type_factor, d_distance_km
    FROM ams_wad.cfg_airport_destination d
    where d.dest_id = i_dest_id;
    
    IF cpd_kg > d_cpd_kg OR cpd_kg < 1 THEN
		SET cpd_kg = d_cpd_kg;
	END IF;
    
    -- set payload class: E-1-B-2-F-3 ppd, 4-cpd class 
    set i_pax_class_id = 4;
    
    SELECT ppd_type_id INTO i_ppd_type_id 
    FROM ams_wad.cfg_ppd_type where pax_class_id = i_pax_class_id
	ORDER BY RAND() LIMIT 1;
    
    SET logNote=CONCAT('i_ppd_type_id:', i_ppd_type_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    SET logNote=CONCAT('cpd_kg:', cpd_kg, ',  d_distance_km:', d_distance_km );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_wad.ams_airport_cpd
	(dest_id, dep_ap_id, sequence, payload_kg, pax_class_id, ppd_type_id, max_ticket_price, note)
    select d.dest_id as dest_id, p.dep_ap_id, p.sequence as sequence,
	 ROUND(p.pax,2) as pax, p.pax_class_id as pax_class_id, p.ppd_type_id as ppd_type_id, 
	 ROUND((p.pax * pwkg.param_value * d_distance_km * d_ap_type_factor)*(if((d.ap_type_factor<5 and d.distance_km < 150), 4 , 1)), 2) as max_ticket_price,
	concat(round(p.pax,2),' kg ', pt.ppd_type_name, ' from ', dep.ap_name, ' (', if(dep.ap_iata is null, dep.ap_icao, dep.ap_iata), ' -> ', if(arr.ap_iata is null, arr.ap_icao, arr.ap_iata), ')') as note
	FROM ams_wad.cfg_airport_destination d
	join (select in_dep_ap_id as dep_ap_id, cpd_kg as pax, i_pax_class_id as pax_class_id, i_ppd_type_id as ppd_type_id, in_sequence as sequence from dual) p on 1=1
	join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'max_ticket_price_cpd_kg_per_km' ) pwkg on 1=1
    join ams_wad.cfg_pax_class c on c.pax_class_id = p.pax_class_id
	join ams_wad.cfg_ppd_type pt on pt.ppd_type_id = p.ppd_type_id
	join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id
    join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
	where d.dest_id = i_dest_id;
    
     SET iId = LAST_INSERT_ID();
        
	SET logNote=CONCAT('inserted ',iId,' ams_wad.ams_airport_cpd ');
	-- call ams_wad.log_sp(spname, stTime, logNote);
	COMMIT;
    
    SET d_cpd_kg = d_cpd_kg - cpd_kg;
    set countRows = countRows +1;
	
END WHILE;


-- SET logNote=CONCAT('Updated ',countRows,' airports from ams_wad.calc_airport_payload_demand.');
-- call log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish ', '<- countRows:', countRows );
-- call log_sp(spname, stTime, logNote);


-- select countRows as countRows from dual;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calc_ams_airport_cpd_land_trans` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `calc_ams_airport_cpd_land_trans`()
BEGIN

DECLARE i_cpd_id, i_dep_ap_id, i_booking_sequence INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_ap_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.calc_ams_airport_cpd_land_trans', stTime, logNote);
END;

DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.calc_ams_airport_cpd_land_trans', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.calc_ams_airport_cpd_land_trans';
SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);

call ams_wad.calc_ams_airport_cpd_land_trans_dep();

call ams_wad.calc_ams_airport_cpd_land_trans_arr();

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calc_ams_airport_cpd_land_trans_arr` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `calc_ams_airport_cpd_land_trans_arr`()
BEGIN

DECLARE i_cpd_id, i_arr_ap_id, i_dep_ap_id, i_booking_sequence, i_dest_id INT;
DECLARE countRows, i_cpdrows_tr, i_cpdrows_price INT;
DECLARE old_arr_ap_id, new_dest_id, old_dep_ap_id INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote, old_arr_ap_name VARCHAR(4000);

DECLARE spname, s_ap_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_flp CURSOR FOR SELECT p.cpd_id, ifnull(p.booking_sequence_arr, 0) as booking_sequence
							FROM ams_wad.ams_airport_cpd p
                            left join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
							join ams_wad.cfg_airport ap on ap.ap_id = d.arr_ap_id
							join ams_wad.ams_airport_counters apc on apc.ap_id = ap.ap_id
                            join (
								SELECT max(if(param_id=45, param_value, null)) as pcpd_month_since_adate,
								max(if(param_id=46, param_value, null)) as pcpd_days_after_udate  
								FROM ams_al.cfg_callc_param where param_id in(45, 46)
								group by measure
							) par on 1=1
							where apc.flpns_arr_count=0 -- and apc.flpns_dep_count=0 
							and  p.org_arr_ap_id is null  and TIMESTAMPDIFF(MONTH,p.adate, now()) > par.pcpd_month_since_adate
                            and TIMESTAMPDIFF(day,p.udate, now()) > par.pcpd_days_after_udate
							order by ap.ap_type_id,  p.adate limit 50;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.calc_ams_airport_cpd_land_trans_arr', stTime, logNote);
END;

DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.calc_ams_airport_cpd_land_trans_arr', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.calc_ams_airport_cpd_land_trans_arr';
SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;
SET i_cpdrows_tr = 0;
SET i_cpdrows_price = 0;

OPEN cursor_flp;

read_loop: LOOP
	
    FETCH cursor_flp INTO i_cpd_id, i_booking_sequence;
	
	IF done THEN
		SET logNote=CONCAT('No i_cpd_id in queue found ', 'done' );
		 call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
    
    IF i_booking_sequence < 10 THEN
		SET logNote=CONCAT('i_cpd_id: ', i_cpd_id, ' update price ', ((i_booking_sequence+1)*20), '%');
		-- call ams_wad.log_sp(spname, stTime, logNote);
        
        UPDATE ams_wad.ams_airport_cpd p
		set p.max_ticket_price = ROUND((p.max_ticket_price*1.2),2),
		p.booking_sequence_arr = (ifnull(p.booking_sequence_arr, 0) + 1)
		where p.cpd_id = i_cpd_id;
        COMMIT;
        SET i_cpdrows_price = i_cpdrows_price + 1;
    ELSE
		SET countRows = 0;
        
        select count(*) INTO countRows
		FROM ams_wad.cfg_airport ap 
		join ams_wad.ams_airport_counters apc on apc.ap_id = ap.ap_id and apc.flpns_arr_count>0  
        join ams_wad.ams_airport_cpd p on 1=1 and p.cpd_id = i_cpd_id
        join ams_wad.cfg_airport_destination da on da.dest_id = p.dest_id
		join ams_wad.cfg_airport_destination d on d.dep_ap_id = da.arr_ap_id and d.arr_ap_id = ap.ap_id
		order by d.distance_km, apc.flpns_dep_count desc;
        
		select p.dep_ap_id, ap.ap_name, d.arr_ap_id
        INTO old_dep_ap_id, old_arr_ap_name, old_arr_ap_id 
        from ams_wad.ams_airport_cpd p
        join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
        join ams_wad.cfg_airport ap on ap.ap_id = d.arr_ap_id 
        where p.cpd_id = i_cpd_id;
        
        IF countRows = 0 THEN
			SET logNote=CONCAT('No alternate arrival airport found for ap: ', old_arr_ap_id, ' ', old_arr_ap_name, ' ...');
			call ams_wad.log_sp(spname, stTime, logNote);
        ELSE
        
			SELECT ap.ap_id, ap.ap_name  INTO i_arr_ap_id, s_ap_name
            FROM ams_wad.cfg_airport ap 
			join ams_wad.ams_airport_counters apc on apc.ap_id = ap.ap_id and apc.flpns_arr_count>0  
			join ams_wad.ams_airport_cpd p on 1=1 and p.cpd_id = i_cpd_id
			join ams_wad.cfg_airport_destination da on da.dest_id = p.dest_id
			join ams_wad.cfg_airport_destination d on d.dep_ap_id = da.arr_ap_id and d.arr_ap_id = ap.ap_id
			order by d.distance_km, apc.flpns_arr_count desc
			limit 1;
			
            
			SET logNote=CONCAT('Try Land Transport to: ', i_arr_ap_id, ' ', s_ap_name, '.');
			-- call ams_wad.log_sp(spname, stTime, logNote);
                
			IF i_arr_ap_id > 0 THEN
				set new_dest_id = 0;
				call ams_wad.get_cfg_airport_destination(old_dep_ap_id, i_arr_ap_id, new_dest_id);
				
				UPDATE ams_wad.ams_airport_cpd p
				set p.org_arr_ap_id = old_arr_ap_id,
					p.dest_id = new_dest_id,
					p.note = concat(p.note, ' On arrival land transport from ', old_arr_ap_name, ' to ', s_ap_name)
				where p.cpd_id = i_cpd_id;
				commit;
                SET i_cpdrows_tr = i_cpdrows_tr + 1;
				SET logNote=CONCAT(' On arrival land transport from ', old_arr_ap_name, ' to ', s_ap_name, '.');
				-- call ams_wad.log_sp(spname, stTime, logNote);
			END IF;
            
        END IF;
		
    END IF;

	SET logNote=CONCAT('Finish i_cpd_id:', i_cpd_id, ',  i_booking_sequence_arr:', i_booking_sequence );
	-- call ams_wad.log_sp(spname, stTime, logNote);
END LOOP;
 
CLOSE cursor_flp;
SET logNote=CONCAT('Price Changed for ', i_cpdrows_price, ' cpd rows and land transport for ', i_cpdrows_tr, ' cpd rows.' );
call ams_wad.log_sp(spname, stTime, logNote);
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calc_ams_airport_cpd_land_trans_dep` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `calc_ams_airport_cpd_land_trans_dep`()
BEGIN

DECLARE i_cpd_id, i_dep_ap_id, i_booking_sequence, new_dest_id INT;
DECLARE countRows, i_cpdrows_tr, i_cpdrows_price INT;
DECLARE old_dep_ap_id, i_arr_ap_id INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote, old_dep_ap_name VARCHAR(4000);

DECLARE spname, s_ap_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_flp CURSOR FOR SELECT p.cpd_id, ifnull(p.booking_sequence, 0) as booking_sequence
							FROM ams_wad.ams_airport_cpd p
							join ams_wad.cfg_airport ap on ap.ap_id = p.dep_ap_id
							join ams_wad.ams_airport_counters apc on apc.ap_id = ap.ap_id
                            join (
								SELECT max(if(param_id=45, param_value, null)) as pcpd_month_since_adate,
								max(if(param_id=46, param_value, null)) as pcpd_days_after_udate  
								FROM ams_al.cfg_callc_param where param_id in(45, 46)
								group by measure
							) par on 1=1
							where apc.flpns_dep_count=0 and  p.org_dep_ap_id is null  
                            and TIMESTAMPDIFF(MONTH,p.adate, now()) > par.pcpd_month_since_adate
                            and TIMESTAMPDIFF(day,p.udate, now()) > par.pcpd_days_after_udate
							order by ap.ap_type_id,  p.adate limit 50;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.calc_ams_airport_cpd_land_trans_dep', stTime, logNote);
END;

DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.calc_ams_airport_cpd_land_trans_dep', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.calc_ams_airport_cpd_land_trans_dep';
SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;
SET i_cpdrows_price = 0;
SET i_cpdrows_tr = 0;

OPEN cursor_flp;

read_loop: LOOP
	
    FETCH cursor_flp INTO i_cpd_id, i_booking_sequence;
	
	IF done THEN
		SET logNote=CONCAT('No i_cpd_id in queue found ', 'done' );
		 call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
    
    IF i_booking_sequence < 10 THEN
		SET logNote=CONCAT('i_cpd_id: ', i_cpd_id, ' update price ', ((i_booking_sequence+1)*20), '%');
		call ams_wad.log_sp(spname, stTime, logNote);
        
        UPDATE ams_wad.ams_airport_cpd p
		set p.max_ticket_price = ROUND((p.max_ticket_price*1.2),2),
		p.booking_sequence = (ifnull(p.booking_sequence, 0) + 1)
		where p.cpd_id = i_cpd_id;
        COMMIT;
        SET i_cpdrows_price = i_cpdrows_price + 1;
    ELSE
		SET countRows = 0;
        SET old_dep_ap_id = 0;
        SET i_arr_ap_id = 0;
        
        select count(*) INTO countRows
		FROM ams_wad.cfg_airport ap 
		join ams_wad.ams_airport_counters apc on apc.ap_id = ap.ap_id and apc.flpns_dep_count>0  
        join ams_wad.ams_airport_cpd p on 1=1 and p.cpd_id = i_cpd_id 
		join ams_wad.cfg_airport_destination d on d.dep_ap_id = p.dep_ap_id and d.arr_ap_id = ap.ap_id
		order by distance_km, flpns_dep_count desc;
        
        select p.dep_ap_id, ap.ap_name, d.arr_ap_id
        INTO old_dep_ap_id, old_dep_ap_name, i_arr_ap_id 
        from ams_wad.ams_airport_cpd p
        join ams_wad.cfg_airport ap on ap.ap_id = p.dep_ap_id 
        join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
        where p.cpd_id = i_cpd_id;
        
        IF countRows = 0 THEN
			SET logNote=CONCAT('No alternate departure airport found for ap: ', i_dep_ap_id, ' ', old_dep_ap_name, ' ...');
            call ams_wad.log_sp(spname, stTime, logNote);
        ELSE
        
			select ap.ap_id, ap.ap_name
			INTO i_dep_ap_id, s_ap_name
			FROM ams_wad.cfg_airport ap 
			join ams_wad.ams_airport_counters apc on apc.ap_id = ap.ap_id and apc.flpns_dep_count>0  
			join ams_wad.ams_airport_cpd p on 1=1 and p.cpd_id = i_cpd_id 
			join ams_wad.cfg_airport_destination d on d.dep_ap_id = p.dep_ap_id and d.arr_ap_id = ap.ap_id
			order by distance_km, flpns_dep_count desc
			limit 1;
           
			SET logNote=CONCAT('Try Land Transport to: ', i_dep_ap_id, ' ', s_ap_name, '.');
			call ams_wad.log_sp(spname, stTime, logNote);
                
			IF i_dep_ap_id > 0 THEN
				set new_dest_id = 0;
				call ams_wad.get_cfg_airport_destination(i_dep_ap_id, i_arr_ap_id, new_dest_id);
            
				UPDATE ams_wad.ams_airport_cpd p
				set p.org_dep_ap_id = old_dep_ap_id,
					p.dep_ap_id = i_dep_ap_id,
                    p.dest_id = new_dest_id,
					p.note = concat(p.note, ' Land transport to ', s_ap_name)
				where p.cpd_id = i_cpd_id;
				commit;
				SET logNote=CONCAT('Land transport to: ', s_ap_name, '.');
				call ams_wad.log_sp(spname, stTime, logNote);
			END IF;
            
        END IF;
		
    END IF;

	SET logNote=CONCAT('Finish i_ppd_id:', i_cpd_id, ',  i_booking_sequence:', i_booking_sequence );
	call ams_wad.log_sp(spname, stTime, logNote);
END LOOP;
 
CLOSE cursor_flp;

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calc_ams_airport_ppd` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `calc_ams_airport_ppd`(in_dep_ap_id int, in_ppd_count int, in in_sequence int)
BEGIN

DECLARE countRows, loopIdx INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE done INT DEFAULT FALSE;
DECLARE i_arr_ap_id, iperiod_days, ippdCount, i_dest_id, i_ppd_b_count INT;
DECLARE iId, ipaxCount, i_ap_type_factor, i_pax_class_id, i_ppd_type_id INT;
declare d_ap_type_factor, d_distance_km DOUBLE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    SET done = TRUE;
    call ams_wad.log_sp('ams_wad.calc_ams_airport_ppd', stTime, logNote);
END;


DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.calc_ams_airport_ppd', stTime, logNote);
END;


set loopIdx = 1;
set stTime = current_timestamp();
set spname = 'ams_wad.calc_ams_airport_ppd';
set ippdCount = in_ppd_count;
set i_ppd_b_count = 0;

SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;
-- SET logNote=CONCAT('in_dep_ap_id:', in_dep_ap_id, ',  in_ppd_count:', in_ppd_count );
call ams_wad.log_sp(spname, stTime, logNote);


ppdCountLoop: WHILE ippdCount > 0 DO
	SET logNote=CONCAT(loopIdx,' -> in_dep_ap_id:', in_dep_ap_id, ',  ippdCount:', ippdCount );
	call ams_wad.log_sp(spname, stTime, logNote);
	-- Get Arrival Airport ID Randome a.wad_status_id = 4 and 
	SELECT a.ap_id INTO i_arr_ap_id
	FROM ams_wad.cfg_airport a
    join (select * from ams_wad.cfg_airport where ap_id = in_dep_ap_id) dep on 1=1
    join ams_wad.cfg_airport_type apt on apt.ap_type_id = dep.ap_type_id
	JOIN (SELECT  111.045 AS distance_unit from dual) AS p ON 1=1
	where a.ap_id <> in_dep_ap_id and a.ap_active = 1 and
    a.ap_lat
	 BETWEEN dep.ap_lat  - (apt.range_km / p.distance_unit)
		 AND dep.ap_lat  + (apt.range_km / p.distance_unit)
	AND a.ap_lon
	 BETWEEN dep.ap_lon - (apt.range_km / (p.distance_unit * COS(RADIANS(dep.ap_lat))))
		 AND dep.ap_lon + (apt.range_km / (p.distance_unit * COS(RADIANS(dep.ap_lat))))
	ORDER BY RAND()
	LIMIT 1;
    /**
	SELECT ap_id INTO i_arr_ap_id
	FROM ams_wad.cfg_airport
	where wad_status_id = 4 and ap_id <> in_dep_ap_id
	ORDER BY RAND()
	LIMIT 1;
	*/
    SET logNote=CONCAT(loopIdx,' -> i_arr_ap_id:', i_arr_ap_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    -- Config Destination Row
    set i_dest_id = null;
	call ams_wad.get_cfg_airport_destination(in_dep_ap_id, i_arr_ap_id, i_dest_id);
	
    SET logNote=CONCAT(loopIdx, ' i_dest_id:', i_dest_id);
	-- call ams_wad.log_sp(spname, stTime, logNote);

	if(i_dest_id is null) then
		SET logNote=CONCAT('ERROR: Failed to get dest_id for dep_ap_id', in_dep_ap_id, ',  arr_ap_id:', i_arr_ap_id );
		call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE ppdCountLoop;
	END IF;
    -- Callculate max-ppd to add -> ipaxCount
    SELECT FLOOR(RAND() * ((FLOOR(d.ap_type_factor)+1)-1) +1 ) as ipaxCount, d.ap_type_factor, d.distance_km
    INTO ipaxCount, d_ap_type_factor, d_distance_km
    FROM ams_wad.cfg_airport_destination d
    where d.dest_id = i_dest_id;
    
    IF ipaxCount > ippdCount THEN
		SET ipaxCount = ippdCount;
	END IF;
    SET logNote=CONCAT(loopIdx, ' -> ipaxCount:', ipaxCount);
	-- call ams_wad.log_sp(spname, stTime, logNote);
    -- TODO get E-1-B-2-F-3 ppd class 
    set i_pax_class_id = 1;
    
    SELECT if((s.ppd_f/t.pax)<ppdf.param_value and ppfkm.param_value < d.distance_km AND i_ppd_b_count=0, 3, if((s.ppd_b/t.pax)<ppdb.param_value AND i_ppd_b_count=0,2,1)) as pax_class_id
	INTO i_pax_class_id
    FROM ams_wad.st_airport_destination s
	join ams_wad.cfg_airport_destination d on d.dest_id = s.dest_id
	join (SELECT dest_id, (ppd_e + ppd_b + ppd_f+1) as pax
		FROM ams_wad.st_airport_destination where dest_id = i_dest_id) t on t.dest_id = s.dest_id
	join (select param_value FROM ams_al.cfg_callc_param where param_name = 'ppd_f' ) ppdf on 1=1
	join (select param_value FROM ams_al.cfg_callc_param where param_name = 'ppd_b' ) ppdb on 1=1
	join (select param_value FROM ams_al.cfg_callc_param where param_name = 'ppd_f_km_min' ) ppfkm on 1=1
	 where s.dest_id = i_dest_id;
    
	SET logNote=CONCAT(loopIdx,' -> i_pax_class_id:', i_pax_class_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    IF i_pax_class_id > 1 AND ipaxCount > 2 THEN
		set ipaxCount = 1;
    END IF;
    IF i_pax_class_id > 1 then
		set i_ppd_b_count = ipaxCount;
    END IF;
    
    
    SELECT ppd_type_id INTO i_ppd_type_id 
    FROM ams_wad.cfg_ppd_type where pax_class_id = i_pax_class_id
	ORDER BY RAND() LIMIT 1;
    
    SET logNote=CONCAT(loopIdx,' -> i_ppd_type_id:', i_ppd_type_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    SET logNote=CONCAT(loopIdx,' -> ipaxCount:', ipaxCount, ',  d_distance_km:', d_distance_km );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_wad.ams_airport_ppd
	(dest_id, dep_ap_id, sequence, pax, pax_class_id, ppd_type_id, payload_kg, max_ticket_price, note)
    select d.dest_id as dest_id, p.dep_ap_id, p.sequence as sequence,
	 p.pax as pax, p.pax_class_id as pax_class_id, p.ppd_type_id as ppd_type_id, 
	 (p.pax*pwkg.param_value) as payload_kg, 
	 round((p.pax * 
		if( p.pax_class_id =1, d.max_ticket_price_e, 
		if(p.pax_class_id=2, d.max_ticket_price_b, d.max_ticket_price_f)))*(if((d.ap_type_factor<5 and d.distance_km < 150), 4 , 1)),2) as max_ticket_price,
	concat(p.pax, ' ', pt.ppd_type_name, ' from ', dep.ap_name, ' (', if(dep.ap_iata is null, dep.ap_icao, dep.ap_iata), ' -> ', if(arr.ap_iata is null, arr.ap_icao, arr.ap_iata), ')') as note
	FROM ams_wad.cfg_airport_destination d
	join (select in_dep_ap_id as dep_ap_id, ipaxCount as pax, i_pax_class_id as pax_class_id, i_ppd_type_id as ppd_type_id, in_sequence as sequence from dual) p on 1=1
	join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'pax_weight_kg' ) pwkg on 1=1
    join ams_wad.cfg_pax_class c on c.pax_class_id = p.pax_class_id
	join ams_wad.cfg_ppd_type pt on pt.ppd_type_id = p.ppd_type_id
	join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id
    join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
	where d.dest_id = i_dest_id;
    
     SET iId = LAST_INSERT_ID();
        
	SET logNote=CONCAT('inserted ',iId,' ams_wad.ams_airport_ppd ');
	-- call ams_wad.log_sp(spname, stTime, logNote);
	COMMIT;
    
    SET ippdCount = ippdCount - ipaxCount;
	
END WHILE;


-- SET logNote=CONCAT('Updated ',countRows,' airports from ams_wad.calc_airport_payload_demand.');
-- call log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish ', '<- ppdCount:', ippdCount );
-- call log_sp(spname, stTime, logNote);


-- select ippdCount as ppdCount from dual;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calc_ams_airport_ppd_class_downgrade` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `calc_ams_airport_ppd_class_downgrade`()
BEGIN

DECLARE i_ppd_id INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_ap_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_flp CURSOR FOR SELECT p.ppd_id
				FROM ams_wad.ams_airport_ppd p
				join (
					SELECT max(if(param_id=45, param_value, null)) as pcpd_month_since_adate,
					max(if(param_id=46, param_value, null)) as pcpd_days_after_udate  
					FROM ams_al.cfg_callc_param where param_id in(45, 46)
					group by measure
				) par on 1=1
				where p.pax_class_id > 1 and TIMESTAMPDIFF(MONTH,p.adate, now()) > par.pcpd_month_since_adate
				order by p.adate  limit 50;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.calc_ams_airport_ppd_class_downgrade', stTime, logNote);
END;

DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.calc_ams_airport_ppd_class_downgrade', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.calc_ams_airport_ppd_class_downgrade';
SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;

OPEN cursor_flp;

read_loop: LOOP
	
    FETCH cursor_flp INTO i_ppd_id;
	
	IF done THEN
		SET logNote=CONCAT('No i_ppd_id in queue found ', 'done' );
		 call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
    
    IF i_ppd_id > 0 THEN
		SET logNote=CONCAT(countRows, ' - Downgrade i_ppd_id: ',i_ppd_id, ' ');
		call ams_wad.log_sp(spname, stTime, logNote);
		UPDATE ams_wad.ams_airport_ppd p
		set p.pax_class_id = 1, p.note = concat(p.note, ' Downgrade to E.')
		where p.ppd_id = i_ppd_id;
		commit;
		SET countRows = countRows + 1;
	END IF;
		
END LOOP;
 
CLOSE cursor_flp;
SET logNote=CONCAT('Finish. Downgraded to economy ', countRows, ' ppd rows.');
call ams_wad.log_sp(spname, stTime, logNote);
-- SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calc_ams_airport_ppd_land_trans` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `calc_ams_airport_ppd_land_trans`()
BEGIN

DECLARE i_ppd_id, i_dep_ap_id, i_booking_sequence INT;
DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname, s_ap_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.calc_ams_airport_ppd_land_trans', stTime, logNote);
END;

DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.calc_ams_airport_ppd_land_trans', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.calc_ams_airport_ppd_land_trans';
SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;

call ams_wad.calc_ams_airport_ppd_land_trans_dep();
call ams_wad.calc_ams_airport_ppd_land_trans_arr();
CALL ams_wad.calc_ams_airport_ppd_class_downgrade();


SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calc_ams_airport_ppd_land_trans_arr` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `calc_ams_airport_ppd_land_trans_arr`()
BEGIN

DECLARE i_ppd_id, i_arr_ap_id, i_dep_ap_id, i_booking_sequence, i_dest_id INT;
DECLARE countRows, i_cpdrows_tr, i_cpdrows_price INT;
DECLARE old_arr_ap_id, new_dest_id, old_dep_ap_id INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote, old_arr_ap_name VARCHAR(4000);

DECLARE spname, s_ap_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_flp CURSOR FOR SELECT p.ppd_id, ifnull(p.booking_sequence_arr, 0) as booking_sequence
							FROM ams_wad.ams_airport_ppd p
							left join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
							join ams_wad.cfg_airport ap on ap.ap_id = d.arr_ap_id
							join ams_wad.ams_airport_counters apc on apc.ap_id = ap.ap_id
							join (
								SELECT max(if(param_id=45, param_value, null)) as pcpd_month_since_adate,
								max(if(param_id=46, param_value, null)) as pcpd_days_after_udate  
								FROM ams_al.cfg_callc_param where param_id in(45, 46)
								group by measure
							) par on 1=1
							where apc.flpns_arr_count=0 
							and  p.org_arr_ap_id is null  and TIMESTAMPDIFF(MONTH,p.adate, now()) > par.pcpd_month_since_adate
                            and TIMESTAMPDIFF(day,p.udate, now()) > par.pcpd_days_after_udate
							order by ap.ap_type_id,  p.adate  limit 50;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.calc_ams_airport_ppd_land_trans_arr', stTime, logNote);
END;

DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.calc_ams_airport_ppd_land_trans_arr', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.calc_ams_airport_ppd_land_trans_arr';
SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;
SET i_cpdrows_tr = 0;
SET i_cpdrows_price = 0;

OPEN cursor_flp;

read_loop: LOOP
	
    FETCH cursor_flp INTO i_ppd_id, i_booking_sequence;
	
	IF done THEN
		SET logNote=CONCAT('No i_ppd_id in queue found ', 'done' );
		 call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
    
    IF i_booking_sequence < 10 THEN
		SET logNote=CONCAT('i_ppd_id: ', i_ppd_id, ' update price ', ((i_booking_sequence+1)*10), '%');
		-- call ams_wad.log_sp(spname, stTime, logNote);
        
        UPDATE ams_wad.ams_airport_ppd p
		set p.max_ticket_price = ROUND((p.max_ticket_price*1.2),2),
		p.booking_sequence_arr = (ifnull(p.booking_sequence_arr, 0) + 1)
		where p.ppd_id = i_ppd_id;
        COMMIT;
        SET i_cpdrows_price = i_cpdrows_price + 1;
    ELSE
		SET countRows = 0;
        
        select count(*) -- INTO countRows
		FROM ams_wad.cfg_airport ap 
		join ams_wad.ams_airport_counters apc on apc.ap_id = ap.ap_id and apc.flpns_arr_count>0  
        join ams_wad.ams_airport_ppd p on 1=1 and p.ppd_id = i_ppd_id
        join ams_wad.cfg_airport_destination da on da.dest_id = p.dest_id
		join ams_wad.cfg_airport_destination d on d.dep_ap_id = da.arr_ap_id and d.arr_ap_id = ap.ap_id
		order by d.distance_km, apc.flpns_dep_count desc;
        
		select p.dep_ap_id, ap.ap_name, d.arr_ap_id
        INTO old_dep_ap_id, old_arr_ap_name, old_arr_ap_id 
        from ams_wad.ams_airport_ppd p
        join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
        join ams_wad.cfg_airport ap on ap.ap_id = d.arr_ap_id 
        where p.ppd_id = i_ppd_id;
        
        IF countRows = 0 THEN
			SET logNote=CONCAT('No alternate arrival airport found for ap: ', old_arr_ap_id, ' ', old_arr_ap_name, ' ...');
			call ams_wad.log_sp(spname, stTime, logNote);
        ELSE
        
			SELECT ap.ap_id, ap.ap_name  INTO i_arr_ap_id, s_ap_name
            FROM ams_wad.cfg_airport ap 
			join ams_wad.ams_airport_counters apc on apc.ap_id = ap.ap_id and apc.flpns_arr_count>0  
			join ams_wad.ams_airport_ppd p on 1=1 and p.ppd_id = i_ppd_id
			join ams_wad.cfg_airport_destination da on da.dest_id = p.dest_id
			join ams_wad.cfg_airport_destination d on d.dep_ap_id = da.arr_ap_id and d.arr_ap_id = ap.ap_id
			order by d.distance_km, apc.flpns_dep_count desc
			limit 1;
            SET logNote=CONCAT('Try Land Transport to: ', i_arr_ap_id, ' ', s_ap_name, '.');
			-- call ams_wad.log_sp(spname, stTime, logNote);
            
			IF i_arr_ap_id > 0 THEN
				set new_dest_id = 0;
				
				call ams_wad.get_cfg_airport_destination(old_dep_ap_id, i_arr_ap_id, new_dest_id);
				UPDATE ams_wad.ams_airport_ppd p
				set p.org_arr_ap_id = old_arr_ap_id,
					p.dest_id = new_dest_id,
					p.note = concat(p.note, ' On arrival land transport from ', old_arr_ap_name, ' to ', s_ap_name)
				where p.ppd_id = i_ppd_id;
				commit;
                SET i_cpdrows_tr = i_cpdrows_tr + 1;
				SET logNote=CONCAT('Land transport from: ', s_ap_name, '.');
				-- call ams_wad.log_sp(spname, stTime, logNote);
			END IF;
            
        END IF;
		
    END IF;

	SET logNote=CONCAT('Finish i_ppd_id:', i_ppd_id, ',  i_booking_sequence:', i_booking_sequence );
	-- call ams_wad.log_sp(spname, stTime, logNote);
END LOOP;
 
CLOSE cursor_flp;
SET logNote=CONCAT('Price Changed for ', i_cpdrows_price, ' ppd rows and land transport for ', i_cpdrows_tr, ' ppd rows.' );
call ams_wad.log_sp(spname, stTime, logNote);
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calc_ams_airport_ppd_land_trans_dep` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `calc_ams_airport_ppd_land_trans_dep`()
BEGIN

DECLARE i_ppd_id, i_dep_ap_id, i_booking_sequence, new_dest_id INT;
DECLARE countRows, i_ppdrows_tr, i_ppdrows_price  INT;
DECLARE old_dep_ap_id, i_arr_ap_id INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote, old_dep_ap_name VARCHAR(4000);

DECLARE spname, s_ap_name VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursor_flp CURSOR FOR SELECT p.ppd_id, ifnull(p.booking_sequence, 0) as booking_sequence
							FROM ams_wad.ams_airport_ppd p
							join ams_wad.cfg_airport ap on ap.ap_id = p.dep_ap_id
							join ams_wad.ams_airport_counters apc on apc.ap_id = ap.ap_id
                            join (
								SELECT max(if(param_id=45, param_value, null)) as pcpd_month_since_adate,
								max(if(param_id=46, param_value, null)) as pcpd_days_after_udate  
								FROM ams_al.cfg_callc_param where param_id in(45, 46)
								group by measure
							) par on 1=1
							where apc.flpns_dep_count=0 and  p.org_dep_ap_id is null  
                            and TIMESTAMPDIFF(MONTH,p.adate, now()) > par.pcpd_month_since_adate
                            and TIMESTAMPDIFF(day,p.udate, now()) > par.pcpd_days_after_udate
							order by ap.ap_type_id,  p.adate limit 50;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.calc_ams_airport_ppd_land_trans_dep', stTime, logNote);
END;

DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.calc_ams_airport_ppd_land_trans', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.calc_ams_airport_ppd_land_trans_dep';
SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;
SET i_ppdrows_tr = 0;
SET i_ppdrows_price = 0;

OPEN cursor_flp;

read_loop: LOOP
	
    FETCH cursor_flp INTO i_ppd_id, i_booking_sequence;
	
	IF done THEN
		SET logNote=CONCAT('No i_ppd_id in queue found ', 'done' );
		 call ams_wad.log_sp(spname, stTime, logNote);
		LEAVE read_loop;
	END IF;
    
    IF i_booking_sequence < 10 THEN
		SET logNote=CONCAT('i_ppd_id: ', i_ppd_id, ' update price ', ((i_booking_sequence+1)*10), '%');
		-- call ams_wad.log_sp(spname, stTime, logNote);
        
        UPDATE ams_wad.ams_airport_ppd p
		set p.max_ticket_price = ROUND((p.max_ticket_price*1.2),2),
		p.booking_sequence = (ifnull(p.booking_sequence, 0) + 1)
		where p.ppd_id = i_ppd_id;
        COMMIT;
        SET i_ppdrows_price = i_ppdrows_price + 1;
    ELSE
		SET countRows = 0;
       
        select count(*) INTO countRows
		FROM ams_wad.cfg_airport ap 
		join ams_wad.ams_airport_counters apc on apc.ap_id = ap.ap_id and apc.flpns_dep_count>0  
        join ams_wad.ams_airport_ppd p on 1=1 and p.ppd_id = i_ppd_id 
		join ams_wad.cfg_airport_destination d on d.dep_ap_id = p.dep_ap_id and d.arr_ap_id = ap.ap_id
        order by distance_km, flpns_dep_count desc;
        
        set old_dep_ap_id = 0;
        set i_arr_ap_id = 0;
        
        select p.dep_ap_id, ap.ap_name, d.arr_ap_id
        INTO old_dep_ap_id, old_dep_ap_name, i_arr_ap_id 
        from ams_wad.ams_airport_ppd p
        join ams_wad.cfg_airport ap on ap.ap_id = p.dep_ap_id 
        join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
        where p.ppd_id = i_ppd_id;
        
        IF countRows = 0 THEN
			SET logNote=CONCAT('No alternate airport found for departure airport id: ', old_dep_ap_id, ' ...');
			call ams_wad.log_sp(spname, stTime, logNote);
        ELSE
			set new_dest_id = 0;
			select ap.ap_id, ap.ap_name
			INTO i_dep_ap_id, s_ap_name
			FROM ams_wad.cfg_airport ap 
			join ams_wad.ams_airport_counters apc on apc.ap_id = ap.ap_id and apc.flpns_dep_count>0  
			join ams_wad.ams_airport_ppd p on 1=1 and p.ppd_id = i_ppd_id 
			join ams_wad.cfg_airport_destination d on d.dep_ap_id = p.dep_ap_id and d.arr_ap_id = ap.ap_id
			order by distance_km, flpns_dep_count desc
			limit 1;
            
			SET logNote=CONCAT('Try Land Transport to: ', i_dep_ap_id, ' ', s_ap_name, '.');
			-- call ams_wad.log_sp(spname, stTime, logNote);
                
			IF i_dep_ap_id > 0 THEN
				call ams_wad.get_cfg_airport_destination(i_dep_ap_id, i_arr_ap_id, new_dest_id);
            
				UPDATE ams_wad.ams_airport_ppd p
				set p.org_dep_ap_id = old_dep_ap_id,
				p.dep_ap_id = i_dep_ap_id,
                p.dest_id = new_dest_id,
				p.note = concat(p.note, ' Land transport to ', s_ap_name);
				commit;
                SET i_ppdrows_tr = i_ppdrows_tr + 1;
				SET logNote=CONCAT('Land transport to: ', s_ap_name, '.');
				-- call ams_wad.log_sp(spname, stTime, logNote);
			END IF;
            
        END IF;
		
    END IF;

	SET logNote=CONCAT('Finish i_ppd_id:', i_ppd_id, ',  i_booking_sequence:', i_booking_sequence );
	-- call ams_wad.log_sp(spname, stTime, logNote);
END LOOP;
 
CLOSE cursor_flp;
SET logNote=CONCAT('Price update ', i_ppdrows_price, ' rows,  Transfered ', i_ppdrows_tr, ' ppd rows.' );
call ams_wad.log_sp(spname, stTime, logNote);
    
SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calc_city_payload_demand` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `calc_city_payload_demand`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE done INT DEFAULT FALSE;
DECLARE i_city_id, i_ap_count INT;

DECLARE cursorCityIds CURSOR FOR SELECT a.city_id, count(a.ap_id) as ap_count 
	FROM ams_wad.cfg_airport a -- where a.ap_active > 0
    group by a.city_id
	order by ap_count desc;
    
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    SET done = TRUE;
    call ams_wad.log_sp('ams_wad.calc_city_payload_demand', stTime, logNote);
END;


DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.calc_city_payload_demand', stTime, logNote);
END;



set stTime = current_timestamp();
set spname = 'ams_wad.calc_city_payload_demand';

SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;

TRUNCATE `ams_wad`.`calc_city_payload_demand`;
commit;

OPEN cursorCityIds;
read_loop: LOOP
    FETCH cursorCityIds INTO i_city_id, i_ap_count;
    
    IF done THEN
      LEAVE read_loop;
    END IF;
    IF i_ap_count>0 THEN
		CALL ams_wad.calc_city_payload_demand_insert(i_city_id);
		SET countRows = countRows+1;
	END IF;
   
END LOOP;

CLOSE cursorCityIds;
  
COMMIT;

SET logNote=CONCAT('Updated ',countRows,' calc_city_payload_demand rows.');
call log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish ', '<-' );
call log_sp(spname, stTime, logNote);


select countRows as rowsCount from dual;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calc_city_payload_demand_insert` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `calc_city_payload_demand_insert`(IN in_city_id int)
BEGIN

DECLARE countRows, iId INT;
DECLARE i_population, i_pax_demand_for_year, i_ap_type_sum, i_ap_count INT;
DECLARE d_pax_demand_for_day, d_pax_demand_period_days DOUBLE;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
declare iH, iM, i_sequence int;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.calc_city_payload_demand_insert', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.calc_city_payload_demand_insert';

SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);

SELECT ifnull(max(sequence),0)+1 as sequence  
INTO i_sequence
FROM ams_wad.calc_airport_payload_demand
where city_id=in_city_id;

DELETE FROM ams_wad.calc_city_payload_demand WHERE city_id = in_city_id;
DELETE FROM ams_wad.calc_airport_payload_demand WHERE city_id = in_city_id;

COMMIT;

select ifnull(c.population, 1000),
round(ifnull(c.population, 1000) * 
	(case 
		when ifnull(c.population, 1000) > 10000000 then 0.12
		when ifnull(c.population, 1000) < 10000000 and ifnull(c.population, 1000) >= 500000 then 0.018
		when ifnull(c.population, 1000) < 500000 and ifnull(c.population, 1000) >= 100000 then 0.025
        when ifnull(c.population, 1000) < 100000 and ifnull(c.population, 1000) >= 1000 then 0.055
		when ifnull(c.population, 1000) < 1000 then 0.09
	end),0) as pax_demand_for_year,
round((ifnull(c.population, 1000) * 
	(case 
		when ifnull(c.population, 1000) > 10000000 then 0.12
		when ifnull(c.population, 1000) < 10000000 and ifnull(c.population, 1000) >= 500000 then 0.018
		when ifnull(c.population, 1000) < 500000 and ifnull(c.population, 1000) >= 100000 then 0.025
		when ifnull(c.population, 1000) < 100000 and ifnull(c.population, 1000) >= 1000 then 0.055
		when ifnull(c.population, 1000) < 1000 then 0.09
	end))/365,3) as pax_demand_for_day
into i_population, i_pax_demand_for_year, d_pax_demand_for_day 
from ams_wad.cfg_city c where c.city_id=in_city_id;  

SET logNote=CONCAT(' in_city_id:', in_city_id, ', i_population:', i_population, ', i_pax_demand_for_year:', i_pax_demand_for_year, ', d_pax_demand_for_day:', d_pax_demand_for_day);
 call ams_wad.log_sp(spname, stTime, logNote);   

SELECT a.city_id, count(a.ap_id) ap_count, 
sum(if(ifnull(a.ap_type_id, 1)=0, 1, ifnull(a.ap_type_id, 1))) as ap_type_sum
into countRows, i_ap_count, i_ap_type_sum
FROM ams_wad.cfg_airport a where a.city_id = in_city_id;

SET logNote=CONCAT(' i_ap_count:', i_ap_count, ', i_ap_type_sum:', i_ap_type_sum, ', in_city_id:', in_city_id);
 call ams_wad.log_sp(spname, stTime, logNote); 

IF i_pax_demand_for_year > 0 THEN

	select FLOOR(RAND()*23)+1 as iH, FLOOR(RAND()*59)+1 as iM 
    INTO iH, iM from dual;
    SET logNote=CONCAT('time   ', iH, ':', iM, ':00');
	call ams_wad.log_sp(spname, stTime, logNote);

	set d_pax_demand_period_days = 1;
    IF d_pax_demand_for_day < 1 THEN
		SET d_pax_demand_period_days = round(1/d_pax_demand_for_day, 0);
    END IF;
    
    SET logNote=CONCAT('d_pax_demand_period_days  ', d_pax_demand_period_days);
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
	INSERT INTO ams_wad.calc_city_payload_demand
	(city_id, pax_demand_for_year, pax_demand_for_day,
		pax_demand_for_period, cargo_demand_for_period_kg, pax_demand_period_days, 
		pax_demand_period_nexttime, ap_count, ap_type_sum)
	select city_id as city_id, 
    i_pax_demand_for_year as pax_demand_for_year, 
    d_pax_demand_for_day as pax_demand_for_day,
	if(d_pax_demand_for_day>1, round(d_pax_demand_for_day, 0),
	round((1/d_pax_demand_for_day)*d_pax_demand_for_day, 0)) as pax_demand_for_period,
	round(d_pax_demand_for_day * d_pax_demand_period_days * 86 ,0) as cargo_demand_for_period_kg,
    d_pax_demand_period_days as pax_demand_period_days,
    DATE_ADD(concat(date_format(current_timestamp(), '%Y-%m-%d'), ' ', iH, ':', iM, ':00'), INTERVAL (if(d_pax_demand_period_days is null, 1, d_pax_demand_period_days)*-1) DAY),
	-- FROM_UNIXTIME( UNIX_TIMESTAMP(current_timestamp()) + (if(d_pax_demand_period_days is null, 0, d_pax_demand_period_days)*86400)) as pax_demand_period_nexttime,
	-- current_timestamp() as pax_demand_period_nexttime, 
    round(i_ap_count,0), round(i_ap_type_sum,0) 
	from ams_wad.cfg_city c 
	where c.city_id=in_city_id;
    
    SET iId = LAST_INSERT_ID();
        
	SET logNote=CONCAT('inserted calc_city_payload_demand id ', iId);
	-- call ams_wad.log_sp(spname, stTime, logNote);
    commit;
    
    if i_sequence is null then set i_sequence = 1; END IF;
    
    INSERT INTO ams_wad.calc_airport_payload_demand
    (ap_id, city_id, pax_demand_for_day, 
    pax_demand_for_period, cargo_demand_for_period_kg,
    period_days, period_nexttime, sequence)
    SELECT a.ap_id, a.city_id, 
		round((a.ap_type_id/cpd.ap_type_sum)*cpd.pax_demand_for_day,4) as pax_demand_for_day,
		 if( 
			((a.ap_type_id/cpd.ap_type_sum)*cpd.pax_demand_for_day)>1, 
			round((a.ap_type_id/cpd.ap_type_sum)*cpd.pax_demand_for_day, 0),
			1) as pax_demand_for_period,
		round(((a.ap_type_id/cpd.ap_type_sum)*cpd.pax_demand_for_day) * 
			(if( 
			((a.ap_type_id/cpd.ap_type_sum)*cpd.pax_demand_for_day)>1, 
			cpd.pax_demand_period_days,
			round((1/((a.ap_type_id/cpd.ap_type_sum)*cpd.pax_demand_for_day)), 0)))
			* 86 ,0) as cargo_demand_for_period_kg,
		if( 
			((a.ap_type_id/cpd.ap_type_sum)*cpd.pax_demand_for_day)>1, 
			cpd.pax_demand_period_days,
			round((1/((a.ap_type_id/cpd.ap_type_sum)*cpd.pax_demand_for_day)), 0)) as period_days,
            DATE_ADD(concat(date_format(current_timestamp(), '%Y-%m-%d'), ' ', iH, ':', iM, ':00'), INTERVAL (if(cpd.pax_demand_period_days is null, 1, cpd.pax_demand_period_days)*-1) DAY),
            i_sequence
            -- FROM_UNIXTIME( UNIX_TIMESTAMP(current_timestamp()) + (if(cpd.pax_demand_period_days is null, 0, cpd.pax_demand_period_days)*86400)) as period_nexttime
		-- current_timestamp() as period_nexttime 
	FROM ams_wad.cfg_airport a 
	left join ams_wad.calc_city_payload_demand cpd on cpd.city_id = a.city_id
	where a.city_id=in_city_id; 
    -- and a.ap_active > 0
    SET iId = LAST_INSERT_ID();
        
	SET logNote=CONCAT('inserted calc_city_payload_demand id ', iId);
	-- call ams_wad.log_sp(spname, stTime, logNote);
    commit;

END IF;



SET logNote=CONCAT('Finish ', '<-' );
 call log_sp(spname, stTime, logNote);
 
select 1 as rowsCount from dual;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calc_mtow_rwid` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `calc_mtow_rwid`(IN `rwId` int)
BEGIN

DECLARE countRows INT;
DECLARE apId INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('calc_mtow_rwid', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'calc_mtow_rwid';

SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);

select rw.ap_id into apId from ams_wad.cfg_airport_runway rw where rw.rw_id=rwId;  

START TRANSACTION;
update ams_wad.cfg_airport_runway rw
    join ams_wad.cfg_airport a on a.ap_id = rw.ap_id
    join ams_wad.cfg_rw_surface s on s.rw_surface_id = rw.rw_serface_id
    set rw.mtow_kg = IF( rw.rw_lenght_m>50 ,ROUND((
				 ((rw.rw_lenght_m * 1387*2.4)/(s.mtow_factor/1.746))/
				 (if( a.ap_elevation_m<100, 100, a.ap_elevation_m)/(s.mtow_factor ))
				 ),0), 450) ,
        rw_rodation_radians =  round(radians((rw.rw_rodation_degree) - 90),4),
		rw_rodation_radians_180 = round(radians((rw.rw_rodation_degree) + 90),4),
		rw.rw_svg_path_cercle = CONCAT('M 0, 0 m ',-1*rw.rw_radius,
      ', 0 a ',rw.rw_radius,',',rw.rw_radius,
      ' 0 1,0 ',2*rw.rw_radius,',0 a ',rw.rw_radius,',',rw.rw_radius,
      ' 0 1,0 ',-2*rw.rw_radius,',0 '),
      
      rw.rw_svg_line = (CASE rw.rw_type WHEN 2 THEN
      concat('M ',  round(rw.rw_radius * cos(round(radians((rw.rw_rodation_degree) - 90),4))) , ', ',
			 round(rw.rw_radius *  sin(round(radians((rw.rw_rodation_degree) - 90),4))) + 
             if(rw.rw_direction like '%R%L%', 2,0) - if(rw.rw_direction like '%L%R%', 2,0),
		' L ', round(rw.rw_radius *  cos(round(radians((rw.rw_rodation_degree) + 90),4))), ', ',
		 round(rw.rw_radius *  sin(round(radians((rw.rw_rodation_degree) + 90),4))) +
         if(rw.rw_direction like '%R%L%', 2,0) - if(rw.rw_direction like '%L%R%', 2,0)
		)
		
      ELSE ' M 1 8 v -6 M 1 5 H 3 M 4 8 v -6 ' END)
	where rw.rw_id=rwId;  
  /*
   concat('M ',  round(rw.rw_radius * cos(round(radians((rw.rw_rodation_degree) - 90),4))) , ', ',
			 round(rw.rw_radius *  sin(round(radians((rw.rw_rodation_degree) - 90),4))) ,
		' L ', round(rw.rw_radius *  cos(round(radians((rw.rw_rodation_degree) + 90),4))), ', ',
		 round(rw.rw_radius *  sin(round(radians((rw.rw_rodation_degree) + 90),4)))
		) 
        */
	SET countRows =  ROW_COUNT();
    
COMMIT;


SET logNote=CONCAT('Updated MTOW and SVG for runway ', rwId, '.');
call ams_wad.log_sp(spname, stTime, logNote);   

SET logNote=CONCAT('Update Airport SVG for ap_id = ', apId, '.');
call log_sp(spname, stTime, logNote);

START TRANSACTION;
update ams_wad.cfg_airport a
left join ams_wad.cfg_airport_runway r1 on r1.ap_id = a.ap_id and r1.rw_order = 1
left join ams_wad.cfg_airport_runway r2 on r2.ap_id = a.ap_id and r2.rw_order = 2
left join ams_wad.cfg_airport_runway r3 on r3.ap_id = a.ap_id and r3.rw_order = 3
left join ams_wad.cfg_city c on c.city_id = a.city_id
left join ams_wad.cfg_country ct on ct.country_id = a.country_id
set a.rw_svg_line = concat(r1.rw_svg_line, ' ', IFNULL(r2.rw_svg_line, ''), ' ', IFNULL(r3.rw_svg_line, '')),
a.rw_svg_path_circle = r1.rw_svg_path_cercle,
a.ap_sname = CONCAT(IF((a.ap_iata is null or a.ap_iata = ''), IF((a.ap_icao is null or a.ap_icao = ''), a.ap_id, a.ap_icao), a.ap_iata), ' ',ifnull(c.city_name, c.city_id),', ',ct.country_iso2, ' - ',ifnull(a.ap_name, '')) 
where a.ap_id = apId;

-- SET countRows =  ROW_COUNT();
SELECT row_count() INTO countRows;

COMMIT;

SET logNote=CONCAT('Callculate airport max for ', countRows, ' Airports.');
call log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Callculate max values for ap_id = ', apId, '.');
call log_sp(spname, stTime, logNote);

START TRANSACTION;

	update ams_wad.cfg_airport a
	join (select rw.ap_id, 
	max(rw.rw_lenght_m) as max_rw_lenght_m,
	max(rw.mtow_kg) as max_mtow_kg
	from  ams_wad.cfg_airport_runway rw 
	where rw.ap_id = apId
	group by rw.ap_id) r on r.ap_id = a.ap_id
	set a.max_rw_lenght_m = r.max_rw_lenght_m,
	a.max_mtow_kg = r.max_mtow_kg
	where a.ap_id = apId;

SELECT row_count() INTO countRows;

COMMIT;

SET logNote=CONCAT('Updated airport max values for ', countRows, ' Airports.');
call log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish ', '<-' );
call log_sp(spname, stTime, logNote);

 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calc_rw_mtow_cvg_all` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `calc_rw_mtow_cvg_all`()
BEGIN


DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE done INT DEFAULT FALSE;
DECLARE rwId INT;

DECLARE cursorRwIds CURSOR FOR SELECT rw.rw_id FROM ams_wad.cfg_airport_runway rw order by rw.rw_id;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('calc_rw_mtow_cvg_all', stTime, logNote);
END;


DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('calc_rw_mtow_cvg_all', stTime, logNote);
END;



set stTime = current_timestamp();
set spname = 'calc_rw_mtow_cvg_all';

SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;
OPEN cursorRwIds;
read_loop: LOOP
    FETCH cursorRwIds INTO rwId;
    
    IF done THEN
      LEAVE read_loop;
    END IF;
    
    CALL ams_wad.calc_mtow_rwid(rwId);
	SET countRows = countRows+1;
  END LOOP;

  CLOSE cursorRwIds;
  

SET logNote=CONCAT('Updated ',countRows,' runways mtow and svg fields.');
call log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish ', '<-' );
call log_sp(spname, stTime, logNote);

select countRows from dual;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_cfg_airport_destination` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `get_cfg_airport_destination`(IN in_dep_ap_id INT, IN in_arr_ap_id INT, OUT out_dest_id int)
BEGIN


DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE done INT DEFAULT FALSE;
DECLARE dep_lat, dep_lon, max_distance_km double;
DECLARE i_dep_range_km, i_dep_ap_type, i_dep_country_id INT;
declare lon1, lon2 float;
declare lat1, lat2 float;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    SET done = TRUE;
    call ams_wad.log_sp('ams_wad.get_cfg_airport_destination', stTime, logNote);
END;


DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.get_cfg_airport_pcpd_destination', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.get_cfg_airport_destination';

SET logNote=CONCAT('Start', '-> in_dep_ap_id:', in_dep_ap_id, ', in_arr_ap_id:', in_arr_ap_id );
-- call ams_wad.log_sp(spname, stTime, logNote);

SET countRows = 0;
SELECT count(*) INTO countRows
FROM ams_wad.cfg_airport_destination
WHERE dep_ap_id = in_dep_ap_id and arr_ap_id = in_arr_ap_id;

if countRows>0 then
	SELECT dest_id INTO out_dest_id
	FROM ams_wad.cfg_airport_destination
	WHERE dep_ap_id = in_dep_ap_id and arr_ap_id = in_arr_ap_id;
END IF;
SET logNote=CONCAT('out_dest_id: ', out_dest_id );
-- call ams_wad.log_sp(spname, stTime, logNote);

SET countRows = 0;
IF out_dest_id is null THEN
	SET logNote=CONCAT('Insert record for in_dep_ap_id:', in_dep_ap_id, ', in_arr_ap_id:', in_arr_ap_id);
	-- call log_sp(spname, stTime, logNote);
    
    INSERT INTO ams_wad.cfg_airport_destination
	(dep_ap_id, arr_ap_id, distance_km, max_flight_h, dest_type_id,
	 ap_type_factor, max_ticket_price_e, max_ticket_price_b, max_ticket_price_f,
     min_rw_lenght_m, min_mtow_kg)
	select t.dep_ap_id, t.arr_ap_id, ROUND(t.distance_km,2) as distance_km, 
    ROUND((t.distance_km/s.min_speed_kmph), 2) as max_flight_h,
    t.dest_type_id, ap_type_factor,
    if(ROUND((ppd.param_value  * ap_type_factor * pwkg.param_value * t.distance_km),2)<9,9,
    ROUND((ppd.param_value  * ap_type_factor * pwkg.param_value * t.distance_km),2))as max_ticket_price_e,
    IF(ROUND((ppd.param_value  * ap_type_factor * pwkg.param_value * t.distance_km * factor_b.param_value),2)<18,18,
    ROUND((ppd.param_value  * ap_type_factor * pwkg.param_value * t.distance_km * factor_b.param_value),2)) as max_ticket_price_b,
    IF(ROUND((ppd.param_value  * ap_type_factor * pwkg.param_value * t.distance_km * factor_f.param_value),2)<36,36,
    ROUND((ppd.param_value  * ap_type_factor * pwkg.param_value * t.distance_km * factor_f.param_value),2)) as max_ticket_price_f,
    t.min_rw_lenght_m, t.min_mtow_kg
    FROM(
	SELECT dep.ap_id as dep_ap_id, arr.ap_id as  arr_ap_id, 
    dep.ap_type_id as dep_ap_type_id, arr.ap_type_id as arr_ap_type_id,
    -- round(((dep.ap_type_id+arr.ap_type_id)/2),2) as ap_type_factor,
    round((10 - ((arr.ap_type_id + dep.ap_type_id)/2)),2) as ap_type_factor,
    dep.distance_unit
	 * DEGREES(ACOS(COS(RADIANS(dep.ap_lat))
	 * COS(RADIANS(arr.ap_lat))
	 * COS(RADIANS(dep.ap_lon - arr.ap_lon))
	 + SIN(RADIANS(dep.ap_lat))
	 * SIN(RADIANS(arr.ap_lat)))) AS distance_km, 
     if(dep.country_id = arr.country_id, 1,
     if(dep.subregion_id = arr.subregion_id ||
     dep.region_id = arr.region_id,2,3)) as dest_type_id,
     if(dep.max_rw_lenght_m<arr.max_rw_lenght_m, dep.max_rw_lenght_m, arr.max_rw_lenght_m) as min_rw_lenght_m,
     if(dep.max_mtow_kg<arr.max_mtow_kg, dep.max_mtow_kg, arr.max_mtow_kg) as min_mtow_kg
   
	from (SELECT d.ap_id, d.ap_lat, d.ap_lon, t.range_km, d.ap_type_id, 
    d.country_id, d.subregion_id, d.region_id,
    111.045 AS distance_unit,  d.max_rw_lenght_m, d.max_mtow_kg
	FROM ams_wad.cfg_airport d
	JOIN ams_wad.cfg_airport_type t on d.ap_type_id = t.ap_type_id
	WHERE d.ap_id = in_dep_ap_id) dep
    join 
     (SELECT d.ap_id, d.ap_lat, d.ap_lon, t.range_km, d.ap_type_id, 
     d.country_id, d.subregion_id, d.region_id,
     111.045 AS distance_unit,  d.max_rw_lenght_m, d.max_mtow_kg
	FROM ams_wad.cfg_airport d
	JOIN ams_wad.cfg_airport_type t on d.ap_type_id = t.ap_type_id
	WHERE d.ap_id = in_arr_ap_id) arr on 1=1 ) t
    join (select param_value FROM ams_al.cfg_callc_param where param_name = 'max_ticket_price_ppd_kg_per_km')  ppd on 1=1
	join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'pax_weight_kg' ) pwkg on 1=1
	join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'ticket_price_factor_b' ) factor_b on 1=1
	join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'ticket_price_factor_f' ) factor_f on 1=1
    join(SELECT min(cruise_speed_kmph) as min_speed_kmph FROM ams_ac.cfg_mac where cruise_speed_kmph is not null) s on 1=1;

    SELECT d.ap_lat, d.ap_lon, t.range_km, d.ap_type_id, d.country_id
    INTO dep_lat, dep_lon, i_dep_range_km, i_dep_ap_type, i_dep_country_id
	FROM ams_wad.cfg_airport d
	 JOIN ams_wad.cfg_airport_type t on d.ap_type_id = t.ap_type_id
	 WHERE d.ap_id = in_dep_ap_id;
 
	SET out_dest_id = LAST_INSERT_ID();
    COMMIT;
    
    SELECT dest_id INTO out_dest_id
	FROM ams_wad.cfg_airport_destination
	WHERE dep_ap_id = in_dep_ap_id and arr_ap_id = in_arr_ap_id;
    SET logNote=CONCAT('out_dest_id2: ', out_dest_id );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
END IF;

IF out_dest_id > 0 then

	SET countRows = 0;
	SELECT COUNT(*) INTO countRows 
    from ams_wad.st_airport_destination where dest_id = out_dest_id;

	SET logNote=CONCAT('st_airport_destination count rows:', countRows);
	-- call log_sp(spname, stTime, logNote);
    
	IF countRows = 0 THEN
		INSERT INTO ams_wad.st_airport_destination(dest_id) values (out_dest_id);
        COMMIT;
	END IF;
END IF;

SET logNote=CONCAT('Finish ', '<- out_dest_id:', out_dest_id );
-- call log_sp(spname, stTime, logNote);


-- select out_dest_id from dual;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `get_cfg_airport_destination_row` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `get_cfg_airport_destination_row`(IN in_dep_ap_id INT, IN in_arr_ap_id INT)
BEGIN

DECLARE countRows, i_dest_id, i_rev_dest_id INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.get_cfg_airport_destination_row', stTime, logNote);
END;


DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.get_cfg_airport_destination_row', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.get_cfg_airport_destination_row';
set countRows = 0;
SET logNote=CONCAT('Start (',in_dep_ap_id, ', ', in_arr_ap_id,') ->' );
call ams_wad.log_sp(spname, stTime, logNote);

call ams_wad.get_cfg_airport_destination(in_dep_ap_id, in_arr_ap_id, i_dest_id);
    
call ams_wad.get_cfg_airport_destination( in_arr_ap_id, in_dep_ap_id, i_rev_dest_id);

SET logNote=CONCAT('i_dest_id:', i_dest_id , ', i_rev_dest_id:', i_rev_dest_id );
call ams_wad.log_sp(spname, stTime, logNote);

select cfg.dest_id as destId,
cfg.dep_ap_id as depApId,
cfg.arr_ap_id as arrApId,
cfg.ap_type_factor as apTypeFactor,
cfg.dest_type_id as destTypeId,
cfg.distance_km as distanceKm,
cfg.max_flight_h as flightH,
cfg.max_ticket_price_e as maxPriceE,
cfg.max_ticket_price_b as maxPriceB,
cfg.max_ticket_price_f as maxPriceF,
cfg.min_rw_lenght_m as minRwLenghtM,
cfg.min_mtow_kg as minMtowKg,
concat(ifnull(dep.ap_iata, dep.ap_icao), ' - ', ifnull(arr.ap_iata, arr.ap_icao)) as destName,
ROUND(flp.param_value* cfg.max_ticket_price_e, 2) as priceE, 
ROUND(flp.param_value* cfg.max_ticket_price_b, 2) as priceB,
ROUND(flp.param_value* cfg.max_ticket_price_f, 2) as priceF,
ROUND(flp.param_value * (cfg.distance_km * pwkg.param_value), 4) as priceCpKg
from ams_wad.cfg_airport_destination cfg
join ams_wad.cfg_airport dep on dep.ap_id = cfg.dep_ap_id
join ams_wad.cfg_airport arr on arr.ap_id = cfg.arr_ap_id
join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'max_ticket_price_cpd_kg_per_km' ) pwkg on 1=1
join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'flight_charter_pct_from_max_price' ) flp on 1=1
where cfg.dest_id = i_dest_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `log_sp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `log_sp`(IN `spname` VARCHAR(512), IN `startTime` TIMESTAMP, IN `note` VARCHAR(4000))
BEGIN

INSERT into ams_wad.log_sp (sp_name,exec_time,runing_time, note) 
values(spname, startTime, timestampdiff(microsecond, startTime, current_timestamp())*0.001, note);
commit;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `process_calc_airport_payload_demand` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `process_calc_airport_payload_demand`()
BEGIN

DECLARE countRows, i_rows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE done INT DEFAULT FALSE;
DECLARE i_ap_id, iperiod_days, ipdp,  i_sequence INT;
declare dcdp DOUBLE;

DECLARE cursorApIds CURSOR FOR SELECT capd.ap_id, capd.pax_demand_for_period,  
	capd.cargo_demand_for_period_kg, capd.period_days, sequence 
	FROM ams_wad.calc_airport_payload_demand capd 
    where capd.period_nexttime < now() 
	order by capd.period_nexttime Limit 50;
    
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    SET done = TRUE;
    call ams_wad.log_sp('ams_wad.process_calc_airport_payload_demand', stTime, logNote);
END;


DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.process_calc_airport_payload_demand', stTime, logNote);
END;



set stTime = current_timestamp();
set spname = 'ams_wad.process_calc_airport_payload_demand';

SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;

OPEN cursorApIds;
read_loop: LOOP
    FETCH cursorApIds INTO i_ap_id, ipdp, dcdp, iperiod_days, i_sequence;
    SET logNote=CONCAT('ap:',i_ap_id,', ipdp:',ipdp,', iperiod_days:', iperiod_days);
	-- call log_sp(spname, stTime, logNote);
    
    IF done THEN
      LEAVE read_loop;
    END IF;
   
   -- Calculate PPD for airport
   IF ipdp > 0  THEN
	SET logNote=CONCAT(' Calculate PPD ipdp:',ipdp, ' for ap_id:',i_ap_id);
	-- call log_sp(spname, stTime, logNote);
    select count(*) INTO i_rows FROM ams_wad.ams_airport_ppd p
	where p.dep_ap_id = i_ap_id and p.sequence = i_sequence;

	SET logNote=CONCAT(i_rows, ' rows found for ap_id:',i_ap_id,', i_sequence:', i_sequence);
	-- call log_sp(spname, stTime, logNote);
    IF i_rows > 0 THEN
		DELETE FROM ams_wad.ams_airport_ppd
		where dep_ap_id = i_ap_id and sequence = i_sequence;
        COMMIT;
        SET i_rows =  ROW_COUNT();
		SET logNote=CONCAT(i_rows, ' rows deleted for ap_id:',i_ap_id,', i_sequence:', i_sequence);
		-- call log_sp(spname, stTime, logNote);
    END IF;
   
	CALL ams_wad.calc_ams_airport_ppd(i_ap_id, ipdp, i_sequence);
    SET logNote=CONCAT('Finish PPD for ap_id:',i_ap_id,', pax_demand_for_period:', ipdp);
	-- call log_sp(spname, stTime, logNote);
   END IF;
   
   -- Callculate CPD for airport
   IF dcdp > 0  THEN
	SET logNote=CONCAT(' Calculate PPD dcdp:',dcdp, ' for ap_id:',i_ap_id);
	-- call log_sp(spname, stTime, logNote);
    select count(*) INTO i_rows FROM ams_wad.ams_airport_cpd p
	where p.dep_ap_id = i_ap_id and p.sequence = i_sequence;

	SET logNote=CONCAT(i_rows, ' rows found in CPD for ap_id:',i_ap_id,', i_sequence:', i_sequence);
	-- call log_sp(spname, stTime, logNote);
    IF i_rows > 0 THEN
		DELETE FROM ams_wad.ams_airport_cpd
		where dep_ap_id = i_ap_id and sequence = i_sequence;
        COMMIT;
        SET i_rows =  ROW_COUNT();
		SET logNote=CONCAT(i_rows, ' rows deleted from CPD for ap_id:',i_ap_id,', i_sequence:', i_sequence);
		-- call log_sp(spname, stTime, logNote);
    END IF;
    
    CALL ams_wad.calc_ams_airport_cpd(i_ap_id, dcdp, i_sequence);
    SET logNote=CONCAT('Finish CPD for ap_id:',i_ap_id,', cargo_demand_for_period_kg:', dcdp);
	-- call log_sp(spname, stTime, logNote);
   END IF;
    
	-- Update statistic
	 update ams_wad.st_airport_destination s 
	 join(
		select p.dest_id, sum(if(p.pax_class_id =1, p.pax, 0)) as ppd_e,
		 sum(if(p.pax_class_id =2, p.pax, 0)) as ppd_b,
		 sum(if(p.pax_class_id =3, p.pax, 0)) as ppd_f
		 from ams_wad.ams_airport_ppd p
		 join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
		 where d.dep_ap_id = i_ap_id
		group by p.dest_id) c on c.dest_id = s.dest_id
	  set s.ppd_e = c.ppd_e, s.ppd_b = c.ppd_b, s.ppd_f = c.ppd_f;
	SET i_rows =  ROW_COUNT();
	SET logNote=CONCAT(i_rows, ' rows updated in st_airport_destination  PPD for ap_id:',i_ap_id);
	-- call log_sp(spname, stTime, logNote);
	COMMIT;
     update ams_wad.st_airport_destination s 
	 join(
		select p.dest_id, 
         sum(if(p.payload_kg is null, 0, p.payload_kg)) as cpd
         From ams_wad.ams_airport_cpd p 
		 join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
		 where d.dep_ap_id = i_ap_id
		group by p.dest_id) c on c.dest_id = s.dest_id
	  set s.cpd = c.cpd;
    SET i_rows =  ROW_COUNT();
	SET logNote=CONCAT(i_rows, ' rows updated in st_airport_destination for ap_id:',i_ap_id);
	-- call log_sp(spname, stTime, logNote);
    COMMIT;
	
    
    Update ams_wad.calc_airport_payload_demand capd
    set capd.sequence = (capd.sequence+1), 
     capd.period_nexttime = DATE_ADD(concat(date_format(current_timestamp(), '%Y-%m-%d'), ' ', date_format(capd.period_nexttime, '%H:%i:00')), INTERVAL capd.period_days DAY)
    where capd.ap_id = i_ap_id;
    commit;
    
    SET countRows = countRows+1;
	SET logNote=CONCAT('Finish ap_id:',i_ap_id,' iperiod_days:', iperiod_days);
	-- call log_sp(spname, stTime, logNote);
	
    -- LEAVE read_loop;
END LOOP;

CLOSE cursorApIds;
  
COMMIT;

SET logNote=CONCAT('Updated ',countRows,' airports PPD/CPD from ams_wad.process_calc_airport_payload_demand.');
call log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish ', '<-' );
call log_sp(spname, stTime, logNote);


-- select countRows as rowsCount from dual;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ams_airport_cpd_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ams_airport_cpd_update`()
BEGIN

DECLARE i_dest_id, i_apRows, affectedRows INT; 

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursorApIds CURSOR FOR SELECT p.dest_id FROM ams_wad.ams_airport_cpd p
									join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
									where d.ap_type_factor < 5 and d.distance_km < 150
									group by p.dest_id;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    SET done = TRUE;
    call ams_wad.log_sp('ams_wad.sp_ams_airport_cpd_update', stTime, logNote);
END;
DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.sp_ams_airport_cpd_update', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.sp_ams_airport_cpd_update';

SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;
set i_apRows = 0;
OPEN cursorApIds;
read_loop: LOOP
    FETCH cursorApIds INTO i_dest_id;
    IF done THEN
      LEAVE read_loop;
    END IF;
    SET countRows = countRows + 1;
    set affectedRows = 0;
	SET logNote=CONCAT( countRows, ' -  dest_id:', i_dest_id );
	call ams_wad.log_sp(spname, stTime, logNote);
	
    START TRANSACTION;
		update ams_wad.ams_airport_cpd p 
		join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
		join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'max_ticket_price_cpd_kg_per_km' ) pwkg on 1=1
		set p.max_ticket_price = ROUND((p.payload_kg * pwkg.param_value * d.distance_km * d.ap_type_factor)*(if((d.ap_type_factor<5 and d.distance_km < 150), 3 , 1)), 4)  
		where p.dest_id = i_dest_id;
 
		SET affectedRows =  ROW_COUNT();
    COMMIT;
	SET logNote=CONCAT('Updated: ', FORMAT(affectedRows, 0), ' records in ams_wad.ams_airport_cpd.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    set i_apRows = i_apRows + affectedRows;
    
 END LOOP;
 
 CLOSE cursorApIds;
 
start transaction;
	delete p from ams_ac.ams_airport_cpd p
	join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
	where d.ap_type_factor<5 and d.distance_km < 150;
    SET affectedRows =  ROW_COUNT();
COMMIT;
SET logNote=CONCAT('Deleted: ', FORMAT(affectedRows, 0), ' records in ams_ac.ams_airport_cpd.' );
call ams_wad.log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish <- updated ', FORMAT(countRows, 0) , ' destination affected ', FORMAT(i_apRows, 0), ' rows in ams_wad.ams_airport_cpd table' );
call log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ams_airport_pcpd_for_flp_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ams_airport_pcpd_for_flp_day`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.sp_ams_airport_pcpd_for_flp_day', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.sp_ams_airport_pcpd_for_flp_day';
set countRows = 0;

SET logNote=CONCAT('Start ->' );
call ams_wad.log_sp(spname, stTime, logNote);

truncate table ams_ac.ams_airport_cpd;
commit;
insert into ams_ac.ams_airport_cpd
select * from ams_wad.ams_airport_cpd;
SET countRows =  ROW_COUNT();
commit;
SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' rows from ams_ac.ams_airport_cpd.' );
call ams_wad.log_sp(spname, stTime, logNote);

truncate table ams_ac.ams_airport_ppd;
commit;
insert into ams_ac.ams_airport_ppd
select * from ams_wad.ams_airport_ppd;
SET countRows =  ROW_COUNT();
commit;
SET logNote=CONCAT('Inserted ', FORMAT(countRows, 0), ' rows from ams_ac.ams_airport_ppd.' );
call ams_wad.log_sp(spname, stTime, logNote);
    
truncate table ams_wad.ams_airport_cpd_for_flp;
INSERT INTO ams_wad.ams_airport_cpd_for_flp
SELECT p.cpd_id AS cpd_id,
	ROUND(p.max_ticket_price, 2) AS cpd_price,
	p.pax_class_id AS cpd_pax_class_id,
    p.ppd_type_id,
	p.payload_kg AS cpd_payload_kg,
	p.note AS cpd_note,
	p.adate as cpd_adate,
    pd.dep_ap_id AS dep_ap_id,
	pd.arr_ap_id AS arr_ap_id,
	pd.distance_km AS distance_km
from ams_wad.ams_airport_cpd p 
JOIN ams_wad.cfg_airport_destination pd ON pd.dest_id = p.dest_id;
SET countRows =  ROW_COUNT();
COMMIT;
SET logNote=CONCAT('Inserted ', countRows, ' rows in ams_wad.ams_airport_cpd_for_flp.');
call ams_wad.log_sp(spname, stTime, logNote);
 
Set countRows = 0;
truncate table ams_wad.ams_airport_ppd_for_flp;
INSERT INTO ams_wad.ams_airport_ppd_for_flp
select p.ppd_id AS ppd_id,
		ROUND(p.max_ticket_price, 2) AS max_ticket_price,
        p.pax_class_id,
        p.pax,
        p.payload_kg,
        p.note,
        p.adate,
        pd.dep_ap_id AS dep_ap_id,
        pd.arr_ap_id AS arr_ap_id,
        pd.distance_km AS distance_km
from ams_wad.ams_airport_ppd p 
JOIN ams_wad.cfg_airport_destination pd ON pd.dest_id = p.dest_id;
SET countRows =  ROW_COUNT();
COMMIT;
SET logNote=CONCAT('Inserted ', countRows, ' rows in ams_wad.ams_airport_ppd_for_flp.');
call ams_wad.log_sp(spname, stTime, logNote);

call ams_wad.sp_ams_airport_pcpd_for_flp_day();

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);    

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ams_airport_ppd_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ams_airport_ppd_update`()
BEGIN

DECLARE i_dest_id, i_apRows, affectedRows INT; 

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursorApIds CURSOR FOR SELECT p.dest_id FROM ams_wad.ams_airport_ppd p
									join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
									where d.ap_type_factor < 5 and d.distance_km < 150
									group by p.dest_id;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    SET done = TRUE;
    call ams_wad.log_sp('ams_wad.sp_ams_airport_ppd_update', stTime, logNote);
END;
DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.sp_ams_airport_ppd_update', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.sp_ams_airport_ppd_update';

SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;
set i_apRows = 0;
OPEN cursorApIds;
read_loop: LOOP
    FETCH cursorApIds INTO i_dest_id;
    IF done THEN
      LEAVE read_loop;
    END IF;
    SET countRows = countRows + 1;
    set affectedRows = 0;
	SET logNote=CONCAT( countRows, ' -  dest_id:', i_dest_id );
	call ams_wad.log_sp(spname, stTime, logNote);
	
    START TRANSACTION;
		update ams_wad.ams_airport_ppd p 
		join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
		set p.max_ticket_price = round((p.pax * 
				if( p.pax_class_id =1, d.max_ticket_price_e, 
				if(p.pax_class_id=2, d.max_ticket_price_b, d.max_ticket_price_f)))*(if((d.ap_type_factor<5 and d.distance_km < 150), 4 , 1)),2) 
		where p.dest_id = i_dest_id;
 
		SET affectedRows =  ROW_COUNT();
    COMMIT;
	SET logNote=CONCAT('Updated: ', FORMAT(affectedRows, 0), ' records in ams_wad.ams_airport_ppd.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    set i_apRows = i_apRows + affectedRows;
    
 END LOOP;
 
 CLOSE cursorApIds;
 
start transaction;
	delete p from ams_ac.ams_airport_ppd p
	join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
	where d.ap_type_factor<5 and d.distance_km < 150;
    SET affectedRows =  ROW_COUNT();
COMMIT;
SET logNote=CONCAT('Deleted: ', FORMAT(affectedRows, 0), ' records in ams_ac.ams_airport_ppd.' );
call ams_wad.log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish <- updated ', FORMAT(countRows, 0) , ' destination affevted ', FORMAT(i_apRows, 0), ' rows in ams_wad.ams_airport_ppd table' );
call log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_cfg_airport_destination_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_cfg_airport_destination_update`()
BEGIN

DECLARE i_dep_ap_id, i_apRows, affectedRows INT; 

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;

DECLARE cursorApIds CURSOR FOR SELECT d.dep_ap_id, count(*) as apRows FROM ams_wad.cfg_airport_destination d 
							group by d.dep_ap_id order by d.dest_id;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    SET done = TRUE;
    call ams_wad.log_sp('ams_wad.sp_cfg_airport_destination_update', stTime, logNote);
END;
DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.sp_cfg_airport_destination_update', stTime, logNote);
END;


                            

set stTime = current_timestamp();
set spname = 'ams_wad.sp_cfg_airport_destination_update';

SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;
OPEN cursorApIds;
read_loop: LOOP
    FETCH cursorApIds INTO i_dep_ap_id, i_apRows;
    IF done THEN
      LEAVE read_loop;
    END IF;
    SET countRows = countRows + 1;
    set affectedRows = 0;
	SET logNote=CONCAT( countRows, ' -  dep ap_id:', i_dep_ap_id, ' rows:', format(i_apRows,0) );
	call ams_wad.log_sp(spname, stTime, logNote);
	
    START TRANSACTION;
		update ams_wad.cfg_airport_destination d 
		join ams_wad.cfg_airport dep on dep.ap_id = d.dep_ap_id
		join ams_wad.cfg_airport arr on arr.ap_id = d.arr_ap_id
		join (select param_value FROM ams_al.cfg_callc_param where param_name = 'max_ticket_price_ppd_kg_per_km')  ppd on 1=1
		join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'pax_weight_kg' ) pwkg on 1=1
		join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'ticket_price_factor_b' ) factor_b on 1=1
		join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'ticket_price_factor_f' ) factor_f on 1=1
		set d.ap_type_factor = round((10 - ((arr.ap_type_id + dep.ap_type_id)/2)),2),
		d.max_ticket_price_e = ROUND((ppd.param_value  * (10 - ((arr.ap_type_id + dep.ap_type_id)/2)) * pwkg.param_value * d.distance_km),2),
		d.max_ticket_price_b = ROUND((ppd.param_value  * (10 - ((arr.ap_type_id + dep.ap_type_id)/2)) * pwkg.param_value * d.distance_km * factor_b.param_value),2) ,
		d.max_ticket_price_f = ROUND((ppd.param_value  * (10 - ((arr.ap_type_id + dep.ap_type_id)/2)) * pwkg.param_value * d.distance_km * factor_f.param_value),2) 
		where d.dep_ap_id=i_dep_ap_id;
 
		SET affectedRows =  ROW_COUNT();
    COMMIT;
	SET logNote=CONCAT('Updated: ', FORMAT(affectedRows, 0), ' records in ams_wad.cfg_airport_destination.' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
 END LOOP;
 
 CLOSE cursorApIds;

SET logNote=CONCAT('Finish <- updated ', FORMAT(countRows, 0), ' rows in ams_wad.cfg_airport_destination' );
call log_sp(spname, stTime, logNote);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_cfg_charter_calculate` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_cfg_charter_calculate`(in in_charter_id INT)
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.sp_cfg_charter_calculate', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.sp_cfg_charter_calculate';
set countRows = 0;

	SET logNote=CONCAT('Start (',in_charter_id,') ->' );
	call ams_wad.log_sp(spname, stTime, logNote);
	
    UPDATE ams_wad.cfg_charter ch
	join ams_wad.cfg_airport dep on dep.ap_id=ch.d_ap_id 
	join ams_wad.cfg_airport arr on arr.ap_id = ch.a_ap_id
	join (select * from ams_al.v_al_callc_param_h where al_id = 7) p on 1=1
	set ch.distance_km = Round(p.distance_unit
		 * DEGREES(ACOS(COS(RADIANS(dep.ap_lat))
		 * COS(RADIANS(arr.ap_lat))
		 * COS(RADIANS(dep.ap_lon - arr.ap_lon))
		 + SIN(RADIANS(dep.ap_lat))
		 * SIN(RADIANS(arr.ap_lat)))),2), 
	ch.payload_kg = if(payload_type='C', ch.cargo_kg, Round((ch.pax*p.pax_weight_kg),2))
    where ch.charter_id = in_charter_id;
    
	COMMIT;
    
    UPDATE ams_wad.cfg_charter ch
    join ams_wad.cfg_airport dep on dep.ap_id=ch.d_ap_id 
	join ams_wad.cfg_airport arr on arr.ap_id = ch.a_ap_id
	join (select * from ams_al.v_al_callc_param_h where al_id = 7) p on 1=1
    set price = Round( (
		if(payload_type='C', p.max_ticket_price_cpd_kg_per_km, p.max_ticket_price_ppd_kg_per_km) *
        ((dep.ap_type_id+arr.ap_type_id)/8) * ch.distance_km * ch.payload_kg *
        if(payload_type='B', p.ticket_price_factor_b, (if(payload_type='F', p.ticket_price_factor_f, 1)) *
        p.flight_charter_pct_from_max_price
        ) ),2)
	 where ch.charter_id = in_charter_id;
     COMMIT;
	SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
    select in_charter_id as charter_id from dual;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_cfg_charter_update` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_cfg_charter_update`(IN fakeIn INT)
BEGIN

DECLARE countRows, i_charter_id INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE done INT DEFAULT FALSE;
DECLARE cursor_tr CURSOR FOR SELECT charter_id 
							FROM ams_wad.cfg_charter;
                        
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.sp_cfg_charter_update', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.sp_cfg_charter_update';
set countRows = 0;

OPEN cursor_tr;

	read_loop: LOOP
			FETCH cursor_tr INTO i_charter_id;
		
        IF done THEN
			SET logNote=CONCAT('No i_charter_id found ', 'done' );
			-- call ams_wad.log_sp(spname, stTime, logNote);
			LEAVE read_loop;
		END IF;
        
        SET logNote=CONCAT('i_charter_id: ',i_charter_id, ' update...');
		-- call ams_wad.log_sp(spname, stTime, logNote);
        call ams_wad.sp_cfg_charter_calculate(i_charter_id);
		SET countRows = countRows+1;
		
     END LOOP;

CLOSE cursor_tr;
COMMIT;
SET logNote=CONCAT('Executed ', countRows, ' charter_calculate;');
call ams_wad.log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ev_min_five` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ev_min_five`()
BEGIN

DECLARE countRows, vtask_id INT;
DECLARE vname, vsp_name VARCHAR(1000);
DECLARE stTime TIMESTAMP;
DECLARE logNote, sqlScr VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;
DECLARE cursor_tr CURSOR FOR SELECT task_id, task_name, sp_name 
							FROM ams_wad.cfg_task
							where task_type='sp_ev_min_five' and is_running = 0 and
							(if(next_run is null, 0, UNIX_TIMESTAMP(next_run)) - UNIX_TIMESTAMP(current_timestamp()))<0;
                        
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('sp_ev_min_five', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'sp_ev_min_five';
set countRows = 0;

SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_tr;

	read_loop: LOOP
			FETCH cursor_tr INTO vtask_id, vname, vsp_name;
		
        IF done THEN
			SET logNote=CONCAT('No task found ', 'done' );
			call ams_wad.log_sp(spname, stTime, logNote);
			LEAVE read_loop;
		END IF;
        
        SET logNote=CONCAT('task_id: ',vtask_id, ', vname: ',vname, ', vsp_name: ',vsp_name,  ' prepare...');
		call ams_wad.log_sp(spname, stTime, logNote);
        
        UPDATE ams_wad.cfg_task t set t.start = now(), 
			t.is_running = 1
            where t.task_id=vtask_id;
        
        SET @sqlScr = CONCAT('call ',vsp_name ,'();');
			
		PREPARE stmt FROM @sqlScr;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
    
		UPDATE ams_wad.cfg_task t set t.finish = now(), 
			t.is_running = 0,  
            t.exec_time = timestampdiff(microsecond, t.start, current_timestamp())*0.001,
			t.next_run = DATE_ADD(concat(date_format(t.start, '%Y-%m-%d'), ' ', date_format(t.next_run, '%H:%i:00')), INTERVAL t.period_min MINUTE)
            -- t.next_run = DATE_ADD(t.next_run, INTERVAL t.period_min MINUTE)
            where t.task_id=vtask_id;
		
        SET logNote=CONCAT('task_id: ',vtask_id, ', vname: ',vname, ', vsp_name: ',vsp_name,  ' finish');
		call ams_wad.log_sp(spname, stTime, logNote);
        
		SET countRows = countRows+1;
		
     END LOOP;

	CLOSE cursor_tr;
    COMMIT;
    SET logNote=CONCAT('Executed ', countRows, ' tasks;');
	call ams_wad.log_sp(spname, stTime, logNote);

	SET logNote=CONCAT('Finish ', '<-' );
	call ams_wad.log_sp(spname, stTime, logNote);
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_ev_min_one` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_ev_min_one`()
BEGIN

DECLARE countRows, vtask_id INT;
DECLARE vname, vsp_name VARCHAR(1000);
DECLARE stTime TIMESTAMP;
DECLARE logNote, sqlScr VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;
DECLARE cursor_tr CURSOR FOR SELECT task_id, task_name, sp_name 
							FROM ams_wad.cfg_task
							where task_type='sp_ev_min_one' and 
							(if(next_run is null, 0, UNIX_TIMESTAMP(next_run)) - UNIX_TIMESTAMP(current_timestamp()))<0;
                        
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
  
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('sp_ev_min_one', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'sp_ev_min_one';
set countRows = 0;

SET logNote=CONCAT('Start', '->' );
-- call ams_wad.log_sp(spname, stTime, logNote);

OPEN cursor_tr;

	read_loop: LOOP
			FETCH cursor_tr INTO vtask_id, vname, vsp_name;
		
        IF done THEN
			SET logNote=CONCAT('No task found ', 'done' );
			-- call ams_wad.log_sp(spname, stTime, logNote);
			LEAVE read_loop;
		END IF;
        
        SET logNote=CONCAT('task_id: ',vtask_id, ', vname: ',vname, ', vsp_name: ',vsp_name,  ' prepare...');
		-- call ams_wad.log_sp(spname, stTime, logNote);
        
        UPDATE ams_wad.cfg_task t set t.start = now(), 
			t.is_running = 1
            where t.task_id=vtask_id;
        
        SET @sqlScr = CONCAT('call ',vsp_name ,'();');
			
		PREPARE stmt FROM @sqlScr;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
    
		UPDATE ams_wad.cfg_task t set t.finish = now(), 
			t.is_running = 0,  
            t.exec_time = timestampdiff(microsecond, t.start, current_timestamp())*0.001,
            t.next_run = DATE_ADD(concat(date_format(t.start, '%Y-%m-%d'), ' ', date_format(t.start, '%H:%i:00')), INTERVAL t.period_min MINUTE)
            -- t.next_run = DATE_ADD(t.next_run, INTERVAL t.period_min MINUTE)
            where task_id=vtask_id;
            
		SET countRows = countRows+1;
		
     END LOOP;

	CLOSE cursor_tr;
    COMMIT;
    SET logNote=CONCAT('Executed ', countRows, ' tasks;');
	-- call ams_wad.log_sp(spname, stTime, logNote);

	SET logNote=CONCAT('Finish ', '<-' );
	-- call ams_wad.log_sp(spname, stTime, logNote);
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_log_clear` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb3 */ ;
/*!50003 SET character_set_results = utf8mb3 */ ;
/*!50003 SET collation_connection  = utf8mb3_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `sp_log_clear`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote, sqlScr VARCHAR(4000);

DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';
DECLARE done INT DEFAULT FALSE;
					
  
DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('sp_log_clear', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'sp_log_clear';
set countRows = 0;

SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);

TRUNCATE ams_wad.log_sp;
TRUNCATE ams_al.log_gui;
TRUNCATE iordanov_bwt.log_gui;
COMMIT;
    

SET logNote=CONCAT('Finish ', '<-' );
call ams_wad.log_sp(spname, stTime, logNote);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `st_airport_destination_day` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `st_airport_destination_day`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    call ams_wad.log_sp('ams_wad.st_airport_destination_day', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.st_airport_destination_day';
set countRows = 0;

SET logNote=CONCAT('Start ->' );
-- call ams_wad.log_sp(spname, stTime, logNote);
update ams_wad.st_airport_destination s 
 left join(
	select p.dest_id, ifnull(sum(if(p.pax_class_id =1, p.pax, 0)),0) as ppd_e,
	 ifnull(sum(if(p.pax_class_id =2, p.pax, 0)),0) as ppd_b,
	 ifnull(sum(if(p.pax_class_id =3, p.pax, 0)),0) as ppd_f
	 from ams_wad.ams_airport_ppd p
	 join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
	group by p.dest_id) c on c.dest_id = s.dest_id
set s.ppd_e = ifnull(c.ppd_e, 0), s.ppd_b = ifnull(c.ppd_b, 0), s.ppd_f = ifnull(c.ppd_f, 0);
SET countRows =  ROW_COUNT();
SET logNote=CONCAT('Updated ppd: ', countRows, ' rows in ams_wad.st_airport_destination.');
-- call ams_wad.log_sp(spname, stTime, logNote);
 COMMIT;
 
 update ams_wad.st_airport_destination s 
 join(
	select p.dest_id, 
	 sum(if(p.payload_kg is null, 0, p.payload_kg)) as cpd
	 From ams_wad.ams_airport_cpd p 
	 join ams_wad.cfg_airport_destination d on d.dest_id = p.dest_id
	group by p.dest_id) c on c.dest_id = s.dest_id
  set s.cpd = ifnull(c.cpd,0);
 SET countRows =  ROW_COUNT();
SET logNote=CONCAT('Updated cpd: ', countRows, ' rows in ams_wad.st_airport_destination.');
-- call ams_wad.log_sp(spname, stTime, logNote);
 COMMIT;
 
INSERT INTO ams_wad.st_airport_destination_day (dest_id, dep_ap_id, arr_ap_id, ppd_e, ppd_b, ppd_f, pax, cpd)
SELECT s.dest_id, d.dep_ap_id, d.arr_ap_id, s.ppd_e, s.ppd_b, s.ppd_f,
(s.ppd_e + s.ppd_b + s.ppd_f) as pax, s.cpd 
FROM ams_wad.st_airport_destination s
join ams_wad.cfg_airport_destination d on d.dest_id = s.dest_id;	

SET countRows =  ROW_COUNT();
COMMIT;

SET logNote=CONCAT('Inserted: ', countRows, ' rows in ams_wad.st_airport_destination_day.');
-- call ams_wad.log_sp(spname, stTime, logNote);
INSERT INTO ams_wad.st_airport_destination_day_h
SELECT * FROM ams_wad.st_airport_destination_day
where datediff(adate, now())<-21;
COMMIT;
delete FROM ams_wad.st_airport_destination_day
where datediff(adate, now())<-21;
COMMIT;

truncate table ams_wad.ams_airport_counters;
INSERT INTO ams_wad.ams_airport_counters (ap_id, flpns_dep_count, flpns_arr_count)
SELECT a.ap_id,
sum( if(flpnsd.flpns_id is null or gd.grp_id is null, 0, 1)) as flpns_dep_count,
sum( if(flpnsa.flpns_id is null or ga.grp_id is null, 0, 1)) as flpns_arr_count
FROM ams_wad.cfg_airport a 
left join ams_al.ams_al_flp_number flpnd on flpnd.dep_ap_id = a.ap_id
left join ams_al.ams_al_flp_number_schedule flpnsd on flpnsd.flpn_id = flpnd.flpn_id
left join ams_al.ams_al_flp_group gd on gd.grp_id = flpnsd.grp_id and gd.is_active > 0
left join ams_al.ams_al_flp_number flpna on flpna.arr_ap_id = a.ap_id
left join ams_al.ams_al_flp_number_schedule flpnsa on flpnsa.flpn_id = flpna.flpn_id
left join ams_al.ams_al_flp_group ga on ga.grp_id = flpnsa.grp_id and ga.is_active > 0
group by a.ap_id;

SET countRows =  ROW_COUNT();
COMMIT;

SET logNote=CONCAT('ams_airport_counters updated: ', countRows, ' airports.');
call ams_wad.log_sp(spname, stTime, logNote);

SET logNote=CONCAT('Finish ', '<-' );
-- call ams_wad.log_sp(spname, stTime, logNote);    

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_cfg_airport_destination` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `update_cfg_airport_destination`()
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE done INT DEFAULT FALSE;
DECLARE dep_lat, dep_lon, max_distance_km double;
DECLARE i_dep_range_km, i_dep_ap_type, i_dep_country_id INT;
declare lon1, lon2 float;
declare lat1, lat2 float;
DECLARE i_distance_km, i_max_flight_h, i_dest_type_id, i_ap_type_factor INT; 
DECLARE d_max_ticket_price_e, d_max_ticket_price_b, d_max_ticket_price_f double;
DECLARE i_min_rw_lenght_m, i_min_mtow_kg INT;

DECLARE in_dep_ap_id, in_arr_ap_id, out_dest_id INT;

DECLARE cursorApIds CURSOR FOR SELECT d.dep_ap_id, d.arr_ap_id, d.dest_id FROM ams_wad.cfg_airport_destination d order by d.dest_id;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;


DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    SET done = TRUE;
    call ams_wad.log_sp('ams_wad.update_cfg_airport_destination', stTime, logNote);
END;


DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.update_cfg_airport_destination', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.update_cfg_airport_destination';

SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;
OPEN cursorApIds;
read_loop: LOOP
    FETCH cursorApIds INTO in_dep_ap_id, in_arr_ap_id, out_dest_id;
    IF done THEN
      LEAVE read_loop;
    END IF;
    
	SET logNote=CONCAT( out_dest_id, ' -> in_dep_ap_id:', in_dep_ap_id, ', in_arr_ap_id:', in_arr_ap_id );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	call ams_wad.update_cfg_airport_destination_id(out_dest_id);
	
    SET countRows = countRows+1;
    if(countRows mod 10)=0 then
		SET logNote=CONCAT('updated rows:', countRows);
		call log_sp(spname, stTime, logNote);
	end if;
    
 END LOOP;
 
 CLOSE cursorApIds;

SET logNote=CONCAT('Finish ', '<- updated ', countRows, ' rows in ams_wad.cfg_airport_destination' );
call log_sp(spname, stTime, logNote);


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_cfg_airport_destination_ap_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `update_cfg_airport_destination_ap_id`(in in_ap_id INT)
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE done INT DEFAULT FALSE;
DECLARE dep_lat, dep_lon, max_distance_km double;
DECLARE i_dep_range_km, i_dep_ap_type, i_dep_country_id INT;
declare lon1, lon2 float;
declare lat1, lat2 float;
DECLARE i_distance_km, i_max_flight_h, i_dest_type_id, i_ap_type_factor INT; 
DECLARE d_max_ticket_price_e, d_max_ticket_price_b, d_max_ticket_price_f double;
DECLARE i_min_rw_lenght_m, i_min_mtow_kg INT;

DECLARE in_dep_ap_id, in_arr_ap_id, out_dest_id INT;

DECLARE cursorApIds CURSOR FOR SELECT d.dep_ap_id, d.arr_ap_id, d.dest_id 
		FROM ams_wad.cfg_airport_destination d 
        where dep_ap_id = in_ap_id or arr_ap_id = in_ap_id
        order by d.dest_id;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;


DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    SET done = TRUE;
    call ams_wad.log_sp('ams_wad.update_cfg_airport_destination_ap_id', stTime, logNote);
END;


DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.update_cfg_airport_destination_ap_id', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.update_cfg_airport_destination_ap_id';

SET logNote=CONCAT('Start', '->' );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;
OPEN cursorApIds;
read_loop: LOOP
    FETCH cursorApIds INTO in_dep_ap_id, in_arr_ap_id, out_dest_id;
    IF done THEN
      LEAVE read_loop;
    END IF;
    
	SET logNote=CONCAT( out_dest_id, ' -> in_dep_ap_id:', in_dep_ap_id, ', in_arr_ap_id:', in_arr_ap_id );
	call ams_wad.log_sp(spname, stTime, logNote);
    
	call ams_wad.update_cfg_airport_destination_id(out_dest_id);
	
    SET countRows = countRows+1;
    if(countRows mod 10)=0 then
		SET logNote=CONCAT('updated rows:', countRows);
		call log_sp(spname, stTime, logNote);
	end if;
    
 END LOOP;
 
 CLOSE cursorApIds;

SET logNote=CONCAT('Finish ', '<- updated ', countRows, ' rows in ams_wad.cfg_airport_destination' );
call log_sp(spname, stTime, logNote);


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_cfg_airport_destination_id` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_0900_ai_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`iordanov_ams`@`%` PROCEDURE `update_cfg_airport_destination_id`(IN in_dest_id INT)
BEGIN

DECLARE countRows INT;
DECLARE stTime TIMESTAMP;
DECLARE logNote VARCHAR(4000);
DECLARE spname VARCHAR(250);
DECLARE msg TEXT;
DECLARE errno CHAR(5) DEFAULT '00000';

DECLARE done INT DEFAULT FALSE;
DECLARE dep_lat, dep_lon, max_distance_km double;
DECLARE i_dep_range_km, i_dep_ap_type, i_dep_country_id INT;
declare lon1, lon2 float;
declare lat1, lat2 float;
DECLARE i_distance_km, i_dest_type_id, i_ap_type_factor INT; 
DECLARE d_max_ticket_price_e, d_max_ticket_price_b, d_max_ticket_price_f, d_max_flight_h double;
DECLARE i_min_rw_lenght_m, i_min_mtow_kg INT;

DECLARE in_dep_ap_id, in_arr_ap_id INT;

DECLARE CONTINUE HANDLER FOR SQLEXCEPTION BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Error: ', errno, ' ', msg);
    SET done = TRUE;
    call ams_wad.log_sp('ams_wad.update_cfg_airport_destination_id', stTime, logNote);
END;


DECLARE CONTINUE HANDLER FOR SQLWARNING BEGIN
	GET DIAGNOSTICS CONDITION 1
    msg = MESSAGE_TEXT, errno = MYSQL_ERRNO;
	SET logNote = concat('Warning: ', errno, ' ', msg);
    call log_sp('ams_wad.update_cfg_airport_destination_id', stTime, logNote);
END;

set stTime = current_timestamp();
set spname = 'ams_wad.update_cfg_airport_destination_id';

SET logNote=CONCAT('Start', '-> in_dest_id:', in_dest_id );
call ams_wad.log_sp(spname, stTime, logNote);
SET countRows = 0;
SET in_dep_ap_id = 0;
SET in_arr_ap_id = 0;

SELECT d.dep_ap_id, d.arr_ap_id 
INTO in_dep_ap_id, in_arr_ap_id
FROM ams_wad.cfg_airport_destination d
WHERE d.dest_id = in_dest_id;
    IF in_dep_ap_id > 0 and in_arr_ap_id > 0 THEN
    
		SET logNote=CONCAT(  ' in_dep_ap_id:', in_dep_ap_id, ', in_arr_ap_id:', in_arr_ap_id );
		call ams_wad.log_sp(spname, stTime, logNote);
		
		select ROUND(t.distance_km,2) as distance_km, 
		ROUND((t.distance_km/s.min_speed_kmph), 2) as max_flight_h,
		t.dest_type_id, ap_type_factor,
		ROUND((ppd.param_value  * ap_type_factor * pwkg.param_value * t.distance_km),2) as max_ticket_price_e,
		ROUND((ppd.param_value  * ap_type_factor * pwkg.param_value * t.distance_km * factor_b.param_value),2) as max_ticket_price_b,
		ROUND((ppd.param_value  * ap_type_factor * pwkg.param_value * t.distance_km * factor_f.param_value),2) as max_ticket_price_f,
		t.min_rw_lenght_m, t.min_mtow_kg
		into i_distance_km, d_max_flight_h, i_dest_type_id,
		 i_ap_type_factor, d_max_ticket_price_e, d_max_ticket_price_b, d_max_ticket_price_f,
		 i_min_rw_lenght_m, i_min_mtow_kg
		FROM(
		SELECT dep.ap_id as dep_ap_id, arr.ap_id as  arr_ap_id, 
		dep.ap_type_id as dep_ap_type_id, arr.ap_type_id as arr_ap_type_id,
		-- round(((dep.ap_type_id+arr.ap_type_id)/2),2) as ap_type_factor,
		round((10 - ((arr.ap_type_id + dep.ap_type_id)/2)),2)as ap_type_factor,
		dep.distance_unit
		 * DEGREES(ACOS(COS(RADIANS(dep.ap_lat))
		 * COS(RADIANS(arr.ap_lat))
		 * COS(RADIANS(dep.ap_lon - arr.ap_lon))
		 + SIN(RADIANS(dep.ap_lat))
		 * SIN(RADIANS(arr.ap_lat)))) AS distance_km, 
		 if(dep.country_id = arr.country_id, 1,
		 if(dep.subregion_id = arr.subregion_id ||
		 dep.region_id = arr.region_id,2,3)) as dest_type_id,
		 if(dep.max_rw_lenght_m<arr.max_rw_lenght_m, dep.max_rw_lenght_m, arr.max_rw_lenght_m) as min_rw_lenght_m,
		 if(dep.max_mtow_kg<arr.max_mtow_kg, dep.max_mtow_kg, arr.max_mtow_kg) as min_mtow_kg
	   
		from (SELECT d.ap_id, d.ap_lat, d.ap_lon, t.range_km, d.ap_type_id, 
		d.country_id, d.subregion_id, d.region_id,
		111.045 AS distance_unit,  d.max_rw_lenght_m, d.max_mtow_kg
		FROM ams_wad.cfg_airport d
		JOIN ams_wad.cfg_airport_type t on d.ap_type_id = t.ap_type_id
		WHERE d.ap_id = in_dep_ap_id) dep
		join 
		 (SELECT d.ap_id, d.ap_lat, d.ap_lon, t.range_km, d.ap_type_id, 
		 d.country_id, d.subregion_id, d.region_id,
		 111.045 AS distance_unit,  d.max_rw_lenght_m, d.max_mtow_kg
		FROM ams_wad.cfg_airport d
		JOIN ams_wad.cfg_airport_type t on d.ap_type_id = t.ap_type_id
		WHERE d.ap_id = in_arr_ap_id) arr on 1=1 ) t
		join (select param_value FROM ams_al.cfg_callc_param where param_name = 'max_ticket_price_ppd_kg_per_km')  ppd on 1=1
		join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'pax_weight_kg' ) pwkg on 1=1
		join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'ticket_price_factor_b' ) factor_b on 1=1
		join (select param_value FROM ams_al.cfg_callc_param pwkg where param_name = 'ticket_price_factor_f' ) factor_f on 1=1
		join(SELECT min(cruise_speed_kmph) as min_speed_kmph FROM ams_ac.cfg_mac where cruise_speed_kmph is not null) s on 1=1;

		UPDATE ams_wad.cfg_airport_destination
		set distance_km = i_distance_km,
		max_flight_h = d_max_flight_h,
		dest_type_id = i_dest_type_id, ap_type_factor = i_ap_type_factor,
		max_ticket_price_e = if(d_max_ticket_price_e<9, 9, d_max_ticket_price_e),
		max_ticket_price_b = if(d_max_ticket_price_b<18, 18, d_max_ticket_price_b),
		max_ticket_price_f = if(d_max_ticket_price_f<36, 36, d_max_ticket_price_f),
		min_rw_lenght_m = i_min_rw_lenght_m, min_mtow_kg = i_min_mtow_kg
		where dest_id = in_dest_id;
	 
		COMMIT;
		SET logNote=CONCAT('updated in_dest_id:', in_dest_id);
		call log_sp(spname, stTime, logNote);
	END IF;

SET logNote=CONCAT('Finish ', '<- ');
call log_sp(spname, stTime, logNote);


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2025-02-11 14:12:04
